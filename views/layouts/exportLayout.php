<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Legislature Parliament of Nepal</title>
    <?php $this->head() ?>
</head>
<body class="skin-blue sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper">
<?php

if (!\Yii::$app->user->isGuest){
    ?>
           
            <div class="content-wrapper" style="min-height: 916px;">

                <section class="content">   
                    <?= $content; ?>
                </section>

            </div>
            </div>
          
    <?php
}
else{
        ?>
    <div class="content-wrapper" style="margin:0px;">
        <!-- Main content -->
        <section class="content">   
            <?= $content; ?>
        </section>
    </div>
    </div>
   
<?php
    }
?>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
