<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
?>  
<aside class="main-sidebar">
    <section class="sidebar">
      <!-- Sidebar user panel -->
        <ul class="sidebar-menu">
            <li class="header"> MAIN NAVIGATION </li>


            <li class="treeview">
                <?= Html::a('<i class="fa fa-gear"></i> <span>SETTING MODULES</span> <i class="fa fa-angle-left pull-right"></i>', Yii::$app->urlManager->createUrl('setting')); ?>
            </li>
            <li class="treeview">
                <?= Html::a('<i class="glyphicon glyphicon-info-sign"></i> <span>DARTA DESK</span> <i class="fa fa-angle-left pull-right"></i>', Yii::$app->urlManager->createUrl('program/darta-desk/darta')); ?>
            </li>
            <li class="treeview">
                <?= Html::a('<i class="glyphicon glyphicon-th-large"></i> <span>GENERATE DAILY PAPER</span> <i class="fa fa-angle-left pull-right"></i>', Yii::$app->urlManager->createUrl('program/darta-desk/daily-paper-dashboard')); ?>
            </li>
            
            <li class="treeview">
                <?= Html::a('<i class="glyphicon glyphicon-list-alt"></i> <span>TODAY DP LIST</span> <i class="fa fa-angle-left pull-right"></i>', Yii::$app->urlManager->createUrl('program/daily-paper/active-daily-paper')); ?>
            </li>
            <li class="treeview">
                <?= Html::a('<i class="glyphicon glyphicon-th-large"></i> <span>GENERATE RP</span> <i class="fa fa-angle-left pull-right"></i>', Yii::$app->urlManager->createUrl('program/daily-paper/to-day-d-p')); ?>
            </li>
            <li class="treeview">
                <?= Html::a('<i class="glyphicon glyphicon-list-alt"></i> <span>TODAY RP LIST</span> <i class="fa fa-angle-left pull-right"></i>', Yii::$app->urlManager->createUrl('program/reading-paper/to-day-reading-paper')); ?>
            </li>

            <li class="treeview">
                <?= Html::a('<i class="glyphicon glyphicon-list-alt"></i><span>TODAY NOTICE</span> <i class="fa fa-angle-left pull-right"></i>', Yii::$app->urlManager->createUrl('program/notice-paper/notice')); ?>
            </li>
            <!-- 
            <li class="treeview">
                <?= Html::a('<i class="glyphicon glyphicon-list-alt"></i> <span>ALL READING PAPER LIST</span> <i class="fa fa-angle-left pull-right"></i>', Yii::$app->urlManager->createUrl('program/reading-paper')); ?>
            </li>
             <li class="treeview">
                <?= Html::a('<i class="glyphicon glyphicon-list-alt"></i> <span>ALL DAILY PAPER LIST</span> <i class="fa fa-angle-left pull-right"></i>', Yii::$app->urlManager->createUrl('program/daily-paper')); ?>
            </li> -->
            <li class="treeview">
                <?= Html::a('<i class="glyphicon glyphicon-th-list"></i> <span>MEETING LISTS</span> <i class="fa fa-angle-left pull-right"></i>', Yii::$app->urlManager->createUrl('program/meeting-details/index')); ?>
            </li>
            <li class="treeview">
                <?= Html::a('<i class="glyphicon glyphicon-blackboard"></i> <span>TODAY MEETING</span> <i class="fa fa-angle-left pull-right"></i>', Yii::$app->urlManager->createUrl('program/meeting-details/meeting-details?status='.true)); ?>
            </li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-laptop"></i>
                <span>REPORT SECTION</span>
                <span class="pull-right-container">
                  <i class="fa fa-briefcase fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?= Yii::$app->request->baseUrl.'/program/darta-desk/billing-report?type=bill' ?>"><i class="fa fa-bar-chart"></i> BILLING REPORT</a></li>
                <li><a href="<?= Yii::$app->request->baseUrl.'/program/darta-desk/report?type=ordinance' ?>"><i class="fa fa-bar-chart"></i> ORDINANCE REPORT</a></li>
                 <li><a href="<?= Yii::$app->request->baseUrl.'/program/darta-desk/report?type=treaties' ?>"><i class="fa fa-bar-chart"></i> TREATY REPORT</a></li>
                <li><a href="<?= Yii::$app->request->baseUrl.'/setting/political-party' ?>"><i class="fa fa-bar-chart"></i> Political Party</a></li>
                <li><a href="<?= Yii::$app->request->baseUrl.'/setting/parliament-member' ?>"><i class="fa fa-bar-chart"></i> Parliment Members</a></li>
                <li><a href="<?= Yii::$app->request->baseUrl.'/program/participate-member-discussion' ?>"><i class="fa fa-bar-chart"></i> Participate Member</a></li>
                <li><a href="<?= Yii::$app->request->baseUrl.'/setting/political-party/party-member' ?>"><i class="fa fa-bar-chart"></i> Seat LP2</a></li>
                <li><a href="<?= Yii::$app->request->baseUrl.'/setting/political-party/party-member-ministers' ?>"><i class="fa fa-bar-chart"></i> Seat LP</a></li>
              </ul>
            </li>
          
            <li class="treeview">
              <?= Html::a('<i class="fa fa-sign-out"></i> <span>Log Out</span>', Yii::$app->urlManager->createUrl('site/logout'), ['data-method' => 'post']); ?>
            </li>

        </ul>
    </section>
</aside>
