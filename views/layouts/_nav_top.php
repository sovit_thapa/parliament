  <?php
  use yii\helpers\Url;  
  use yii\helpers\Html;
  use yii\bootstrap\Nav;
  use yii\bootstrap\NavBar;
  ?>

  <?= Html::a('<span class="sr-only">Toggle navigation</span>', 'javascript:;', ['class' => 'sidebar-toggle', 'data-toggle' => 'offcanvas', 'role' => 'button']); ?>

  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
      <!-- Messages: style can be found in dropdown.less-->

      <!-- User Account: style can be found in dropdown.less -->
      <li class="dropdown user user-menu">
        
        
        <?= $this -> render('_user_account'); ?>
          
      </li>
      <!-- Control Sidebar Toggle Button -->
      <li>
        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
      </li>
    </ul>
  </div>