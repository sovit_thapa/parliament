<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
?>  
<aside class="main-sidebar">
    <section class="sidebar">
      <!-- Sidebar user panel -->
        <ul class="sidebar-menu">
            <li class="header"> MAIN NAVIGATION </li>
            <li class="treeview">
                <?= Html::a('<i class="glyphicon glyphicon-th-list"></i> <span>DOWNLOAD RP</span> <i class="fa fa-angle-left pull-right"></i>', Yii::$app->urlManager->createUrl('program/reading-paper/today-all-r-p')); ?>
            </li>
            <li class="treeview">
                <?= Html::a('<i class="glyphicon glyphicon-th-list"></i> <span>START MEETING</span> <i class="fa fa-angle-left pull-right"></i>', Yii::$app->urlManager->createUrl('reading/paper/reading-section')); ?>
            </li>
           
            <li class="treeview">
              <?= Html::a('<i class="fa fa-sign-out"></i> <span>Log Out</span>', Yii::$app->urlManager->createUrl('site/logout'), ['data-method' => 'post']); ?>
            </li>

        </ul>
    </section>
</aside>
