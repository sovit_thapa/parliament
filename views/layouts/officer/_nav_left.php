<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
?>  
<aside class="main-sidebar">
    <section class="sidebar">
      <!-- Sidebar user panel -->
        <ul class="sidebar-menu">
            <li class="header"> MAIN NAVIGATION </li>
            <li class="treeview">
                <?= Html::a('<i class="glyphicon glyphicon-th-list"></i> <span>TODAY MEETING</span> <i class="fa fa-angle-left pull-right"></i>', Yii::$app->urlManager->createUrl('program/meeting-details/meeting-details?status=1')); ?>
            </li>
            <li class="treeview">
                <?= Html::a('<i class="glyphicon glyphicon-list-alt"></i> <span>TODAY DAILY PAPER</span> <i class="fa fa-angle-left pull-right"></i>', Yii::$app->urlManager->createUrl('program/daily-paper/active-daily-paper')); ?>
            </li>
           
            <li class="treeview">
                <?= Html::a('<i class="glyphicon glyphicon-list-alt"></i> <span>READING PAPER LIST</span> <i class="fa fa-angle-left pull-right"></i>', Yii::$app->urlManager->createUrl('program/reading-paper/to-day-reading-paper')); ?>
            </li>
            <li class="treeview">
                <?= Html::a('<i class="glyphicon glyphicon-list-alt"></i> <span>DOWNLOAD DP</span> <i class="fa fa-angle-left pull-right"></i>', Yii::$app->urlManager->createUrl('program/daily-paper/view-d-p')); ?>
            </li>
            <li class="treeview">
                <?= Html::a('<i class="glyphicon glyphicon-list-alt"></i> <span>DOWNLOAD RP</span> <i class="fa fa-angle-left pull-right"></i>', Yii::$app->urlManager->createUrl('program/reading-paper/today-all-r-p')); ?>
            </li>
            <li class="treeview">
              <?= Html::a('<i class="fa fa-sign-out"></i> <span>Log Out</span>', Yii::$app->urlManager->createUrl('site/logout'), ['data-method' => 'post']); ?>
            </li>

        </ul>
    </section>
</aside>
