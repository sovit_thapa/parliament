
<footer class="footer">
    <div class="ui grid container">
        <div class="row">
            <div class="col-md-6 copyright" style="text-align: left;">
                <p>2016 all rights reserved at : <a href="http://parliament.gov.np">parliament.gov.np</a></p>
            </div>
            <div class="col-md-5 copyright" style="text-align: right;">
                <p>Powered By : <a href="http://encrafttech.com.np">encrafttech.com</a></p>
            </div>
        </div>
    </div>
</footer>