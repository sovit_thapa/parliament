<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Legislature Parliament of Nepal</title>
    <?php $this->head() ?>
</head>
<body class="skin-blue sidebar-mini" style="background: #0054a6">
<?php $this->beginBody() ?>
<div class="wrapper">
<?php

if (!\Yii::$app->user->isGuest){
    ?>
            <header class="main-header hidden-print">
                <a href="<?= Yii::$app->homeUrl; ?>" class="logo">
                    <span class="logo-mini"><b>Parliament</b></span>
                    <span class="logo-lg"><b>Parliament</b></span>
                </a>
                
                <nav class="navbar navbar-static-top" role="navigation">
                    <?= $this -> render('_nav_top'); ?>
                </nav>
            </header>
            <div class="content-wrapper" style="min-height: 916px;">

               <?php
                if (Yii:: $app->user->can('admin')) 
                    echo $this->render('_nav_left');
                else if (Yii:: $app->user->can('officer'))
                    echo $this->render('officer/_nav_left');
                else if (Yii:: $app->user->can('reader'))
                    echo $this->render('reader/_nav_left');
                else 
                     echo $this->render('_nav_left');
               ?>
               
                <section class="content-header hidden-print">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]); ?>
                </section>
                <section class="content">   
                    <?= $content; ?>
                </section>

            </div>
            </div>
            <footer class="main-footer hidden-print">
                <?= $this -> render('footer') ?>
            </footer>
    <?php
}
else{
        ?>
    <div class="content-wrapper" style="margin:0px;">
        <!-- Main content -->
        <section class="content">   
            <?= $content; ?>
        </section>
    </div>
    </div>
    <footer class="main-footer hidden-print"  style="margin:0px;">
        <?= $this -> render('footer') ?>
    </footer>
<?php
    }
?>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
