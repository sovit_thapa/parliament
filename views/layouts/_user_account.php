<?php
	use yii\helpers\Url;
	use yii\helpers\Html;

	use common\components\UtilityFunctions;
	use common\models\User;
?>

<a data-toggle="dropdown" class="dropdown-toggle" href="#">	
	
	<span class="hidden-xs"><?= Yii::$app->user->identity->username; ?></span>
</a>
<ul class="dropdown-menu">
	<li class="user-header">
		<p>
			<span class="hidden-xs">
<?= Html::img(Yii::$app->request->baseUrl.'/img/sambidhan.png');?><br /><?= Yii::$app->user->identity->username; ?></span>
		</p>
	</li>
	
	<li class="user-footer">
		<div class="pull-right">
			<?= Html::a('Sign Out', Yii::$app->urlManager->createUrl('site/logout'), ['class' => 'btn btn-default btn-flat', 'data-method' => 'post']); ?>
		</div>

		<div class="pull-left">
			<?= Html::a('Change Password', Yii::$app->urlManager->createUrl('site/change-password'), ['class' => 'btn btn-default btn-flat', 'data-method' => 'post']); ?>
		</div>
	</li>
</ul>