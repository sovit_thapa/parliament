<?php

use yii\helpers\Html;

use app\components\UtilityFunctions;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\DartaDesk */

$this->title = 'DASHBOARD';

$this->params['breadcrumbs'][] = $this->title;
?>

<style>
  .div-square {
    padding:5px;
    border:3px double #e1e1e1;
    -webkit-border-radius:8px;
    -moz-border-radius:8px;
    border-radius:8px;
    margin:5px;
    min-height: 150px !important;

  }
  .div-square> a,.div-square> a:hover {
    color:#0073b7;
    text-decoration:none;
  }
</style>

<h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />
<div class="row">



  <div class="row text-center pad-top">

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
      <div class="div-square">
        <a href="<?= Yii::$app->homeUrl; ?>setting/ministry/index" >
          <i class="fa fa-archive fa-5x"></i>
          <h4>MINISTRY</h4>
        </a>
      </div> 
    </div>

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
      <div class="div-square">
        <a href="<?= Yii::$app->homeUrl; ?>setting/ministers/index" >
          <i class="ion ion-ios-people-outline fa-5x"></i>
          <h4>MINISTERS</h4>
        </a>
      </div> 
    </div>
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
      <div class="div-square">
        <a href="<?= Yii::$app->homeUrl; ?>setting/committee/index" >
          <i class="fa fa-circle-o fa-5x"></i>
          <h4>COMMITTEES</h4>
        </a>
      </div> 
    </div>


    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
      <div class="div-square">
        <a href="<?= Yii::$app->homeUrl; ?>setting/committee-member-details/index" >
          <i class="fa fa-child fa-5x"></i>
          <h4>सभासदहरु</h4>
        </a>
      </div> 
    </div>

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
      <div class="div-square">
        <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?section=letter" >
          <i class="fa fa-envelope-o fa-5x"></i>
          <h4>पत्र दर्ता</h4>
        </a>
      </div>


    </div>

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
      <div class="div-square">
        <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?section=bill" >
          <i class="fa fa-clipboard fa-5x"></i>
          <h4>विधेयक</h4>
        </a>
      </div>
    </div>


    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
      <div class="div-square">
        <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?section=ordinance" >
          <i class="fa fa-briefcase fa-5x"></i>
          <h4>अध्यादेश</h4>
        </a>
      </div> 
    </div>
    
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
      <div class="div-square">
        <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?section=aggrement" >
          <i class="fa fa-circle-o fa-5x"></i>
          <h4>सन्धि / महासन्धि / <br /> सम्झौता</h4>
        </a>
      </div>
    </div>

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
      <div class="div-square">
        <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?section=report" >
          <i class="fa fa-lightbulb-o fa-5x"></i>
          <h4>प्रतिवेदन पेश</h4>
        </a>
      </div>
    </div>

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
      <div class="div-square">
        <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?section=rule" >
          <i class="fa fa-bookmark fa-5x"></i>
          <h4>नीति तथा कार्यक्रम</h4>
        </a>
      </div> 
    </div>

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
      <div class="div-square">
        <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?section=income" >
          <i class="fa fa-money fa-5x"></i>
          <h4>राजस्व व्ययको <br />बार्षिक अनुमान </h4>
        </a>
      </div>
    </div>

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
      <div class="div-square">
        <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?section=proposal" >
          <i class="fa fa-folder fa-5x"></i>
          <h4>प्रस्तावहरु</h4>
        </a>
      </div>
    </div>

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
      <div class="div-square">
        <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?section=election" >
          <i class="fa fa-cubes fa-5x"></i>
          <h4>निर्वाचन</h4>
        </a>
      </div> 
    </div>

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
      <div class="div-square">
        <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?section=other" >
          <i class="fa fa-bars fa-5x"></i>
          <h4>अन्य</h4>
        </a>
      </div> 
    </div>

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
      <div class="div-square">
        <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?section=direct-question" >
          <i class="fa fa-question-circle fa-5x"></i>
          <h4>प्रत्यक्ष प्रस्नहरू</h4>
        </a>
      </div> 
    </div>

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
      <div class="div-square">
        <a href="<?= Yii::$app->homeUrl; ?>setting/political-party/index" >
          <i class="fa fa-group fa-5x"></i>
          <h4>POLITICAL PARTY</h4>
        </a>
      </div> 
    </div>

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
      <div class="div-square">
        <a href="<?= Yii::$app->homeUrl; ?>setting/official-post/index" >
          <i class="ion ion-beaker fa-5x"></i>
          <h4>POST</h4>
        </a>
      </div> 
    </div>


    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
      <div class="div-square">
        <a href="<?= Yii::$app->homeUrl; ?>setting/committee/index" >
          <i class="fa fa-circle-o fa-5x"></i>
          <h4>COMMITTEES</h4>
        </a>
      </div> 
    </div>


    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
      <div class="div-square">
        <a href="<?= Yii::$app->homeUrl; ?>setting/parliament-party-official/index" >
          <i class="ion ion-ios-people-outline fa-5x"></i>
          <h4>PARLIAMENT PARTY OFFICALS</h4>
        </a>
      </div> 
    </div>
  </div>
</div>











