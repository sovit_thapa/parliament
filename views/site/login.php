<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
    $this -> title = 'Login';
?>
<div class="login-box">
    <div class="login-box-body">
    <div class="login-logo"><?= Html::img(Yii::$app->request->baseUrl.'/img/logo.jpg');?> </div>
        <p class="login-box-msg">LOGIN PANEL</p>
        <?php 
            $form = ActiveForm::begin([
                'id' => 'login-form',
                'enableClientValidation' => true,
                'options' => [
                    'validateOnSubmit' => true,
                ],
            ]); 
        ?>
        
            <div class="form-group has-feedback">
                <?= $form -> field($model, 'username', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'User Name']]) -> label(false); ?>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <?= $form -> field($model, 'password', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Password']]) -> label(false) -> passwordInput(); ?>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <?= $form -> field($model, 'rememberMe') -> checkbox() ;?>
                    </div>
                </div>

                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Login In</button>
                </div>
            </div>

        <?php ActiveForm::end(); ?>

        <?php Html::a('I forgot my password', 'javascript:;'); ?> <br />
        <?php Html::a('Register', 'javascript:;'); ?>
        
    </div>
</div>
