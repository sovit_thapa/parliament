$('.alpha').sortable({
  connectWith: '.gamma',
  receive: function (event, ui) {
    if ($(ui.item).hasClass('special')) {
      ui.sender.sortable('cancel');
    }
  }
});

$('.beta').sortable({
  connectWith: '.gamma',
  receive: function (event, ui) {
    if (!$(ui.item).hasClass('special')) {
      ui.sender.sortable('cancel');
    }
  }
});

$('.gamma').sortable({
  appendTo: document.body,
  items: '.tile',
  connectWith: '.alpha, .beta',
  receive: function (event, ui) {
    //console.log(event, ui.item);
    //ui.item.remove(); // remove original item
  }
});