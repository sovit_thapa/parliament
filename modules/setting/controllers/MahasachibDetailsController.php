<?php

namespace app\modules\setting\controllers;

use Yii;
use app\modules\setting\models\MahasachibDetails;
use app\modules\setting\services\MahasachibDetailsService;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MahasachibDetailsController implements the CRUD actions for MahasachibDetails model.
 */
class MahasachibDetailsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MahasachibDetails models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MahasachibDetailsService();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MahasachibDetails model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MahasachibDetails model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MahasachibDetails();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $old_mahasachib_details = MahasachibDetails::find()->where('id!=:id AND status=:status',[':id'=>$model->id,'status' => 1])->all();
            if(!empty($old_mahasachib_details)){
                foreach ($old_mahasachib_details as $mahasachib) {
                    $mahasachib_details = MahasachibDetails::findOne($mahasachib->id);
                    $mahasachib_details -> status = 0;
                    $mahasachib_details -> update(false);
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MahasachibDetails model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $old_mahasachib_details = MahasachibDetails::find()->where('id!=:id AND status=:status',[':id'=>$model->id,'status' => 1])->all();
            if(!empty($old_mahasachib_details) && $model->status == 1){
                foreach ($old_mahasachib_details as $mahasachib) {
                    $mahasachib_details = MahasachibDetails::findOne($mahasachib->id);
                    $mahasachib_details -> status = 0;
                    $mahasachib_details -> update(false);
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MahasachibDetails model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MahasachibDetails model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MahasachibDetails the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MahasachibDetails::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
