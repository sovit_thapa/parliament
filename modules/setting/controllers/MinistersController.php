<?php

namespace app\modules\setting\controllers;

use Yii;
use yii\filters\AccessControl;
use app\modules\setting\models\ParliamentPartyOfficial;
use app\modules\setting\services\ParliamentPartyOfficialService;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * 
 */
class MinistersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
         'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

        ];
    }

    /**
     * Lists all ParliamentPartyOfficial models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ParliamentPartyOfficialService();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ParliamentPartyOfficial model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ParliamentPartyOfficial model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ParliamentPartyOfficial();

        if ($model->load(Yii::$app->request->post())) 
        {
            $error_array = array();
            $transaction = Yii::$app->db->beginTransaction();
            $ministry_array = $model->ministry;
            $parliament_id = $model->parliament_id;
            $parliament_member_id = $model->parliament_member_id;
            $post = $model->post;
            $political_party = $model->political_party;
            $start_from = $model->start_from;
            $to_date = $model->to_date;
            $letter_received_date = $model->letter_received_date;
            $status = $model->status;
            $_order = $model->_order;
            for ($i=0; $i < sizeof($ministry_array) ; $i++) { 
                $new_mode = ParliamentPartyOfficial::find()->where(['parliament_member_id'=>$parliament_member_id,'post'=>$post,'status'=>'सकृय','ministry'=>$ministry_array[$i]])->one();
                if(empty($new_mode))
                    $new_mode = new ParliamentPartyOfficial();
                $new_mode->ministry = $ministry_array[$i];
                $new_mode->parliament_id = $parliament_id;
                $new_mode->parliament_member_id = $parliament_member_id;
                $new_mode->political_party = $political_party;
                $new_mode->post = $post;
                $new_mode->start_from = $start_from;
                $new_mode->to_date = $to_date;
                $new_mode->to_date = $to_date;
                $new_mode->letter_received_date = $letter_received_date;
                $new_mode->status = 'सकृय';
                $new_mode->_order = $_order;
                if(!$new_mode->save()){
                    $error_array[] = 'false';
                    echo '<pre>';
                    echo print_r($new_mode->errors);
                    exit;
                }
            }
            if(!in_array('false', $error_array)){
                $transaction->commit();
                return $this->redirect(['index']);
            }else{
                $transaction->rollback();
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        } 
        else 
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ParliamentPartyOfficial model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $model->letter_received_date= isset($model->letter_received_date) ? Yii::$app->UtilityFunctions->EnglishToNepali($model->letter_received_date) : '';
            $model->to_date=isset($model->to_date) ? Yii::$app->UtilityFunctions->EnglishToNepali($model->to_date) : '';
            $model->start_from= isset($model->start_from) ?Yii::$app->UtilityFunctions->EnglishToNepali($model->start_from) : '';
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ParliamentPartyOfficial model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ParliamentPartyOfficial model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ParliamentPartyOfficial the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ParliamentPartyOfficial::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    
}
