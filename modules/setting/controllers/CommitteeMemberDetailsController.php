<?php

namespace app\modules\setting\controllers;

use Yii;
use yii\filters\AccessControl;
use app\modules\setting\models\CommitteeMemberDetails;
use app\modules\setting\services\CommitteeMemberDetailsService;
use app\modules\setting\models\ParliamentMember;
use app\modules\setting\models\PoliticalParty;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

/**
 * CommitteeMemberDetailsController implements the CRUD actions for CommitteeMemberDetails model.
 */
class CommitteeMemberDetailsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
         'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

        ];
    }

    /**
     * Lists all CommitteeMemberDetails models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CommitteeMemberDetailsService();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CommitteeMemberDetails model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CommitteeMemberDetails model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CommitteeMemberDetails();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    /**
    *parliament member details
    */

    public function actionParliamentMember(){

        $political_party_id = null;
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost && $request->post('depdrop_parents') && is_array($request->post('depdrop_parents'))) {
            $political_party = PoliticalParty::find()->where(['name'=>$request->post('depdrop_parents')[0]])->one();
            $political_party_id = $political_party ? $political_party->id : null;
        }
        $parliament_member_lists = ParliamentMember::find()
            ->where('political_id =:political_id AND status=:status', [
                    ':political_id' => $political_party_id,
                    ':status' => 'सकृय'
                ]
            )
            ->all();
        $parliament_information = array();
        if(!empty($parliament_member_lists)){
            $sn = 0;
            foreach ($parliament_member_lists as $member_info) {
                $parliament_information[$sn]['id'] = $member_info->id;
                $parliament_information[$sn]['name'] = $member_info->member_title.' '.$member_info->first_name.' '.$member_info->last_name;
                $sn++;
            }
        }
        echo Json::encode(['output' => $parliament_information, 'selected' => '']);
    }
    /**
     * Updates an existing CommitteeMemberDetails model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CommitteeMemberDetails model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CommitteeMemberDetails model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CommitteeMemberDetails the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CommitteeMemberDetails::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
