<?php

namespace app\modules\setting\controllers;

use Yii;
use yii\filters\AccessControl;
use app\modules\setting\models\PoliticalParty;
use app\modules\setting\models\Committee;
use app\modules\setting\services\PoliticalPartyService;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PoliticalPartyController implements the CRUD actions for PoliticalParty model.
 */
class PoliticalPartyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
         'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

        ];
    }

    /**
     * Lists all PoliticalParty models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PoliticalPartyService();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPartyMember($export=null)
    {
        $parties=PoliticalParty::find()->all();
        $party=new PoliticalParty();
        $parties=$party->sortByMemberCount($parties,'totalMemberCount');
        $committee=Committee::find()->all();
       

        //Exporting CSV FILE
        if($export != null && $export == 'csv')
        {
            $filename = 'Data-'.Date('Y-m-d:H-i-s').'-PartyCommitteeMember.csv';
            //header("Content-type: application/vnd-ms-excel");
            header("content-type:application/csv;");
            header("Content-Disposition: attachment; filename=".$filename);
            echo "\xEF\xBB\xBF";
            echo $this->renderPartial('csv/partymember', [
            'party'=>$parties,
            'committees'=>$committee
                    ]);
        }
        else
        {
            return $this->render('partymember', [
                'party'=>$parties,
                'committees'=>$committee
            ]);
        }   
    }


    public function actionPartyMemberMinisters($export=null)
    {
        $parties=PoliticalParty::find()->all();
        $party=new PoliticalParty();
        $parties=$party->sortByMemberCount($parties,'totalMemberCount');
       

        $committee=Committee::find()->all();
        $searchModel = new PoliticalPartyService();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        //Exporting CSV FILE
        if($export != null && $export == 'csv')
        {
            $filename = 'Data-'.Date('Y-m-d:H-i-s').'-PartyMemberMinisters.csv';
            //header("Content-type: application/vnd-ms-excel");
            header("content-type:application/csv;");
            header("Content-Disposition: attachment; filename=".$filename);
            echo "\xEF\xBB\xBF";
            echo $this->renderPartial('csv/partymemberministers', [
            'party'=>$parties,
            'committees'=>$committee
                    ]);
        }
        else
        {
            return $this->render('partymemberministers', [
                'party'=>$parties,
                'committees'=>$committee
            ]);
        }   
    }


    /**
     * Displays a single PoliticalParty model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PoliticalParty model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PoliticalParty();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PoliticalParty model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PoliticalParty model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PoliticalParty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PoliticalParty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PoliticalParty::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
