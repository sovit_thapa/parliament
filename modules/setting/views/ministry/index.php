<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\services\MinistryService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ministries';
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ministry-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ministry', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'description:ntext',
            'created_by',
            'created_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
