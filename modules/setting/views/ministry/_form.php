<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\Ministry */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ministry-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row well">
	    <div class="col-md-6">
	    	<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
	    </div>
	    <div class="col-md-6">

	    	<?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
	    </div>
    </div>
<!-- 
    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'created_date')->textInput() ?> -->

    <div class="form-group text-center">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
