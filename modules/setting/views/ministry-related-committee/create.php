<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\MinistryRelatedCommittee */

$this->title = 'Ministry && Committee Related';
$this->params['breadcrumbs'][] = ['label' => 'Ministry Related Committees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ministry-related-committee-create">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
