<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\setting\models\Ministry;
use app\modules\setting\models\Committee;
use app\components\UtilityFunctions;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\MinistryRelatedCommittee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ministry-related-committee-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row well">
        <?= $form->field($model, 'parliament_id')->hiddenInput(['value'=>UtilityFunctions::SambhidhanSava()])->label(false) ?>
    <div class="col-md-6">

    <?= $form->field($model, 'ministry')->dropDownList(ArrayHelper::map(Ministry::find()->all(), 'title', 'title'),['prompt' => 'Select Ministry']);
        ?>
    <?= $form->field($model, 'committee')->dropDownList(ArrayHelper::map(Committee::find()->all(), 'name', 'name'),['prompt' => 'Select Committee']);
        ?>
    </div>
    <div class="col-md-6">
    <?= $form->field($model, 'applied_from')->widget(
        DatePicker::className(), [
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
    ]);?>
    </div>
    </div>

    <!-- <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'created_date')->textInput() ?>
 -->
    <div class="form-group text-center">
        <?= Html::submitButton('Save Information', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
