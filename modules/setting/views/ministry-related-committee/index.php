<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\services\MinistryRelatedCommitteeService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ministry Related Committees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ministry-related-committee-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="text-align: right;">
        <?= Html::a('Create Ministry Related Committee', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'ministry',
            'committee',
            'applied_from',
            // 'created_by',
            // 'created_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
