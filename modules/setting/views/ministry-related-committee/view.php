<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\MinistryRelatedCommittee */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ministry Related Committees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ministry-related-committee-view">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ministry',
            'committee',
            'applied_from',
            'created_by',
            'created_date',
        ],
    ]) ?>

</div>
