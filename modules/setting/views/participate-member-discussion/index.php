<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\program\services\ParticipateMemberDiscussionService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Participate Member Discussions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participate-member-discussion-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Participate Member Discussion', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'parliament_id',
            'meeting_id',
            'type',
            'parliament_member_id',
            // 'speech_order',
            // 'text_',
            // 'is_read',
            // 'created_by',
            // 'created_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
