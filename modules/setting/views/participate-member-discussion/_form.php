<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\ParticipateMemberDiscussion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="participate-member-discussion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parliament_id')->textInput() ?>

    <?= $form->field($model, 'meeting_id')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList([ 'zeor_time' => 'Zeor time', 'special_time' => 'Special time', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'parliament_member_id')->textInput() ?>

    <?= $form->field($model, 'speech_order')->textInput() ?>

    <?= $form->field($model, 'text_')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_read')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
