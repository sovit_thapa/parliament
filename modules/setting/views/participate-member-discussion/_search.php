<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\program\services\ParticipateMemberDiscussionService */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="participate-member-discussion-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'parliament_id') ?>

    <?= $form->field($model, 'meeting_id') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'parliament_member_id') ?>

    <?php // echo $form->field($model, 'speech_order') ?>

    <?php // echo $form->field($model, 'text_') ?>

    <?php // echo $form->field($model, 'is_read') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
