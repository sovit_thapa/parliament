<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\program\models\ParticipateMemberDiscussion */

$this->title = 'Create Participate Member Discussion';
$this->params['breadcrumbs'][] = ['label' => 'Participate Member Discussions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participate-member-discussion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
