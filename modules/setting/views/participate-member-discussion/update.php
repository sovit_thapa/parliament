<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\ParticipateMemberDiscussion */

$this->title = 'Update Participate Member Discussion: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Participate Member Discussions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="participate-member-discussion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
