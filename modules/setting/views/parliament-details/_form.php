<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use app\components\UtilityFunctions;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\ParliamentDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parliament-details-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row well">
        <div class="col-md-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        
        <?= $form->field($model, 'parliament_total_member')->textInput() ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'start_session')->widget(
                DatePicker::className(), [
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
            ]);?>

            <?= $form->field($model, 'end_session')->widget(
                DatePicker::className(), [
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
            ]);?>


            <?= $form->field($model, 'start_session_nepali_date')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9999-99-99','options' => ['value'=> UtilityFunctions::EnglishToNepali($model->start_session_nepali_date), 'class'=>'form-control']
            ]) ?>
            <?= $form->field($model, 'end_session_nepali_date')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9999-99-99','options' => ['value'=> UtilityFunctions::EnglishToNepali($model->end_session_nepali_date), 'class'=>'form-control']
            ]) ?>

        </div>
    </div>
    <?php
        if(!$model->isNewRecord){
    ?>
    <div class="row well">   
    <?= $form->field($model, 'extented_time')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'in-active' => 'In-active', ], ['prompt' => '']) ?>
    </div>
    <?php
    }
    ?>

    <div class="form-group text-center">
        <?= Html::submitButton('Save Parliament Detail', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


7187, 7192, 6132, 6796, 6898, 6856, 6039, 6045, 5767, 7089, 5593,6175
