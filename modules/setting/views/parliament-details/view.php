<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\ParliamentDetails */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = ['label' => 'Parliament Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parliament-details-view">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'parliament_total_member',
            'start_session',
            'end_session',
            'start_session_nepali_date',
            'end_session_nepali_date',
            'extented_time',
            'description:ntext',
            'status',
            'created_by',
            'created_ip',
            'created_date',
        ],
    ]) ?>

</div>
