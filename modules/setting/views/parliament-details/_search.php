<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\services\ParliamentDetailsService */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parliament-details-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'parliament_total_member') ?>

    <?= $form->field($model, 'start_session') ?>

    <?= $form->field($model, 'end_session') ?>

    <?php // echo $form->field($model, 'start_session_nepali_date') ?>

    <?php // echo $form->field($model, 'end_session_nepali_date') ?>

    <?php // echo $form->field($model, 'extented_time') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_ip') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
