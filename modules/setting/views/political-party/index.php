<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\services\PoliticalPartyService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Political Parties';
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="political-party-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Political Party', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            
           [
                'attribute' => 'pratakshya',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->pratashyaMember;
                }
            ],

             [
                'attribute' => 'samanupatik',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->samanupatikMember;
                }
            ],

             [
                'attribute' => 'manonit',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->manonitMember;
                }
            ],

            [
                'attribute' => 'totalMemberCount',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->totalMember;
                }
            ],

            // 'created_by',
            // 'create_ip',
            // 'created_date',

            [
                'header' => 'Action',
                'class' => 'yii\grid\ActionColumn'
            ],
        ],


        'panel'=>  [
                        'type'=>GridView::TYPE_DEFAULT,
                        //'heading'=>'<h4 class="text-center"><STRONG> asd</STRONG></h4>',
                    ],

       'toolbar' => [
        '{export}',
        '{toggleData}'
    ]
    ]); ?>
</div>
<?php
    echo $this->registerJsFile('@web/bootstrap/js/bootstrap.min.js');
?>
