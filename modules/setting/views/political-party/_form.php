<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\UtilityFunctions;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\PoliticalParty */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="political-party-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row well">
        <div class="col-md-6">   
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'party_establish_date')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9999-99-99','options' => ['value'=> UtilityFunctions::EnglishToNepali($model->party_establish_date), 'class'=>'form-control']
            ]) ?>


            <?= $form->field($model, 'party_register_date')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9999-99-99','options' => ['value'=> UtilityFunctions::EnglishToNepali($model->party_register_date), 'class'=>'form-control']
            ]) ?>
            <?php
                if(!$model->isNewRecord)
                    echo $form->field($model, 'name')->dropDownList(['1'=>'ACTIVE','0'=>'IN-ACTIVE']); 

            ?>
        </div>
        <div class="col-md-6">
            
            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
        </div>
    </div>
<!-- 
    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'create_ip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_date')->textInput() ?>
 -->
    <div class="form-group text-center">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
