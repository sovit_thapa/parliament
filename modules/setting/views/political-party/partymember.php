<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\services\PoliticalPartyService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'व्यवस्थापिका–संसदमा प्रतिनिधित्व गर्ने राजनैतिक दलहरुको व्यवस्थापिका–संसदका समितिहरुमा हालसम्म प्रतिनिधित्व भएको संख्या';
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = $this->title;
?>
<br>
<div class="political-party-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

</div>
<hr>
    <?= Html::a('EXPORT TO CSV', Yii::$app->request->baseUrl.'/'.Yii::$app->controller->module->id.'/'.Yii::$app->controller->id.'/'.Yii::$app->controller->action->id.'?export=csv', ['class'=>'btn btn-info pull-right hidden-print'

                            ]) ?>
<table class="table table-bordered" style="background:white;">
    <th>क्र.सं.</th>
    <th>राजनैतिक पार्टी</th>
    <th>प्रत्यक्ष</th>
    <th>समानुपातिक</th>
    <th>मनोनित</th>
    <th>जम्मा सिट संख्या</th>
   
<?php
    foreach ($committees as $key => $value) 
    { ?>
       
        <th><?= $value->name; ?></th>


<?php } ?>

<?php
    $sn=0;
    foreach ($party as $key => $value) 
    { $sn++; ?>
       
        <tr>
            <td><?= $sn; ?></td>
            <td><?= $value->name; ?></td>
            <td><?= $value->pratashyaMember; ?></td>
            <td><?= $value->samanupatikMember; ?></td>
            <td><?= $value->manonitMember; ?></td>
            <td><?= $value->finalTotalMember; ?></td>
            <?php
                foreach ($committees as $comkey => $committee) 
                { ?>
                   
                    <td><?= $value->committeeMembersCount($committee->id); ?></td>


            <?php } ?>

        <tr>


<?php } ?>

        
</table>