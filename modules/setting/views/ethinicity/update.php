<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\Ethinicity */

$this->title = 'Update Ethinicity: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = ['label' => 'Ethinicities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ethinicity-update">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3> <hr />

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
