<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\export\ExportMenu;


use app\modules\setting\models\Ethinicity;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\services\EthinicityServices */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ethinicities/Cast';
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ethinicity-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ethinicity', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    $gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
             [
                'attribute' => 'parent_id',
                'label'=>'Parent Title',
                'format' => 'raw',
                'value' => function ($data) {
                    return Ethinicity::findOne($data->parent_id) ? Ethinicity::findOne($data->parent_id)->name : 'not-set';
                }
            ],
            'name',
            'description',
];

    // Renders a export dropdown menu
    echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' =>  $gridColumns,
        'target'=>'TARGET_SELF',
        'pdfLibraryPath'=>'@vendor/kartik-v/mpdf',
        'onInitSheet' => function (PHPExcel_Worksheet $sheet, $grid) {
            $sheet->getDefaultStyle()->applyFromArray(['borders' => [ 'allborders' => ['style' => PHPExcel_Style_Border::BORDER_THIN ]]]);
        },
        'exportConfig'=> [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false
        ]

    ]);

    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
             [
                'attribute' => 'parent_id',
                'label'=>'Parent Title',
                'format' => 'raw',
                'value' => function ($data) {
                    return Ethinicity::findOne($data->parent_id) ? Ethinicity::findOne($data->parent_id)->name : 'not-set';
                }
            ],
            'name',
            'description',
            //'created_by',
            // 'created_date',

            [
                'header' => 'Action',
                'class' => 'yii\grid\ActionColumn'
            ],
        ],
    ]); ?>
</div>
<?php
    echo $this->registerJsFile('@web/bootstrap/js/bootstrap.min.js');
?>