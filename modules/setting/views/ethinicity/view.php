<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\setting\models\Ethinicity;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\Ethinicity */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = ['label' => 'Ethinicities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ethinicity-view">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
             [
                'attribute' => 'parent_id',
                'label'=>'Parent Title',
                'format' => 'raw',
                'value' => Ethinicity::findOne($model->parent_id) ? Ethinicity::findOne($model->parent_id)->name : 'not-set',
            ],
            'name',
            'description',
            'created_by',
            'created_date',
        ],
    ]) ?>

</div>
