<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\modules\setting\models\Ethinicity;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\Ethinicity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ethinicity-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row well">
        <div class="col-md-6">
            <?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map(Ethinicity::find()->where(['parent_id'=>0])->all(), 'id', 'name'),['prompt'=>'PARENT ETHINICITY']);?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'description')->textArea(['maxlength' => true, 'cols'=>4, 'rows'=>5]) ?>

        </div>
    </div>
<!-- 
    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'created_date')->textInput() ?> -->

    <div class="form-group text-center">
        <?= Html::submitButton('Save Ethinicity', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
