<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\Location */

$this->title = 'Create Location';
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = ['label' => 'Location', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="location-create">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
