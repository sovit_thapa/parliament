<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\Location */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="location-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row well">
    <div class="col-md-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'nepali_name')->textInput(['maxlength' => true]) ?>
    </div>
<!-- 
    <?= $form->field($model, 'parent_id')->textInput() ?>

    <?= $form->field($model, 'location_level')->textInput() ?>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'location_slug')->textInput(['maxlength' => true]) ?> -->

    <div class="form-group text-center">
        <?= Html::submitButton('Save District', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
