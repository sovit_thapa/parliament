<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\ParliamentMember */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = ['label' => 'Parliament Members', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parliament-member-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'parliament_id',
            'political_id',
            'member_title',
            'first_name',
            'last_name',
            'district_id',
            'elected_area_number',
            'ethinicity_id',
            'elected_type',
            'date_of_birth',
            'dob_nepali_day',
            'dob_nepali_month',
            'dob_nepali_year',
            'sex',
            'rural_area',
            'oath_of_parliament_member',
            'oath_nepali_day',
            'oath_nepali_month',
            'oath_nepali_year',
            'parliament_status',
            'status',
            'created_by',
            'created_date',
            'created_ip',
        ],
    ]) ?>

</div>
