<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\Services\ParliamentMemberService */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parliament-member-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'parliament_id') ?>

    <?= $form->field($model, 'political_id') ?>

    <?= $form->field($model, 'member_title') ?>

    <?= $form->field($model, 'first_name') ?>

    <?php // echo $form->field($model, 'last_name') ?>

    <?php // echo $form->field($model, 'district_id') ?>

    <?php // echo $form->field($model, 'elected_area_number') ?>

    <?php // echo $form->field($model, 'ethinicity_id') ?>

    <?php // echo $form->field($model, 'elected_type') ?>

    <?php // echo $form->field($model, 'date_of_birth') ?>

    <?php // echo $form->field($model, 'dob_nepali_day') ?>

    <?php // echo $form->field($model, 'dob_nepali_month') ?>

    <?php // echo $form->field($model, 'dob_nepali_year') ?>

    <?php // echo $form->field($model, 'sex') ?>

    <?php // echo $form->field($model, 'rural_area') ?>

    <?php // echo $form->field($model, 'oath_of_parliament_member') ?>

    <?php // echo $form->field($model, 'oath_nepali_day') ?>

    <?php // echo $form->field($model, 'oath_nepali_month') ?>

    <?php // echo $form->field($model, 'oath_nepali_year') ?>

    <?php // echo $form->field($model, 'parliament_status') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'created_ip') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
