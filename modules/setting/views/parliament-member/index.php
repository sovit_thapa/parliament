<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\Services\ParliamentMemberService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Parliament Members';
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parliament-member-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Parliament Member', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?php /*echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'target'=>'TARGET_SELF',
        'filterModel' => $searchModel,
        'columns' => [
           ['class' => 'yii\grid\SerialColumn'],


             [
                'attribute' => 'parliament_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->parliament ? $data->parliament->name : '';
                }
            ],

             [
                'attribute' => 'political_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->political ? $data->political->name : '';
                }
            ],
            'member_title',
            'first_name',
            'last_name',
            'sex',
             [
                'attribute' => 'district_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->district ? $data->district->name : '';
                }
            ],
            'elected_area_number',

             [
                'attribute' => 'ethinicity_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->ethinicity ? $data->ethinicity->name : '';
                }
            ],
            'elected_type',
            // 'date_of_birth',
            // 'dob_nepali_day',
            // 'dob_nepali_month',
            // 'dob_nepali_year',
            // 'sex',
            'rural_area',
            'oath_of_parliament_member',
        ],

        'pdfLibraryPath'=>'@vendor/kartik-v/mpdf',
        'onInitSheet' => function (PHPExcel_Worksheet $sheet, $grid) {
            $sheet->getDefaultStyle()->applyFromArray(['borders' => [ 'allborders' => ['style' => PHPExcel_Style_Border::BORDER_THIN ]]]);
        },
        'encoding'=>'utf8',
        'exportConfig'=> [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false
        ]
    ]);*/ ?>





    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


             [
                'attribute' => 'parliament_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->parliament ? $data->parliament->name : '';
                }
            ],

             [
                'attribute' => 'political_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->political ? $data->political->name : '';
                }
            ],
            'member_title',
            'first_name',
            'last_name',
            'sex',
             [
                'attribute' => 'district_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->district ? $data->district->name : '';
                }
            ],
            'elected_area_number',

             [
                'attribute' => 'ethinicity_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->ethinicity ? $data->ethinicity->name : '';
                }
            ],
            'elected_type',
            // 'date_of_birth',
            // 'dob_nepali_day',
            // 'dob_nepali_month',
            // 'dob_nepali_year',
            // 'sex',
            'rural_area',
            'oath_of_parliament_member',
            // 'oath_nepali_day',
            // 'oath_nepali_month',
            // 'oath_nepali_year',
            // 'parliament_status',
            // 'status',
            // 'created_by',
            // 'created_date',
            // 'created_ip',

            [
                'header' => 'Action Links',
            'class' => 'yii\grid\ActionColumn'],
        ],

         'panel'=>  [
                        'type'=>GridView::TYPE_DEFAULT,
                        //'heading'=>'<h4 class="text-center"><STRONG> asd</STRONG></h4>',
                    ],

       'toolbar' => [
        '{export}',
        '{toggleData}'
    ]
    ]); ?>
</div>

<?php
    echo $this->registerJsFile('@web/bootstrap/js/bootstrap.min.js');
?>
