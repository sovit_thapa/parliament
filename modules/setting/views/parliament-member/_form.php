<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\UtilityFunctions;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;

use app\modules\setting\models\Ethinicity;
use app\modules\setting\models\Location;
use app\modules\setting\models\ParliamentDetails;
use app\modules\setting\models\PoliticalParty;
/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\ParliamentMember */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parliament-member-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row well">
        <div class="col-md-6"> 
            <?= $form->field($model, 'parliament_id')->hiddenInput(['value'=>UtilityFunctions::SambhidhanSava()])->label(false); ?>

            <?= $form->field($model, 'political_id')->dropDownList(ArrayHelper::map(PoliticalParty::find()->all(), 'id', 'name'),['prompt'=>' POLITICAL PARTY ']);?>
            <div class="row"> 
                <div class="col-md-2"> <STRONG>नाम, थर :- </STRONG>  </div><div class="col-md-2"><?= $form->field($model, 'member_title')->textInput(['maxlength' => true, 'placeholder'=>'मानर्थ'])->label(false); ?></div><div class="col-md-4"><?= $form->field($model, 'first_name')->textInput(['maxlength' => true, 'placeholder'=>'नाम'])->label(false); ?></div><div class="col-md-4"><?= $form->field($model, 'last_name')->textInput(['maxlength' => true, 'placeholder'=>'थर'])->label(false); ?></div>
                </div>

            <?= $form->field($model, 'district_id')->dropDownList(ArrayHelper::map(Location::find()->all(), 'id', 'name'),['prompt'=>' DISTRICTS ']);?>


            <?= $form->field($model, 'elected_area_number')->textInput() ?>

            <?= $form->field($model, 'ethinicity_id')->dropDownList(ArrayHelper::map(Ethinicity::find()->where(['parent_id'=>0])->all(), 'id', 'name'),['prompt'=>' Ethinicity ']);?>

            <?= $form->field($model, 'elected_type')->dropDownList(['प्रत्यक्ष-निर्वाचन'=>'प्रत्यक्ष-निर्वाचन','उपनिर्वाचन'=>'उपनिर्वाचन','समानुपातिक'=>'समानुपातिक',' मनोनित'=>'मनोनित','अन्य'=>'अन्य'], ['prompt' => '']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'date_of_birth')->widget(
                DatePicker::className(), [
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
            ]);?>


            <div class="row"> 
                <div class="col-md-3"> <STRONG>जन्म मिती (B.S) :- </STRONG>  </div><div class="col-md-3"><?= $form->field($model, 'dob_nepali_year')->textInput(['maxlength' => true, 'placeholder'=>'yyyy'])->label(false); ?></div><div class="col-md-3"><?= $form->field($model, 'dob_nepali_month')->textInput(['maxlength' => true, 'placeholder'=>'mm'])->label(false); ?></div><div class="col-md-3"><?= $form->field($model, 'dob_nepali_day')->textInput(['maxlength' => true, 'placeholder'=>'dd'])->label(false); ?></div>
                </div>

            <?= $form->field($model, 'sex')->dropDownList([ 'पुरुष' => 'पुरुष' ,'स्त्री' => 'स्त्री','अन्य' =>'अन्य'], ['prompt' => '']) ?>

            <?= $form->field($model, 'rural_area')->dropDownList([ 'हो' => 'हो' ,'होइन' => 'होइन'], ['prompt' => '']) ?>

            <?= $form->field($model, 'oath_of_parliament_member')->widget(
                DatePicker::className(), [
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
            ]);?>

            <div class="row"> 
                <div class="col-md-4"> <STRONG>शपथ ग्रहण मिती (B.S) :- </STRONG>  </div><div class="col-md-3"><?= $form->field($model, 'oath_nepali_year')->textInput(['maxlength' => true, 'placeholder'=>'yyyy'])->label(false); ?></div><div class="col-md-2"><?= $form->field($model, 'oath_nepali_month')->textInput(['maxlength' => true, 'placeholder'=>'mm'])->label(false); ?></div><div class="col-md-2"><?= $form->field($model, 'oath_nepali_day')->textInput(['maxlength' => true, 'placeholder'=>'dd'])->label(false); ?></div>
                </div>

            <?= $form->field($model, 'status')->dropDownList(['सकृय','निलम्बन','राजीनामा','पद खारेज','मृत्यू','राष्ट्रपतिमा निर्वाचित','उपराष्ट्रपतिमा निर्वाचित'], ['prompt' => '']) ?>
        </div>
        </div><!-- 
            <?= $form->field($model, 'created_by')->textInput() ?>

            <?= $form->field($model, 'created_date')->textInput() ?>

            <?= $form->field($model, 'created_ip')->textInput(['maxlength' => true]) ?> -->

    <div class="form-group text-center">
        <?= Html::submitButton('Save Parliament Member', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
