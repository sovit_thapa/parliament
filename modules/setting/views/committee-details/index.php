<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\services\CommitteeDetailsService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Committee Details';
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="committee-details-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Committee Details', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'target'=>'TARGET_SELF',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
             [
                'attribute' => 'parliament_id',
                'label'=>'Parliament',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->parliament ? $data->parliament->name : '';
                }
            ],
             [
                'attribute' => 'party_id',
                'label'=>'Party',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->party ? $data->party->name : '';
                }
            ],

             [
                'attribute' => 'committee_id',
                'label'=>'Committee',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->committee ? $data->committee->name : '';
                }
            ],
            'number',
             [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->status == 1 ? 'Active' : 'In-Active';
                }
            ],
        ],

        'pdfLibraryPath'=>'@vendor/kartik-v/mpdf',
        'onInitSheet' => function (PHPExcel_Worksheet $sheet, $grid) {
            $sheet->getDefaultStyle()->applyFromArray(['borders' => [ 'allborders' => ['style' => PHPExcel_Style_Border::BORDER_THIN ]]]);
        },
        'exportConfig'=> [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false
        ]
    ]); ?>





    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
             [
                'attribute' => 'parliament_id',
                'label'=>'Parliament',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->parliament ? $data->parliament->name : '';
                }
            ],

             [
                'attribute' => 'party_id',
                'label'=>'Party',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->party ? $data->party->name : '';
                }
            ],

             [
                'attribute' => 'committee_id',
                'label'=>'Committee',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->committee ? $data->committee->name : '';
                }
            ],
            'number',
             [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->status == 1 ? 'Active' : 'In-Active';
                }
            ],
            // 'status',
            // 'created_by',
            // 'created_ip',
            // 'created_date',

            [
                'header' => '&nbsp;&nbsp; Action &nbsp;&nbsp;',
            'class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<?php
    echo $this->registerJsFile('@web/bootstrap/js/bootstrap.min.js');
?>
