<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\setting\models\PoliticalParty;
use app\modules\setting\models\Committee;
use app\components\UtilityFunctions;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\CommitteeDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="committee-details-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row well">
        <?= $form->field($model, 'parliament_id')->hiddenInput(['value'=>UtilityFunctions::SambhidhanSava()])->label(false); ?>
        <div class="col-md-6">


            <?= $form->field($model, 'party_id')->dropDownList(ArrayHelper::map(PoliticalParty::find()->all(), 'id', 'name'),['prompt'=>' POLITICAL PARTY ']);?>

            <?= $form->field($model, 'committee_id')->dropDownList(ArrayHelper::map(Committee::find()->all(), 'id', 'name'),['prompt'=>' POLITICAL PARTY ']);?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'number')->textInput() ?>

            <?php
                if(!$model->isNewRecord)
                    echo $form->field($model, 'status')->dropDownList(array('1'=>' Active ', '0'=>' In-Active ')); ?>
        </div>
    </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save Details', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
