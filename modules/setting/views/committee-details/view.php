<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\CommitteeDetails */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = ['label' => 'Committee Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="committee-details-view">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',

             [
                'attribute' => 'parliament_id',
                'label'=>'Parliament',
                'format' => 'raw',
                'value' => $model->parliament ? $model->parliament->name : '',
            ],

             [
                'attribute' => 'party_id',
                'label'=>'Party',
                'format' => 'raw',
                'value' => $model->party ? $model->party->name : '',
            ],

             [
                'attribute' => 'committee_id',
                'label'=>'Committee',
                'format' => 'raw',
                'value' => $model->committee ? $model->committee->name : '',
            ],
            'number',
            'status',

             [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => $model->status == 1 ? 'Active' : 'In-Active',
            ],
            'created_by',
            'created_ip',
            'created_date',
        ],
    ]) ?>

</div>
