<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\UtilityFunctions;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\MemberStatusDetails */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Member Status Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="member-status-details-view">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',

             [
                'attribute' => 'member_id',
                'label'=>'Member',
                'format' => 'raw',
                'value' => $model->member ? $model->member->member_title.' '.$model->member->first_name.' '.$model->member->last_name : '',
            ],
            'status',
            'type',
            [
                'attribute' => 'start_date',
                'label'=>'START DATE',
                'format' => 'raw',
                'value' => UtilityFunctions::EnglishToNepali($model->start_date),
            ],
            [
                'attribute' => 'end_date',
                'label'=>'END DATE',
                'format' => 'raw',
                'value' => UtilityFunctions::EnglishToNepali($model->end_date),
            ],
            'description',
            'created_by',
            'create_ip',
            'created_date',
        ],
    ]) ?>

</div>
