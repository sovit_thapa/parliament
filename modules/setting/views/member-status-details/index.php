<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\components\UtilityFunctions;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\services\MemberStatusDetailsService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Member Status Details';
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="member-status-details-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Member Status Details', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
             [
                'attribute' => 'member_id',
                'label'=>'Member',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->member ? $data->member->member_title.' '.$data->member->first_name.' '.$data->member->last_name : '';
                }
            ],
            'status',
            'type',
            [
                'attribute' => 'start_date',
                'label'=>'START DATE',
                'format' => 'raw',
                'value' => function ($data) {
                    return UtilityFunctions::EnglishToNepali($data->start_date);
                }
            ],
            [
                'attribute' => 'end_date',
                'label'=>'END DATE',
                'format' => 'raw',
                'value' => function ($data) {
                    return UtilityFunctions::EnglishToNepali($data->end_date);
                }
            ],
            // 'created_by',
            // 'created_ip',
            // 'created_date',

            [
                'header' => 'ACTION',
                'class' => 'yii\grid\ActionColumn'
            ],
        ],
    ]); ?>
</div>
