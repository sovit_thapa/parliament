<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\setting\models\ParliamentMember;
use app\components\UtilityFunctions;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\MemberStatusDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="member-status-details-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row well">
    <div class="col-md-6">
    <?= $form->field($model, 'member_id')->dropDownList(ArrayHelper::map(UtilityFunctions::ActiveParliamentMember(), 'id', 'name'),
        ['prompt' => 'Select Parliament Member']); ?>

    <?= $form->field($model, 'status')->dropDownList([ 'सकृय' => 'सकृय','निलम्बन' => 'निलम्बन','राजीनामा' => 'राजीनामा','पद खारेज' => 'पद खारेज','मृत्यू' => 'मृत्यू' ,'राष्ट्रपतिमा निर्वाचित' => 'राष्ट्रपतिमा निर्वाचित','उपराष्ट्रपतिमा निर्वाचित' => 'उपराष्ट्रपतिमा निर्वाचित'], ['prompt' => '']) ?>

    <?= $form->field($model, 'type')->dropDownList([ 'premanent' => 'Premanent', 'temporary' => 'Temporary', ], ['prompt' => '']) ?>

    
    </div>
    <div class="col-md-6">

    <?= $form->field($model, 'start_date')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9999-99-99','options' => ['value'=> UtilityFunctions::EnglishToNepali($model->start_date), 'class'=>'form-control']
            ]) ?>

    <?= $form->field($model, 'end_date')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9999-99-99','options' => ['value'=> UtilityFunctions::EnglishToNepali($model->end_date), 'class'=>'form-control']
            ]) ?>
    <?= $form->field($model, 'description')->textarea(['rows' => '6']) ?>
    </div>
    </div>


    <div class="form-group text-center">
        <?= Html::submitButton('Save Member Status', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
