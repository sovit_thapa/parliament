<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\MemberStatusDetails */

$this->title = 'Update Member Status Details: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Member Status Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="member-status-details-update">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
