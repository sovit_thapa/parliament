<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\services\OfficialPostService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Official Posts';
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="official-post-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Official Post', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'type',
            'name',
            'is_only_one',
            'description:ntext',
            // 'created_by',
            // 'create_ip',
            // 'created_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
