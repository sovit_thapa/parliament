<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\OfficialPost */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="official-post-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row well">
        <div class="col-md-6">

            <?= $form->field($model, 'type')->dropDownList([ 'goverment' => 'Goverment', 'party' => 'Party', ], ['prompt' => '']) ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'is_only_one')->dropDownList([ 'in_party' => 'In party', 'in_goverment' => 'In goverment', 'none' => 'None', ], ['prompt' => '']) ?>
        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
        </div>
    </div>
<!-- 
    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'create_ip')->textInput() ?>

    <?= $form->field($model, 'created_date')->textInput() ?>
 -->
    <div class="form-group text-center">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
