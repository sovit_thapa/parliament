<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\ParliamentPartyOfficial */

$this->title = 'MINISTERS';
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting/ministers']];
$this->params['breadcrumbs'][] = ['label' => 'Ministers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parliament-party-official-create">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
