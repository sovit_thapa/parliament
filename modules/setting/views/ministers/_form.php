<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\components\UtilityFunctions;
use app\modules\setting\models\PoliticalParty;
use app\modules\setting\models\ParliamentDetails;
use app\modules\setting\models\ParliamentMember;
use app\modules\setting\models\OfficialPost;
use app\modules\setting\models\Ministry;

use kartik\widgets\Select2;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\ParliamentPartyOfficial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parliament-party-official-form">

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'parliament_id')->hiddenInput(['value'=>UtilityFunctions::SambhidhanSava()])->label(false); ?>

<div class="row well">
    <div class="col-md-6">
        <?= $form->field($model, 'political_party')->dropDownList(ArrayHelper::map(PoliticalParty::find()->all(), 'name', 'name'),
        [
        'prompt' => 'Select Political Party',

        ]);
        ?>
        <?php
        $details_member = ParliamentMember::findOne($model->parliament_member_id);
        $parliament_member = $details_member ? $details_member->member_title.' '.$details_member->first_name.' '.$details_member->last_name : '';
        ?>

    <?=
    // Dependent Dropdown
    $form
        ->field($model, 'parliament_member_id')
        ->widget(DepDrop::classname(), ['options' => ['id' => 'parliamentpartyofficial-parliament_member_id'],
            'data'=> [$parliament_member],
            'pluginOptions' => [
            'depends' => ['parliamentpartyofficial-political_party'],
            'url' => Url::to(['/setting/committee-member-details/parliament-member']),
            'placeholder' => 'Select Parliament Member...',
            'loadingText' => 'Loading Parliament Member ...',
            ]
        ]);
    ?>

        <?= $form->field($model, 'post')->dropDownList(ArrayHelper::map(OfficialPost::find()->where(['type'=>'goverment'])->all(), 'name', 'name'),
        [
        'prompt' => 'Select Post',

        ]);
        ?>

        <?php 
        // Tagging support Multiple
        echo $form->field($model, 'ministry')->widget(Select2::classname(), [
            'options' => ['placeholder' => 'Select a color ...', 'multiple' => true],
            'data'=>ArrayHelper::map(Ministry::find()->all(), 'title', 'title'),
            'pluginOptions' => [
                'tags' => true,
                'tokenSeparators' => [',', ' '],
                'maximumInputLength' => 10
            ],
        ])->label('नाम लिस्टहरु');
        ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'start_from')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '9999-99-99','options' => ['value'=> $model->start_from, 'class'=>'form-control']
        ]) ?>

        <?= $form->field($model, 'to_date')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '9999-99-99','options' => ['value'=> $model->to_date, 'class'=>'form-control']
        ]) ?>


        <?= $form->field($model, 'letter_received_date')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '9999-99-99','options' => ['value'=>$model->letter_received_date, 'class'=>'form-control']
        ]) ?>
        <?= $form->field($model, 'status')->dropDownList(['सकृय' => 'सकृय','निलम्बन' => 'निलम्बन','राजीनामा' => 'राजीनामा','पद खारेज' => 'पद खारेज','मृत्यू' => 'मृत्यू','राष्ट्रपतिमा निर्वाचित' => 'राष्ट्रपतिमा निर्वाचित','उपराष्ट्रपतिमा निर्वाचित' =>'उपराष्ट्रपतिमा निर्वाचित' ], ['prompt' => '']) ?>

         <?= $form->field($model, '_order')->textInput(['type'=>'number']); ?>
    </div>
    

    <div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

</div>




<?php ActiveForm::end(); ?>

</div>
