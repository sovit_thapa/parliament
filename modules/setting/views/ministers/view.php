<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\components\UtilityFunctions;
/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\ParliamentPartyOfficial */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = ['label' => 'Ministers', 'url' => ['/setting/ministers']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parliament-party-official-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',



             [
                'attribute' => 'parliament_id',
                'label'=>'Parliament',
                'format' => 'raw',
                'value' => $model->parliament ? $model->parliament->name : '',
            ],
            'political_party',
            'post',
            'ministry',
             [
                'attribute' => 'parliament_member_id',
                'label'=>'Member',
                'format' => 'raw',
                'value' => $model->parliamentMember ? $model->parliamentMember->member_title.' '.$model->parliamentMember->first_name.' '.$model->parliamentMember->last_name : '',
            ],

            [
                'attribute' => 'start_from',
                'label'=>'FROM DATE',
                'format' => 'raw',
                'value' => UtilityFunctions::EnglishToNepali($model->start_from),
            ],
            [
                'attribute' => 'to_date',
                'label'=>'TO DATE',
                'format' => 'raw',
                'value' => UtilityFunctions::EnglishToNepali($model->to_date),
            ],
            [
                'attribute' => 'letter_received_date',
                'label'=>'LETTER RECEIVED',
                'format' => 'raw',
                'value' => UtilityFunctions::EnglishToNepali($model->letter_received_date),
            ],
            'status',
            'created_by',
            'created_ip',
            'created_date',
        ],
    ]) ?>

</div>
