<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\CommitteeMemberDetails */

$this->title = 'Update Committee Member Details: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = ['label' => 'Committee Member Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="committee-member-details-update">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
