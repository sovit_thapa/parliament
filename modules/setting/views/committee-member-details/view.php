<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\UtilityFunctions;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\CommitteeMemberDetails */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = ['label' => 'Committee Member Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="committee-member-details-view">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

             [
                'attribute' => 'parliament_id',
                'label'=>'Parliament',
                'format' => 'raw',
                'value' => $model->parliament ? $model->parliament->name : '',
            ],

             [
                'attribute' => 'committee_id',
                'label'=>'Committee',
                'format' => 'raw',
                'value' => $model->committee ? $model->committee->name : '',
            ],

            [
                'attribute' => 'party_id',
                'label'=>'Party',
                'format' => 'raw',
                'value' => $model->party ? $model->party->name : '',
            ],

             [
                'attribute' => 'member_id',
                'label'=>'Member',
                'format' => 'raw',
                'value' => $model->member ? $model->member->member_title.' '.$model->member->first_name.' '.$model->member->last_name : '',
            ],

            [
                'attribute' => 'from_date',
                'label'=>'FROM DATE',
                'format' => 'raw',
                'value' => UtilityFunctions::EnglishToNepali($model->from_date),
            ],
            [
                'attribute' => 'to_date',
                'label'=>'TO DATE',
                'format' => 'raw',
                'value' => UtilityFunctions::EnglishToNepali($model->to_date),
            ],
            [
                'attribute' => 'status',
                'label'=>'STATUS',
                'format' => 'raw',
                'value' => $model->status == 1 ? 'ACTIVE' : 'IN-ACTIVE',
            ],
            'created_by',
            'created_ip',
            'created_date',
        ],
    ]) ?>

</div>
