<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use kartik\export\ExportMenu;
use app\components\UtilityFunctions;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\services\CommitteeMemberDetailsService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Committee Member Details';
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="committee-member-details-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="text-center" >
        <?= Html::a('Create Committee Member Details', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?php /*echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'target'=>'TARGET_SELF',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
             [
                'attribute' => 'parliament_id',
                'label'=>'Parliament',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->parliament ? $data->parliament->name : '';
                }
            ],

             [
                'attribute' => 'committee_id',
                'label'=>'Committee',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->committee ? $data->committee->name : '';
                }
            ],

            [
                'attribute' => 'party_id',
                'label'=>'Party',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->party ? $data->party->name : '';
                }
            ],

             [
                'attribute' => 'member_id',
                'label'=>'Member',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->member ? $data->member->member_title.' '.$data->member->first_name.' '.$data->member->last_name : '';
                }
            ],

            [
                'attribute' => 'from_date',
                'label'=>'FROM DATE',
                'format' => 'raw',
                'value' => function ($data) {
                    return UtilityFunctions::EnglishToNepali($data->from_date);
                }
            ],
            [
                'attribute' => 'to_date',
                'label'=>'TO DATE',
                'format' => 'raw',
                'value' => function ($data) {
                    return UtilityFunctions::EnglishToNepali($data->to_date);
                }
            ],
            [
                'attribute' => 'status',
                'label'=>'STATUS',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->status == 1 ? 'ACTIVE' : 'IN-ACTIVE';
                }
            ],
        ],

        'pdfLibraryPath'=>'@vendor/kartik-v/mpdf',
        'filename' => 'Committee Member Details',
        'onInitSheet' => function (PHPExcel_Worksheet $sheet, $grid) {
            $sheet->getDefaultStyle()->applyFromArray(['borders' => [ 'allborders' => ['style' => PHPExcel_Style_Border::BORDER_THIN ]]]);
        },
        'exportConfig'=> [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false
        ]
    ]);*/ ?>





    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
             [
                'attribute' => 'parliament_id',
                'label'=>'Parliament',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->parliament ? $data->parliament->name : '';
                }
            ],

             [
                'attribute' => 'committee_id',
                'label'=>'Committee',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->committee ? $data->committee->name : '';
                }
            ],

            [
                'attribute' => 'party_id',
                'label'=>'Party',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->party ? $data->party->name : '';
                }
            ],

             [
                'attribute' => 'member_id',
                'label'=>'Member',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->member ? $data->member->member_title.' '.$data->member->first_name.' '.$data->member->last_name : '';
                }
            ],

            [
                'attribute' => 'from_date',
                'label'=>'FROM DATE',
                'format' => 'raw',
                'value' => function ($data) {
                    return UtilityFunctions::EnglishToNepali($data->from_date);
                }
            ],
            [
                'attribute' => 'to_date',
                'label'=>'TO DATE',
                'format' => 'raw',
                'value' => function ($data) {
                    return UtilityFunctions::EnglishToNepali($data->to_date);
                }
            ],
            [
                'attribute' => 'status',
                'label'=>'STATUS',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->status == 1 ? 'ACTIVE' : 'IN-ACTIVE';
                }
            ],
            // 'from_date',
            // 'to_date',
            // 'status',
            // 'created_by',
            // 'created_ip',
            // 'created_date',

            [
            'header' => 'ACTION\'S BUTTON',
            'class' => 'yii\grid\ActionColumn'],
        ],
        
        'panel'=>  [
                        'type'=>GridView::TYPE_DEFAULT,
                        //'heading'=>'<h4 class="text-center"><STRONG> asd</STRONG></h4>',
                    ],

       'toolbar' => [
        '{export}',
        '{toggleData}'
    ]
    ]); ?>
</div>
<?php
    echo $this->registerJsFile('@web/bootstrap/js/bootstrap.min.js');
?>