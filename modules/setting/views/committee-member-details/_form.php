<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\components\UtilityFunctions;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;

use app\modules\setting\models\PoliticalParty;
use app\modules\setting\models\Committee;
use app\modules\setting\models\ParliamentMember;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\CommitteeMemberDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="committee-member-details-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row well">

        <div class="col-md-6">

            <?= $form->field($model, 'party_id')->dropDownList(ArrayHelper::map(PoliticalParty::find()->all(), 'id', 'name'),['prompt'=>' POLITICAL PARTY ']);?>


            <?= $form
                ->field($model, 'committee_id')
                ->dropDownList(ArrayHelper::map(Committee::find()->all(), 'id', 'name'),
                    [
                        'prompt' => "COMMITTEE LIST'S"
                    ]);
            ?>


    <?= $form
        ->field($model, 'member_id')
        ->widget(DepDrop::classname(), ['options' => ['id' => 'committeememberdetails-member_id'],
            'data'=> [ArrayHelper::map(UtilityFunctions::ActiveParliamentMember($model->party_id), 'id', 'name')],
            'pluginOptions' => [
            'depends' => ['committeememberdetails-party_id'],
            'url' => Url::to(['/setting/committee-member-details/parliament-member']),
            'placeholder' => 'Select Parliament Member...',
            'loadingText' => 'Loading Parliament Member ...',
            ]
        ]);
    ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'from_date')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9999-99-99','options' => ['value'=> UtilityFunctions::EnglishToNepali($model->from_date), 'class'=>'form-control']
            ]) ?>

            <?= $form->field($model, 'to_date')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9999-99-99','options' => ['value'=> UtilityFunctions::EnglishToNepali($model->to_date), 'class'=>'form-control']
            ]) ?>

            <?= $form->field($model, 'status')->dropDownList([1=>'ACTIVE', '0'=>'IN-ACTIVE']) ?>
        </div>
    </div>

        <?= $form->field($model, 'parliament_id')->hiddenInput(['value'=>UtilityFunctions::SambhidhanSava()])->label(false); ?>
    <div class="form-group text-center">
        <?= Html::submitButton('Save Detail Information', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
