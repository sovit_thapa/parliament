<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\MahasachibDetails */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Mahasachib Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mahasachib-details-view">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',

            [
                'attribute' => 'description',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->description;
                }
            ],

            [
                'attribute' => 'status',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->status== 1 ? 'ACTIVE':'IN-ACTIVE';
                }
            ],
            /*'created_by',
            'created_date',*/
        ],
    ]) ?>

</div>
