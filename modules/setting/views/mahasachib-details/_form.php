<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\MahasachibDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mahasachib-details-form well">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row ">
    <div class="col-md-6 col-sm-12">
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6 col-sm-12">

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    </div>
    </div>

    <div class="row ">
    <?php if(!$model->isNewRecord){
        echo $form->field($model, 'status')->dropDownList([ '1' => 'ACTIVE', '0' => 'In-Active']);
        } ?>
    </div>


    <div class="form-group text-center">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
