<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\MahasachibDetails */

$this->title = 'NEW Mahasachib Details';
$this->params['breadcrumbs'][] = ['label' => 'Mahasachib Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mahasachib-details-create">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
