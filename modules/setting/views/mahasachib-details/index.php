<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\services\MahasachibDetailsService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mahasachib Information Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mahasachib-details-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="float: right;">
        <?= Html::a('Create Mahasachib Details', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            [
                'attribute' => 'description',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->description;
                }
            ],

            [
                'attribute' => 'status',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->status== 1 ? 'ACTIVE':'IN-ACTIVE';
                }
            ],
            //'created_by',
            // 'created_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
