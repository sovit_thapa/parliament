<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\Committee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="committee-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row well">

        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
        </div>
    </div>
    <!-- 

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'create_ip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_date')->textInput() ?>
 -->
    <div class="form-group text-center">
        <?= Html::submitButton('Save Committee', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
