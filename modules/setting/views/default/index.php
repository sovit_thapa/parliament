<?Php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
    <!-- Main content -->
        <section class="content">
          <!-- Info boxes -->
          <div class="row">

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class="fa fa-location-arrow"></i></span>
                <div class="info-box-content">
                <?= Html::a('DISTRICT', ['location/index']) ?>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                  <?= Html::a('ETHINICITY', ['ethinicity/index']) ?>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class="fa fa-flag"></i></span>
                <div class="info-box-content">
                <?= Html::a('POLITICAL PARTY', ['political-party/index']) ?>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>  
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                 <?= Html::a('MINISTRY OF NEPAL', ['/setting/ministry']) ?>
                  <span class="info-box-number"</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->


            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                 <?= Html::a('MINISTER OF NEPAL', ['/setting/ministers']) ?>
                  <span class="info-box-number"</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class="fa fa-address-card" aria-hidden="true"></i></span>
                <div class="info-box-content">
                 <?= Html::a('COMMITTEE', ['committee/index']) ?>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class="ion ion-clipboard"></i></span>
                <div class="info-box-content">
                <?= Html::a('PARLIAMENT DETAILS', ['parliament-details/index']) ?>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class="ion ion-beaker"></i></span>
                <div class="info-box-content">
                <?= Html::a('MEETING DETAILS', ['/program/meeting-details/index']) ?>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->


            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class="ion ion-beaker"></i></span>
                <div class="info-box-content">
                <?= Html::a('POST', ['official-post/index']) ?>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            
            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class="ion ion-erlenmeyer-flask"></i></span>
                <div class="info-box-content">
                <?= Html::a('PARLIAMENT MEMBER DETAILS', ['parliament-member/index']) ?>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->



            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class="ion ion-university"></i></span>
                <div class="info-box-content">
               <?= Html::a('PARTY WISE COMMITTEE DETAILS', ['committee-details/index']) ?>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class="ions ion-ios-book"></i></span>
                <div class="info-box-content">
               <?= Html::a('PARLIMENT MEMBERS COMMITTEE DETAILS', ['committee-member-details/index']) ?>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                 <?= Html::a('POLITICAL PARTY OFFICIAL', ['parliament-party-official/index']) ?>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->


            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                 <?= Html::a('PARLIAMENT STATUS DETAILS', ['member-status-details/index']) ?>
                  <span class="info-box-number"</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                 <?= Html::a('MINISTRY AND COMMITTEE RELATION', ['ministry-related-committee/index']) ?>
                  <span class="info-box-number"</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                 <?= Html::a('व्यवस्थापिका–संसदको महासचिव', ['mahasachib-details/index']) ?>
                  <span class="info-box-number"</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->



            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                 <?= Html::a('अधिवेशन', ['adhibasen/index']) ?>
                  <span class="info-box-number"</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

          </div><!-- /.row -->
</section>


</body>
</html>