<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\components\UtilityFunctions;
use app\modules\setting\models\PoliticalParty;
use app\modules\setting\models\ParliamentDetails;
use app\modules\setting\models\ParliamentMember;
use app\modules\setting\models\OfficialPost;
use app\modules\setting\models\Ministry;

use kartik\depdrop\DepDrop;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\ParliamentPartyOfficial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parliament-party-official-form">

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'parliament_id')->hiddenInput(['value'=>UtilityFunctions::SambhidhanSava()])->label(false); ?>

<div class="row well">
    <div class="col-md-6">
        <?= $form->field($model, 'political_party')->dropDownList(ArrayHelper::map(PoliticalParty::find()->all(), 'name', 'name'),
        [
        'prompt' => 'Select Political Party',

        ]);
        ?>


    <?=
    // Dependent Dropdown
    $form
        ->field($model, 'parliament_member_id')
        ->widget(DepDrop::classname(), ['options' => ['id' => 'parliamentpartyofficial-parliament_member_id'],
            //'data'=> [ArrayHelper::map(ParliamentMember::find()->where(['political_id'=>$model->political_party_id])->all(), 'id','last_name')],
            'pluginOptions' => [
            'depends' => ['parliamentpartyofficial-political_party'],
            'url' => Url::to(['/setting/committee-member-details/parliament-member']),
            'placeholder' => 'Select Parliament Member...',
            'loadingText' => 'Loading Parliament Member ...',
            ]
        ]);
    ?>

        <?= $form->field($model, 'post')->dropDownList(ArrayHelper::map(OfficialPost::find()->where(['type'=>'party'])->all(), 'name', 'name'),
        [
        'prompt' => 'Select Post',

        ]);
        ?>

        <?= $form->field($model, 'ministry')->dropDownList(ArrayHelper::map(Ministry::find()->all(), 'title', 'title'),
        [
        'prompt' => 'Select Post',

        ]);
        ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'start_from')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '9999-99-99','options' => ['value'=> $model->start_from, 'class'=>'form-control']
        ]) ?>

        <?= $form->field($model, 'to_date')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '9999-99-99','options' => ['value'=> $model->to_date, 'class'=>'form-control']
        ]) ?>


        <?= $form->field($model, 'letter_received_date')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '9999-99-99','options' => ['value'=>$model->letter_received_date, 'class'=>'form-control']
        ]) ?>
        <?= $form->field($model, 'status')->dropDownList(['सकृय' => 'सकृय','निलम्बन' => 'निलम्बन','राजीनामा' => 'राजीनामा','पद खारेज' => 'पद खारेज','मृत्यू' => 'मृत्यू','राष्ट्रपतिमा निर्वाचित' => 'राष्ट्रपतिमा निर्वाचित','उपराष्ट्रपतिमा निर्वाचित' =>'उपराष्ट्रपतिमा निर्वाचित' ], ['prompt' => '']) ?>

        <?= $form->field($model, '_order')->textInput(['type'=>'number']); ?>
    </div>
    

    <div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

</div>




<?php ActiveForm::end(); ?>

</div>
