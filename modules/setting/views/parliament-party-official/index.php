<?php

use yii\helpers\Html;
use yii\grid\GridView;

use app\components\UtilityFunctions;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\Services\ParliamentPartyOfficialService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Parliament Party Officials';
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parliament-party-official-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Parliament Party Official', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],



             [
                'attribute' => 'parliament_id',
                'label'=>'Parliament',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->parliament ? $data->parliament->name : '';
                }
            ],
            'political_party',
            'post',
            'ministry',
             [
                'attribute' => 'parliament_member_id',
                'label'=>'Member',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->parliamentMember ? $data->parliamentMember->member_title.' '.$data->parliamentMember->first_name.' '.$data->parliamentMember->last_name : '';
                }
            ],

            [
                'attribute' => 'start_from',
                'label'=>'FROM DATE',
                'format' => 'raw',
                'value' => function ($data) {
                    return UtilityFunctions::EnglishToNepali($data->start_from);
                }
            ],
            [
                'attribute' => 'to_date',
                'label'=>'TO DATE',
                'format' => 'raw',
                'value' => function ($data) {
                    return UtilityFunctions::EnglishToNepali($data->to_date);
                }
            ],
            [
                'attribute' => 'letter_received_date',
                'label'=>'LETTER RECEIVED',
                'format' => 'raw',
                'value' => function ($data) {
                    return UtilityFunctions::EnglishToNepali($data->letter_received_date);
                }
            ],
            'status',
            '_order',
            // 'created_by',
            // 'created_ip',
            // 'created_date',

            [
                'header' => 'POSTLINKS',
            'class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
