<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\Adhibasen */

$this->title = 'Adhibasen';
$this->params['breadcrumbs'][] = ['label' => 'Adhibasens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adhibasen-create">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
