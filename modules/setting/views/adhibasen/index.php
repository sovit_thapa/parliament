<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\services\AdhibasenService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Adhibasens';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adhibasen-index">

    <h2 class="text-center"><?= Html::encode($this->title) ?></h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="row" style="text-align: right;">
        <?= Html::a('Create Adhibasen', ['create'], ['class' => 'btn btn-success']) ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'year',
            'start_date_time',
            'end_date_time',

            [
                'attribute' => 'status',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->status==1 ? 'ACTIVE':'DE-ACTIVE';
                },
            ],
            // 'created_by',
            // 'created_date',

            [
            'header' =>'ACTION/LINK',
            'class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
