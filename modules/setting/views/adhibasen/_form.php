<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datetimepicker\DateTimePicker;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\Adhibasen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adhibasen-form well">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'year')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
        <?= $form->field($model, 'start_date_time')->widget(DateTimePicker::className(), [
            'size' => 'ms',
            'pickButtonIcon' => 'glyphicon glyphicon-time',
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd H:i:s',
                'todayBtn' => true
            ]
        ]);?>
        </div>
        <div class="col-md-6">
        <?= $form->field($model, 'end_date_time')->widget(DateTimePicker::className(), [
            'size' => 'ms',
            'pickButtonIcon' => 'glyphicon glyphicon-time',
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd H:i:s',
                'todayBtn' => true
            ]
        ]);?>
        </div>
    </div>
    <?php
    if(!$model->isNewRecord)
        echo $form->field($model, 'status')->dropDownList([1 => 'Active','0' => 'De-Active']);
    ?>
    <div class="form-group">
        <?= Html::submitButton('Save Information', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
