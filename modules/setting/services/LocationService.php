<?php

namespace app\modules\setting\services;

use Yii;
use yii\filters\AccessControl;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\setting\models\Location;

/**
 * LocationService represents the model behind the search form of `app\modules\setting\models\Location`.
 */
class LocationService extends Location
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'location_level', 'created_by'], 'integer'],
            [['name', 'nepali_name', 'created_date', 'location_slug'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Location::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'location_level' => $this->location_level,
            'created_date' => $this->created_date,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'nepali_name', $this->nepali_name])
            ->andFilterWhere(['like', 'location_slug', $this->location_slug]);

        return $dataProvider;
    }
}
