<?php

namespace app\modules\setting\services;

use Yii;
use yii\filters\AccessControl;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\setting\models\Adhibasen;

/**
 * AdhibasenService represents the model behind the search form of `app\modules\setting\models\Adhibasen`.
 */
class AdhibasenService extends Adhibasen
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by'], 'integer'],
            [['year', 'start_date_time', 'end_date_time', 'created_date', 'name','status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Adhibasen::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'created_date' => $this->created_date,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'year', $this->year])
        ->andFilterWhere(['like', 'name', $this->name])
        ->andFilterWhere(['like', 'start_date_time', $this->start_date_time])
        ->andFilterWhere(['like', 'end_date_time', $this->end_date_time]);

        return $dataProvider;
    }
}
