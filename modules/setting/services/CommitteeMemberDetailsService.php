<?php

namespace app\modules\setting\services;

use Yii;
use yii\filters\AccessControl;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\setting\models\CommitteeMemberDetails;

/**
 * CommitteeMemberDetailsService represents the model behind the search form of `app\modules\setting\models\CommitteeMemberDetails`.
 */
class CommitteeMemberDetailsService extends CommitteeMemberDetails
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['from_date', 'to_date', 'created_ip', 'created_date', 'parliament_id', 'committee_id', 'party_id', 'member_id', 'status', 'created_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */


    public function search($params)
    {
        $query = CommitteeMemberDetails::find();
        $query->joinWith(['committee','parliament','member','party']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'from_date' => $this->from_date,
            'to_date' => $this->to_date,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'created_date' => $this->created_date,
        ]);

        $query->andFilterWhere(['like', 'committee.name', $this->committee_id])
            ->andFilterWhere(['like', 'political_party.name', $this->party_id])
            ->andFilterWhere(['like', 'parliament_member.first_name', $this->member_id])
            ->andFilterWhere(['like', 'parliament_details.name', $this->parliament_id])
            ->andFilterWhere(['like', 'created_ip', $this->created_ip]);

        return $dataProvider;
    }
}
