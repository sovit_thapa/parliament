<?php

namespace app\modules\setting\Services;

use Yii;
use yii\filters\AccessControl;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\setting\models\ParliamentPartyOfficial;

/**
 * ParliamentPartyOfficialService represents the model behind the search form of `app\modules\setting\models\ParliamentPartyOfficial`.
 */
class ParliamentPartyOfficialService extends ParliamentPartyOfficial
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['start_from', 'to_date', 'letter_received_date', 'status', 'created_ip', 'created_date', 'parliament_id', 'political_party', 'parliament_member_id', 'post', 'created_by', 'ministry'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */


    public function search($params)
    {
        $partyArray=array('प्रधान मन्त्री','मन्त्री');
        $query = ParliamentPartyOfficial::find()->where(['post'=>$partyArray]);

        $query->joinWith(['parliamentMember','parliament']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'start_from' => $this->start_from,
            'to_date' => $this->to_date,
            'letter_received_date' => $this->letter_received_date,
            'created_by' => $this->created_by,
            'created_date' => $this->created_date,
        ]);
        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'post', $this->post])
            ->andFilterWhere(['like', 'ministry', $this->ministry])
            ->andFilterWhere(['like', 'political_party', $this->political_party])
            ->andFilterWhere(['like', 'parliament_member.first_name', $this->parliament_member_id])
            ->andFilterWhere(['like', 'parliament_details.name', $this->parliament_id])
            ->andFilterWhere(['like', 'created_ip', $this->created_ip]);

        return $dataProvider;
    }


    public function searchParty($params)
    {
        $partyArray=array('सभापती','उप सभापती');
        $query = ParliamentPartyOfficial::find()->where(['post'=>$partyArray]);

        $query->joinWith(['parliamentMember','parliament']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'start_from' => $this->start_from,
            'to_date' => $this->to_date,
            'letter_received_date' => $this->letter_received_date,
            'created_by' => $this->created_by,
            'created_date' => $this->created_date,
        ]);
        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'post', $this->post])
            ->andFilterWhere(['like', 'ministry', $this->ministry])
            ->andFilterWhere(['like', 'political_party', $this->political_party])
            ->andFilterWhere(['like', 'parliament_member.first_name', $this->parliament_member_id])
            ->andFilterWhere(['like', 'parliament_details.name', $this->parliament_id])
            ->andFilterWhere(['like', 'created_ip', $this->created_ip]);

        return $dataProvider;
    }
}
