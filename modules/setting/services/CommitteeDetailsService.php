<?php

namespace app\modules\setting\services;

use Yii;
use yii\filters\AccessControl;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\setting\models\CommitteeDetails;

/**
 * CommitteeDetailsService represents the model behind the search form of `app\modules\setting\models\CommitteeDetails`.
 */
class CommitteeDetailsService extends CommitteeDetails
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','number', 'status', 'created_by'], 'integer'],
            [['created_ip', 'created_date', 'parliament_id', 'party_id', 'committee_id' ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CommitteeDetails::find();
        $query->joinWith(['committee','parliament','party']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'number' => $this->number,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'created_date' => $this->created_date,
        ]);



        $query->andFilterWhere(['like', 'created_ip', $this->created_ip])
            ->andFilterWhere(['like', 'parliament.name', $this->parliament_id])
            ->andFilterWhere(['like', 'committee.name', $this->committee_id])
            ->andFilterWhere(['like', 'party.name', $this->party_id]);

        return $dataProvider;
    }
}
