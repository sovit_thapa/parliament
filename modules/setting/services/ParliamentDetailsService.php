<?php

namespace app\modules\setting\services;

use Yii;
use yii\filters\AccessControl;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\setting\models\ParliamentDetails;

/**
 * ParliamentDetailsService represents the model behind the search form of `app\modules\setting\models\ParliamentDetails`.
 */
class ParliamentDetailsService extends ParliamentDetails
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parliament_total_member', 'created_by'], 'integer'],
            [['name', 'start_session', 'end_session', 'start_session_nepali_date', 'end_session_nepali_date', 'extented_time', 'description', 'status', 'created_ip', 'created_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ParliamentDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parliament_total_member' => $this->parliament_total_member,
            'start_session' => $this->start_session,
            'end_session' => $this->end_session,
            'created_by' => $this->created_by,
            'created_date' => $this->created_date,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'start_session_nepali_date', $this->start_session_nepali_date])
            ->andFilterWhere(['like', 'end_session_nepali_date', $this->end_session_nepali_date])
            ->andFilterWhere(['like', 'extented_time', $this->extented_time])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'created_ip', $this->created_ip]);

        return $dataProvider;
    }
}
