<?php

namespace app\modules\setting\services;

use Yii;
use yii\filters\AccessControl;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\setting\models\PoliticalParty;

/**
 * PoliticalPartyService represents the model behind the search form of `app\modules\setting\models\PoliticalParty`.
 */
class PoliticalPartyService extends PoliticalParty
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'party_establish_date', 'party_register_date', 'created_by'], 'integer'],
            [['name', 'description', 'create_ip', 'created_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PoliticalParty::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'party_establish_date' => $this->party_establish_date,
            'party_register_date' => $this->party_register_date,
            'created_by' => $this->created_by,
            'created_date' => $this->created_date,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'create_ip', $this->create_ip]);

        return $dataProvider;
    }
}
