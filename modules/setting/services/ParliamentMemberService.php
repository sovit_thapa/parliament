<?php

namespace app\modules\setting\Services;

use Yii;
use yii\filters\AccessControl;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\setting\models\ParliamentMember;

/**
 * ParliamentMemberService represents the model behind the search form of `app\modules\setting\models\ParliamentMember`.
 */
class ParliamentMemberService extends ParliamentMember
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parliament_id', 'political_id', 'district_id', 'elected_area_number', 'ethinicity_id', 'dob_nepali_day', 'dob_nepali_month', 'dob_nepali_year', 'oath_nepali_day', 'oath_nepali_month', 'oath_nepali_year', 'created_by', 'created_date'], 'integer'],
            [['member_title', 'first_name', 'last_name', 'elected_type', 'date_of_birth', 'sex', 'rural_area', 'oath_of_parliament_member', 'parliament_status', 'status', 'created_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ParliamentMember::find();

        $query->joinWith(['ethinicity','district','parliament','political']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'elected_area_number' => $this->elected_area_number,
            'date_of_birth' => $this->date_of_birth,
            'dob_nepali_day' => $this->dob_nepali_day,
            'dob_nepali_month' => $this->dob_nepali_month,
            'dob_nepali_year' => $this->dob_nepali_year,
            'oath_of_parliament_member' => $this->oath_of_parliament_member,
            'oath_nepali_day' => $this->oath_nepali_day,
            'oath_nepali_month' => $this->oath_nepali_month,
            'oath_nepali_year' => $this->oath_nepali_year,
            'created_by' => $this->created_by,
            'created_date' => $this->created_date,
        ]);

        $query->andFilterWhere(['like', 'member_title', $this->member_title])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])

            ->andFilterWhere(['like', 'ethinicity.name', $this->ethinicity_id])
            ->andFilterWhere(['like', 'district.nepali_name', $this->district_id])
            ->andFilterWhere(['like', 'parliament.name', $this->parliament_id])
            ->andFilterWhere(['like', 'political.name', $this->political_id])

            ->andFilterWhere(['like', 'elected_type', $this->elected_type])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'rural_area', $this->rural_area])
            ->andFilterWhere(['like', 'parliament_status', $this->parliament_status])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'created_ip', $this->created_ip]);

        return $dataProvider;
    }
}
