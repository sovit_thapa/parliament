<?php

namespace app\modules\setting\services;

use Yii;
use yii\filters\AccessControl;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\setting\models\MinistryRelatedCommittee;

/**
 * MinistryRelatedCommitteeService represents the model behind the search form of `app\modules\setting\models\MinistryRelatedCommittee`.
 */
class MinistryRelatedCommitteeService extends MinistryRelatedCommittee
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parliament_id', 'created_by'], 'integer'],
            [['ministry', 'committee', 'applied_from', 'created_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MinistryRelatedCommittee::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parliament_id' => $this->parliament_id,
            'applied_from' => $this->applied_from,
            'created_by' => $this->created_by,
            'created_date' => $this->created_date,
        ]);

        $query->andFilterWhere(['like', 'ministry', $this->ministry])
            ->andFilterWhere(['like', 'committee', $this->committee]);

        return $dataProvider;
    }
}
