<?php

namespace app\modules\setting\models;

use Yii;
use yii\filters\AccessControl;

/**
 * This is the model class for table "location".
 *
 * @property int $id
 * @property int $parent_id parent_id for the location from the location to denotes higher level of that location
 * @property string $name name of the location
 * @property string $nepali_name
 * @property int $location_level level of that location
 * @property string $nepali_date
 * @property string $created_date
 * @property int $created_by
 * @property string $location_slug
 *
 * @property ParliamentMember[] $parliamentMembers
 */
class Location extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'location_level', 'created_by'], 'integer'],
            [['name', 'location_level'], 'required'],
            [['created_date'], 'safe'],
            [['name','location_slug'], 'string', 'max' => 250],
            [['nepali_name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent',
            'name' => 'Name',
            'nepali_name' => 'Nepali Name',
            'location_level' => 'Location Level',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'location_slug' => 'Location Slug',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParliamentMembers()
    {
        return $this->hasMany(ParliamentMember::className(), ['district_id' => 'id']);
    }

    public function beforeSave($insert){
        if($this->isNewRecord){
            $this->location_level = 3;
            $this->location_slug = 'ds';
            $this->created_by = Yii::$app->user->id;
            $this->created_date = new \yii\db\Expression('NOW()');
        }
        return parent::beforeSave($insert);
    }
}
