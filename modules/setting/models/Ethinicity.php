<?php

namespace app\modules\setting\models;

use Yii;
use yii\filters\AccessControl;

/**
 * This is the model class for table "ethinic_cast".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $description
 * @property int $created_by
 * @property string $created_date
 *
 * @property ParliamentMember[] $parliamentMembers
 */
class Ethinicity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ethinic_cast';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'created_by'], 'integer'],
            [['name'], 'required'],
            [['created_date'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent',
            'name' => 'Name',
            'description' => 'Description',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParliamentMembers()
    {
        return $this->hasMany(ParliamentMember::className(), ['ethinicity_id' => 'id']);
    }
}
