<?php

namespace app\modules\setting\models;

use Yii;
use yii\filters\AccessControl;

/**
 * This is the model class for table "official_post".
 *
 * @property int $id
 * @property string $type
 * @property string $name
 * @property string $is_only_one
 * @property string $description
 * @property int $created_by
 * @property int $create_ip
 * @property int $created_date
 *
 * @property ParliamentPartyOfficial[] $parliamentPartyOfficials
 */
class OfficialPost extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'official_post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'is_only_one', 'description'], 'string'],
            [['name'], 'required'],
            [['created_by', 'create_ip', 'created_date'], 'integer'],
            [['name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'name' => 'Name',
            'is_only_one' => 'Is Only One',
            'description' => 'Description',
            'created_by' => 'Created By',
            'create_ip' => 'Create Ip',
            'created_date' => 'Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParliamentPartyOfficials()
    {
        return $this->hasMany(ParliamentPartyOfficial::className(), ['post_id' => 'id']);
    }
}
