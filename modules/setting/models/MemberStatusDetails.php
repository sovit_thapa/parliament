<?php

namespace app\modules\setting\models;

use Yii;
use yii\filters\AccessControl;
use app\components\UtilityFunctions;

/**
 * This is the model class for table "parliament_member_status".
 *
 * @property int $id
 * @property int $member_id
 * @property string $status
 * @property string $type
 * @property int $start_date
 * @property int $end_date
 * @property int $description
 * @property int $created_by
 * @property int $create_ip
 * @property int $created_date
 *
 * @property ParliamentMember $member
 */
class MemberStatusDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parliament_member_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'status'], 'required'],
            [['member_id', 'created_by'], 'integer'],
            [['status', 'type','create_ip','description'], 'string'],
            [['start_date', 'end_date', 'created_date'],'safe'],
            [['member_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParliamentMember::className(), 'targetAttribute' => ['member_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member',
            'status' => 'Status',
            'type' => 'Type',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'description' => 'Description',
            'created_by' => 'Created By',
            'create_ip' => 'Create Ip',
            'created_date' => 'Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(ParliamentMember::className(), ['id' => 'member_id']);
    }


     public function beforeSave($insert){
        if($this->isNewRecord){
            $this->create_ip = Yii::$app->getRequest()->getUserIP();
            $this->created_by = Yii::$app->user->id;
            $this->created_date = new \yii\db\Expression('NOW()');
        }
        $explode_from = explode('-', $this->start_date);
        $explode_to = explode('-', $this->end_date);
        $this->start_date = UtilityFunctions::NepaliToEnglish((int) $explode_from[0], (int) $explode_from[1], (int) $explode_from[2]);
        $this->end_date = UtilityFunctions::NepaliToEnglish((int) $explode_to[0], (int) $explode_to[1], (int) $explode_to[2]);
        return parent::beforeSave($insert);
    }

    private function englishDateConversion($date)
    {
        $date_divide = explode('-', $date);
        return Yii::$app->UtilityFunctions->NepaliToEnglish($date_divide[0],$date_divide[1],$date_divide[2]);
    }

}
