<?php

namespace app\modules\setting\models;

use Yii;
use yii\filters\AccessControl;

/**
 * This is the model class for table "adhibasen".
 *
 * @property int $id
 * @property int $name adhibasen name, 
 * @property string $year nepali year 
 * @property string $start_date_time
 * @property string $end_date_time
 * @property int $created_by
 * @property string $created_date
 */
class Adhibasen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adhibasen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'year', 'start_date_time'], 'required'],
            [['created_by','status'], 'integer'],
            [['name', 'start_date_time', 'end_date_time', 'created_date'], 'safe'],
            [['year'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'year' => 'Year',
            'start_date_time' => 'Start Date Time',
            'end_date_time' => 'End Date Time',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
        ];
    }
}
