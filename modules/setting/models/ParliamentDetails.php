<?php

namespace app\modules\setting\models;

use Yii;
use yii\filters\AccessControl;

/**
 * This is the model class for table "parliament_details".
 *
 * @property int $id
 * @property string $name
 * @property int $parliament_total_member
 * @property string $start_session
 * @property string $end_session
 * @property string $start_session_nepali_date
 * @property string $end_session_nepali_date
 * @property string $extented_time
 * @property string $description
 * @property string $status
 * @property int $created_by
 * @property string $created_ip
 * @property string $created_date
 *
 * @property CommitteeDetails[] $committeeDetails
 * @property CommitteeMemberDetails[] $committeeMemberDetails
 * @property ParliamentMember[] $parliamentMembers
 * @property ParliamentPartyOfficial[] $parliamentPartyOfficials
 */
class ParliamentDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parliament_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['parliament_total_member', 'created_by'], 'integer'],
            [['start_session', 'end_session', 'created_date'], 'safe'],
            [['description', 'status'], 'string'],
            [['name'], 'string', 'max' => 250],
            [['start_session_nepali_date', 'end_session_nepali_date'], 'string', 'max' => 10],
            [['extented_time'], 'string', 'max' => 200],
            [['created_ip'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'parliament_total_member' => 'Parliament Total Member',
            'start_session' => 'Start Session',
            'end_session' => 'End Session',
            'start_session_nepali_date' => 'Start Session Nepali Date',
            'end_session_nepali_date' => 'End Session Nepali Date',
            'extented_time' => 'Extented Time',
            'description' => 'Description',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'created_date' => 'Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommitteeDetails()
    {
        return $this->hasMany(CommitteeDetails::className(), ['parliament_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommitteeMemberDetails()
    {
        return $this->hasMany(CommitteeMemberDetails::className(), ['parliament_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParliamentMembers()
    {
        return $this->hasMany(ParliamentMember::className(), ['parliament_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParliamentPartyOfficials()
    {
        return $this->hasMany(ParliamentPartyOfficial::className(), ['parliament_id' => 'id']);
    }

    public function beforeSave($insert){
        if($this->isNewRecord){
            $this->created_ip = Yii::$app->request->getUserIP();
            $this->created_by = Yii::$app->user->id;
            $this->created_date = new \yii\db\Expression('NOW()');
        }
        return parent::beforeSave($insert);
    }
}
