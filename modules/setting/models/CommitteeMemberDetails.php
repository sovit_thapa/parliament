<?php

namespace app\modules\setting\models;

use Yii;
use yii\filters\AccessControl;
use app\components\UtilityFunctions;

/**
 * This is the model class for table "committee_member_details".
 *
 * @property int $id
 * @property int $parliament_id
 * @property int $committee_id
 * @property int $party_id
 * @property int $member_id
 * @property string $from_date
 * @property string $to_date
 * @property int $status
 * @property int $created_by
 * @property string $created_ip
 * @property string $created_date
 *
 * @property Committee $committee
 * @property ParliamentDetails $parliament
 * @property ParliamentMember $member
 * @property PoliticalParty $party
 */
class CommitteeMemberDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'committee_member_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parliament_id', 'committee_id', 'party_id', 'member_id'], 'required'],
            [['parliament_id', 'committee_id', 'party_id', 'member_id', 'status', 'created_by'], 'integer'],
            [['from_date', 'to_date', 'created_date'], 'safe'],
            [['created_ip'], 'string', 'max' => 200],
            [['committee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Committee::className(), 'targetAttribute' => ['committee_id' => 'id']],
            [['parliament_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParliamentDetails::className(), 'targetAttribute' => ['parliament_id' => 'id']],
            [['member_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParliamentMember::className(), 'targetAttribute' => ['member_id' => 'id']],
            [['party_id'], 'exist', 'skipOnError' => true, 'targetClass' => PoliticalParty::className(), 'targetAttribute' => ['party_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parliament_id' => 'Parliament',
            'committee_id' => 'Committee',
            'party_id' => 'Party',
            'member_id' => 'Member',
            'from_date' => 'From Date',
            'to_date' => 'To Date',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'created_date' => 'Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommittee()
    {
        return $this->hasOne(Committee::className(), ['id' => 'committee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParliament()
    {
        return $this->hasOne(ParliamentDetails::className(), ['id' => 'parliament_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(ParliamentMember::className(), ['id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParty()
    {
        return $this->hasOne(PoliticalParty::className(), ['id' => 'party_id']);
    }

    
     public function beforeSave($insert){
        if($this->isNewRecord){
            $this->created_ip = Yii::$app->getRequest()->getUserIP();
            $this->created_by = Yii::$app->user->id;
            $this->created_date = new \yii\db\Expression('NOW()');
        }
        $explode_from = explode('-', $this->from_date);
        $explode_to = explode('-', $this->to_date);
        $this->from_date = UtilityFunctions::NepaliToEnglish((int) $explode_from[0], (int) $explode_from[1], (int) $explode_from[2]);
        $this->to_date = UtilityFunctions::NepaliToEnglish((int) $explode_to[0], (int) $explode_to[1], (int) $explode_to[2]);
        return parent::beforeSave($insert);
    }
}
