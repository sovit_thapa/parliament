<?php

namespace app\modules\setting\models;

use Yii;
use yii\filters\AccessControl;

/**
 * This is the model class for table "ministry_related_committee".
 *
 * @property int $id
 * @property int $parliament_id
 * @property string $ministry
 * @property string $committee
 * @property string $applied_from
 * @property int $created_by
 * @property string $created_date
 */
class MinistryRelatedCommittee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ministry_related_committee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parliament_id', 'ministry', 'committee'], 'required'],
            [['parliament_id', 'created_by','_status'], 'integer'],
            [['applied_from', 'created_date'], 'safe'],
            [['ministry', 'committee'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parliament_id' => 'Parliament ID',
            'ministry' => 'Ministry',
            'committee' => 'Committee',
            '_status' => 'Status',
            'applied_from' => 'Applied From',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
        ];
    }
}
