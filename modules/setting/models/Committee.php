<?php

namespace app\modules\setting\models;

use Yii;
use yii\filters\AccessControl;

/**
 * This is the model class for table "committee".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $created_by
 * @property string $create_ip
 * @property string $created_date
 *
 * @property CommitteeDetails[] $committeeDetails
 * @property CommitteeMemberDetails[] $committeeMemberDetails
 */
class Committee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'committee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['created_by'], 'integer'],
            [['created_date'], 'safe'],
            [['name'], 'string', 'max' => 250],
            [['create_ip'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'created_by' => 'Created By',
            'create_ip' => 'Create Ip',
            'created_date' => 'Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommitteeDetails()
    {
        return $this->hasMany(CommitteeDetails::className(), ['committee_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommitteeMemberDetails()
    {
        return $this->hasMany(CommitteeMemberDetails::className(), ['committee_id' => 'id']);
    }
}
