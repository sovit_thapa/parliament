<?php

namespace app\modules\setting\models;

use Yii;
use yii\filters\AccessControl;
use app\modules\setting\models\Ethinicity;
use app\modules\setting\models\Location;
use app\modules\setting\models\ParliamentDetails;
use app\modules\setting\models\PoliticalParty;

/**
 * This is the model class for table "parliament_member".
 *
 * @property int $id
 * @property int $parliament_id
 * @property int $political_id
 * @property string $member_title
 * @property string $first_name
 * @property string $last_name
 * @property int $district_id
 * @property int $elected_area_number
 * @property int $ethinicity_id
 * @property string $elected_type
 * @property string $date_of_birth
 * @property int $dob_nepali_day
 * @property int $dob_nepali_month
 * @property int $dob_nepali_year
 * @property string $sex
 * @property string $rural_area
 * @property string $oath_of_parliament_member
 * @property int $oath_nepali_day
 * @property int $oath_nepali_month
 * @property int $oath_nepali_year
 * @property string $parliament_status
 * @property string $status
 * @property int $created_by
 * @property int $created_date
 * @property string $created_ip
 *
 * @property CommitteeMemberDetails[] $committeeMemberDetails
 * @property EthinicCast $ethinicity
 * @property Location $district
 * @property ParliamentDetails $parliament
 * @property PoliticalParty $political
 * @property ParliamentMemberStatus[] $parliamentMemberStatuses
 * @property ParliamentPartyOfficial[] $parliamentPartyOfficials
 */
class ParliamentMember extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parliament_member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parliament_id', 'political_id', 'member_title', 'first_name', 'last_name', 'district_id', 'ethinicity_id'], 'required'],
            [['parliament_id', 'political_id', 'district_id', 'elected_area_number', 'ethinicity_id', 'dob_nepali_day', 'dob_nepali_month', 'dob_nepali_year', 'oath_nepali_day', 'oath_nepali_month', 'oath_nepali_year', 'created_by', 'created_date'], 'integer'],
            [['elected_type', 'sex', 'rural_area', 'parliament_status', 'status'], 'string'],
            [['date_of_birth', 'oath_of_parliament_member'], 'safe'],
            [['member_title'], 'string', 'max' => 20],
            [['first_name', 'last_name'], 'string', 'max' => 250],
            [['created_ip'], 'string', 'max' => 100],
            [['ethinicity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ethinicity::className(), 'targetAttribute' => ['ethinicity_id' => 'id']],
            [['district_id'], 'exist', 'skipOnError' => true, 'targetClass' => Location::className(), 'targetAttribute' => ['district_id' => 'id']],
            [['parliament_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParliamentDetails::className(), 'targetAttribute' => ['parliament_id' => 'id']],
            [['political_id'], 'exist', 'skipOnError' => true, 'targetClass' => PoliticalParty::className(), 'targetAttribute' => ['political_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parliament_id' => 'संसद',
            'political_id' => 'राजनीति पार्टी ',
            'member_title' => 'मानर्थ',
            'first_name' => 'नाम',
            'last_name' => 'थर',
            'district_id' => 'जिल्ला',
            'elected_area_number' => 'निर्वाचन क्षेत्र ',
            'ethinicity_id' => 'समुह',
            'elected_type' => 'निर्वाचन',
            'date_of_birth' => 'जन्म मिती ( A.D ) ',
            'dob_nepali_day' => 'Dob Nepali Day',
            'dob_nepali_month' => 'Dob Nepali Month',
            'dob_nepali_year' => 'Dob Nepali Year',
            'sex' => 'लिङ्ग',
            'rural_area' => 'पिछडिएको क्षेत्र',
            'oath_of_parliament_member' => 'शपथ ग्रहण मिती (A.D)',
            'oath_nepali_day' => 'Oath Nepali Day',
            'oath_nepali_month' => 'Oath Nepali Month',
            'oath_nepali_year' => 'Oath Nepali Year',
            'parliament_status' => 'Parliament Status',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'created_ip' => 'Created Ip',
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommitteeMemberDetails()
    {
        return $this->hasMany(CommitteeMemberDetails::className(), ['member_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEthinicity()
    {
        return $this->hasOne(Ethinicity::className(), ['id' => 'ethinicity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(Location::className(), ['id' => 'district_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParliament()
    {
        return $this->hasOne(ParliamentDetails::className(), ['id' => 'parliament_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolitical()
    {
        return $this->hasOne(PoliticalParty::className(), ['id' => 'political_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParliamentMemberStatuses()
    {
        return $this->hasMany(ParliamentMemberStatus::className(), ['member_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParliamentPartyOfficials()
    {
        return $this->hasMany(ParliamentPartyOfficial::className(), ['parliament_member_id' => 'id']);
    }

     public function beforeSave($insert){
        if($this->isNewRecord){
            $this->created_ip = Yii::$app->getRequest()->getUserIP();
            $this->status = 'सकृय';
            $this->created_by = Yii::$app->user->id;
            $this->created_date = new \yii\db\Expression('NOW()');
        }
        return parent::beforeSave($insert);
    }

}
