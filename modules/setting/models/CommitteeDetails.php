<?php

namespace app\modules\setting\models;

use Yii;
use yii\filters\AccessControl;
use app\modules\setting\models\Committee;
use app\modules\setting\models\ParliamentDetails;
use app\modules\setting\models\PoliticalParty;

/**
 * This is the model class for table "committee_details".
 *
 * @property int $id
 * @property int $parliament_id
 * @property int $party_id
 * @property int $committee_id
 * @property int $number
 * @property int $status
 * @property int $created_by
 * @property string $created_ip
 * @property string $created_date
 *
 * @property Committee $committee
 * @property ParliamentDetails $parliament
 * @property PoliticalParty $party
 */
class CommitteeDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'committee_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parliament_id', 'party_id', 'committee_id', 'number'], 'required'],
            [['parliament_id', 'party_id', 'committee_id', 'number', 'status', 'created_by'], 'integer'],
            [['created_date'], 'safe'],
            [['created_ip'], 'string', 'max' => 100],
            [['committee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Committee::className(), 'targetAttribute' => ['committee_id' => 'id']],
            [['parliament_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParliamentDetails::className(), 'targetAttribute' => ['parliament_id' => 'id']],
            [['party_id'], 'exist', 'skipOnError' => true, 'targetClass' => PoliticalParty::className(), 'targetAttribute' => ['party_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parliament_id' => 'Parliament',
            'party_id' => 'Party',
            'committee_id' => 'Committee',
            'number' => 'Number',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'created_date' => 'Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommittee()
    {
        return $this->hasOne(Committee::className(), ['id' => 'committee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParliament()
    {
        return $this->hasOne(ParliamentDetails::className(), ['id' => 'parliament_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParty()
    {
        return $this->hasOne(PoliticalParty::className(), ['id' => 'party_id']);
    }
    

    public function beforeSave($insert){
        if($this->isNewRecord){
            $this->created_ip = Yii::$app->getRequest()->getUserIP();
            $this->created_by = Yii::$app->user->id;
            $this->created_date = new \yii\db\Expression('NOW()');
        }
        return parent::beforeSave($insert);
    }

}
