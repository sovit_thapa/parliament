<?php

namespace app\modules\setting\models;

use Yii;
use yii\filters\AccessControl;
use app\components\UtilityFunctions;

/**
 * This is the model class for table "political_party".
 *
 * @property int $id
 * @property string $name
 * @property int $party_establish_date
 * @property int $party_register_date register date in parliament
 * @property string $description
 * @property int $created_by
 * @property string $create_ip
 * @property string $created_date
 *
 * @property CommitteeDetails[] $committeeDetails
 * @property CommitteeMemberDetails[] $committeeMemberDetails
 * @property ParliamentMember[] $parliamentMembers
 * @property ParliamentPartyOfficial[] $parliamentPartyOfficials
 */
class PoliticalParty extends \yii\db\ActiveRecord
{
    public $totalMemberCount=null;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'political_party';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_by', 'status'], 'integer'],
            [['description','party_establish_date', 'party_register_date'], 'string'],
            [['created_date'], 'safe'],
            [['name'], 'string', 'max' => 250],
            [['create_ip'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'राजनैतिक पार्टी',
            'party_establish_date' => 'Party Establish Date',
            'party_register_date' => 'Party Register Date',
            'description' => 'Description',
            'status' => 'Status',
            'created_by' => 'Created By',
            'create_ip' => 'Create Ip',
            'created_date' => 'Created Date',
            'pratakshya'=>'प्रत्यक्ष',
            'samanupatik'=>'समानुपातिक',
            'manonit'=>'मनोनित',
            'totalMemberCount'=>'जम्मा सिट संख्या',

        ];
    }


    public function beforeSave($insert){
        if($this->isNewRecord){
            $this->created_date = Yii::$app->getRequest()->getUserIP();
            $this->created_by = Yii::$app->user->id;
            $this->status = 1;
            $this->created_date = new \yii\db\Expression('NOW()');
        }
        $explode_establish = explode('-', $this->party_establish_date);
        $explode_register = explode('-', $this->party_register_date);
        $this->party_establish_date = UtilityFunctions::NepaliToEnglish((int) $explode_establish[0], (int) $explode_establish[1], (int) $explode_establish[2]);
        $this->party_register_date = UtilityFunctions::NepaliToEnglish((int) $explode_register[0], (int) $explode_register[1], (int) $explode_register[2]);
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommitteeDetails()
    {
        return $this->hasMany(CommitteeDetails::className(), ['party_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommitteeMemberDetails()
    {
        return $this->hasMany(CommitteeMemberDetails::className(), ['party_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParliamentMembers()
    {
        return $this->hasMany(ParliamentMember::className(), ['political_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParliamentPartyOfficials()
    {
        return $this->hasMany(ParliamentPartyOfficial::className(), ['political_party_id' => 'id']);
    }

    /** Member COunt by type */
    public function getPratashyaMember()
    {
        return ParliamentMember::find()->where('political_id=:political_id AND elected_type=:type',[':political_id'=>$this->id,':type'=>'प्रत्यक्ष-निर्वाचन'])->count();
    }
    public function getSamanupatikMember()
    {
        return ParliamentMember::find()->where('political_id=:political_id AND elected_type=:type',[':political_id'=>$this->id,':type'=>'समानुपातिक'])->count();
    }
    public function getManonitMember()
    {
        return ParliamentMember::find()->where('political_id=:political_id AND elected_type=:type',[':political_id'=>$this->id,':type'=>'मनोनित'])->count();
    }
    public function getUpanirbachanMember()
    {
        return ParliamentMember::find()->where('political_id=:political_id AND elected_type=:type',[':political_id'=>$this->id,':type'=>'उपनिर्वाचन'])->count();
    }
     public function getOtherMember()
    {
        return ParliamentMember::find()->where('political_id=:political_id AND elected_type=:type',[':political_id'=>$this->id,':type'=>'अन्य'])->count();
    }

    public function getTotalMember()
    {
        return $this->PratashyaMember + $this->SamanupatikMember;
    }

     public function getFinalTotalMember()
    {
        return $this->PratashyaMember + $this->SamanupatikMember + $this->ManonitMember;
    }
    /** END:Member Count functions */


    public function committeeMembersCount($committee_id)
    {
        return CommitteeMemberDetails::find()->where('party_id=:party AND committee_id=:committee_id',[':party'=>$this->id,':committee_id'=>$committee_id])->count();
    }

    public function ministersCount()
    {
        return ParliamentPartyOfficial::find()->where('political_party=:party AND (post=:post OR post=:post2)',[':party'=>$this->name,':post'=>'मन्त्री',':post2'=>'प्रधान मन्त्री'])->count();
    }

    public function sortByMemberCount($model,$attribute)
    {
        foreach ($model as $key => $value) 
        {
            $value->totalMemberCount=$value->finalTotalMember;
           
        }


        //Selection Sorting Method
        $oldArray=$model;
        foreach ($oldArray as $key => $value) 
        {
             $index = $key;
           for ($j = $key+1; $j < sizeof($oldArray); $j++) 
           {
                if ($oldArray[$j]->$attribute > $oldArray[$index]->$attribute)
                    $index = $j;
           }
      
            $smallerNumber = $oldArray[$index]; 
            $oldArray[$index] = $oldArray[$key];
            $oldArray[$key] = $smallerNumber;

        }
        return $oldArray;
    }

}
