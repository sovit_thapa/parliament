<?php

namespace app\modules\setting\models;

use Yii;
use yii\filters\AccessControl;
use app\components\UtilityFunctions;
use app\modules\setting\models\ParliamentDetails;
use app\modules\setting\models\OfficialPost;
use app\modules\setting\models\PoliticalParty;
/**
 * This is the model class for table "parliament_party_official".
 *
 * @property int $id
 * @property int $parliament_id
 * @property int $political_party_id
 * @property int $parliament_member_id
 * @property int $post_id
 * @property string $start_from
 * @property string $to_date
 * @property string $letter_received_date
 * @property string $status
 * @property int $created_by
 * @property string $created_ip
 * @property string $created_date
 *
 * @property OfficialPost $post
 * @property ParliamentMember $parliamentMember
 * @property ParliamentDetails $parliament
 * @property PoliticalParty $politicalParty
 */
class ParliamentPartyOfficial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parliament_party_official';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parliament_id', 'political_party', 'ministry', 'post', 'parliament_member_id'], 'required'],
            [['parliament_id', 'parliament_member_id', 'created_by','_order'], 'integer'],
            [['start_from', 'to_date', 'letter_received_date', 'created_date'], 'safe'],
            [['status'], 'string'],
            [['created_ip'], 'string', 'max' => 50],
            [['parliament_member_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParliamentMember::className(), 'targetAttribute' => ['parliament_member_id' => 'id']],
            [['parliament_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParliamentDetails::className(), 'targetAttribute' => ['parliament_id' => 'id']],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ministry' => 'Ministry',
            'parliament_id' => 'Parliament',
            'political_party' => 'Political Party',
            'parliament_member_id' => 'Parliament Member',
            'post' => 'Post',
            'start_from' => 'Start From',
            'to_date' => 'To Date',
            'letter_received_date' => 'Letter Received Date',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'created_date' => 'Created Date',
            '_order'=>'Hierachy'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParliamentMember()
    {
        return $this->hasOne(ParliamentMember::className(), ['id' => 'parliament_member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParliament()
    {
        return $this->hasOne(ParliamentDetails::className(), ['id' => 'parliament_id']);
    }


    public function beforeSave($insert){
        if($this->isNewRecord){
            $this->created_ip = Yii::$app->getRequest()->getUserIP();
            $this->created_by = Yii::$app->user->id;
            $this->created_date = new \yii\db\Expression('NOW()');
        }
        return parent::beforeSave($insert);
    }


    private function englishDateConversion($date)
    {
        $date_divide = explode('-', $date);
        return Yii::$app->UtilityFunctions->NepaliToEnglish($date_divide[0],$date_divide[1],$date_divide[2]);
    }
}
