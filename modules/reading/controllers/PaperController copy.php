<?php

namespace app\modules\reading\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\modules\program\models\ReadingPaper;
use app\modules\program\models\DailyPaper;
use app\modules\program\models\DartaDesk;
use app\modules\program\models\MeetingDetails;
use app\modules\program\models\MeetingHalt;
use app\modules\program\models\ReadingPaperContent;
use app\modules\setting\models\ParliamentMember;
use app\modules\program\models\ParticipateMemberDiscussion;
use app\modules\program\models\ProcessingDetails;
use app\modules\program\models\Statement;

use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\UtilityFunctions;

/**
 * Default controller for the `reading` module
 */
class PaperController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


        /**
    *readung paper view section
    */
    public function actionPages(){
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $model = new ReadingPaperContent();
        $state = null;
        $daily_paper_detail = DailyPaper::findOne($id);
        $darta_desk = DartaDesk::findOne($daily_paper_detail->forum_id);
        $reading_paper_details = ReadingPaper::find()->where(['meeting_id'=>$daily_paper_detail->meeting_id,'daily_paper_id'=>$id,'paper_status'=>1])->one();
        if(empty($reading_paper_details) || empty($darta_desk))
            throw new NotFoundHttpException('Daily Paper Cannot make its reading paper, please contact admin.');
        Yii::$app->session->remove('_reading_type');
        Yii::$app->session->remove('_reading_id');
        Yii::$app->session['_reading_type'] = 'single';
        Yii::$app->session['_reading_id'] = $reading_paper_details->id;
        $meeting_detail = MeetingDetails::find()->where(['id'=>$reading_paper_details->meeting_id,'status'=>'सुचारू'])->one();
        $participate_member_discussion = ParticipateMemberDiscussion::find()->where('parliament_id=:parliament_id AND meeting_id=:meeting_id AND type=:type AND reading_paper_id=:reading_paper_id AND status=:status order by speech_order ASC',[':parliament_id'=>UtilityFunctions::SambhidhanSava(),':meeting_id'=>UtilityFunctions::ActiveMeeting(),':type'=>'on_topic',':reading_paper_id'=>$reading_paper_details->id, ':status'=>1])->all();
        $state = $reading_paper_details->state;
        if($reading_paper_details->meeting_header == 'व्यवस्थापन कार्य'){
            if($state == 'अनुमति माग्ने')
                $all_pages = ReadingPaper::aumati_magne;
            if($state == 'विचार गरियोस्')
                if(!empty($participate_member_discussion))
                    $all_pages = ReadingPaper::bichar_gariyos_general;
                else
                    $all_pages = ReadingPaper::bichar_gariyos_no_discuss;
                $result_pages = ReadingPaper::bichar_gariyos_result;
            }

            $participate_member = array();
            if(!empty($participate_member_discussion)){
                foreach ($participate_member_discussion as $prt_member) {
                    $participate_member[] = $prt_member->parliament_member_id;
                }
            }
            $firt_mem_id = isset($participate_member[0]) ? $participate_member[0] : null;
            $_discuss_member = isset($participate_member[1]) ? $participate_member[1] : null;
            $parliament_member = ParliamentMember::findOne($firt_mem_id);
           // $all_pages = array_merge($page_only, $result_pages);
            $first_member = $parliament_member ?  $parliament_member->member_title.' '.$parliament_member->first_name.' '.$parliament_member->last_name: null;
            $paper = isset($_GET['paper']) ? $_GET['paper'] : '_first_content';
            $index = array_search($paper,$all_pages);
            $next_page = isset($all_pages[$index + 1]) ? $all_pages[$index + 1] : null;
            $prv_page = isset($all_pages[$index - 1]) ? $all_pages[$index - 1] : null;
            if(in_array($paper, $result_pages)){
                $sizeof = sizeof($all_pages);
                $prv_page = isset($all_pages[$sizeof - 1]) ? $all_pages[$sizeof - 1] : null;
                $next_page = null;
            }
            $member_id = $next_member = $prv_member = null;
            if($paper == '_discuss_body'){
                $member_id = isset($_GET['participate_member']) ? $_GET['participate_member'] : $_discuss_member;
                $phase_index = array_search($member_id, $participate_member);
                $next_member = isset($participate_member[$phase_index+1]) ? $participate_member[$phase_index+1] : null;
                $prv_member = isset($participate_member[$phase_index-1]) && $phase_index > 1 ? $participate_member[$phase_index-1] : null;
                if($prv_member)
                    $prv_page ='_discuss_body';
                else
                    $prv_page ='_discuss';
                if($next_member)
                    $next_page = '_discuss_body';
                else
                    $next_page = '_discuss_reply';
            }
            $error_array = array();
            $transaction = Yii::$app->db->beginTransaction();
            $parliament_member_details = ParliamentMember::findOne($member_id);
            $member_name = $parliament_member_details ?  $parliament_member_details->member_title.' '.$parliament_member_details->first_name.' '.$parliament_member_details->last_name: null;

            $reading_paper_content = ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND pages=:pages',[':reading_paper_id'=>$reading_paper_details->id, ':pages'=>$paper])->one();
            $update_reading_paper_status = ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND pages=:pages',[':reading_paper_id'=>$reading_paper_details->id, ':pages'=>$prv_page])->one();
            if($update_reading_paper_status){
                $update_reading_paper_status->is_read = 1;
                $update_reading_paper_status->read_by = Yii::$app->user->id;
                $update_reading_paper_status->read_date = new \yii\db\Expression('NOW()');
                if(!$update_reading_paper_status->save())
                    $error_array[] = 'false';
            }

            if($reading_paper_details->state == 'अनुमति माग्ने'){
                if(!$next_page){
                    $darta_desk->state = 'अनुमति माग्ने';
                    $darta_desk->state_status = 'स्वीकृत';
                    $darta_desk->check_state = 'विचार गरियोस्';
                    $darta_desk->is_generate_dp = 0;
                    if(!$darta_desk->save(false))
                        $error_array[] = 'false';
                    $darta_processing = new ProcessingDetails();
                    $darta_processing->attributes = $darta_desk->attributes;

                    $darta_processing->darta_id = $darta_desk->id;
                    $darta_processing->_result = 'स्वीकृत';
                    $darta_processing->date = new \yii\db\Expression('NOW()');
                    if(!$darta_processing->save())
                        $error_array[] = 'false';
                    $reading_paper_details->is_read = 1;
                    $reading_paper_details->read_at = new \yii\db\Expression('NOW()');
                    $reading_paper_details->status = 'सर्बसम्ती स्वीकृत';
                    if(!$reading_paper_details->update(false))
                        $error_array[] = 'false';

                    $update_reading_paper_status_last = ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND pages=:pages',[':reading_paper_id'=>$reading_paper_details->id, ':pages'=>$paper])->one();
                    if($update_reading_paper_status_last){
                        $update_reading_paper_status_last->is_read = 1;
                        $update_reading_paper_status_last->read_by = Yii::$app->user->id;
                        $update_reading_paper_status_last->read_date = new \yii\db\Expression('NOW()');
                        if(!$update_reading_paper_status_last->save())
                            $error_array[] = 'false';
                    }
                }
            }
            if($reading_paper_details->state == 'विचार गरियोस्'){
                if(in_array($paper, $result_pages)){
                    $state_status = in_array($paper, ['_result_yes','_result_overall_yes']) ? 'स्वीकृत' : 'अस्वीकृत';
                    $index_ = array_search($paper, $result_pages);
                    $paper_sec = ReadingPaper::result_nepali;
                    $nepali_result = $paper_sec && isset($paper_sec[$index_]) ? $paper_sec[$index_] : 'null';
                    $darta_desk->state = 'विचार गरियोस्';
                    $darta_desk->state_status = $state_status;
                    $darta_desk->check_state = $state_status == 'स्वीकृत' ? 'संशोधनको म्याद' : 'अस्वीकृत';
                    $darta_desk->is_generate_dp = 0;
                    if(!$darta_desk->update())
                        $error_array[] = 'false';
                    $darta_processing = new ProcessingDetails();
                    $darta_processing->attributes = $darta_desk->attributes;
                    $darta_processing->darta_id = $darta_desk->id;
                    $darta_processing->_result = $state_status;
                    $darta_processing->date = new \yii\db\Expression('NOW()');
                    if(!$darta_processing->save())
                        $error_array[] = 'false';
                    if($state_status == 'स्वीकृत'){
                        for ($i=1; $i <= 3 ; $i++) { 
                            $date = date('Y-m-d', strtotime("+".$i." days"));
                            $darta_processing_ = new ProcessingDetails();
                            $darta_processing_->attributes = $darta_desk->attributes;
                            $darta_processing_->darta_id = $darta_desk->id;
                            $darta_processing_->_result = $state_status;
                            $darta_processing_->state = 'संशोधनको म्याद';
                            $darta_processing_->state_status = 'प्रकृया';
                            $darta_processing_->date = $date;
                            if(!$darta_processing_->save())
                                $error_array[] = 'false';
                        }
                    }

                    $reading_paper_details->is_read = 1;
                    $reading_paper_details->read_at = new \yii\db\Expression('NOW()');
                    $reading_paper_details->status = $nepali_result;
                    if(!$reading_paper_details->update(false))
                        $error_array[] = 'false';

                    $update_reading_paper_status_last_ = ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND pages=:pages',[':reading_paper_id'=>$reading_paper_details->id, ':pages'=>$paper])->one();
                    if($update_reading_paper_status_last_){
                        $update_reading_paper_status_last_->is_read = 1;
                        $update_reading_paper_status_last_->read_by = Yii::$app->user->id;
                        $update_reading_paper_status_last_->read_date = new \yii\db\Expression('NOW()');
                        if(!$update_reading_paper_status_last_->save())
                            $error_array[] = 'false';
                    }
                }
            }
            if(!in_array('false', $error_array))
                $transaction->commit();
            else
                $transaction->rollback();
            return $this->render('_reading_paper',['meeting_detail'=>$meeting_detail,'model'=>$model, 'id'=>$id, 'next_page'=>$next_page, 'prv_page'=>$prv_page, 'first_member'=>$first_member, 'member_name'=>$member_name , 'next_member'=>$next_member, 'prv_member'=>$prv_member, 'member_id'=>$member_id,'reading_paper_content'=>$reading_paper_content,'paper'=>$paper]);
    }

    /**
    *re-arrange order of reading paper list section 
    */

    public function actionReArrangeOrder(){
        $message = isset($_GET['message']) ? $_GET['message'] : null;
        $reading_paper_list = ReadingPaper::find()->where('parliament_id=:parliament_id AND meeting_id=:meeting_id AND paper_status=:paper_status order by order_number'  ,[':parliament_id'=>UtilityFunctions::SambhidhanSava(),':meeting_id'=>UtilityFunctions::ActiveMeeting(),':paper_status'=>1])->all();
        $meeting_detail = MeetingDetails::findOne(UtilityFunctions::ActiveMeeting());
        if(empty($meeting_detail))
            throw new NotFoundHttpException('The requested page does not exist.');
        return $this->render('_re_order_reading_paper',['reading_paper_list'=>$reading_paper_list, 'meeting_detail'=>$meeting_detail, 'message'=>$message]);
    }


    public function actionRPReArrangeOrder(){
        $error_array = array();
        $transaction = Yii::$app->db->beginTransaction();
        if(isset($_POST['reading_paper_']) && sizeof($_POST['reading_paper_']) > 0){
            $reading_paper_array = $_POST['reading_paper_'];
            for ($i=0; $i < sizeof($reading_paper_array); $i++) { 
                $reading_paper_details = ReadingPaper::findOne($reading_paper_array[$i]);
                if($reading_paper_details){
                    $reading_paper_details->order_number = $i + 1;
                    if(!$reading_paper_details->save(false))
                        $error_array[] = 'false';
                }
            }
            if(!in_array('false', $error_array)){
                $transaction->commit();
                $result_message = 1;
            }else{
                $transaction->rollback();
                $result_message = 400;
            }
            return $this->redirect(['paper/re-arrange-order/?message='.$result_message]);
        }else
            return $this->redirect(['paper/re-arrange-order/?message=400']);

    }



    /**
    *re-arrange order of reading paper list section 
    */

    public function actionReArrangeDPOrder(){
        $message = isset($_GET['message']) ? $_GET['message'] : null;

        $daily_paper_details = DailyPaper::find()->where('meeting_id=:meeting_id AND status=:status GROUP BY CASE WHEN minister IS NULL THEN id ELSE minister END ',[':meeting_id'=>UtilityFunctions::ActiveMeeting(),':status'=>'active'])->orderBy('order_number ASC')->all();
        $meeting_detail = MeetingDetails::findOne(UtilityFunctions::ActiveMeeting());
        if(empty($meeting_detail))
            throw new NotFoundHttpException('The requested page does not exist.');
        return $this->render('_re_arrange_dp',['daily_paper_details'=>$daily_paper_details, 'meeting_detail'=>$meeting_detail, 'message'=>$message]);
    }


    public function actionDPReArrangeOrder(){
        $error_array = array();
        $transaction = Yii::$app->db->beginTransaction();
        if(isset($_POST['daily_paper_']) && sizeof($_POST['daily_paper_']) > 0){
            $daily_paper_array = $_POST['daily_paper_'];
            for ($i=0; $i < sizeof($daily_paper_array); $i++) { 
                $daily_paper_details = DailyPaper::findOne(['id' => $daily_paper_array[$i], 'status'=>['active','processing']]);
                if($daily_paper_details){
                    $daily_paper_details->order_number = $i + 1;
                    if(!$daily_paper_details->save(false))
                        $error_array[] = 'false';
                }
            }
            if(!in_array('false', $error_array)){
                $transaction->commit();
                $result_message = 1;
            }else{
                $transaction->rollback();
                $result_message = 400;
            }
            return $this->redirect(['paper/re-arrange-d-p-order/?message='.$result_message]);
        }else
            return $this->redirect(['paper/re-arrange-d-p-order/?message=400']);

    }


    public function actionRead(){
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        Yii::$app->session->remove('_reading_type');
        Yii::$app->session->remove('_reading_id');
        Yii::$app->session['_reading_type'] = 'multiple';
        Yii::$app->session['_reading_id'] = '';
        $model = new ReadingPaperContent();
        $reading_paper_list = ReadingPaper::find()->where('parliament_id=:parliament_id AND meeting_id=:meeting_id AND paper_status=:paper_status order by primary_order, order_number'  ,[':parliament_id'=>UtilityFunctions::SambhidhanSava(),':meeting_id'=>UtilityFunctions::ActiveMeeting(),':paper_status'=>1])->all();
        $meeting_detail = MeetingDetails::findOne(UtilityFunctions::ActiveMeeting());
        if(empty($meeting_detail))
            throw new NotFoundHttpException('The requested page does not exist.');
        if(!$id)
        return $this->render('_re_order_reading_paper',['reading_paper_list'=>$reading_paper_list, 'meeting_detail'=>$meeting_detail]);

        $reading_paper_details = ReadingPaper::findOne($id);
        if(empty($reading_paper_details))
            throw new NotFoundHttpException('Reading Paper Cannot make its reading paper, please contact admin.');
        $daily_paper_id = $reading_paper_details->daily_paper_id;
        $daily_paper_details = DailyPaper::findOne($daily_paper_id);
        if(!empty($daily_paper_details)){
        $darta_desk = DartaDesk::findOne($daily_paper_details->forum_id);
        if(empty($darta_desk))
            $darta_desk = new DartaDesk;
        }
        $meeting_detail = MeetingDetails::find()->where(['id'=>$reading_paper_details->meeting_id,'status'=>'सुचारू'])->one();
        if(in_array($reading_paper_details->meeting_header, ['शून्य समय','विशेष समय'])){
            $header_type = $reading_paper_details->meeting_header == 'शून्य समय' ? 'zero_time' : 'special_time';
            $participate_member_discussion = ParticipateMemberDiscussion::find()->where('meeting_id=:meeting_id AND type=:type AND reading_paper_id=:reading_paper_id AND status=:status order by speech_order ASC',[':meeting_id'=>UtilityFunctions::ActiveMeeting(),':type'=>$header_type,':reading_paper_id'=>$reading_paper_details->id, ':status'=>1])->all();
        }else{
            $participate_member_discussion = ParticipateMemberDiscussion::find()->where('parliament_id=:parliament_id AND meeting_id=:meeting_id AND type=:type AND reading_paper_id=:reading_paper_id AND status=:status order by speech_order ASC',[':parliament_id'=>UtilityFunctions::SambhidhanSava(),':meeting_id'=>UtilityFunctions::ActiveMeeting(),':type'=>'on_topic',':reading_paper_id'=>$reading_paper_details->id, ':status'=>1])->all();
        }
        $pages_information = UtilityFunctions::ReadingTitle($id);
        $all_pages = isset($pages_information['all_pages']) ? $pages_information['all_pages'] : array();
        $result_pages = isset($pages_information['result_pages']) ? $pages_information['result_pages'] : array();
        $participate_member = array();
        if(!empty($participate_member_discussion)){
            foreach ($participate_member_discussion as $prt_member) {
                $participate_member[] = $prt_member->parliament_member_id;
            }
        }
        $firt_mem_id = isset($participate_member[0]) ? $participate_member[0] : null;
        $_discuss_member = isset($participate_member[1]) ? $participate_member[1] : null;
        $parliament_member = ParliamentMember::findOne($firt_mem_id);
       // $all_pages = array_merge($page_only, $result_pages);
        $first_member = $parliament_member ?  $parliament_member->member_title.' '.$parliament_member->first_name.' '.$parliament_member->last_name: null;
        $paper = isset($_GET['paper']) ? $_GET['paper'] : '_first_content';
        $index = array_search($paper,$all_pages);
        $next_page = isset($all_pages[$index + 1]) ? $all_pages[$index + 1] : null;
        $prv_page = isset($all_pages[$index - 1]) ? $all_pages[$index - 1] : null;
        if(in_array($paper, $result_pages)){
            $sizeof = sizeof($all_pages);
            $prv_page = isset($all_pages[$sizeof - 1]) ? $all_pages[$sizeof - 1] : null;
            $next_page = null;
        }
        $member_id = $next_member = $prv_member = null;
        if($paper == '_discuss_body'){
            $member_id = isset($_GET['participate_member']) ? $_GET['participate_member'] : $_discuss_member;
            $phase_index = array_search($member_id, $participate_member);
            $next_member = isset($participate_member[$phase_index+1]) ? $participate_member[$phase_index+1] : null;
            $prv_member = isset($participate_member[$phase_index-1]) && $phase_index > 1 ? $participate_member[$phase_index-1] : null;
            if($prv_member)
                $prv_page ='_discuss_body';
            else
                $prv_page ='_discuss';
            if($next_member)
                $next_page = '_discuss_body';
            else{
                if(!in_array($reading_paper_details->meeting_header, ['शून्य समय','विशेष समय']))
                    $next_page = '_discuss_reply';
            }
        }
        $error_array = array();
        $transaction = Yii::$app->db->beginTransaction();
        $parliament_member_details = ParliamentMember::findOne($member_id);
        $member_name = $parliament_member_details ?  $parliament_member_details->member_title.' '.$parliament_member_details->first_name.' '.$parliament_member_details->last_name: null;
        
        $reading_paper_content = ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND pages=:pages',[':reading_paper_id'=>$reading_paper_details->id, ':pages'=>$paper])->one();
        $update_reading_paper_status = ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND pages=:pages',[':reading_paper_id'=>$reading_paper_details->id, ':pages'=>$prv_page])->one();
        if($update_reading_paper_status){
            $update_reading_paper_status->is_read = 1;
            $update_reading_paper_status->read_by = Yii::$app->user->id;
            $update_reading_paper_status->read_date = new \yii\db\Expression('NOW()');
            if(!$update_reading_paper_status->save())
                $error_array[] = 'false';
        }
        $state_section = UtilityFunctions::StateProcess($reading_paper_details->main_type);
        $index = array_search($reading_paper_details->state, $state_section);
        if(in_array($paper, ['_result_yes','_result_no','_result_overall_no','_result_overall_yes'])){
            if($paper=='_result_overall_yes'){
                $next_state = isset($state_section[$index+1]) ? $state_section[$index+1] : $reading_paper_details->state;
                $darta_status = 'स्वीकृत';
                $reading_paper_status = 'सर्बसम्ती स्वीकृत';
            }
            if($paper=='_result_yes'){
                $next_state = isset($state_section[$index+1]) ? $state_section[$index+1] : $reading_paper_details->state;
                $darta_status = 'स्वीकृत';
                $reading_paper_status = 'बहुमत स्वीकृत';
            }
            if($paper=='_result_overall_no'){
                $next_state = $reading_paper_details->state;
                $darta_status = 'अस्वीकृत';
                $reading_paper_status = 'सर्बसम्ती अस्वीकृति';
            }
            if($paper=='_result_no'){
                $next_state = $reading_paper_details->state;
                $darta_status = 'अस्वीकृत';
                $reading_paper_status = 'बहुमत अस्वीकृति';
            }

        }else{
            $next_state = isset($state_section[$index+1]) ? $state_section[$index+1] : $reading_paper_details->state;
            $darta_status = 'स्वीकृत';
            $reading_paper_status = 'सर्बसम्ती स्वीकृत';
        }
        if(isset($darta_desk) && !empty($darta_desk)){
            if(in_array($paper, $result_pages)|| !$next_page){
                $darta_desk->state = $reading_paper_details->state;
                $darta_desk->state_status = $darta_status;
                $darta_desk->check_state = $next_state;
                $darta_desk->is_generate_dp = 0;
                if(!$darta_desk->save(false))
                    $error_array[] = 'false';
                $darta_processing = new ProcessingDetails();
                $darta_processing->attributes = $darta_desk->attributes;

                $darta_processing->darta_id = $darta_desk->id;
                $darta_processing->_result = $darta_status;
                $darta_processing->date = new \yii\db\Expression('NOW()');
                if(!$darta_processing->save())
                    $error_array[] = 'false';
            }

        }
        $update_reading_paper_status_last_ = ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND pages=:pages',[':reading_paper_id'=>$reading_paper_details->id, ':pages'=>$paper])->one();
        if($update_reading_paper_status_last_){
            $update_reading_paper_status_last_->is_read = 1;
            $update_reading_paper_status_last_->read_by = Yii::$app->user->id;
            $update_reading_paper_status_last_->read_date = new \yii\db\Expression('NOW()');
            if(!$update_reading_paper_status_last_->save())
                $error_array[] = 'false';
        }
        if(!in_array('false', $error_array))
            $transaction->commit();
        else
            $transaction->rollback();
        return $this->render('_general_reading_paper',['meeting_detail'=>$meeting_detail,'model'=>$model, 'id'=>$id, 'next_page'=>$next_page, 'prv_page'=>$prv_page, 'first_member'=>$first_member, 'member_name'=>$member_name , 'next_member'=>$next_member, 'prv_member'=>$prv_member, 'member_id'=>$member_id,'reading_paper_content'=>$reading_paper_content,'paper'=>$paper,'reading_paper_details'=>$reading_paper_details]);
    }




    public function actionReadingSection(){
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $initial_page = !$id ? true : false;
        $statement_id = isset($_GET['statement_id']) ? $_GET['statement_id'] : null;
        $meeting_detail = MeetingDetails::find()->where(['status' => ['active','halt']])->one();
        if(empty($meeting_detail))
            throw new NotFoundHttpException('Meeting is not set, so please check it.');
        $model = new ReadingPaperContent();
        $reading_paper_list = ReadingPaper::find()->where('parliament_id=:parliament_id AND meeting_id=:meeting_id AND paper_status=:paper_status order by primary_order, order_number'  ,[':parliament_id'=>UtilityFunctions::SambhidhanSava(),':meeting_id'=>UtilityFunctions::ActiveMeeting(),':paper_status'=>1])->all();
        $reading_paper_id = $statement_id_array = array();
        if(!empty($reading_paper_list)){
            foreach ($reading_paper_list as $reading_paper_inf) {
                $reading_paper_id[] = $reading_paper_inf->id;
            }
        }
        if(!$id){
            $id = !$id && isset($reading_paper_id[0]) && $reading_paper_id[0] ? $reading_paper_id[0] : $id;
            $halt_meeting_details = MeetingHalt::find()->where(['meeting_id'=>UtilityFunctions::ActiveMeeting(),'status'=>1])->one();
            if(!empty($halt_meeting_details)){
              $first_unread_paper = ReadingPaper::find()->where('parliament_id=:parliament_id AND meeting_id=:meeting_id AND paper_status=:paper_status AND is_read=:is_read '  ,[':parliament_id'=>UtilityFunctions::SambhidhanSava(),':meeting_id'=>UtilityFunctions::ActiveMeeting(),':paper_status'=>1,':is_read'=>0])->orderBy(['primary_order' => SORT_ASC, 'order_number'=>SORT_ASC])->one();
                if(!empty($first_unread_paper)){
                    $reading_paper_con_details = ReadingPaperContent::find()->where(['reading_paper_id'=>$first_unread_paper->id,'content_status'=>1, 'pages'=>'_first_content'])->one();

                }
                $halt_meeting_details->status = 0;
                $halt_meeting_details->save(false);
            }else{
                $reading_paper_con_details = ReadingPaperContent::find()->where(['reading_paper_id'=>$id,'content_status'=>1, 'pages'=>'_first_content'])->one();
            }
            if(empty($reading_paper_con_details))
                throw new NotFoundHttpException('Reading Paper is already complete.');
            return $this->render('_first_reading_paper',['reading_paper_details'=>$reading_paper_con_details,'halt_meeting_details'=>$halt_meeting_details]);
        }
        $id = !$id && isset($reading_paper_id[0]) && $reading_paper_id[0] ? $reading_paper_id[0] : $id;
        $reading_index = array_search($id, $reading_paper_id);
        $next_reading_paper = isset($reading_paper_id[$reading_index+1]) ? $reading_paper_id[$reading_index+1] : null;
        $prv_reading_paper = isset($reading_paper_id[$reading_index-1]) ? $reading_paper_id[$reading_index-1] : null;

        $prv_id = $next_id = $id;
        $reading_paper_details = ReadingPaper::findOne($id);
        if(empty($reading_paper_details))
            throw new NotFoundHttpException('Reading Paper is blank.');
        $daily_paper_id = $reading_paper_details->daily_paper_id;
        $daily_paper_details = DailyPaper::findOne($daily_paper_id);
        if(!empty($daily_paper_details)){
        $darta_desk = DartaDesk::findOne($daily_paper_details->forum_id);
        if(empty($darta_desk))
            $darta_desk = new DartaDesk;
        }
        $meeting_detail = MeetingDetails::find()->where(['id'=>$reading_paper_details->meeting_id,'status'=>'सुचारू'])->one();
        if(in_array($reading_paper_details->meeting_header, ['शून्य समय','विशेष समय'])){
            $header_type = $reading_paper_details->meeting_header == 'शून्य समय' ? 'zero_time' : 'special_time';
            $participate_member_discussion = ParticipateMemberDiscussion::find()->where('meeting_id=:meeting_id AND type=:type AND reading_paper_id=:reading_paper_id AND status=:status order by speech_order ASC',[':meeting_id'=>UtilityFunctions::ActiveMeeting(),':type'=>$header_type,':reading_paper_id'=>$reading_paper_details->id, ':status'=>1])->all();
        }else{
            $participate_member_discussion = ParticipateMemberDiscussion::find()->where('parliament_id=:parliament_id AND meeting_id=:meeting_id AND type=:type AND reading_paper_id=:reading_paper_id AND status=:status order by speech_order ASC',[':parliament_id'=>UtilityFunctions::SambhidhanSava(),':meeting_id'=>UtilityFunctions::ActiveMeeting(),':type'=>'on_topic',':reading_paper_id'=>$reading_paper_details->id, ':status'=>1])->all();
        }


        $statement_information = Statement::find()->where(['status'=>1, 'reading_paper_id'=>$id])->orderBy(['statement_order'=>SORT_ASC])->all();
        if(!empty($statement_information)){
            foreach ($statement_information as $statement_) {
                $statement_id_array[] = $statement_->id;
            }
        }
        $statement_id = !empty($statement_information) ? $statement_id : null;
        $paper = isset($_GET['paper']) && $_GET['paper'] != '' ? $_GET['paper'] : '_first_content';
        $page_detail_information = UtilityFunctions::NextPageInformation($id, $statement_id, UtilityFunctions::ActiveMeeting(), $paper);
        $prv_rp_id = $page_detail_information['prv_rp_id'];
        $next_rp_id = $page_detail_information['next_rp_id'];
        $prv_page = $page_detail_information['prv_page'];
        $next_page = $page_detail_information['next_page'];
        $prv_statement_id = $page_detail_information['prv_statement_id'];
        $next_statement_id = $page_detail_information['next_statement_id'];
        $reading_paper_end = $page_detail_information['reading_paper_end'];
        $statemet_end = $page_detail_information['statemet_end'];
        if($reading_paper_details->main_type == 'ordinance' && $reading_paper_details->state == 'छलफल' && !$statement_id){
            $reading_conentent_informtion = ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND content_status=:content_status AND statement_id IS NULL',[':reading_paper_id'=>$reading_paper_details->id, ':content_status'=>1])->orderBy(['order_'=>SORT_ASC])->all();
        }else{

            if(empty($statement_information)){
                $reading_conentent_informtion = ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND content_status=:content_status AND statement_id IS NULL',[':reading_paper_id'=>$reading_paper_details->id, ':content_status'=>1])->orderBy(['order_'=>SORT_ASC])->all();
            }
            else{
                $initial_statement_id = isset($statement_id_array[0]) ? $statement_id_array[0] : null;
                $statement_id = !$statement_id ? $initial_statement_id : $statement_id;
                $reading_conentent_informtion = ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND content_status=:content_status AND statement_id=:statement_id',[':reading_paper_id'=>$reading_paper_details->id, ':content_status'=>1, ':statement_id'=>$statement_id])->orderBy(['order_'=>SORT_ASC])->all();
            }
        }

        $participate_member = $supporter_member = array();
        if(!empty($participate_member_discussion)){
            foreach ($participate_member_discussion as $prt_member) {
                $participate_member[] = $prt_member->parliament_member_id;
            }
        }
        if(isset($darta_desk) && !empty($darta_desk) && $reading_paper_details->main_type =='proposal'){
            $supporter_member = explode(',', $darta_desk->suppoter);
        }

        $firt_mem_id = isset($participate_member[0]) ? $participate_member[0] : null;
        $_discuss_member = isset($participate_member[1]) ? $participate_member[1] : null;
        $parliament_member = ParliamentMember::findOne($firt_mem_id);
        $first_member = $parliament_member ?  $parliament_member->member_title.' '.$parliament_member->first_name.' '.$parliament_member->last_name: null;
        $member_id = $next_member = $prv_member = null;
        if($paper == '_first_content' && $reading_paper_details->state == 'विचार गरियोस्'){
            if(sizeof($participate_member) > 0){
                $next_page = '_discuss';
            }
            else
                $next_page = '_decision_no_discuss';
        }
        if($paper == '_discuss_body'){
            $member_id = isset($_GET['participate_member']) ? $_GET['participate_member'] : $_discuss_member;
            $phase_index = array_search($member_id, $participate_member);
            $next_member = isset($participate_member[$phase_index+1]) ? $participate_member[$phase_index+1] : null;
            $prv_member = isset($participate_member[$phase_index-1]) && $phase_index > 1 ? $participate_member[$phase_index-1] : null;
            if($prv_member)
                $prv_page ='_discuss_body';
            if($reading_paper_details->main_type=='bill')
                $prv_page ='_discuss';
            if($next_member)
                $next_page = '_discuss_body';
            else{
                if(!in_array($reading_paper_details->meeting_header, ['शून्य समय','विशेष समय']))
                    $next_page = '_discuss_reply';
            }
        }
        if($paper == '_discuss')
            $next_member = isset($participate_member[1]) ? $participate_member[1] : null;
        $parliament_member_details = ParliamentMember::findOne($member_id);
        $member_name = $parliament_member_details ?  $parliament_member_details->member_title.' '.$parliament_member_details->first_name.' '.$parliament_member_details->last_name: null;
        if($paper == '_discuss_suppoter'){
            $member_name = isset($_GET['participate_member']) ? $_GET['participate_member'] : $supporter_member[0];
            $phase_index = array_search($member_name, $supporter_member);
            $next_member = isset($supporter_member[$phase_index+1]) ? $supporter_member[$phase_index+1] : null;
            $prv_member = isset($supporter_member[$phase_index-1]) && $phase_index > 1 ? $supporter_member[$phase_index-1] : null;
            if($prv_member)
                $prv_page ='_discuss_suppoter';
            else
                $prv_page ='_third_content';
            if($next_member)
                $next_page = '_discuss_suppoter';
        }
        $error_array = array();
        $transaction = Yii::$app->db->beginTransaction();
        if(empty($statement_information)){
            $reading_paper_content = ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND pages=:pages',[':reading_paper_id'=>$reading_paper_details->id, ':pages'=>$paper])->one();
            $update_reading_paper_status = ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND pages=:pages',[':reading_paper_id'=>$reading_paper_details->id, ':pages'=>$prv_page])->one();
        }
        else{
            $reading_paper_content = ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND pages=:pages AND statement_id=:statement_id',[':reading_paper_id'=>$reading_paper_details->id, ':pages'=>$paper, ':statement_id'=>$statement_id])->one();
            $update_reading_paper_status = ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND pages=:pages AND statement_id=:statement_id',[':reading_paper_id'=>$reading_paper_details->id, ':pages'=>$prv_page,':statement_id'=>$statement_id])->one();
        }

        $state_section = UtilityFunctions::StateProcess($reading_paper_details->main_type);
        $state_index = array_search($reading_paper_details->state, $state_section);
        if($statement_id){
            if($paper == '_st_re_back_one'){
                $statement_update_information = Statement::findOne($statement_id);
                if(!empty($statement_update_information)){
                    $statement_update_information->_result = 'फिर्ता';
                    if(!$statement_update_information->save(false))
                        $error_array[] = 'false';
                }
            }
            if($paper == '_result_yes'){
                $statement_update_information = Statement::findOne($statement_id);
                if(!empty($statement_update_information)){
                    $statement_update_information->_result = 'स्वीकृत';
                    if(!$statement_update_information->save(false)){
                        $error_array[] = 'false';
                        echo "<pre>";
                        echo print_r($statement_update_information->errors);
                        exit;
                    }
                }
            }
            if($paper == '_result_no'){
                $statement_update_information = Statement::findOne($statement_id);
                if(!empty($statement_update_information)){
                    $statement_update_information->_result = 'अस्वीकृत';
                    if(!$statement_update_information->save());
                        $error_array[] = 'false';
                }
            }
        }

        if($update_reading_paper_status){
            $update_reading_paper_status->is_read = 1;
            $update_reading_paper_status->read_by = Yii::$app->user->id;
            $update_reading_paper_status->read_date = new \yii\db\Expression('NOW()');
            if(!$update_reading_paper_status->save())
                $error_array[] = 'false';
        }
        if($reading_paper_end){
            if(in_array($paper, ['_result_yes','_result_no','_result_overall_no','_result_overall_yes'])){
                if($paper=='_result_overall_yes'){
                    $next_state = isset($state_section[$state_index+1]) ? $state_section[$state_index+1] : $reading_paper_details->state;
                    $darta_status = 'स्वीकृत';
                    $reading_paper_status = 'सर्बसम्ती स्वीकृत';
                }
                if($paper=='_result_yes'){
                    $next_state = isset($state_section[$state_index+1]) ? $state_section[$state_index+1] : $reading_paper_details->state;
                    $darta_status = 'स्वीकृत';
                    $reading_paper_status = 'बहुमत स्वीकृत';
                }
                if($paper=='_result_overall_no'){
                    $next_state = $reading_paper_details->state;
                    $darta_status = 'अस्वीकृत';
                    $reading_paper_status = 'सर्बसम्ती अस्वीकृति';
                }
                if($paper=='_result_no'){
                    $next_state = $reading_paper_details->state;
                    $darta_status = 'अस्वीकृत';
                    $reading_paper_status = 'बहुमत अस्वीकृति';
                }
            }else{
                if($statement_id){
                    $result_section = array();
                    $statement_information_sec = Statement::find()->where(['reading_paper_id'=>$id,'status'=>1])->all();
                    if(!empty($statement_information_sec)){
                        foreach ($statement_information_sec as $inf_sect) {
                            $result_section[] = $inf_sect->_result;
                        }
                    }
                    if(in_array('स्वीकृत', $result_section)){
                        $next_state = $reading_paper_details->state;
                        $darta_status = 'अस्वीकृत';
                        $reading_paper_status = 'अस्वीकृत';

                    }else{
                        $next_state = isset($state_section[$state_index+1]) ? $state_section[$state_index+1] : $reading_paper_details->state;
                        $darta_status = 'स्वीकृत';
                        $reading_paper_status = 'स्वीकृत';

                    }
                }else{
                    $next_state = isset($state_section[$state_index+1]) ? $state_section[$state_index+1] : $reading_paper_details->state;
                    $darta_status = 'स्वीकृत';
                    $reading_paper_status = 'सर्बसम्ती स्वीकृत';
                }
            }
            if(empty($statement_information)){
                $last_page_update_conent = ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND pages=:pages',[':reading_paper_id'=>$reading_paper_details->id, ':pages'=>$paper])->one();
            }
            else{
                $last_page_update_conent = ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND pages=:pages AND statement_id=:statement_id',[':reading_paper_id'=>$reading_paper_details->id, ':pages'=>$paper,':statement_id'=>$statement_id])->one();
            }

            if($last_page_update_conent){
                $last_page_update_conent->is_read = 1;
                $last_page_update_conent->read_by = Yii::$app->user->id;
                $last_page_update_conent->read_date = new \yii\db\Expression('NOW()');
                if(!$last_page_update_conent->save())
                    $error_array[] = 'false';
            }
            $reading_paper_details->status = $reading_paper_status;
            $reading_paper_details->is_read = 1;
            $reading_paper_details->win_by = $reading_paper_status == 'अस्वीकृत' ? 'हुन्न' : 'हुन्छ';
            $reading_paper_details->read_at = new \yii\db\Expression('NOW()');
            if(!$reading_paper_details->save()){
                $error_array[] = 'false';
                echo "<pre>";
                echo "ERROR IN READING PAPER UPDATE";
                echo print_r($reading_paper_details->errors);
                exit;
            }
        }
        if(isset($darta_desk) && !empty($darta_desk) && $reading_paper_end){

            $darta_processing = ProcessingDetails::find()->where(['darta_id'=>$darta_desk->id,'state'=>$darta_desk->state, '_result'=>$darta_status])->one();
            if(empty($darta_processing))
                $darta_processing = new ProcessingDetails();
            $darta_desk->state = $reading_paper_details->state;
            $darta_desk->state_status = $darta_status;
            $darta_desk->check_state = $next_state;
            $darta_desk->is_generate_dp = 0;
            if(!$darta_desk->save(false))
                $error_array[] = 'false';
            if($next_state == 'संशोधनको म्याद'){
                
                $darta_processing->attributes = $darta_desk->attributes;
                $darta_processing->darta_id = $darta_desk->id;
                $darta_processing->_result = $darta_status;
                $darta_processing->date = new \yii\db\Expression('NOW()');
                if(!$darta_processing->save())
                    $error_array[] = 'false';
                if(empty($darta_processing)){
                    for ($i=1; $i <= 3 ; $i++) { 
                        $today = date('Y-m-d H:i:s');
                        $date_days = strtotime(date("Y-m-d H:i:s", strtotime($today)) . " +".$i."days");
                        $darta_processing_ = new ProcessingDetails();
                        $darta_processing_->attributes = $darta_desk->attributes;
                        $darta_processing_->state = 'संशोधनको म्याद';
                        $darta_processing_->darta_id = $darta_desk->id;
                        $darta_processing_->_result = $darta_status;
                        $darta_processing_->date = date("Y-m-d H:i:s",$date_days);
                        if(!$darta_processing_->save())
                            $error_array[] = 'false';
                    }
                }

            }elseif ($darta_desk->main_type == 'ordinance' && $darta_desk->check_state == 'अध्यादेश अस्वीकार गरियोस्') {
                $darta_processing->attributes = $darta_desk->attributes;
                $darta_processing->darta_id = $darta_desk->id;
                $darta_processing->_result = $darta_status;
                $darta_processing->date = new \yii\db\Expression('NOW()');
                if(!$darta_processing->save())
                    $error_array[] = 'false';
                if(empty($darta_processing)){
                    for ($j=1; $j <= 2 ; $j++) { 
                        $today = date('Y-m-d H:i:s');
                        $date_days = strtotime(date("Y-m-d H:i:s", strtotime($today)) . " +".$j."days");
                        $darta_processing__ = new ProcessingDetails();
                        $darta_processing__->attributes = $darta_desk->attributes;
                        $darta_processing__->state = 'अध्यादेश अस्वीकार गरियोस्';
                        $darta_processing__->darta_id = $darta_desk->id;
                        $darta_processing__->_result = 'प्रकृया';
                        $darta_processing__->darta_status = 'प्रकृया';
                        $darta_processing__->date = date("Y-m-d H:i:s",$date_days);
                        if(!$darta_processing__->save())
                            $error_array[] = 'false';
                    }
                }
            }
            else{
                $darta_processing->attributes = $darta_desk->attributes;
                $darta_processing->darta_id = $darta_desk->id;
                $darta_processing->_result = $darta_status;
                $darta_processing->date = new \yii\db\Expression('NOW()');
                if(!$darta_processing->save())
                    $error_array[] = 'false';
            }
        }
        if(!in_array('false', $error_array)){
            $transaction->commit();
        }
        else{
            $transaction->rollback();
        }
        return $this->render('_paper',['meeting_detail'=>$meeting_detail,'model'=>$model, 'id'=>$id, 'first_member'=>$first_member, 'member_name'=>$member_name , 'next_member'=>$next_member, 'prv_member'=>$prv_member, 'member_id'=>$member_id,'reading_paper_content'=>$reading_paper_content,'paper'=>$paper,'reading_paper_details'=>$reading_paper_details,'statement_id'=>$statement_id,'next_page'=>$next_page, 'prv_page'=>$prv_page,'prv_rp_id'=>$prv_rp_id, 'next_rp_id'=>$next_rp_id, 'prv_page'=>$prv_page, 'next_page'=>$next_page, 'prv_statement_id'=>$prv_statement_id, 'next_statement_id'=>$next_statement_id]);
    }


    

    public function actionEndReadingPaper(){
        $message = isset($_GET['status']) ? $_GET['status'] : null;
        $next_meeting_details = MeetingDetails::find()->where(['status'=>'next'])->orderBy(['_meeting_number'=>SORT_DESC])->one();
        return $this->render('_end_reading_paper',['next_meeting_details'=>$next_meeting_details, 'message'=>$message]);
    }

    /**
    *meeting halt for certian time and will get same meeting number
    */
    public function actionMeetingHalt(){
        $meeting_halt = MeetingHalt::find()->where(['meeting_id'=>UtilityFunctions::ActiveMeeting(),'status'=>1])->one();
        return $this->render('_meeting_halt',['meeting_halt'=>$meeting_halt]);
    }
    public function actionHalt(){
        if(isset($_POST['_csrf'])){
        $meeting_details = MeetingDetails::findOne(UtilityFunctions::ActiveMeeting());

        $halt_meeting = MeetingHalt::find()->where(['meeting_id'=>UtilityFunctions::ActiveMeeting(),'status'=>1])->one();
        if(empty($halt_meeting))
            $halt_meeting = new MeetingHalt();
        $halt_meeting->meeting_id = UtilityFunctions::ActiveMeeting();
        $halt_meeting->_time = $_POST['minutes_hours'];
        if($halt_meeting->save())
            return $this->render('_the_end');
        }
    }

    public function actionEndMeeting(){
        if(isset($_POST['_csrf'])){
            $error_array = array();
            $transaction = Yii::$app->db->beginTransaction();
            $ActiveAdhibasen = UtilityFunctions::ActiveAdhibasen();
            $name = isset($ActiveAdhibasen['name']) ? $ActiveAdhibasen['name'] : '';
            $year = isset($ActiveAdhibasen['year']) ? $ActiveAdhibasen['year'] : '';
            $year_ = isset($_POST['_nepali_year']) ? $_POST['_nepali_year'] :'';
            $month_ = isset($_POST['_nepali_month']) ? $_POST['_nepali_month'] :'';
            $day_ = isset($_POST['_nepali_day']) ? $_POST['_nepali_day'] :'';
            $nepali_date = $year_.'-'.$month_.'-'.$day_;
            $previous_meeting = MeetingDetails::find()->where(['status'=>'active'])->one();
            $previous_meeting->status = 'end';
            if(!$previous_meeting->save(false))
                $error_array[] = 'false';
            $next_meeting_details = MeetingDetails::find()->where(['status'=>'next'])->orderBy(['_meeting_number'=>SORT_DESC])->one();
            if(empty($next_meeting_details))
                $next_meeting_details = new MeetingDetails();
            $next_meeting_details->parliament_id = UtilityFunctions::SambhidhanSava();
            $next_meeting_details->adhibasen = $name;
            $next_meeting_details->adhibasen_year = $year;
            $next_meeting_details->_meeting_number = UtilityFunctions::MeetingNumber();
            $next_meeting_details->_date_time = UtilityFunctions::NepaliToEnglish($nepali_date);
            $next_meeting_details->_nepali_year = isset($_POST['_nepali_year']) ? $_POST['_nepali_year'] :'';
            $next_meeting_details->_nepali_month = isset($_POST['_nepali_month']) ? $_POST['_nepali_month'] :'';
            $next_meeting_details->_nepali_day = isset($_POST['_nepali_day']) ? $_POST['_nepali_day'] :'';
            $next_meeting_details->_time = isset($_POST['_time']) ? $_POST['_time'] :'';
            $next_meeting_details->status = 'active';
            if(!$next_meeting_details->save())
                $error_array[] = 'false';
            if(!in_array('false', $error_array)){
                $transaction->commit();
                return $this->render('_the_end');
            }else{
                $transaction->rollback();
                return $this->redirect(['paper/end-reading-paper/?status=cancel']);

            }
        }else{
            die('errors');
        }
    }


}
