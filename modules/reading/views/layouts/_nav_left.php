<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use app\components\UtilityFunctions;
    use app\modules\program\models\ReadingPaperContent;
  use app\modules\program\models\ReadingPaper;
use app\modules\program\models\DartaDesk;
?>  
<aside class="main-sidebar">
    <section class="sidebar">
      <!-- Sidebar user panel -->
        <ul class="sidebar-menu" style=" max-height: 800px !important; overflow: scroll; ">
            <li class="header text-center"><STRONG>READING PAPER <br /> CONTENT</STRONG></li><br />
            <?php
              if(isset(Yii::$app->session['_reading_type']) && Yii::$app->session['_reading_type'] = 'single'){
                $menu_section = UtilityFunctions::SingleReadingTitle();
                for ($i=0; $i < sizeof($menu_section); $i++) { 
                    $content = ReadingPaperContent::find()->where(['reading_paper_id'=>Yii::$app->session['_reading_id'],'page_type'=>'general','pages'=>$menu_section[$i]])->one();
                    $content_text = $content ? substr( strip_tags($content->content),0,60) : '.................';
                    $title = strtoupper(str_replace("_", " ", $menu_section[$i]));
                    $status = $content && $content->is_read == 1 ? 'read' : '';
                   ?>
                   <a href="<?= Yii::$app->homeUrl; ?>reading/paper/pages/?id=<?= Yii::$app->session['_reading_id']; ?>&paper=<?= $menu_section[$i]; ?>" class="small-box-footer">
                   <div class="row">
                    <div class="col-lg-12 col-xs-12">
                      <!-- small box -->
                      <?php
                        if($content && $content->is_read == 1){
                          ?>
                          <div class="small-box bg-green">
                          <?php
                        }else{
                      ?>
                      <div class="small-box bg-aqua">
                      <?php
                        }
                      ?>
                        <div class="inner">
                          <h6><?= ($i+1).' / '.$title; ?><span style="float: right;"><?= $status; ?></span></h6>

                          <p style="text-align: justify;"><?= $content_text; ?></p>
                        </div>
                        <div class="icon">
                          <i class="ion ion-bag"></i>
                        </div>
                        &nbsp;&nbsp;&nbsp;&nbsp; read page <i class="fa fa-arrow-circle-right"></i>
                      </div>
                    </div>
                   </div>
                   </a>
                   <?php
                }
              }
              if(isset(Yii::$app->session['_reading_type']) && Yii::$app->session['_reading_type'] = 'multiple'){
                $main_menu_section = UtilityFunctions::MultipleReadingTitle();
                for ($i=0; $i < sizeof($main_menu_section); $i++) {
                    $reading_id = $main_menu_section[$i];
                    $reading_information = ReadingPaper::findOne($reading_id);
                    if(empty($reading_information))
                      continue;
                    $data_information = DartaDesk::findOne($reading_information->forum_id);
                    ?>
                    <div class="row" style="box-shadow: 10px 10px 5px #888888; margin-left: 5px !important;">
                      <div class="alert alert-danger"><?= $data_information ?  substr( strip_tags($data_information->title),0,80) : ''; ?></div>
                    <?php
                    $menu_informaiton_ = UtilityFunctions::ReadingTitle($reading_id);
                    $single_menu_section = sizeof($menu_informaiton_) > 0  && isset($menu_informaiton_['all_pages']) ? $menu_informaiton_['all_pages'] : array();
                    for ($j=0; $j < sizeof($single_menu_section); $j++) { 
                        $content = ReadingPaperContent::find()->where(['reading_paper_id'=>$reading_id,'page_type'=>'general','pages'=>$single_menu_section[$j]])->one();
                        $content_text = $content ? substr( strip_tags($content->content),0,60) : '.................';
                        $title = strtoupper(str_replace("_", " ", $single_menu_section[$j]));
                        $status = $content && $content->is_read == 1 ? 'read' : '';
                       ?>
                       <a href="<?= Yii::$app->homeUrl; ?>reading/paper/read/?id=<?= $reading_id; ?>&paper=<?= $single_menu_section[$j]; ?>" class="small-box-footer">
                       <div class="row">
                        <div class="col-lg-12 col-xs-12">
                          <!-- small box -->
                          <?php
                            if($content && $content->is_read == 1){
                              ?>
                              <div class="small-box bg-green">
                              <?php
                            }else{
                          ?>
                          <div class="small-box bg-aqua">
                          <?php
                            }
                          ?>
                            <div class="inner">
                              <h6><?= ($j+1).' / '.$title; ?><span style="float: right;"><?= $status; ?></span></h6>

                              <p style="text-align: justify;"><?= $content_text; ?></p>
                            </div>
                            <div class="icon">
                              <i class="ion ion-bag"></i>
                            </div>
                            &nbsp;&nbsp;&nbsp;&nbsp; read page <i class="fa fa-arrow-circle-right"></i>
                          </div>
                        </div>
                       </div>
                       </a>
                       <?php
                    }

                    ?>
                    </div>
                    <?php
                }
              }
            ?>
        </ul>
    </section>
</aside>
