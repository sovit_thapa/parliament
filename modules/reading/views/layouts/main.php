<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\ReadingAsset;

ReadingAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Legislature Parliament of Nepal</title>
    <?php $this->head() ?>
</head>
<body class="skin-blue sidebar-mini" style="background: #0054a6">
<?php $this->beginBody() ?>
<div class="wrapper">
<?php

if (!\Yii::$app->user->isGuest){
    ?>
            <header class="main-header">
                <a href="<?= Yii::$app->homeUrl; ?>" class="logo">
                    <span class="logo-mini"><b>Parliament</b></span>
                    <span class="logo-lg"><b>Parliament</b></span>
                </a>
                
                <nav class="navbar navbar-static-top" role="navigation">
                    <?= $this -> render('_nav_top'); ?>
                </nav>
            </header>
            <div class="row">
                <section class="content">   
                    <?= $content; ?>
                </section>
            </div>
    <?php
}
?>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
