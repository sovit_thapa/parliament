<?php
	use yii\helpers\Url;
	use yii\helpers\Html;

	use common\components\UtilityFunctions;
	use common\models\User;
?>

<a data-toggle="dropdown" class="dropdown-toggle" href="#">	
	
	<i class="fa fa-gears"></i>
</a>
<ul class="dropdown-menu" >
	<li class="user-header" style="background: white;">
		<a href="<?= Yii::$app->homeUrl; ?>/reading/paper/re-arrange-order">RE-ARRANGE READING LIST<i class="glyphicon glyphicon-th-list"></i></a>
	</li>
	
	<li class="user-footer">
	</li>
</ul>