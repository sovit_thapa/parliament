<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\components\UtilityFunctions;
?>

	<?php
		if(isset($message)){
			if($message == 1)
				echo "<div class='alert alert-success text-center' > READING PAPER HAS BEEN REARRANGED.</DIV>";
			else
				echo "<div class='alert alert-danger text-center' > READING PAPER CANNOT REARRANGED, PLEASE CONSULT TO ADMINISTER.</DIV>";
		}
	?>
		<h3 class="text-center"><STRONG>TODAY READING PAPER : ORDER</STRONG></h3>
	<hr />

    <?php $form = ActiveForm::begin(['action'=>Yii::$app->homeUrl.'reading/paper/r-p-re-arrange-order']); ?>
<div class="row well alpha">
	<?php
		if(!empty($reading_paper_list)){
			$i = 1;
			foreach ($reading_paper_list as $reading_) {
				?>
				<br />
				<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10 title" >
				<table class="table table-striped table-bordered "  style="border: 2px solid #777;">
					<tr><td> TITLE : <?= $reading_->meeting_header; ?></td>
					<td> STATE : <?= $reading_->state; ?></td>
					<td> STATE : <?= $reading_->state; ?></td>
					</tr>
					<tr><td colspan="3" style="text-align: justify-all;"><?= substr( strip_tags($reading_->context), 0, 500) ?></td></tr><tr><td colspan="3" style="text-align: right; color: red;">
					<h3><?= $reading_->order_number;?></h3>
					</td></tr>
				</table>
				<input type="hidden" name="reading_paper_[]" value="<?= $reading_->id; ?>" >
				</div>
				</div>
				<br />
				<?php
				$i++;
			}
		}
	?>
</div>
    <div class="form-group text-center">
        <?= Html::submitButton('RE-ARRANGE ORDER ', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
