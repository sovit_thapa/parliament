 <?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\UtilityFunctions;
?>
<style type="text/css">
	.context-display {
    height: 400px;
    text-align: center;
    padding: 0 20px;
    margin: 20px;
    display: flex;
    justify-content: center; /* align horizontal */
    align-items: center; /* align vertical */

}
</style>
	<div class="row well context-display" style="box-shadow: 3px 3px 3px 4px #888888;min-height: 400px !important;">
		<div class="col-md-1" style="margin-right: 10px !important;">
        </div>
		<div class="col-md-9" style="font-size: 40pt !important; text-align: justify; text-align: center;"> बैठक समाप्त
		</div>
		<div class="col-md-1">
		</div>
	</div>