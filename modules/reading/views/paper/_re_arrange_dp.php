
<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
	use app\modules\program\models\DailyPaper;
    use app\components\UtilityFunctions;
	use yii\widgets\ActiveForm;
?>  

	<?php
		if($message){
			if($message == 1)
				echo "<div class='alert alert-success text-center' > READING PAPER HAS BEEN REARRANGED.</DIV>";
			else
				echo "<div class='alert alert-danger text-center' > READING PAPER CANNOT REARRANGED, PLEASE CONSULT TO ADMINISTER.</DIV>";
		}
	?>

	<div class="row text-center">
		<h3><STRONG>व्यवस्थापिका–संसद : READING PAPER</STRONG></h3>
            <table class="table table-striped table-bordered" style="font-weight: bold;">
                <tr><td>अधिवेशन </td><td>: <?= $meeting_detail ? $meeting_detail->adhibasen : ''; ?></td></tr>
                <tr><td>बैठक संख्या </td><td>: <?= $meeting_detail ? $meeting_detail->_meeting_number : ''; ?></td></tr>
                <tr><td>मिती </td><td>: <?= $meeting_detail ? $meeting_detail->_nepali_year.'-'.$meeting_detail->_nepali_month.'-'.$meeting_detail->_nepali_day : ''; ?></td></tr>
                <tr><td>समय </td><td>: <?= $meeting_detail ? $meeting_detail->_time : ''; ?></td></tr>
            </table>
	</div>
	<hr />


    <?php $form = ActiveForm::begin(['action'=>Yii::$app->homeUrl.'reading/paper/d-p-re-arrange-order']); ?>
	<div class="row well alpha">
	<?php
	if(!empty($daily_paper_details)){
		$sn = 1;
		foreach ($daily_paper_details as $activities) {
			$header = $activities->meeting_header;
			if(in_array($activities->meeting_header, ['राष्ट्रपति','प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालय','राष्ट्रपतिको कार्यालय']))
				$header = $activities->meeting_header.'को पत्र';

			$content = '';
			if($activities->state=='अनुमति माग्ने' && $activities->meeting_header == 'व्यवस्थापन कार्य')
        		$content .= 'मा '.$activities->ministry.' मन्त्री श्री '.$activities->minister."ले ः– <br />";
        	$content .= $activities->context;
        	if($header=='व्यवस्थापन कार्य')
        	{
            	$minister_related_daily_paper = DailyPaper::find()->where('meeting_id=:meeting_id AND status=:status AND ministry=:ministry AND minister=:minister AND id!=:id',[':meeting_id'=>$activities->meeting_id,':status'=>'active',':ministry'=>trim($activities->ministry),':minister'=>trim($activities->minister), ':id'=>$activities->id])->orderBy('order_number ASC')->all();
            	if(!empty($minister_related_daily_paper)){
            		foreach ($minister_related_daily_paper as $related_d_p) {
            			$content .= $related_d_p->context;
            		}
            	}
        	}

			?>
			<br />
			<div class="row title" style="box-shadow: 2px 2px 5px 2px black; ">
				<h4 class="text-center"><strong><?= $header; ?></strong></h4>
				<div class="col-md-1"></div>
				<div class="col-md-11" style="font-size: 18px !important;"> <strong><?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp;';?></strong> <?= ltrim($content, '<p>'); ?></div>
			<input type="hidden" name="daily_paper_[]" value="<?= $activities->id; ?>" >
			</div>
			<br />
			<?php
			$sn++;
		}
	}
	?>
	</div>
    <div class="form-group text-center">
        <?= Html::submitButton('RE-ARRANGE ORDER DAILY PAPER ', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

