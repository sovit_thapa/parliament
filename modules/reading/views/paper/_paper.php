<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\UtilityFunctions;
?>
<style type="text/css">
	.context-display {
    height: 400px;
    text-align: center;
    padding: 0 20px;
    margin: 20px;
    display: flex;
    justify-content: center; /* align horizontal */
    align-items: center; /* align vertical */

}
.big {
    font-size: 25px;
    transition: font 0.5s ease;
}
.small {
    font-size: 16px;
    transition: font 0.5s ease;
}
.read-less {
  display:none;
}

</style>
	<?php
		$reading_content = '';
		if($reading_paper_content)
			$reading_content = $reading_paper_content->content;
		if($reading_paper_content && $first_member && $paper== '_discuss'){
			$reading_content = strip_tags($reading_paper_content->content).' सर्वप्रथम <STRONG>'.$first_member.' </STRONG> लाई छलफलमा भाग लिन समय दिन्छु । <br />(विधेयकमाथिको छलफलमा बोल्ने मा० सदस्यहरुको संख्या हेरी समय तोक्ने)';
		}
		if($reading_paper_content && $member_name && $paper== '_discuss_body'){
			$reading_content = strip_tags($reading_paper_content->content.' '.$member_name);
		}
		if($reading_paper_content && $first_member && $paper== '_first_content' && $reading_paper_details->meeting_header == 'विशेष समय'){
			$reading_content = strip_tags($reading_paper_content->content.' सर्वप्रथम मा० श्री '.$first_member.' लाई बोल्न समय दिन्छु । ');
		}
		if($reading_paper_content && $first_member && $paper== '_first_content' && $reading_paper_details->meeting_header == 'शून्य समय'){
			$reading_content = strip_tags($reading_paper_content->content.' सर्वप्रथम मा० श्री '.$first_member.' लाई बोल्न समय दिन्छु । ');
		}

		if($reading_paper_content && $reading_paper_details->main_type =='proposal' && $paper == '_discuss_suppoter'){
			if($reading_paper_details->meeting_header =='जरुरी सार्वजनिक महत्वको प्रस्ताव'){
				$reading_content = strip_tags($reading_paper_content->content).' '.$member_name.'लाई प्रस्तावको समर्थन गर्दै छलफलमा भाग लिन अनुमति दिन्छु ।(समय ... मिनेट) ';
			}
			if($reading_paper_details->meeting_header =='संकल्प प्रस्ताव'){
				$reading_content = strip_tags($reading_paper_content->content.' '.$member_name.'लाई छलफलमा भाग लिन अनुमति दिन्छु । (समय ३ मिनेट)');

			}
			if($reading_paper_details->meeting_header =='ध्यानाकर्षण प्रस्ताव'){
				$reading_content = strip_tags($reading_paper_content->content.' '.$member_name.'लाई स्पष्टिकरण माग गर्न अनुमति दिन्छु ।');

			}
		}
	?>
	<h4 style="text-align: right; margin-right: 20px !important;"> <?= $reading_paper_content && $reading_paper_content->is_read == 1 ?  'READ' : '-------'; ?></h4>
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10" style="text-align: right;">
			<a href="<?= Yii::$app->homeUrl; ?>reading/paper/end-reading-paper" class="btn btn-success">END MEETING</a>
			<a href="<?= Yii::$app->homeUrl; ?>reading/paper/meeting-halt" class="btn btn-success">HALT METTING</a>
		</div>
		<div class="col-md-1"></div>
	</div>
	<div class="row well context-display" style="box-shadow: 3px 3px 3px 4px #888888;min-height: 400px !important;">
		<div class="col-md-1" style="margin-right: 10px !important;">
		<?php
			if($previous_id){
				if($prv_member){
					?>
					<a href="<?= Yii::$app->homeUrl; ?>reading/paper/reading-section/?id=<?= $previous_id; ?>&participate_member=<?= $prv_member; ?>"> <div class="info-box">
		                <span class="info-box-icon bg-light-blue"><i class="fa fa-arrow-circle-left"></i></span>
		              </div><!-- /.info-box -->
		            </a>
					<?php

				}else{
					?>
					<a href="<?= Yii::$app->homeUrl; ?>reading/paper/reading-section/?id=<?= $previous_id; ?>"> <div class="info-box">
		                <span class="info-box-icon bg-light-blue"><i class="fa fa-arrow-circle-left"></i></span>
		              </div><!-- /.info-box -->
		            </a>
					<?php

				}
			}
			else{
				?>
				<a href="javascript:void(0);">
		          <div class="info-box">
		            <span class="info-box-icon bg-light-blue"><i class="fa fa-ban"></i></span>
		          </div><!-- /.info-box -->
		        </a>
				<?php
			}
		?>
        </div>
		<div class="col-md-9" style="font-size: 16pt !important; text-align: justify;">
			<?= $reading_content; ?>
		</div>
		<div class="col-md-1">
		<?php
			$next_page_information = explode(',', $next_page_id);
			if($next_page_id && sizeof($next_page_information) == 1){
				if($next_member){
					?>
					<a href="<?= Yii::$app->homeUrl; ?>reading/paper/reading-section/?id=<?= $next_page_id; ?>&participate_member=<?= $next_member; ?>"> <div class="info-box">
		                <span class="info-box-icon bg-light-blue"><i class="fa fa-arrow-circle-right"></i></span>
		              </div><!-- /.info-box -->
		            </a>
					<?php
				}
				else{
					?>
					<a href="<?= Yii::$app->homeUrl; ?>reading/paper/reading-section/?id=<?= $next_page_id; ?>"> <div class="info-box">
		                <span class="info-box-icon bg-light-blue"><i class="fa fa-arrow-circle-right"></i></span>
		              </div><!-- /.info-box -->
		            </a>

					<?php
				}
			}
			else{
				?>
				<a href="javascript:void(0);">
		          <div class="info-box">
		            <span class="info-box-icon bg-light-blue"><i class="fa fa-ban"></i></span>
		          </div><!-- /.info-box -->
		        </a>
				<?php
			}
		?>
		</div>
	</div>
	<input type="hidden" name="url" id="url" value="<?= Yii::$app->homeUrl; ?>reading/paper/reading-section/?id=" />
	<?php
		if($reading_paper_details->meeting_header=='अध्यादेश' && $reading_paper_details->state =='छलफल' && $paper == '_second_content' && $statement_id)
			{
				$next_page_information = explode(',', $next_page_id);
			?>

			<div class="row bg-aqua">
				<h2 class="text-center"> CHOOSE ONE SECTION </h2>
				<div class="row" style="box-shadow: 3px 3px 3px 4px #888888;">
				<div class="col-md-2"></div>
					<div class="col-md-4 col-sm-12 "><input type="checkbox" class="result" name="_reback" value="<?= $next_page_information[0] ? $next_page_information[0] : null;?>" style="zoom:9;" />प्रस्ताव पेश नगरेमा वा फिर्ता लिएमा</div>
					<div class="col-md-4 col-sm-12"><input type="checkbox" class="result" name="_no_re_back" value="<?= $next_page_information[1] ? $next_page_information[1] : null;?>" style="zoom:9;" />अध्यादेश अस्वीकार गरियोस् प्रस्ताव पेश गर्नु भएमा</div>
				<div class="col-md-2"></div>
				</div>

			</div>
		<?php
		}
		if($paper == '_st_decision'){
			$next_page_information = explode(',', $next_page_id);
		?>
			<div class="row bg-aqua">
				<h2 class="text-center"> CHOOSE ONE SECTION </h2>
				<div class="row" style="box-shadow: 3px 3px 3px 4px #888888;">
				<div class="col-md-2"></div>
					<div class="col-md-4 col-sm-12 "><input type="checkbox" class="result" name="_reback" value="<?= $next_page_information[0] ? $next_page_information[0] : null;?>" style="zoom:9;" /> फिर्ता लिनुहुन्छ </div>
					<div class="col-md-4 col-sm-12"><input type="checkbox" class="result" name="_no_re_back" value="<?= $next_page_information[1] ? $next_page_information[1] : null;?>" style="zoom:9;" /> फिर्ता लिनुहुन्न </div>
				<div class="col-md-2"></div>
				</div>

			</div>

	<?php
		}
if($paper=='_first_content' && $reading_paper_details->meeting_header == 'ध्यानाकर्षण प्रस्ताव'){
	$next_page_information = explode(',', $next_page_id);
	?>
		<div class="row bg-aqua">
			<h2 class="text-center"> RESULT SECTION </h2>
			<div class="row" style="box-shadow: 3px 3px 3px 4px #888888;">
			<div class="col-md-1"></div>
				<div class="col-md-5"><input type="checkbox" class="result" name="majority_yes" value="<?= $next_page_information[0] ? $next_page_information[0] : null;?>" style="zoom:9;" /> मा० मन्त्रीले तत्काल जवाफ दिन नचाहेमा</div>
				<div class="col-md-5"><input type="checkbox" class="result" name="majority_no" value="<?= $next_page_information[1] ? $next_page_information[1] : null;?>" style="zoom:9;" /> मा० मन्त्रीले तत्काल जवाफ दिन चाहेमा</div>
			</div>


		</div>

<?php

}
if(in_array($paper, ['_general_result','_pertibedan_result','_bidhyak_result','_prastab_result','_final_result','_st_result'])  && sizeof($next_page_information) > 1){
	?>
	<div class="row bg-aqua">
		<h2 class="text-center"> RESULT SECTION </h2>
		<div class="row" style="box-shadow: 3px 3px 3px 4px #888888;">
			<div class="col-md-1"></div>
		<?php
		if(isset($next_page_information[0])){
			?>
			<div class="col-md-5"><input type="checkbox" class="result" name="majority_yes" value="<?= $next_page_information[0] ? $next_page_information[0] : null;?>" style="zoom:9;" /> बहुमत(हुन्छ)</div>
			<?php

		}
		?>
		<?php
		if(isset($next_page_information[1])){
			?>
			<div class="col-md-5"><input type="checkbox" class="result" name="majority_no" value="<?= $next_page_information[1] ? $next_page_information[1] : null;?>" style="zoom:9;" /> बहुमत(हुन्न)</div>
			<?php

		}
		?>
		</div>
	</div>
<?php

}
if($paper=='_result' && sizeof($next_page_information) > 1){
	?>
	<div class="row bg-aqua">
		<h2 class="text-center"> RESULT SECTION </h2>
		<div class="row" style="box-shadow: 3px 3px 3px 4px #888888; text-align: center;">
		<?php
		if(isset($next_page_information[0])){
			?>
			<div class="col-md-3"><input type="checkbox" class="result" name="majority_yes" value="<?= $next_page_information[0] ? $next_page_information[0] : null;?>" style="zoom:9;" /> बहुमत(हुन्छ)</div>
			<?php

		}
		?>
		<?php
		if(isset($next_page_information[1])){
			?>
			<div class="col-md-3"><input type="checkbox" class="result" name="majority_no" value="<?= $next_page_information[1] ? $next_page_information[1] : null;?>" style="zoom:9;" /> बहुमत(हुन्न)</div>
			<?php

		}
		?>
		<?php
		if(isset($next_page_information[2])){
			?>
			<div class="col-md-3"><input type="checkbox" class="result" name="over_all_no" value="<?= $next_page_information[2] ? $next_page_information[2] : null;?>" style="zoom:9;" /> सर्वसम्मति हुन्न</div>
			<?php

		}
		?>
		<?php
		if(isset($next_page_information[3])){
			?>
			<div class="col-md-3"><input type="checkbox" class="result" name="over_all_yes" value="<?= $next_page_information[3] ? $next_page_information[3] : null;?>" style="zoom:9;" /> सर्वसम्मति हुन्छ</div>
			<?php

		}
		?>
		</div>
	</div>
<?php

}
if(!empty($next_page_content_details)){
	?>
	<br />
	<div class="row well"  style="box-shadow: 3px 3px 3px 4px #888888;text-align: justify-all;">
	<div class="data">
	<h3 class="text-center"><STRONG>NEXT PAGE CONTENT</STRONG></h3>
	<div class="col-md-1"></div>
	<span class="read-less">
	<div class="col-md-11"><?= $next_page_content_details ?  $next_page_content_details->content : '';?></div></span>
	</div>
	</div>

	<br /><hr />
	<?php
}
$script = <<< JS
	$(".result").click(function() {
	var url = $("#url").val();
    var checked = $(this).is(':checked');
    $(".result").prop('checked',false);
    if(checked) {
        $(this).prop('checked',true);
        var checkedValue = $('.result:checked').val();
        window.location.replace(url + checkedValue);
    }
});

$('.data').on('mouseover', function() {
$(this).removeClass('small');

$(this).addClass('big');
$(this).children('.read-less').toggle();

});


$('.data').on('mouseout', function() {

$(this).removeClass('big');


$(this).addClass('small');
$(this).children('.read-less').toggle();

});
JS;
$this->registerJs($script);
?>