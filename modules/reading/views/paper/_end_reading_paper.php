<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\UtilityFunctions;
?>
<style type="text/css">
	.context-display {
    height: 400px;
    text-align: center;
    padding: 0 20px;
    margin: 20px;
    display: flex;
    justify-content: center; /* align horizontal */
    align-items: center; /* align vertical */

}
</style>
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10" style="text-align: right;">
			<button class="btn btn-success">END MEETING</button>
			<button class="btn btn-success">HALT METTING</button>
		</div>
		<div class="col-md-1"></div>
	</div>
	<br />
	<?php
		if($message){
			?>
			<div class="alert alert-danger text-center">MEETING CANNOT CREATE, PLEASE CHECK IT AGAING AND FILL DATA PROPERLY.</div>
			<?php
		}
	?>
	<div class="row well context-display" style="box-shadow: 3px 3px 3px 4px #888888;min-height: 400px !important;">
		<div class="col-md-1" style="margin-right: 10px !important;">
        </div>
		<div class="col-md-9" style="font-size: 16pt !important; text-align: justify;">
			<form method="post" action="<?= Yii::$app->homeUrl; ?>reading/paper/end-meeting">
			<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken(); ?>" />
			मा० सदस्यहरु, अब अर्काे बैठक <input type="number" name="_nepali_year" value="<?= $next_meeting_details ?  $next_meeting_details->_nepali_year : '' ?>"></input>  साल <select name='_nepali_month'>
				<option value="1" <?= $next_meeting_details && $next_meeting_details->_nepali_month == 1 ? 'selected' : '';?> > बैशाख </option>
				<option value="2" <?= $next_meeting_details && $next_meeting_details->_nepali_month == 2 ? 'selected' : '';?> > जेष्ठ </option>
				<option value="3" <?= $next_meeting_details && $next_meeting_details->_nepali_month == 3 ? 'selected' : '';?> > आषाढ </option>
				<option value="4" <?= $next_meeting_details && $next_meeting_details->_nepali_month == 4 ? 'selected' : '';?> > श्रावण </option>
				<option value="5" <?= $next_meeting_details && $next_meeting_details->_nepali_month == 5 ? 'selected' : '';?> > भाद्र </option>
				<option value="6" <?= $next_meeting_details && $next_meeting_details->_nepali_month == 6 ? 'selected' : '';?> > आश्विन </option>
				<option value="7" <?= $next_meeting_details && $next_meeting_details->_nepali_month == 7 ? 'selected' : '';?> > कार्तिक </option>
				<option value="8" <?= $next_meeting_details && $next_meeting_details->_nepali_month == 8 ? 'selected' : '';?> > मंसिर </option>
				<option value="9" <?= $next_meeting_details && $next_meeting_details->_nepali_month == 9 ? 'selected' : '';?> > पौष </option>
				<option value="10" <?= $next_meeting_details && $next_meeting_details->_nepali_month == 10 ? 'selected' : '';?> > माघ </option>
				<option value="11" <?= $next_meeting_details && $next_meeting_details->_nepali_month == 11 ? 'selected' : '';?> > माघ </option>
				<option value="12" <?= $next_meeting_details && $next_meeting_details->_nepali_month == 12 ? 'selected' : '';?> > चैत </option>
				</option>
			</select>
			<select name="_nepali_day">
			<?php
				for ($i=1; $i <= 32 ; $i++) { 
					?>
					<option value="<?= $i; ?>" <?= $next_meeting_details && $next_meeting_details->_nepali_day == $i ? 'selected' : '';?> > <?= $i; ?> </option>
					<?php
				}
			?> 
				
			</select>
			गते ................बार <input type="text" name="_time" value="<?= $next_meeting_details ?  $next_meeting_details->_time : '' ?>"> बजे बस्नेगरी यो बैठक स्थगित हुन्छ ।
			<div class="row text-center">
				<button type="submit" class="btn btn-primary">बैठक समाप्त</button>
				</div>
			</form>
		</div>
		<div class="col-md-1">
		</div>
	</div>