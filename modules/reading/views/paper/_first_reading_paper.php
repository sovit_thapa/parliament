<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\UtilityFunctions;
?>
<style type="text/css">
	.context-display {
    height: 400px;
    text-align: center;
    padding: 0 20px;
    margin: 20px;
    display: flex;
    justify-content: center; /* align horizontal */
    align-items: center; /* align vertical */

}
</style>
	<?php
		if(empty($halt_meeting_details))
			$reading_content = 'मा० सदस्यहरु, व्यवस्थापिका–संसदको आजको बैठकको कारवाही प्रारम्भ हुन्छ ।';
		else{
			$reading_content = 'मा० सदस्यहरु, व्यवस्थापिका–संसदको आजै '.$halt_meeting_details->_time.' मिनेट÷घण्टाको लागि स्थगित भएको बैठकको कारवाही प्रारम्भ हुन्छ ।';

		}
	?>
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10" style="text-align: right;">
			<a href="<?= Yii::$app->homeUrl; ?>reading/paper/end-reading-paper" class="btn btn-success">END MEETING</a>
			<a href="<?= Yii::$app->homeUrl; ?>reading/paper/meeting-halt" class="btn btn-success">HALT METTING</a>
		</div>
		<div class="col-md-1"></div>
	</div>
	<div class="row well context-display" style="box-shadow: 3px 3px 3px 4px #888888;min-height: 400px !important;">
		<div class="col-md-1" style="margin-right: 10px !important;">
			<a href="javascript:void(0);">
	          <div class="info-box">
	            <span class="info-box-icon bg-light-blue"><i class="fa fa-ban"></i></span>
	          </div><!-- /.info-box -->
	        </a>
        </div>
		<div class="col-md-9" style="font-size: 40pt !important; text-align: justify; text-align: center;">
			<?= $reading_content; ?>
		</div>
		<div class="col-md-1">
			<?php
			if(!empty($reading_paper_details)){
				?>
					<a href="<?= Yii::$app->homeUrl; ?>reading/paper/reading-section/?id=<?= $reading_paper_details->id; ?>"> <div class="info-box">
		                <span class="info-box-icon bg-light-blue"><i class="fa fa-arrow-circle-right"></i></span>
		              </div><!-- /.info-box -->
		            </a>
				<?php

				}else{
				?><a href="javascript:void(0);">
	          <div class="info-box">
	            <span class="info-box-icon bg-light-blue"><i class="fa fa-ban"></i></span>
	          </div><!-- /.info-box -->
	        </a>
				<?php
			}
			?>
		</div>
	</div>