<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\UtilityFunctions;
?>
<style type="text/css">
	.context-display {
    height: 400px;
    text-align: center;
    padding: 0 20px;
    margin: 20px;
    display: flex;
    justify-content: center; /* align horizontal */
    align-items: center; /* align vertical */

}
</style>
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10" style="text-align: right;">
			<button class="btn btn-success">END MEETING</button>
			<button class="btn btn-success">HALT METTING</button>
		</div>
		<div class="col-md-1"></div>
	</div>
	<div class="row well context-display" style="box-shadow: 3px 3px 3px 4px #888888;min-height: 400px !important;">
		<div class="col-md-1" style="margin-right: 10px !important;">
        </div>
		<div class="col-md-9" style="font-size: 16pt !important; text-align: justify;">
			<form method="post" action="<?= Yii::$app->homeUrl; ?>reading/paper/halt">
			<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken(); ?>" />
			मा० सदस्यहरु, अब अर्काे बैठक <input type="text" name="minutes_hours" placeholder="मिनेट/घाण्टा" value="<?= $meeting_halt ?  $meeting_halt->_time : ''; ?>"></input>मिनेट/घण्टा पछि बस्नेगरी यो बैठक स्थगित हुन्छ ।
			<div class="row text-center">
				<button type="submit" class="btn btn-primary">बैठक समाप्त</button>
				</div>
			</form>
		</div>
		<div class="col-md-1">
		</div>
	</div>