<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\UtilityFunctions;
?>

	<div class="row text-center">
		<h3><STRONG>व्यवस्थापिका–संसद</STRONG></h3>
		<h4><?= $meeting_detail->aadhibasen; ?></h4>
		<h4><?= $meeting_detail->meeting_nepali_year.' '.UtilityFunctions::NepaliMonth($meeting_detail->meeting_nepali_month).' '.$meeting_detail->meeting_nepali_date.' गते'; ?></h4>
		<h5><?= $meeting_detail->nepali_time; ?></h5>
	</div>
	<hr />
	<?php
		$reading_content = '';
		if($reading_paper_content)
			$reading_content = strip_tags($reading_paper_content->content);
		if($reading_paper_content && $first_member && $paper== '_discuss'){
			$reading_content = strip_tags($reading_paper_content->content.' सर्वप्रथम ... '.$first_member.' ... लाई छलफलमा भाग लिन समय दिन्छु । <br />(विधेयकमाथिको छलफलमा बोल्ने मा० सदस्यहरुको संख्या हेरी समय तोक्ने)');
		}
		if($reading_paper_content && $first_member && $paper== '_first_content' && $reading_paper_details->meeting_header == 'शून्य समय'){
			$reading_content = strip_tags($reading_paper_content->content.' सर्वप्रथम मा० श्री '.$first_member.' लाई बोल्न समय दिन्छु । समय ...... मिनेट ।');
		}
		if($reading_paper_content && $first_member && $paper== '_first_content' && $reading_paper_details->meeting_header == 'विशेष समय'){
			$reading_content = strip_tags($reading_paper_content->content.' सर्वप्रथम मा० श्री '.$first_member.' लाई बोल्न समय दिन्छु । ');
		}
	?>
	<h4 style="text-align: right;"> <?= $reading_paper_content->is_read == 1 ?  'READ' : '-------'; ?></h4>
	<div class="row well" style="box-shadow: 3px 3px 3px 4px #888888;">
		<div class="col-md-1" style="margin-right: 10px !important;">

		<?php
			if($prv_page){
				?>
				<a href="<?= Yii::$app->homeUrl; ?>reading/paper/read/?id=<?= $id; ?>&paper=<?= $prv_page; ?>"> <div class="info-box">
	                <span class="info-box-icon bg-light-blue"><i class="fa fa-arrow-circle-left"></i></span>
	              </div><!-- /.info-box -->
	            </a>
				<?php
			}else{
				?>
				<a href="javascript:void(0);">
		          <div class="info-box">
		            <span class="info-box-icon bg-light-blue"><i class="fa fa-ban"></i></span>
		          </div><!-- /.info-box -->
		        </a>
				<?php
			}
		?>
        </div>
		<div class="col-md-9" style="font-size: 16pt !important; text-align: justify;">
			<?= $reading_content; ?>
		</div>
		<div class="col-md-1">


		<?php
			if($next_page){
					if($next_member){
						?>
							<a href="<?= Yii::$app->homeUrl; ?>reading/paper/read/?id=<?= $id; ?>&paper=<?= $next_page; ?>&participate_member=<?= $next_member; ?>">
						<?php
					}else{
						?>
							<a href="<?= Yii::$app->homeUrl; ?>reading/paper/read/?id=<?= $id; ?>&paper=<?= $next_page; ?>">
						<?php
					}
				?>
	                <span class="info-box-icon bg-light-blue"><i class="fa fa-arrow-circle-right"></i></span>
	            </a>
				<?php
			}else{
				?>
				<a href="javascript:void(0);">
		          <div class="info-box">
		            <span class="info-box-icon bg-light-blue"><i class="fa fa-ban"></i></span>
		          </div><!-- /.info-box -->
		        </a>

            	<input type="hidden" name="url" id="url" value="<?= Yii::$app->homeUrl; ?>reading/paper/read/?id=<?= $id; ?>&paper=" />
				<?php
			}
		?>
		</div>
	</div>

	<?php
		if($paper == '_decision_general' || $paper == '_decision_no_discuss'){
	?>
		<div class="row bg-aqua">
			<h2 class="text-center"> RESULT SECTION </h2>
			<div class="row" style="box-shadow: 3px 3px 3px 4px #888888;">
				<div class="col-md-3"><input type="checkbox" class="result" name="over_all_yes" value="_result_overall_yes" style="zoom:9;" /> सर्वसम्मति हुन्छ</div>
				<div class="col-md-3"><input type="checkbox" class="result" name="majority_yes" value="_result_yes" style="zoom:9;" /> बहुमत(हुन्छ)</div>
				<div class="col-md-3"><input type="checkbox" class="result" name="majority_no" value="_result_no" style="zoom:9;" /> बहुमत(हुन्न)</div>
				<div class="col-md-3"><input type="checkbox" class="result" name="over_all_no" value="_result_overall_no" style="zoom:9;" /> सर्वसम्मति हुन्न</div>
			</div>


		</div>

<?php
$script = <<< JS
	$(".result").click(function() {
	var url = $("#url").val();
    var checked = $(this).is(':checked');
    $(".result").prop('checked',false);
    if(checked) {
        $(this).prop('checked',true);
        var checkedValue = $('.result:checked').val();
        window.location.replace(url + checkedValue);
    }
});
JS;
$this->registerJs($script);
}
?>