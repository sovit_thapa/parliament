<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\modules\program\models\ParticipateMemberDiscussion;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\program\services\ParticipateMemberDiscussionService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Participate Member Discussions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participate-member-discussion-index">

    <h1><?= Html::encode($this->title) ?></h1>
    

    <p>
        <?= Html::a('Create Participate Member Discussion', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
           /* 'id',
            'type',
            'parliament_id',
            'meeting_id',
            'reading_paper_id',*/
            [
                'attribute' => 'parliament_member_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->member->member_title.$data->member->first_name.$data->member->last_name;
                }
            ],
            [
                'attribute' => 'party',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->member->political->name;
                }
            ],

            // 'speech_order',
            // 'text_',
            // 'is_read',
            // 'read_at',
            // 'status',
            // 'updated_by',
            // 'updated_date',
            // 'created_by',
            // 'created_date',

           
        ],

        'panel'=>  [
                        'type'=>GridView::TYPE_DEFAULT,
                        //'heading'=>'<h4 class="text-center"><STRONG> asd</STRONG></h4>',
                    ],

       'toolbar' => [
        '{export}',
        '{toggleData}'
    ]
    ]); ?>
</div>
<?php
    echo $this->registerJsFile('@web/bootstrap/js/bootstrap.min.js');
?>
