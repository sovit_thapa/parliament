<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\UtilityFunctions;
use kartik\tabs\TabsX;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use kartik\export\ExportMenu;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\MeetingDetails */

$this->title = 'शून्य समय';
$this->params['breadcrumbs'][] = $this->title;
?>
	<div class="meeting-details-create">

	    <h3 class="text-center"><strong>PARLIAMENT : <?= $meeting_detail->parliament ? $meeting_detail->parliament->name :  '' ;?></strong></h3>
	    <h4 class="text-center"><strong> <?= $meeting_detail ? $meeting_detail->aadhibasen : ''; ?> </strong></h4>
	    <h4 class="text-center"><strong>बैठक संख्या : <?= $meeting_detail ? $meeting_detail->meeting_number : ''; ?></strong></h4>
	    <h4 class="text-center"><strong>मिती : <?= $meeting_detail->meeting_nepali_year.' '.UtilityFunctions::NepaliMonth($meeting_detail->meeting_nepali_month).' '.$meeting_detail->meeting_nepali_date.' गते'; ?></strong></h4>
	    <h4 class="text-center"><strong> समय : <?= $meeting_detail ? $meeting_detail->nepali_time : ''; ?> </strong></h4>
	    <h4 class="text-center"><strong>शून्य समय </strong></h4>

   	</div>
<hr />
   	<div class="row">
   	<div class="col-md-6">
   		

   	</div>
   	<div class="col-md-6" style="box-shadow: 10px 10px 5px #888888;">
   	<table class="table table-striped table-bordered">
   		<thead><th colspan="4"> <h3 class="text-center"> PARTICIPATE MEMBER LIST</h3><th></thead>
   		<tr><td>SN</td><td>NAME</td><td>PARTY</td><td>SPEECH ORDER</td><td>ACTION</td></tr>
   		<?php
   			for ($i=0; $i < sizeof($participate_member) ; $i++) { 
   				?>
   				<tr>
   					<td><?= $i+1; ?></td>
   					<td><?= isset($participate_member[$i]['name']) ? $participate_member[$i]['name'] : '' ; ?></td>
   					<td><?= isset($participate_member[$i]['party']) ? $participate_member[$i]['party'] : '' ; ?></td>
   					<td><?= isset($participate_member[$i]['speech_order']) ? $participate_member[$i]['speech_order'] : '' ; ?></td>
   					<td>............</td>
   				</tr>
   				<?php
   			}
   		?>
   	</table>

   	</div>
   	</div>