<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\program\services\ParticipateMemberDiscussionService */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="participate-member-discussion-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

   

    <?= $form->field($model, 'type')->label('Search By Type')->dropDownList(['zero_time'=>'ZERO TIME','on_topic'=>'ON TOPIC','special_time'=>'SPECIAL TIME'],
                            [
                                'prompt' => 'SHOW ALL',
                                
                            ]);
        ?>

   

 

    <?php // echo $form->field($model, 'parliament_member_id') ?>

    <?php // echo $form->field($model, 'speech_order') ?>

    <?php // echo $form->field($model, 'text_') ?>

    <?php // echo $form->field($model, 'is_read') ?>

    <?php // echo $form->field($model, 'read_at') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'updated_date') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
