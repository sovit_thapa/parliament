<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use app\components\UtilityFunctions;
use app\modules\program\models\ReadingPaper;
use app\modules\program\models\ReadingPaperContent;
/* @var $this yii\web\View */
/* @var $model app\modules\program\models\ReadingPaperContent */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
    $reading_paper_ = ReadingPaper::find()->where(['daily_paper_id'=>$daily_paper_detail->id,'paper_status'=>1])->one();
    
?>
<div class="reading-paper-content-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'page_type')->hiddenInput([ 'value' =>$page_type])->label(false); ?>
    <div class="row well">
        <?php
        if($type=='bill'){
            if(empty($statement_section) || $daily_paper_detail->state == 'प्रतिवेदन सहित छलफल'){
                for ($i=0; $i < sizeof($pages_); $i++) { 
                    $title = $pages_[$i];
                    if(!empty($reading_paper_)){
                        $reading_paper_id = !empty($reading_paper_) ? $reading_paper_->id : null;
                        $reading_paper_content = ReadingPaperContent::find()->where(['reading_paper_id'=>$reading_paper_id,'pages'=>$title])->one();
                    }
                    if(isset($reading_paper_content) && !empty($reading_paper_content))
                        $content_ = $reading_paper_content->content;
                    else
                        $content_ = UtilityFunctions::BillReadingPaperTemplate($data_desk->title, $data_desk->ministry, $data_desk->related_committee, $title, $state, null, $data_desk->presentor);
                    ?>
                    <div class="col-md-6">
                    <?= $form->field($model, $pages_[$i])->widget(CKEditor::className(), [
                        'options' => ['rows' => 3, 'cols'=>40, 'value'=>$content_],
                        'preset' => 'full'
                    ])->label($pages_[$i]) ?>
                    </div>
                    <?php
                }
            }else{
                $sn_ = 0;
                foreach ($statement_section as $statement) {
                    for ($i=0; $i < sizeof($pages_); $i++) { 
                        $title = $pages_[$i];
                        if(!empty($reading_paper_)){
                            $reading_paper_id = !empty($reading_paper_) ? $reading_paper_->id : null;
                            $reading_paper_content = ReadingPaperContent::find()->where(['reading_paper_id'=>$reading_paper_id,'pages'=>$title,'statement_id'=>$statement->id])->one();
                        }
                        if(isset($reading_paper_content) && !empty($reading_paper_content))
                            $content_ = $reading_paper_content->content;
                        else
                            $content_ = UtilityFunctions::BillReadingPaperTemplate($data_desk->title, $data_desk->ministry, $data_desk->related_committee, $title, $state, $statement->id, $data_desk->presentor);
                        ?>
                        <div class="col-md-6">
                        <?= $form->field($model, $title."[".$statement->id."]")->widget(CKEditor::className(), [
                            'options' => ['rows' => 3, 'cols'=>40, 'value'=>$content_,'id'=>$title.$sn_],
                            'preset' => 'full'
                        ])->label($pages_[$i]) ?>
                        </div>
                        <?php
                    }
                    $sn_++;
                    ?>
                    <?= $form->field($model, 'statement_id[]')->hiddenInput([ 'value' =>$statement->id])->label(false); ?>
                    <?php
                }
                ?>
                <?php
            }
        }
        if($type=='letter'){
            for ($i=0; $i < sizeof($pages_); $i++) { 
                $title = $pages_[$i];
                if(!empty($reading_paper_)){
                    $reading_paper_id = !empty($reading_paper_) ? $reading_paper_->id : null;
                    $reading_paper_content = ReadingPaperContent::find()->where(['reading_paper_id'=>$reading_paper_id,'pages'=>$title])->one();
                }
                if(isset($reading_paper_content) && !empty($reading_paper_content))
                    $content_ = $reading_paper_content->content;
                else
                    $content_ = UtilityFunctions::LettersReadingPaperTemplate($data_desk->title, $title_);
                ?>
                <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                <?= $form->field($model, $pages_[$i])->widget(CKEditor::className(), [
                    'options' => ['rows' => 3, 'cols'=>40, 'value'=>$content_],
                    'preset' => 'full'
                ])->label($pages_[$i]) ?>
                </div>
                <div class="col-md-2"></div>
                </div>
                <?php
            }
        }
        
        if($type=='report'){
            for ($i=0; $i < sizeof($pages_); $i++) { 
                $title = $pages_[$i];
                if(!empty($reading_paper_)){
                    $reading_paper_id = !empty($reading_paper_) ? $reading_paper_->id : null;
                    $reading_paper_content = ReadingPaperContent::find()->where(['reading_paper_id'=>$reading_paper_id,'pages'=>$title])->one();
                }
                if(isset($reading_paper_content) && !empty($reading_paper_content))
                    $content_ = $reading_paper_content->content;
                else
                    $content_ = UtilityFunctions::ReportReadingTemplate($data_desk->related_committee, $data_desk->ministry, $data_desk->minister,$data_desk->title, $data_desk->type, $data_desk->presentor);
                ?>
                <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                <?= $form->field($model, $pages_[$i])->widget(CKEditor::className(), [
                    'options' => ['rows' => 3, 'cols'=>40, 'value'=>$content_],
                    'preset' => 'full'
                ])->label($pages_[$i]) ?>
                </div>
                <div class="col-md-2"></div>
                </div>
                <?php
            }
        }        if($type=='other' && $daily_paper_detail->meeting_header=='शोक प्रस्ताव'){
            for ($i=0; $i < sizeof($pages_); $i++) { 
                $paper = $pages_[$i];
                if(!empty($reading_paper_)){
                    $reading_paper_id = !empty($reading_paper_) ? $reading_paper_->id : null;
                    $reading_paper_content = ReadingPaperContent::find()->where(['reading_paper_id'=>$reading_paper_id,'pages'=>$paper])->one();
                }
                if(isset($reading_paper_content) && !empty($reading_paper_content))
                    $content_ = $reading_paper_content->content;
                else
                    $content_ = UtilityFunctions::CondolenceReadingPaperTemplate($data_desk->title, $data_desk->presentor, $paper);
                ?>
                <div class="col-md-6">
                <?= $form->field($model, $pages_[$i])->widget(CKEditor::className(), [
                    'options' => ['rows' => 3, 'cols'=>40, 'value'=>$content_],
                    'preset' => 'full'
                ])->label($pages_[$i]) ?>
                </div>
                <?php
            }
        }
        if($type=='proposal'){
            for ($i=0; $i < sizeof($pages_); $i++) { 
                $paper_ = $pages_[$i];
                $content_ = UtilityFunctions::ReadingPaperProposalTemplate($data_desk->type, $data_desk->presentor, $data_desk->suppoter, $data_desk->title, $data_desk->ministry, $paper_);
                ?>
                <div class="col-md-6">
                <?= $form->field($model, $pages_[$i])->widget(CKEditor::className(), [
                    'options' => ['rows' => 3, 'cols'=>40, 'value'=>$content_],
                    'preset' => 'full'
                ])->label($pages_[$i]) ?>
                </div>
                <?php
            }
        }
        if($type=='aggrement'){
            for ($i=0; $i < sizeof($pages_); $i++) { 
                $paper_ = $pages_[$i];
                $content_ = UtilityFunctions::ReadingPaperAggrementTemplate($data_desk->title, $data_desk->ministry, $paper_, $data_desk->check_state, $data_desk->type);
                ?>
                <div class="col-md-6">
                <?= $form->field($model, $pages_[$i])->widget(CKEditor::className(), [
                    'options' => ['rows' => 3, 'cols'=>40, 'value'=>$content_],
                    'preset' => 'full'
                ])->label($pages_[$i]) ?>
                </div>
                <?php
            }
        }
        if($type=='ordinance'){
            if(empty($asuikriti_prastab)){
                for ($i=0; $i < sizeof($pages_); $i++) { 
                    $paper_ = $pages_[$i];
                    $content_ = UtilityFunctions::OrdinanceReadingPaperTemplate($data_desk->title, $data_desk->ministry, $data_desk->related_committee, $paper_, $data_desk->check_state, null);
                    ?>
                    <div class="col-md-6">
                    <?= $form->field($model, $pages_[$i])->widget(CKEditor::className(), [
                        'options' => ['rows' => 3, 'cols'=>40, 'value'=>$content_],
                        'preset' => 'full'
                    ])->label($pages_[$i]) ?>
                    </div>
                    <?php
                }

            }else{
                $id_s = $asuikriti_prastab->id;
                for ($i=0; $i < sizeof($pages_); $i++) { 
                    $paper_name = $pages_[$i];
                    $content_ = UtilityFunctions::OrdinanceReadingPaperTemplate($data_desk->title, $data_desk->ministry, $data_desk->related_committee, $paper_name, $data_desk->check_state, $asuikriti_prastab, 'asuikrit');
                    ?>
                    <div class="col-md-6">
                    <?= $form->field($model, $paper_name)->widget(CKEditor::className(), [
                        'options' => ['rows' => 3, 'cols'=>40, 'value'=>$content_,'id'=>$paper_name.'_'.$id_s],
                        'preset' => 'full'
                    ])->label($pages_[$i]) ?>
                    </div>
                    <?php
                }/*
                $next_pages = ReadingPaper::asui_sukrit_;
                for ($j=0; $j < sizeof($next_pages); $j++) { 
                    $paper = $next_pages[$j];
                    $content = UtilityFunctions::OrdinanceReadingPaperTemplate($data_desk->title, $data_desk->ministry, $data_desk->related_committee, $paper, $data_desk->check_state, $asuikriti_prastab, 'normal_section');
                    ?>
                    <div class="col-md-6">
                    <?= $form->field($model, $paper)->widget(CKEditor::className(), [
                        'options' => ['rows' => 3, 'cols'=>40, 'value'=>$content,'id'=>$paper.$j],
                        'preset' => 'full'
                    ])->label($paper) ?>
                    </div>
                    <?php
                }*/
            }
        }

        ?>
        </div>

    <div class="row form-group text-center">
        <?= Html::submitButton('GENERATE READING PAPER', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
