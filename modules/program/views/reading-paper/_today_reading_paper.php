<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\program\services\ReadingPaperService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Today Reading Paper List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reading-paper-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'meeting_header',
                'format' => 'html',
                'label' =>'नाम',
                'value' => function ($data) {
                    return $data->meeting_header;
                },
            ],
            [
                'attribute' => 'context',
                'format' => 'html',
                'label' => 'बिबरण',
                'value' => function ($data) {
                    return $data->context;
                },
            ],
            // 'date',
            'minister',
            'related_committee',
            [
                'attribute' => 'order_number',
                'format' => 'html',
                'label' => 'क्र. न. :',
                'value' => function ($data) {
                    return $data->primary_order.' : '.$data->order_number;
                },
            ],
            'state',
            [
                'attribute' => 'status',
                'format' => 'html',
                'label' => 'बिबरण',
                'value' => function ($data) {
                    return $data->paper_status == 1 ? 'active': 'disable';
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'header' => 'ACTION',
                'template' => '{view}{button1}',
                'buttons' => [
                    'button1' => function ($url, $model) {
                        if($model->paper_status == 1 && ($model->state=='विचार गरियोस्' || ($model->meeting_header == 'जरुरी सार्वजनिक महत्वको प्रस्ताव' && $model->state == 'छलफल')|| ($model->meeting_header == 'संकल्प प्रस्ताव' && $model->state == 'छलफल')|| ($model->meeting_header == 'सम्झौता' && $model->state == 'छलफल') || ($model->meeting_header == 'अध्यादेश' && $model->state == 'छलफल'))){
                            return Html::a('<span class="glyphicon glyphicon-user btn btn-primary btn-xs"></span>', Url::to(['/program/reading-paper/reading-paper-discuss/?id='.$model->id]), [
                            'title' => Yii::t('app', 'ADD PARTICIPATE DISCUSS MEMBERS'),
                            ]);

                        }
                    },
                    'view' => function ($url, $model) {
                        if($model->paper_status==1){
                        return Html::a('<span class="glyphicon glyphicon-eye-open btn btn-primary btn-xs"></span>&nbsp;', Url::to(['/program/reading-paper/view/?id='.$model->id]), [
                            'title' => Yii::t('app', 'VIEW DETAILS'),
                        ]);
                        }
                    },

                ],
            ],

        ],
    ]); ?>
</div>
