<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\program\services\ReadingPaperService */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reading-paper-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'parliament_id') ?>

    <?= $form->field($model, 'meeting_id') ?>

    <?= $form->field($model, 'daily_paper_id') ?>

    <?= $form->field($model, 'meeting_header') ?>

    <?php // echo $form->field($model, 'context') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'minister') ?>

    <?php // echo $form->field($model, 'related_committee') ?>

    <?php // echo $form->field($model, 'order_number') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'goes_through_voting') ?>

    <?php // echo $form->field($model, 'win_by') ?>

    <?php // echo $form->field($model, 'is_read') ?>

    <?php // echo $form->field($model, 'read_at') ?>

    <?php // echo $form->field($model, 'paper_status') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
