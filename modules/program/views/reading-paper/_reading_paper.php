<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\UtilityFunctions;
?>

	<div class="row text-center">
		<h3><STRONG>व्यवस्थापिका–संसद</STRONG></h3>
		<h4><?= $meeting_detail->aadhibasen; ?></h4>
		<h3>RP SAMPLE</h3>
		<h4><?= $meeting_detail->meeting_nepali_year.' '.UtilityFunctions::NepaliMonth($meeting_detail->meeting_nepali_month).' '.$meeting_detail->meeting_nepali_date.' गते'; ?></h4>
		<h5><?= $meeting_detail->nepali_time; ?></h5>
	</div>
	<hr />
	<div class="row well" style="box-shadow: 3px 3px 3px 4px #888888;">
		<div class="col-md-1" style="margin-right: 10px !important;">

		<?php
			if($prv_page){
				?>
				<a href="<?= Yii::$app->homeUrl; ?>program/reading-paper/read-reading-paper/?id=<?= $id; ?>&paper=<?= $prv_page; ?>"> <div class="info-box">
	                <span class="info-box-icon bg-light-blue"><i class="fa fa-arrow-circle-left"></i></span>
	              </div><!-- /.info-box -->
	            </a>
				<?php
			}else{
				?>
				<a href="javascript:void(0);">
		          <div class="info-box">
		            <span class="info-box-icon bg-light-blue"><i class="fa fa-ban"></i></span>
		          </div><!-- /.info-box -->
		        </a>
				<?php
			}
		?>
        </div>
		<div class="col-md-9" style="font-size: 16pt !important; text-align: justify;">
			<?= $reading_paper_content ? $reading_paper_content->content : ''; ?>
		</div>
		<div class="col-md-1">


		<?php
			if($next_page){
				?>
				<a href="<?= Yii::$app->homeUrl; ?>program/reading-paper/read-reading-paper/?id=<?= $id; ?>&paper=<?= $next_page; ?>"> <div class="info-box">
	                <span class="info-box-icon bg-light-blue"><i class="fa fa-arrow-circle-right"></i></span>
	              </div><!-- /.info-box -->
	            </a>
				<?php
			}else{
				?>
				<a href="javascript:void(0);">
		          <div class="info-box">
		            <span class="info-box-icon bg-light-blue"><i class="fa fa-ban"></i></span>
		          </div><!-- /.info-box -->
		        </a>

            	<input type="hidden" name="url" id="url" value="<?= Yii::$app->homeUrl; ?>program/reading-paper/read-reading-paper/?id=<?= $id; ?>&paper=" />
				<?php
			}
		?>
		</div>
	</div>

	<?php
		if($paper == '_decision_general' || $paper == '_decision_no_discuss'){
	?>
		<div class="row">
			<h2 class="text-center"> RESULT SECTION </h2><hr />
			<div class="row" style="box-shadow: 3px 3px 3px 4px #888888;">
				<div class="col-md-3"><input type="checkbox" class="result" name="over_all_yes" value="_result_overall_yes" style="zoom:9;" /> सर्वसम्मति हुन्छ</div>
				<div class="col-md-3"><input type="checkbox" class="result" name="majority_yes" value="_result_yes" style="zoom:9;" /> बहुमत(हुन्छ)</div>
				<div class="col-md-3"><input type="checkbox" class="result" name="majority_no" value="_result_no" style="zoom:9;" /> बहुमत(हुन्न)</div>
				<div class="col-md-3"><input type="checkbox" class="result" name="over_all_no" value="_result_overall_no" style="zoom:9;" /> सर्वसम्मति हुन्न</div>
			</div>


		</div>

<?php
$script = <<< JS
	$(".result").click(function() {
	var url = $("#url").val();
    var checked = $(this).is(':checked');
    $(".result").prop('checked',false);
    if(checked) {
        $(this).prop('checked',true);
        var checkedValue = $('.result:checked').val();
        window.location.replace(url + checkedValue);
    }
});
JS;
$this->registerJs($script);
}
?>