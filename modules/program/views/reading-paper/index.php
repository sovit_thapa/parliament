<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\program\services\ReadingPaperService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reading Papers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reading-paper-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('View Reading Paper', ['/reading/paper/reading-section'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'meeting_nepali_date',
                'label'=>'मिति',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->meetingDetails->_nepali_year.'-'.$data->meetingDetails->_nepali_month.'-'.$data->meetingDetails->_nepali_day;
                }
            ],
             [
                'attribute' => 'meeting_no',
                'label'=>'Meeting No',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->meetingDetails->_meeting_number;
                }
            ],
            'meeting_header',
            'context:html',
            'date',
            'minister',
            'related_committee',
            'state'
            // 'order_number',
            // 'status',
            // 'goes_through_voting',
            // 'win_by',
            // 'is_read',
            // 'read_at',
            // 'paper_status',
            // 'created_by',
            // 'created_date',

           
        ],
    ]); ?>
</div>
