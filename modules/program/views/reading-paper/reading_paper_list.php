<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\UtilityFunctions;
use app\modules\program\models\ReadingPaperContent;
use app\modules\program\models\ParticipateMemberDiscussion;
use app\modules\program\models\DartaDesk;
?>
<style>
hr{
   border: 2px solid #ccc;
}
</style>
 <h2><center>READING PAPER</center></h2><br>
<div class="row well" style="box-shadow: 3px 3px 3px 4px #888888; min-height: 800px; font-size: 18px;">
<?php
foreach ($contents as $key => $paper) { 

    $model=ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND content_status=:content_status',[':reading_paper_id'=>$paper->id, ':content_status'=>1])->orderBy(['order_'=>SORT_ASC])->all();
          
?>
    <h2><center><?= $paper->id ?>. <?= $paper->meeting_header; ?></center></h2><br>
   
<?php
$multiple=false;
$nextIds=array();
	foreach ($model as $key => $value) 
	{ 
                if($multiple)
                {
                    foreach ($nextIds as $key => $nextId) 
                        {
                            echo "<div class='col-md-2'></div><div class='col-md-10' style='box-shadow: 3px 3px 3px 4px #888888; margin-bottom:20px'>";
                            $current_content=ReadingPaperContent::findOne($nextId);
                            echo $current_content->content;
                            members($current_content,$paper);
                            echo "</div>";
                             
                        }

                }
                else
                {
                    echo "<div class='col-md-12' style='box-shadow: 1px 1px 1px 1px #888888; margin-bottom:20px'>";
                    echo $value->content;
                    members($value,$paper);
                    echo "</div>";
                     
                }


				$next_id=$value->_next_id;
                $nextIds=explode(',', $next_id);
                if(sizeof($nextIds) > 1)
                {
                    $multiple=true;
            
                }
                else
                {	
                	$multiple=false;

                }
		?>

<?php	} 

    echo "<hr>";
    echo "<br>";

}
?>
</table>

<?php
    function members($model,$paper)
    {
        $darta_desk=DartaDesk::findOne($paper->forum_id);
        if($model->pages == '_discuss_suppoter')
        {
            $supporters=explode(',',$darta_desk->suppoter);
            $suppoterText='';
            if(sizeof($supporters) > 1)
            {
                foreach ($supporters as $key => $value) 
                {
                   $suppoterText.=$value."<br>";
                }
            }
            else
                 $suppoterText.=$supporters[0]."<br>";
            /*$memberModel=ParticipateMemberDiscussion::find()->where(['reading_paper_id'=>$model->reading_paper_id,'statement_id'=>$model->statement_id])->all();
            $text='';
            foreach ($memberModel as $key => $member) 
            {
                $text.=$member->member->member_title.$member->member->first_name.$member->member->last_name."<br>";
            }*/
            echo "<br>";
            echo "Supporter<br>";
            echo $suppoterText;
        }
        else if($model->pages == '_discuss_body')
        {
            echo "<br>";
            echo "Presentor<br>";
            echo $darta_desk->presentor;
        }


    }


?>