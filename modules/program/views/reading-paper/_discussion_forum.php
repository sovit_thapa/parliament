<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\UtilityFunctions;
use kartik\tabs\TabsX;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\MeetingDetails */

$this->title = 'Meeting Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-details-create">

            <h4 class="text-center"><strong>MEETING DETAILS</strong></h4>
            <table class="table table-striped table-bordered" style="font-weight: bold;">
                <tr><td>अधिवेशन </td><td>: <?= $meeting_detail ? $meeting_detail->adhibasen : ''; ?></td></tr>
                <tr><td>बैठक संख्या </td><td>: <?= $meeting_detail ? $meeting_detail->_meeting_number : ''; ?></td></tr>
                <tr><td>मिती </td><td>: <?= $meeting_detail ? $meeting_detail->_nepali_year.'-'.$meeting_detail->_nepali_month.'-'.$meeting_detail->_nepali_day : ''; ?></td></tr>
                <tr><td>समय </td><td>: <?= $meeting_detail ? $meeting_detail->_time : ''; ?></td></tr>
            </table>
    <h4 class="text-center"><strong><?= $title; ?></strong></h4>

    <div class="row well">
    <?php $form = ActiveForm::begin([
        'action' => Yii::$app->homeUrl.'program/reading-paper/save-participate-member'
    ]); ?>


        <?php
            for ($i=0; $i < sizeof($pages_); $i++) { 
                $title = $pages_[$i];
                if($section == 'zero_time')
                    $content_ = UtilityFunctions::ZeroTimeTemplate($title);
                if($section == 'special_time')
                    $content_ = UtilityFunctions::SpecialTimeTemplate($title);
                ?>
                <div class="col-md-4">
                <?= $form->field($new_model, $title)->widget(CKEditor::className(), [
                    'options' => ['rows' => 3, 'cols'=>10, 'value'=>$content_],
                    'preset' => 'full'
                ])->label($pages_[$i]) ?>
                </div>
                <?php
            }
    ?>
    <hr /><br />
    <input type="hidden" name="meeting_id" value="<?= $meeting_detail->id ?>" />
    <input type="hidden" name="type" value="<?= $section; ?>" />
    <?php
        $content = "";
            $content .= "<table class='table table-striped table-bordered' style='border:2px solid black;'><thead>
            <th  style='border:2px solid black;'>SN.</th><th  style='border:2px solid black;'>Political Party</th><th style='border:2px solid black;'>Name</th><th style='border:2px solid black;'>Order</th><th style='border:2px solid black;'>Link</th></thead>";
            for ($i=0; $i < sizeof($participate_member); $i++) { 
                $member_id = isset($participate_member[$i]['member_id']) ? $participate_member[$i]['member_id'] : null;
                $sn = $i +1;
                $content .="<tr><td style='border:2px solid black;'>".$sn."</td><td style='border:2px solid black;'>".$participate_member[$i]['party']."</td><td style='border:2px solid black;'>".$participate_member[$i]['name']."</td><td style='border:2px solid black;'>".$participate_member[$i]['speech_order']."</td><td style='border:2px solid black;'> <a href='".Yii::$app->homeUrl."program/reading-paper/delete-participate-member/?member_id=".$member_id."&section=".$section."&reading_paper=null'><i class='glyphicon glyphicon-trash'></i> </td></tr>";
            
        }
        $content .="</table>";
        $tab_menu = array();
        if(!empty($political_party)){
            $tab_menu[0]['label'] = '<i class="glyphicon glyphicon-flag"></i> Participate Member List';
            $tab_menu[0]['content'] = $content;
            $tab_menu[0]['active'] = true;
            $i = 1;
            foreach ($political_party as $party_info) {
                $tab_menu[$i]['label'] = '<i class="glyphicon glyphicon-flag"></i> '.$party_info->name;
                //$tab_menu[$i]['content'] = $content;
                $tab_menu[$i]['active'] = $i == 0 ? true : '';
                $tab_menu[$i]['linkOptions'] = ['data-url'=>\yii\helpers\Url::to(['/program/reading-paper/parliament-member/?party='.$party_info->id.'&meeting='.$meeting_detail->id.'&section='.$section])];
                $i++;
            }
        }

        echo TabsX::widget([
            'items'=>$tab_menu,
            'position'=>TabsX::POS_LEFT,
            'bordered'=>true,
            'encodeLabels'=>false
        ]);
    ?>
    <br />
    <div class="form-group text-center">
        <button type="submit" class="btn btn-success">SUBMIT INFORMATION</button>
    </div>
    <?php ActiveForm::end(); ?>
    </div>


</div>
