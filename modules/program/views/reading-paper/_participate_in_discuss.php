<?php

use yii\helpers\Html;
use app\components\UtilityFunctions;
use app\modules\setting\models\ParliamentMember;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\MeetingDetails */

$this->title = 'Meeting Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-details-create">

    <h3 class="text-center"><strong>PARLIAMENT : <?= $meeting_detail->parliament ? $meeting_detail->parliament->name :  '' ;?></strong></h3>

    <div class="row well">

    	<div class="col-md-6">
    		<h4 class="text-center"><strong>MEETING DETAILS</strong></h4>
    		<table class="table table-striped table-bordered" style="font-weight: bold;">
                <tr><td colspan="2" class="text-center"><STRONG>व्यवस्थापिका–संसद</STRONG></td>
    			<tr><td>अधिवेशन </td><td>: <?= $meeting_detail ? $meeting_detail->aadhibasen : ''; ?></td></tr>
    			<tr><td>बैठक संख्या </td><td>: <?= $meeting_detail ? $meeting_detail->meeting_number : ''; ?></td></tr>
    			<tr><td>मिती </td><td>: <?= $meeting_detail->meeting_nepali_year.' '.UtilityFunctions::NepaliMonth($meeting_detail->meeting_nepali_month).' '.$meeting_detail->meeting_nepali_date.' गते'; ?></td></tr>
    			<tr><td>समय </td><td>: <?= $meeting_detail ? $meeting_detail->nepali_time : ''; ?></td></tr>
    		</table>
    	</div>
    	<div class="col-md-6">
    		<h4 class="text-center"><strong>PARTICIPATES MEMBERS DETAILS</strong></h4>
    		<table class="table table-striped table-bordered" style="font-weight: bold;">
            <THEAD><th>SN.</th><th>Party</th><th>Name</th><th>Speech Order</th></THEAD>
            <?php
                if(!empty($participate_member)){
                    $sn = 1;
                    foreach ($participate_member as $member_info) {
                        $member_information = ParliamentMember::find()->where(['id'=>$member_info->parliament_member_id,'status'=>'सकृय'])->one();
                        ?>
                        <tr>
                            <td><?= $sn; ?></td>
                            <td><?= $member_information && $member_information->political ? $member_information->political->name : ''; ?></td>
                            <td><?= $member_information ? $member_information->member_title.' '.$member_information->first_name.' '.$member_information->last_name : ''; ?></td>
                            <td><?= $member_info->speech_order; ?></td>
                        </tr>
                        <?php
                        $sn++;
                    }
                }
            ?>
    		</table>
    	</div>

</div>
