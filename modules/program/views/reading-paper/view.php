<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use app\modules\setting\models\ParliamentMember;
use app\modules\program\models\ParticipateMemberDiscussion;
use app\components\UtilityFunctions;
use app\modules\program\models\ReadingPaperContent;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\ReadingPaper */

$this->title = $model->meeting_header;
//$this->params['breadcrumbs'][] = ['label' => 'Reading Papers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
  @media print{
    .reading-paper-view, .print_ { display: none; }
    .print_section { display: block; }
  }
</style>
<div class="reading-paper-view">

  <h1 class="text-center"><strong><?= $this->title ?></strong></h1><hr/>
  <div classs="row well" style="box-shadow: 3px 3px 3px 4px #888888;font-size: 16pt !important; text-align: justify;">
    <?php
    if($model->paper_status == 1 && ($model->state=='विचार गरियोस्' || ($model->meeting_header == 'जरुरी सार्वजनिक महत्वको प्रस्ताव' && $model->state == 'छलफल')|| ($model->meeting_header == 'संकल्प प्रस्ताव' && $model->state == 'छलफल'))){
      echo Html::a('<span class="glyphicon glyphicon-user btn btn-primary btn-xs"></span>', Url::to(['/program/reading-paper/reading-paper-discuss/?id='.$model->id]), [
        'title' => Yii::t('app', 'ADD PARTICIPATE DISCUSS MEMBERS'),
        ]);
    }        
    ?>
    <h3 class="text-center"><STRONG>DETAILS</STRONG></h3><hr />

    <table class="table table-bordered" style="font-weight: bold;">
      <tr>
        <td>शीर्षक/नाम : <?= $dailyPaper->meeting_header; ?></td>
        <td>प्रकृया : <?= $dailyPaper->state; ?></td>
        <td>क्र. नं. : <?= $dailyPaper->order_number; ?></td>
      </tr>
      <?php
      if(!empty($dailyPaper->ministry)){
        ?>
        <tr>
          <td>मन्त्रालय : <?= $dailyPaper->ministry; ?></td>
          <td>सम्बन्धित मन्त्री : <?= $dailyPaper->minister; ?></td>
          <td>सम्बन्धित समिति : <?= $dailyPaper->related_committee; ?></td>
        </tr>
        <?php
      }
      if(!empty($dailyPaper->presentor) || !empty($dailyPaper->suppoter)){
        ?>
        <tr>
          <td>प्रस्तावक : <?= $dailyPaper->presentor; ?></td>
          <td colspan="2">प्रस्तावका समर्थकहरु : <?= $dailyPaper->suppoter; ?></td>
        </tr>
        <?php
      }
      ?>
      <tr>
        <td>नाम/बिवरण</td>
        <td colspan="2" style="text-align: justify-all;">
          <?= $dailyPaper->context; ?>
        </td>
      </tr>
    </table>
  </div>

  <div classs="row well" style="box-shadow: 3px 3px 3px 4px #888888;font-size: 16pt !important; text-align: justify;">
    <h3 class="text-center"><STRONG>STATEMENTS</STRONG></h3><hr />

    <div class="row text-center" >
      <?php 
//Second Button
      if($model->main_type == 'bill' && $model->state == 'अनुमति माग्ने'){
        echo Html::a('<span class="btn btn-primary">विरोधको सूचना दर्ता गर्नुहोस</span>', Url::to(['/program/daily-paper/statement-darta/?id='.$model->forum_id.'&type=birod_suchana']), [
          'title' => Yii::t('app', 'विरोधको सूचना दर्ता गर्नुहोस'),
          'visible' => false,
          ]);

      }
      else if($model->main_type == 'bill' && $model->state == 'संशोधनको म्याद'){
        echo Html::a('<span class="btn btn-primary">संशोधनको सूचना दर्ता गर्नुहोस</span>', Url::to(['/program/daily-paper/statement-darta/id=' . $model->forum_id.'&type=samsodhan_suchana']), [
          'title' => Yii::t('app', 'संशोधनको सूचना दर्ता गर्नुहोस'),
          'visible' => false,
          ]);
      }

//THird Button
      if($model->main_type == 'ordinance' && $model->state == 'अध्यादेश अस्वीकार गरियोस्'){
        echo Html::a('<span class="btn btn-primary">अध्यादेश अस्वीकार गरियोस्</span>', Url::to(['/program/daily-paper/statement-darta/?id='.$model->forum_id.'&type=asuikritgarnus']), [
          'title' => Yii::t('app', 'अध्यादेश अस्वीकार गरियोस्'),
          'visible' => false,
          ]);

      }
//Fourth Button
      if($model->main_type == 'bill' && $model->state == 'संशोधनको म्याद'){
        echo Html::a('<span class="btn btn-primary">समितिमा पठाउँनुस</span>', Url::to(['/program/darta-desk/upgrade-samitima/?id=' . $model->forum_id]), [
          'title' => Yii::t('app', 'समितिमा पठाउँनुस'),
          ]);
      }

      ?>
    </div>

    <table class="table table-bordered" style="font-weight: bold;">
      <tr>
        <th>शीर्षक</th>
        <th>दर्ता गर्नु हुने मान्नेहारु</th>
        <th>प्रकृया</th>
      </tr>
      <?php 
      if($statements != null){
        foreach ($statements as $key => $value) { ?>
        <tr>
          <td><?= $value->statement_type; ?></td>
          <td><?= $value->parliamentMembers(); ?></td>
          <td><?= $value->state; ?></td>
        </tr>
        <?php } } ?>
      </table>
      </div>
      </div>
      <button onclick="print_reading_paper()" class="btn btn-primary print_">PRINT RP</button>
      <div class="row print_section">
      <div class="col-md-1"></div>
      <div class="col-md-10">
      <h4 class="text-center"><strong>READING PAPER</strong></h4>
    <?php
    if(!empty($reading_content_details)){
      $sn = 1;

      foreach ($reading_content_details as $content_information) {
        $cont_id = $content_information->id;
        $multiple = $prv_id = null;
        for ($p=0; $p < sizeof($next_page_id) ; $p++) { 
            $next_page_information_ = explode(':', $next_page_id[$p]);
            $next_page_information = isset($next_page_information_[1]) ? explode(',', $next_page_information_[1]) : array();
            if($next_page_information && in_array($cont_id, $next_page_information) && sizeof($next_page_information) > 1){
              $multiple = true;
              $prv_id = isset($next_page_information_[0]) ? $next_page_information_[0] : null;
            }
        }
        $con_det = ReadingPaperContent::findOne($prv_id);
        if(empty($content_information->statement_id)){
            $participate_member_discussion = ParticipateMemberDiscussion::find()->where('meeting_id=:meeting_id AND type=:type AND reading_paper_id=:reading_paper_id AND status=:status order by speech_order ASC',[':meeting_id'=>UtilityFunctions::ActiveMeeting(),':type'=>'on_topic',':reading_paper_id'=>$content_information->reading_paper_id, ':status'=>1])->all();
        }else{
            $participate_member_discussion = ParticipateMemberDiscussion::find()->where('meeting_id=:meeting_id AND type=:type AND reading_paper_id=:reading_paper_id AND statement_id=:statement_id AND status=:status order by speech_order ASC',[':meeting_id'=>UtilityFunctions::ActiveMeeting(),':type'=>'on_topic',':reading_paper_id'=>$content_information->reading_paper_id, ':statement_id'=>$content_information->statement_id, ':status'=>1])->all();

        }
        $participate_member = $supporter_member = array();
        if(!empty($participate_member_discussion)){
            foreach ($participate_member_discussion as $prt_member) {
                $participate_member[] = $prt_member->parliament_member_id;
            }
        }
        $member_1 = isset($participate_member[0]) ? $participate_member[0] : null;
        $parliament_member = ParliamentMember::findOne($member_1);
        $first_member = $parliament_member ?  $parliament_member->member_title.' '.$parliament_member->first_name.' '.$parliament_member->last_name: null;
        $reading_content = $content_information->content;
        if($content_information && $first_member && $content_information->pages== '_discuss'){
          $reading_content = strip_tags($content_information->content).' सर्वप्रथम <STRONG>'.$first_member.' </STRONG> लाई छलफलमा भाग लिन समय दिन्छु । <br />(विधेयकमाथिको छलफलमा बोल्ने मा० सदस्यहरुको संख्या हेरी समय तोक्ने)';
        }

        if(isset($dailyPaper) && !empty($dailyPaper) && $dailyPaper->main_type =='proposal'){
            $supporter_member = explode(',', $darta_desk->suppoter);
        }

        if($content_information && $first_member && $content_information->pages== '_first_content' && $model->meeting_header == 'विशेष समय'){
          $reading_content = strip_tags($content_information->content.' सर्वप्रथम मा० श्री '.$first_member.' लाई बोल्न समय दिन्छु । ');
        }
        if($content_information && $first_member && $content_information->pages== '_first_content' && $model->meeting_header == 'शून्य समय'){
          $reading_content = strip_tags($content_information->content.' सर्वप्रथम मा० श्री '.$first_member.' लाई बोल्न समय दिन्छु । ');
        }

        if($content_information && $model->main_type =='proposal' && $paper == '_discuss_suppoter'){
          if($model->meeting_header =='जरुरी सार्वजनिक महत्वको प्रस्ताव'){
            for ($k=0; $k < sizeof($supporter_member) ; $k++) {
              $reading_content = strip_tags($content_information->content).' '.$supporter_member[$k].'लाई प्रस्तावको समर्थन गर्दै छलफलमा भाग लिन अनुमति दिन्छु ।(समय ... मिनेट) ';
            }
          }
          if($model->meeting_header =='संकल्प प्रस्ताव'){
            for ($l=0; $l < sizeof($supporter_member) ; $l++) {
                $reading_content = strip_tags($content_information->content.' '.$supporter_member[$l].'लाई छलफलमा भाग लिन अनुमति दिन्छु । (समय ३ मिनेट)');
            }

          }
          if($model->meeting_header =='ध्यानाकर्षण प्रस्ताव'){
            for ($k=0; $k < sizeof($supporter_member) ; $k++) {
              $reading_content = strip_tags($content_information->content.' '.$supporter_member[$k].'लाई स्पष्टिकरण माग गर्न अनुमति दिन्छु ।');
            }

          }
        }
        ?>
        <div class="row well" style="box-shadow: 3px 3px 3px 4px #888888;font-size: 16pt !important; text-align: justify;">
        (<?= UtilityFunctions::NepaliNumber($sn); ?>) .
          <?php

          if($content_information && $content_information->pages== '_discuss_body'){
            for ($i= 1; $i < sizeof($participate_member) ; $i++) { 
              $parliament_member_ = ParliamentMember::findOne($participate_member[$i]);
              $member_name = $parliament_member ? 'मा '.$parliament_member->member_title.' '.$parliament_member->first_name.' '.$parliament_member->last_name: null;
            ?>
            <div class="col-md-6">
              अब <?= $member_name; ?>
            </div>
            <?php
            }
          }else{
            echo $reading_content;
          }
          if($multiple){
            ?>
            <h4 style="text-align: right;"><STRONG>OPTIONAL PAGE (<?= $con_det ? $con_det->pages : '' ;?>)</STRONG></h4>
            <?php
          }
          ?>

        </div>
        <?php
        $sn++;
      }
    }
    ?>
    </div>
    <div class="col-md-1"></div>
</div>
<?php
$script = <<< JS
  $(".print_").click(function() {
    window.print();
});

JS;
$this->registerJs($script);
?>

7953 suvas chandra opt

porgram id : 28