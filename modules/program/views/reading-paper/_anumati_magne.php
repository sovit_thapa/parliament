<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\UtilityFunctions;
use dosamigos\ckeditor\CKEditor;
?>
	<div class="row text-center">
		<h3><STRONG>व्यवस्थापिका–संसद</STRONG></h3>
		<h4><?= $meeting_detail->aadhibasen; ?></h4>
		<h3>RP SAMPLE</h3>
		<h4><?= $meeting_detail->meeting_nepali_year.' '.UtilityFunctions::NepaliMonth($meeting_detail->meeting_nepali_month).' '.$meeting_detail->meeting_nepali_date.' गते'; ?></h4>
		<h5><?= $meeting_detail->nepali_time; ?></h5>
	</div>
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10" style="font-size: 14pt !important; text-align: justify;">

    		<?php $form = ActiveForm::begin(); ?>
    		<?php
    			$content = "अब ".$active_darta_section->minister." लाई ".$active_darta_section->title." लाई अनुमति माग्ने प्रस्ताव प्रस्तुत गर्न अनुमति दिन्छु । <br /> (पेश गरी सकेपछि) <br /> मा० सदस्यहरु, उक्त प्रस्तावमाथि व्यवस्थापिका–संसद नियमावली, २०७३ को नियम १२१ बमोजिम विरोधको सूचना प्राप्त नभएकोले ".$active_darta_section->title." प्रस्तुत गर्न बैठकको अनुमति प्राप्त भएको घोषणा गर्दछु् । <br /> (घोषणा गरि सकेपछि) <br /> अब ".$active_darta_section->title." प्रस्तुत गर्न अनुमति दिन्छु ।";
    		?>
	    <?= $form->field($model, 'context')->widget(CKEditor::className(), [
	        'options' => ['rows' => 4, 'value'=>$content],
	        'preset' => 'full'
	    ]) ?>

        <div class="form-group text-center">
            <?= Html::submitButton('SAVE INFORMATION', ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>
		</div>
		<div class="col-md-1"></div>
	</div>