
<h3 class="text-center"><STRONG><?= $party_information ? $party_information->name : '' ?></STRONG></h3>

<section class="content">   
<div class="row">
	<?php
		for ($i=0; $i < sizeof($parliament_member) ; $i++) {
			$member_id = $parliament_member[$i]['id'];
			$is_checked = in_array($parliament_member[$i]['id'], $participate_member) ? 'checked' : null;
			$order = isset($member_order[$member_id]) ? $member_order[$member_id] : null;
			?>
			<div class="col-md-4" style="box-shadow: 10px 10px 5px #888888;"><br /><span><input type="checkbox" name="member_array[]" value="<?= $parliament_member[$i]['id']; ?>" style="zoom:3" <?= $is_checked; ?> > &nbsp;&nbsp;<?= $parliament_member[$i]['name'] ? $parliament_member[$i]['name'] : ''; ?> &nbsp;&nbsp; <input type="number" name="member_order_<?= $parliament_member[$i]['id']; ?>" min="1" max="100" value="<?= $order; ?>"></span></div>
			<?php
		}
	?>
</div>
</section>