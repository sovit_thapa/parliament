<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use app\modules\setting\models\ParliamentMember;
use app\modules\program\models\ParticipateMemberDiscussion;
use app\components\UtilityFunctions;
use app\modules\program\models\ReadingPaperContent;
?>

<style type="text/css">
  @media print{
    .reading-paper-view, .print_ { display: none; }
    .print_section { display: block; }
  }
</style>
      <button onclick="print_reading_paper()" class="btn btn-primary print_">PRINT RP</button>
      <div class="row print_section">
      <div class="col-md-1"></div>
      <div class="col-md-10">
      <h4 class="text-center"><strong>READING PAPER</strong></h4>
    <?php
      if(!empty($reading_paper_list)){
        $main_sn = 1;
        foreach ($reading_paper_list as $reading_paper_s) {
          $id = $reading_paper_s->id;
        $next_page_id = array();
        $reading_content_details = ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND content_status=:content_status',[':reading_paper_id'=>$id, ':content_status'=>1])->orderBy(['order_'=>SORT_ASC])->all();
        if(!empty($reading_content_details)){
            foreach ($reading_content_details as $reading_content) {
                $next_page_id[] = $reading_content->id.":".$reading_content->_next_id;
            }
        }
        ?>
        <div class="row" style="font-size: 18pt; font-weight: bold;"><?= UtilityFunctions::NepaliCharacter($main_sn).".&nbsp;";?> शीर्षक/नाम : <?= $reading_paper_s->meeting_header; ?> &nbsp;&nbsp;&nbsp;&nbsp; प्रकृया : <?= $reading_paper_s->state; ?> &nbsp;&nbsp;&nbsp;&nbsp; नाम/बिवरण : <?= strip_tags(substr($reading_paper_s->context, 0, 250)); ?></strong></div>
        <?
    if(!empty($reading_content_details)){
      $sn = 1;

      foreach ($reading_content_details as $content_information) {
        $cont_id = $content_information->id;
        $multiple = $prv_id = null;
        for ($p=0; $p < sizeof($next_page_id) ; $p++) { 
            $next_page_information_ = explode(':', $next_page_id[$p]);
            $next_page_information = isset($next_page_information_[1]) ? explode(',', $next_page_information_[1]) : array();
            if($next_page_information && in_array($cont_id, $next_page_information) && sizeof($next_page_information) > 1){
              $multiple = true;
              $prv_id = isset($next_page_information_[0]) ? $next_page_information_[0] : null;
            }
        }
        $con_det = ReadingPaperContent::findOne($prv_id);
        if(empty($content_information->statement_id)){
            $participate_member_discussion = ParticipateMemberDiscussion::find()->where('meeting_id=:meeting_id  AND reading_paper_id=:reading_paper_id AND status=:status order by speech_order ASC',[':meeting_id'=>$reading_paper_s->meeting_id,':reading_paper_id'=>$content_information->reading_paper_id, ':status'=>1])->all();
        }else{
            $participate_member_discussion = ParticipateMemberDiscussion::find()->where('meeting_id=:meeting_id AND type=:type AND reading_paper_id=:reading_paper_id AND statement_id=:statement_id AND status=:status order by speech_order ASC',[':meeting_id'=>$reading_paper_s->meeting_id,':type'=>'on_topic',':reading_paper_id'=>$content_information->reading_paper_id, ':statement_id'=>$content_information->statement_id, ':status'=>1])->all();

        }
        $participate_member = $supporter_member = array();
        if(!empty($participate_member_discussion)){
            foreach ($participate_member_discussion as $prt_member) {
                $participate_member[] = $prt_member->parliament_member_id;
            }
        }
        $member_1 = isset($participate_member[0]) ? $participate_member[0] : null;
        $parliament_member = ParliamentMember::findOne($member_1);
        $first_member = $parliament_member ?  $parliament_member->member_title.' '.$parliament_member->first_name.' '.$parliament_member->last_name: null;
        $reading_content = $content_information->content;
        if($content_information && $first_member && $content_information->pages== '_discuss'){
          $reading_content = strip_tags($content_information->content).' सर्वप्रथम <STRONG>'.$first_member.' </STRONG> लाई छलफलमा भाग लिन समय दिन्छु । <br />(विधेयकमाथिको छलफलमा बोल्ने मा० सदस्यहरुको संख्या हेरी समय तोक्ने)';
        }

        if(isset($dailyPaper) && !empty($dailyPaper) && $dailyPaper->main_type =='proposal'){
            $supporter_member = explode(',', $darta_desk->suppoter);
        }

        if($content_information && $first_member && $content_information->pages== '_first_content' && $reading_paper_s->meeting_header == 'विशेष समय'){
          $reading_content = strip_tags($content_information->content.' सर्वप्रथम मा० श्री '.$first_member.' लाई बोल्न समय दिन्छु । ');
        }
        if($content_information && $first_member && $content_information->pages== '_first_content' && $reading_paper_s->meeting_header == 'शून्य समय'){
          $reading_content = strip_tags($content_information->content.' सर्वप्रथम मा० श्री '.$first_member.' लाई बोल्न समय दिन्छु । ');
        }

        if($content_information && $reading_paper_s->main_type =='proposal' && $paper == '_discuss_suppoter'){
          if($reading_paper_s->meeting_header =='जरुरी सार्वजनिक महत्वको प्रस्ताव'){
            for ($k=0; $k < sizeof($supporter_member) ; $k++) {
              $reading_content = strip_tags($content_information->content).' '.$supporter_member[$k].'लाई प्रस्तावको समर्थन गर्दै छलफलमा भाग लिन अनुमति दिन्छु ।(समय ... मिनेट) ';
            }
          }
          if($reading_paper_s->meeting_header =='संकल्प प्रस्ताव'){
            for ($l=0; $l < sizeof($supporter_member) ; $l++) {
                $reading_content = strip_tags($content_information->content.' '.$supporter_member[$l].'लाई छलफलमा भाग लिन अनुमति दिन्छु । (समय ३ मिनेट)');
            }

          }
          if($reading_paper_s->meeting_header =='ध्यानाकर्षण प्रस्ताव'){
            for ($k=0; $k < sizeof($supporter_member) ; $k++) {
              $reading_content = strip_tags($content_information->content.' '.$supporter_member[$k].'लाई स्पष्टिकरण माग गर्न अनुमति दिन्छु ।');
            }

          }
        }
        ?>
        <div class="row well" style="box-shadow: 3px 3px 3px 4px #888888;font-size: 16pt !important; text-align: justify;">
        <div class="row" style="text-align: left;">
        (<?= UtilityFunctions::NepaliNumber($sn); ?>) .</div>
          <?php

          if($content_information && $content_information->pages== '_discuss_body'){
            for ($p= 1; $p < sizeof($participate_member) ; $p++) { 
              $parliament_member_info = ParliamentMember::findOne($participate_member[$p]);
              $discuss_member_name_ = $parliament_member_info ? 'मा '.$parliament_member_info->member_title.' '.$parliament_member_info->first_name.' '.$parliament_member_info->last_name: null;
            ?>
            <div class="col-md-6">
              <?= $p; ?>. अब <?= $discuss_member_name_; ?>
            </div>
            <?php
            }
          }else{
            echo $reading_content;
          }
          if($multiple){
            ?>
            <h4 style="text-align: right;"><STRONG>OPTIONAL PAGE (<?= $con_det ? $con_det->pages : '' ;?>)</STRONG></h4>
            <?php
          }
          ?>

        </div>
        <?php
        $sn++;
      }
      }
      ?>
      <hr />
      <?php
      $main_sn++;
      }
    }
    ?>
    </div>
    <div class="col-md-1"></div>
    <br /><br />
<div class="row" style="float: right; text-align: justify; margin-right: 10px !important;">
  <?= UtilityFunctions::ActiveMahasachib(); ?> <br />
व्यवस्थापिका–संसदको महासचिव
</div>
</div>
<?php
$script = <<< JS
  $(".print_").click(function() {
    window.print();
});

JS;
$this->registerJs($script);
?>