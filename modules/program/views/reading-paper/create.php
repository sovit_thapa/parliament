<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\program\models\ReadingPaper */

$this->title = 'Create Reading Paper';
$this->params['breadcrumbs'][] = ['label' => 'Reading Papers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reading-paper-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
