<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\ReadingPaper */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reading-paper-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parliament_id')->textInput() ?>

    <?= $form->field($model, 'meeting_id')->textInput() ?>

    <?= $form->field($model, 'daily_paper_id')->textInput() ?>

    <?= $form->field($model, 'meeting_header')->dropDownList([ 'व्यवस्थापन कार्य' => '���्यवस्थापन कार्य', 'राष्ट्रपतिको पत्र' => '���ाष्ट्रपतिको पत्र', 'शोक प्रस्ताव' => '���ोक प्रस्ताव', 'प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालयको पत्र' => '���्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालयको पत्र', 'राष्ट्रपतिको कार्यालयको पत्र' => '���ाष्ट्रपतिको कार्यालयको पत्र', 'प्रतिवेदन पेश' => '���्रतिवेदन पेश', 'प्रधानमन्त्रीको बक्तब्य' => '���्रधानमन्त्रीको बक्तब्य', 'शून्य समय' => '���ून्य समय', 'विशेष समय' => '���िशेष समय', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'context')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'minister')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'related_committee')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order_number')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'सर्बसम्ती स्वीकृत' => '���र्बसम्ती स्वीकृत', 'सर्बसम्ती अस्वीकृति' => '���र्बसम्ती अस्वीकृति', 'बहुमत स्वीकृत' => '���हुमत स्वीकृत', 'बहुमत अस्वीकृति' => '���हुमत अस्वीकृति', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'goes_through_voting')->textInput() ?>

    <?= $form->field($model, 'win_by')->dropDownList([ 'हुन्छ' => '���ुन्छ', 'हुन्न' => '���ुन्न', 'अन्य' => '���न्य', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'is_read')->textInput() ?>

    <?= $form->field($model, 'read_at')->textInput() ?>

    <?= $form->field($model, 'paper_status')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
