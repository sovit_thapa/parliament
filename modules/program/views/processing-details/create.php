<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\program\models\ProcessingDetails */

$this->title = 'Create Processing Details';
$this->params['breadcrumbs'][] = ['label' => 'Processing Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="processing-details-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
