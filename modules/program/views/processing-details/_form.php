<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\ProcessingDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="processing-details-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parliament_id')->textInput() ?>

    <?= $form->field($model, 'meeting_id')->textInput() ?>

    <?= $form->field($model, 'darta_id')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList([ 'व्यवस्थापन कार्य' => '���्यवस्थापन कार्य', 'राष्ट्रपति' => '���ाष्ट्रपति', 'प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालय' => '���्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालय', 'राष्ट्रपति कार्यालय' => '���ाष्ट्रपति कार्यालय', 'प्रतिवेदन पेश' => '���्रतिवेदन पेश', 'सन्धि' => '���न्धि', 'महासन्धि' => '���हासन्धि', 'सम्झौता' => '���म्झौता', 'समिति हेरफेर र गठन' => '���मिति हेरफेर र गठन', 'शोक प्रस्ताव' => '���ोक प्रस्ताव', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'ministry')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'minister')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'related_committee')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'state')->dropDownList([ 'वितरण' => '���ितरण', 'अनुमति माग्ने' => '���नुमति माग्ने', 'विचार गरियोस्' => '���िचार गरियोस्', 'संशोधनको म्याद' => '���ंशोधनको म्याद', 'दफावार छलफल समितिमा पठाइयोस्' => '���फावार छलफल समितिमा पठाइयोस्', 'दफावार छलफल सदनमा संशोधन सहित' => '���फावार छलफल सदनमा संशोधन सहित', 'दफावार छलफल सदनमा संशोधन विना' => '���फावार छलफल सदनमा संशोधन विना', 'समितिको प्रतिवेदन पेश' => '���मितिको प्रतिवेदन पेश', 'प्रतिवेदन सहितको विधेयक समितिमा फिर्ता' => '���्रतिवेदन सहितको विधेयक समितिमा फिर्ता', 'पुनः समितिको प्रतिवेदन पेश' => '���ुनः समितिको प्रतिवेदन पेश', 'प्रतिवेदन सहित छलफल' => '���्रतिवेदन सहित छलफल', 'पारित' => '���ारित', 'पेश' => '���ेश', 'छलफल' => '���लफल', 'अनुमोदन' => '���नुमोदन', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'state_status')->dropDownList([ 'स्वीकृत' => '���्वीकृत', 'अस्वीकृत' => '���स्वीकृत', 'फिर्ता लिनु' => '���िर्ता लिनु', 'प्रकृया' => '���्रकृया', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'remarks')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
