<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\program\models\NoticePaper */

$this->title = 'Create Notice Paper';
$this->params['breadcrumbs'][] = ['label' => 'Notice Papers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notice-paper-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
