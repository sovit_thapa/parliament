<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\program\services\NoticePaperService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notice Papers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notice-paper-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Notice Paper', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'parliament_id',
            'meeting_id',
            'reaing_paper_id',
            'main_type',
            // 'meeting_header',
            // 'state',
            // 'context:ntext',
            // 'date',
            // 'ministry',
            // 'minister',
            // 'related_committee',
            // 'order_number',
            // 'paper_status',
            // 'created_by',
            // 'created_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
