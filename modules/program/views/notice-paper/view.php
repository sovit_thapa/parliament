<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\NoticePaper */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Notice Papers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notice-paper-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'parliament_id',
            'meeting_id',
            'reaing_paper_id',
            'main_type',
            'meeting_header',
            'state',
            'context:ntext',
            'date',
            'ministry',
            'minister',
            'related_committee',
            'order_number',
            'paper_status',
            'created_by',
            'created_date',
        ],
    ]) ?>

</div>
