<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
	use app\modules\program\models\DailyPaper;
	use app\modules\program\models\DartaDesk;
    use app\components\UtilityFunctions;
	use app\modules\program\models\Statement;
	use app\modules\setting\models\ParliamentMember;
	use app\modules\setting\models\ParliamentPartyOfficial;
?>  

	<div class="row well" style="box-shadow: 3px 3px 3px 4px #888888; min-height: 800px;">
	<a class="btn btn-primary hidden-print" href="<?= Yii::$app->request->baseUrl.'/program/notice-paper/notice?meeting_number='.$meeting_details->id.'&&exporttoword=true' ?>" target="_blank">Export to Word</a>
	<a class="btn btn-primary hidden-print" href="<?= Yii::$app->request->baseUrl.'/program/notice-paper/notice?meeting_number='.$meeting_details->id.'&&exporttopdf=true' ?>" target="_blank">Export to PDF</a>
	<a class="btn btn-primary hidden-print" href="<?= Yii::$app->request->baseUrl.'/program/notice-paper/notice?meeting_number='.$meeting_details->id.'&&exporttoprint=true' ?>" target="_blank">Print</a>
	<div class="row text-center">
		<h3><STRONG>व्यवस्थापिका–संसद</STRONG></h3>
		<h4><?= $meeting_details->adhibasen; ?></h4>
		<h3>सूचनापत्र - </h3>
		<h4><?= $meeting_details->_nepali_year.' '.UtilityFunctions::NepaliMonth($meeting_details->_nepali_month).' '.$meeting_details->_nepali_day.' गते'; ?></h4>
		<h5><?= $meeting_details->_time; ?></h5>
	</div>
		<br /><br />
	<div class="row">
		<div class="col-md-1" ></div>
		<div class="col-md-11" style="font-size: 14pt; text-align: justify-all;">
		<?php
		if(!empty($notice_detail)){
			$sn = 1;
			foreach ($notice_detail as $notice_) {
        		?>
        		<div class="row" ><?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp;'; ?> <?= $notice_->context; ?> </div><br />
        		<?php
        		$sn++;
			}
		}
		?>

		</div>
	</div>
<div class="row" style="float: right; text-align: justify; margin-right: 10px !important;">
	<?= UtilityFunctions::ActiveMahasachib(); ?> <br />
व्यवस्थापिका–संसदको महासचिव
</div>
</form>
</div>



