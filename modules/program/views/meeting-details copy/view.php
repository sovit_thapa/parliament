<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\MeetingDetails */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = ['label' => 'Meeting Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-details-view">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'parliament_id',
                'label'=>'Parliament',
                'format' => 'raw',
                'value' => $model->parliament ? $model->parliament->name : '',
            ],
            'parliament_id',
            'aadhibasen',
            'meeting_number',
            'meeting_date',
            'meeting_nepali_year',
            'meeting_nepali_month',
            'meeting_nepali_date',
            'nepali_time',
            'status',
            'created_by',
            'created_ip',
            'created_date',
        ],
    ]) ?>

</div>
