<?php

use yii\helpers\Html;
use app\components\UtilityFunctions;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\MeetingDetails */

$this->title = 'Meeting Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-details-create">

    <h3 class="text-center"><strong>PARLIAMENT : <?= $meetin_details->parliament ? $meetin_details->parliament->name :  '' ;?></strong></h3><hr />
<div class="row">
        <!-- Left col -->
        <div class="col-md-8">
          <!-- MAP & BOX PANE -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title text-center">MEETING DETAILS</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="pad">
                    <!-- Map will be created here -->
                    <table class="table table-striped table-bordered" style="font-weight: bold;">
                        <tr><td>अधिवेशन </td><td>: <?= $meetin_details ? $meetin_details->aadhibasen : ''; ?></td></tr>
                        <tr><td>बैठक संख्या </td><td>: <?= $meetin_details ? $meetin_details->meeting_number : ''; ?></td></tr>
                        <tr><td>मिती </td><td>: <?= date('Y-m-d'); ?></td></tr>
                        <tr><td>समय </td><td>: <?= $meetin_details ? $meetin_details->nepali_time : ''; ?></td></tr>
                    </table>
                  </div>
                </div>
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- /.row -->

          <!-- TABLE: LATEST ORDERS -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4">
          <!-- Info Boxes Style 2 -->

            <a href="<?= Yii::$app->homeUrl; ?>program/daily-paper/view-d-p/?meeting_number=<?= UtilityFunctions::ActiveMeeting(); ?>" class="small-box-footer">
                <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="icon ion-clipboard"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">DAILY PAPER(DP)</span>

                  <div class="progress">
                    <div class="progress-bar" style="width: 50%"></div>
                  </div>
                      <span class="progress-description">
                        
                      </span>
                </div>
                <!-- /.info-box-content -->
              </div>
            </a>
          <!-- /.info-box -->
          <a href="<?= Yii::$app->homeUrl; ?>reading/paper/reading-section" class="small-box-footer">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="icon ion-ios-monitor"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">READING PAPER (RP)</span>
              <span class="info-box-number"></span>

              <div class="progress">
                <div class="progress-bar" style="width: 20%"></div>
              </div>
                  <span class="progress-description">
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          </a>
          <div class="info-box bg-blue">
            <span class="info-box-icon"><i class="icon ion-ios-list-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">NOTICE</span>
              <span class="info-box-number"></span>

              <div class="progress">
                <div class="progress-bar" style="width: 20%"></div>
              </div>
                  <span class="progress-description">
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->

        </div>
        <!-- /.col -->
      </div>
      <div class="row">
          <div class="col-md-4 col-sm-12">
          <a href="<?= Yii::$app->homeUrl; ?>program/reading-paper/priminister-speech/?meeting=<?= UtilityFunctions::ActiveMeeting();?>&status=add" class="small-box-footer">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="icon ion-flag"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">प्रधानमन्त्रीले बोल्नको लागि समय</span>
              <span class="info-box-number"></span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                   
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          </a>
          </div>
          <div class="col-md-4 col-sm-12">
          <a href="<?= Yii::$app->homeUrl; ?>program/reading-paper/prepare-r-p/?meeting=<?= UtilityFunctions::ActiveMeeting();?>&section=zero_time" class="small-box-footer">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="icon ion-ios-stopwatch"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">शून्य समय</span>
              <span class="info-box-number"></span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                   
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          </a>
          </div>
          <div class="col-md-4 col-sm-12">
          <a href="<?= Yii::$app->homeUrl; ?>program/reading-paper/prepare-r-p/?meeting=<?= UtilityFunctions::ActiveMeeting();?>&section=special_time" class="small-box-footer">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="icon ion-ios-time"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">विशेष समय</span>
              <span class="info-box-number"></span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                   
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          </a>
          </div>
      </div>
