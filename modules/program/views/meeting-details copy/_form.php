<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\UtilityFunctions;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\MeetingDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="meeting-details-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row well">
        <div class="col-md-6">

            <?= $form->field($model, 'aadhibasen')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'meeting_number')->textInput() ?>

            <?= $form->field($model, 'meeting_date')->textInput() ?>
        </div>
        <div class="col-md-6">

            <div class="row"> 
            <div class="col-md-2"> <STRONG>बैठक मिती (BS):- </STRONG>  </div><div class="col-md-4"><?= $form->field($model, 'meeting_nepali_year')->textInput(['maxlength' => 4, 'minlength'=>4, 'placeholder'=>'साल'])->label(false); ?></div><div class="col-md-3"><?= $form->field($model, 'meeting_nepali_month')->textInput(['maxlength' => 2, 'minlength'=>2, 'placeholder'=>'महिना'])->label(false); ?></div><div class="col-md-3"><?= $form->field($model, 'meeting_nepali_date')->textInput(['maxlength' => 2, 'minlength'=>2, 'placeholder'=>'गते'])->label(false); ?></div>
            </div>

        <?= $form->field($model, 'nepali_time')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'status')->dropDownList([ 'सुचारू' => 'सुचारू', 'स्थगित' => 'स्थगित' ,'समाप्त' =>'समाप्त'], ['prompt' => '']) ?>
        </div>
        </div>

    <?= $form->field($model, 'parliament_id')->hiddenInput(['value'=>UtilityFunctions::SambhidhanSava()])->label(false); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
