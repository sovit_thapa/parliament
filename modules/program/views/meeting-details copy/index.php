<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\services\MeetingDetailsService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Meeting Details';
$this->params['breadcrumbs'][] = ['label' => 'Setting', 'url' => ['/setting']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-details-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr/>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Meeting Details', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'target'=>'TARGET_SELF',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'parliament_id',
                'label'=>'Parliament',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->parliament ? $data->parliament->name : '';
                }
            ],
            'aadhibasen',
            'meeting_number',
            'meeting_date',
            'meeting_nepali_year',
            'meeting_nepali_month',
            'meeting_nepali_date',
            'nepali_time',
            'status',
        ],
        'filename' => 'Meeting Details',
        'pdfLibraryPath'=>'@vendor/kartik-v/mpdf',
        'onInitSheet' => function (PHPExcel_Worksheet $sheet, $grid) {
            $sheet->getDefaultStyle()->applyFromArray(['borders' => [ 'allborders' => ['style' => PHPExcel_Style_Border::BORDER_THIN ]]]);
        },
    ]); ?>




    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'parliament_id',
                'label'=>'Parliament',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->parliament ? $data->parliament->name : '';
                }
            ],
            'aadhibasen',
            'meeting_number',
            'meeting_date',
            'meeting_nepali_year',
            'meeting_nepali_month',
            'meeting_nepali_date',
            'nepali_time',
            'status',
            // 'created_by',
            // 'created_ip',
            // 'created_date',

            [
            'header' => "BULKACTION",
            'class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
