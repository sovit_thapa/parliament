<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\services\MeetingDetailsService */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="meeting-details-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'parliament_id') ?>

    <?= $form->field($model, 'aadhibasen') ?>

    <?= $form->field($model, 'meeting_number') ?>

    <?= $form->field($model, 'meeting_date') ?>

    <?php // echo $form->field($model, 'meeting_nepali_year') ?>

    <?php // echo $form->field($model, 'meeting_nepali_month') ?>

    <?php // echo $form->field($model, 'meeting_nepali_date') ?>

    <?php // echo $form->field($model, 'nepali_time') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_ip') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
