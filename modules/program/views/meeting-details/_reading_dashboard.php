<?php

use yii\helpers\Html;
use app\components\UtilityFunctions;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\MeetingDetails */

$this->title = 'Meeting Details';
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="meeting-details-create">

  <h3 class="text-center"><strong>PARLIAMENT : <?= $meetin_details->parliament ? $meetin_details->parliament->name :  '' ;?></strong></h3><hr />

  <div class="row well">
    <div class="col-md-1"></div>
    <div class="col-md-10">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">MEETING DETAILS</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div class="pad">
                <table class="table table-striped table-bordered" style="font-weight: bold;">
                  <tr>
                    <th>अधिवेशन</th>
                    <th>बैठक संख्या</th>
                    <th>मिती(AD)</th>
                    <th>मिती(BS)</th>
                    <th>समय</th>
                  </tr>
                  <tr>
                    <td><?= $meetin_details->adhibasen; ?></td>
                    <td><?= $meetin_details->_meeting_number; ?></td>
                    <td><?= $meetin_details->_date_time; ?></td>
                    <td><?= $meetin_details->_nepali_year.'-'.$meetin_details->_nepali_month.'-'.$meetin_details->_nepali_day; ?></td>
                    <td><?= $meetin_details->_time; ?></td>
                  </tr>
                </table>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
<div class="col-md-1"></div>
</div>
<div class="row well">
  <div class="col-md-1"></div>
  <div class="col-md-10">
  <a href="<?= Yii::$app->homeUrl; ?>reading/paper/reading-section" class="small-box-footer">
    <div class="info-box bg-green">
      <span class="info-box-icon"><i class="icon ion-ios-monitor"></i></span>

      <div class="info-box-content">
        <span class="info-box-text text-center">START MEETING</span>
        <span class="info-box-number"></span>

        <div class="progress">
          <div class="progress-bar" style="width: 20%"></div>
        </div>
        <span class="progress-description text-center">
        बैठक सुचारु गर्नुस 
        </span>
      </div>
      <!-- /.info-box-content -->
    </div>
  </a>
  </div>
  <div class="col-md-1"></div>
</div>