<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\program\models\MeetingDetails */

$this->title = 'Meeting Details';
$this->params['breadcrumbs'][] = ['label' => 'Meeting Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-details-create">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
