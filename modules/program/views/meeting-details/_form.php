<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\UtilityFunctions;
use yii\helpers\ArrayHelper;
use dosamigos\datetimepicker\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\MeetingDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="meeting-details-form well">

    <?php $form = ActiveForm::begin(); ?>
    <?php
        $inforamtion = UtilityFunctions::ActiveAdhibasen();
        $adhibasen = isset($inforamtion['name']) ? $inforamtion['name'] : null;
        $adhibasen_year = isset($inforamtion['year']) ? $inforamtion['year'] : null;
    ?>
    <?= $form->field($model, 'parliament_id')->hiddenInput(['value'=>UtilityFunctions::SambhidhanSava()])->label(false); ?>
    <?= $form->field($model, 'adhibasen')->hiddenInput(['value'=>$adhibasen])->label(false); ?>
    <?= $form->field($model, 'adhibasen_year')->hiddenInput(['value'=>$adhibasen_year])->label(false); ?>
    <div class="row">
        <div class="col-md-6">

            <?= $form->field($model, '_date_time')->widget(DateTimePicker::className(), [
                'size' => 'ms',
                'pickButtonIcon' => 'glyphicon glyphicon-time',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd H:i:s',
                    'todayBtn' => true
                ]
            ]);?>
        </div>
        <div class="col-md-6">

            <div class="row"> 
            <div class="col-md-3"> <STRONG>बैठक मिती (BS):- </STRONG>  </div><div class="col-md-3"><?= $form->field($model, '_nepali_year')->textInput(['maxlength' => 4, 'minlength'=>4, 'placeholder'=>'साल'])->label(false); ?></div><div class="col-md-3"><?= $form->field($model, '_nepali_month')->textInput(['maxlength' => 2, 'minlength'=>2, 'placeholder'=>'महिना'])->label(false); ?></div><div class="col-md-3"><?= $form->field($model, '_nepali_day')->textInput(['maxlength' => 2, 'minlength'=>2, 'placeholder'=>'गते'])->label(false); ?></div>
            </div>
        </div>
        </div>

    <?= $form->field($model, '_time')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'end' => 'End', 'cancelled' => 'Cancelled', 'halt' => 'Halt', 'next'=>'Next Meeting']) ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
