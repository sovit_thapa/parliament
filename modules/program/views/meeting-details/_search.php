<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\program\services\MeetingDetailsService */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="meeting-details-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'parliament_id') ?>

    <?= $form->field($model, 'adhibasen') ?>

    <?= $form->field($model, 'adhibasen_year') ?>

    <?= $form->field($model, '_date_time') ?>

    <?php // echo $form->field($model, '_nepali_year') ?>

    <?php // echo $form->field($model, '_nepali_month') ?>

    <?php // echo $form->field($model, '_nepali_day') ?>

    <?php // echo $form->field($model, '_time') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
