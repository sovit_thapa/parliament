<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\program\models\MeetingDetails;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\program\services\MeetingDetailsService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Meeting Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-details-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="row">
        <div class="col-md-11" style="text-align: right;">
        <?= Html::a('Create Meeting', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="col-md-1"></div>
    </div><br />

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'adhibasen',
            'adhibasen_year',
            '_date_time',
            [
                'attribute' => '_nepali_year',
                'format' => 'html',
                'label' => 'Date(BS)',
                'value' => function ($data) {
                    return $data->_nepali_year.'-'.$data->_nepali_month.'-'.$data->_nepali_day.' '.$data->_time;
                }
            ],
            'status',
            [
                'attribute' => 'meeting_links',
                'format' => 'html',
                'label' => "DP/RP/MEETING'S CANCELLATION",
                'value' => function ($data) {
                    return (new MeetingDetails)->Details($data->status, $data->id);
                }
            ],
            // 'created_by',
            // 'created_date',

            [
            'header' => 'ACTION/LINK',
            'class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
