<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\DailyPaper */

$this->title = 'Update Daily Paper: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Daily Papers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="daily-paper-update">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />

    <?= $this->render('_form', [
        'model' => $model,'meeting_details'=>$meeting_details
    ]) ?>

</div>
