<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\DailyPaper */

$this->title = $model->meeting_header;
//$this->params['breadcrumbs'][] = ['label' => 'Daily Papers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-paper-view" style="min-height: 1000px">

    <h3 class="text-center"><STRONG><?= Html::decode($this->title) ?></STRONG></h3><hr />

 <div classs="row">
            <!-- Left col -->
        <div class="col-md-12">
          <!-- MAP & BOX PANE -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 style="text-align: center;">DAILY PAPER</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="pad">
                    <!-- Map will be created here -->
                    <table class="table table-striped table-bordered" style="font-weight: bold;">
                      <tr>
                        <td>शीर्षक/नाम : <?= $model->meeting_header; ?></td>
                        <td>प्रकृया : <?= $model->state; ?></td>
                        <td>क्र. नं. : <?= $model->order_number; ?></td>
                      </tr>
                      <?php
                        if(!empty($model->ministry)){
                      ?>
                      <tr>
                        <td>मन्त्रालय : <?= $model->ministry; ?></td>
                        <td>सम्बन्धित मन्त्री : <?= $model->minister; ?></td>
                        <td>सम्बन्धित समिति : <?= $model->related_committee; ?></td>
                      </tr>
                      <?php
                      }
                      if(!empty($model->presentor) || !empty($model->suppoter)){
                      ?>
                      <tr>
                        <td>प्रस्तावक : <?= $model->presentor; ?></td>
                        <td colspan="2">प्रस्तावका समर्थकहरु : <?= $model->suppoter; ?></td>
                      </tr>
                      <?php
                      }
                      ?>
                      <tr>
                        <td>नाम/बिवरण</td>
                        <td colspan="2" style="text-align: justify-all;">
                          <?= $model->context; ?>
                        </td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- /.row -->

          <!-- TABLE: LATEST ORDERS -->
          <!-- /.box -->
        </div>
    </div>

    <div classs="row">
            <!-- Left col -->
        <div class="col-md-6">
          <!-- MAP & BOX PANE -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title text-center">STATEMENTS</h3>

              <div class="box-tools pull-right">
              <?php 
                  //Second Button
                  if($model->main_type == 'bill' && $model->state == 'अनुमति माग्ने'){
                        echo Html::a('<span class="btn btn-primary">विरोधको सूचना दर्ता गर्नुहोस</span>', Url::to(['/program/daily-paper/statement-darta/?id='.$model->forum_id.'&type=birod_suchana']), [
                            'title' => Yii::t('app', 'विरोधको सूचना दर्ता गर्नुहोस'),
                            'visible' => false,
                        ]);

                        }
                    else if($model->main_type == 'bill' && $model->state == 'संशोधनको म्याद'){
                        echo Html::a('<span class="btn btn-primary">संशोधनको सूचना दर्ता गर्नुहोस</span>', Url::to(['/program/daily-paper/statement-darta/id=' . $model->forum_id.'&type=samsodhan_suchana']), [
                            'title' => Yii::t('app', 'संशोधनको सूचना दर्ता गर्नुहोस'),
                            'visible' => false,
                        ]);
                      }

                  //THird Button
                         if($model->main_type == 'ordinance' && $model->state == 'अध्यादेश अस्वीकार गरियोस्'){
                        echo Html::a('<span class="btn btn-primary">अध्यादेश अस्वीकार गरियोस्</span>', Url::to(['/program/daily-paper/statement-darta/?id='.$model->forum_id.'&type=asuikritgarnus']), [
                            'title' => Yii::t('app', 'अध्यादेश अस्वीकार गरियोस्'),
                            'visible' => false,
                        ]);

                        }
                  //Fourth Button
                         if($model->main_type == 'bill' && $model->state == 'संशोधनको म्याद'){
                        echo Html::a('<span class="btn btn-primary">समितिमा पठाउँनुस</span>', Url::to(['/program/darta-desk/upgrade-samitima/?id=' . $model->forum_id]), [
                            'title' => Yii::t('app', 'समितिमा पठाउँनुस'),
                        ]);
                        }

                ?>
              </div>
 
             
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="pad">
                    <!-- Map will be created here -->
                    <table class="table table-striped table-bordered" style="font-weight: bold;">
                    <tr>
                        <th>शीर्षक</th>
                        <th>दर्ता गर्नु हुने मान्नेहारु</th>
                        <th>प्रकृया</th>
                    </tr>
                    <?php 
                    if($statements != null){
                    foreach ($statements as $key => $value) { ?>
                        <tr>
                           <td><?= $value->statement_type; ?></td>
                           <td><?= $value->parliamentMembers(); ?></td>
                           <td><?= $value->state; ?></td>
                       </tr>
                    <?php } } ?>
                    </table>
                  </div>
                </div>
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- /.row -->

          <!-- TABLE: LATEST ORDERS -->
          <!-- /.box -->
        </div>



    </div>


</div>
