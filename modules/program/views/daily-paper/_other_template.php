<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\components\UtilityFunctions;
use dosamigos\ckeditor\CKEditor;
use app\modules\setting\models\Ministry;


/* @var $this yii\web\View */
/* @var $model app\modules\program\models\DartaDesk */

$this->title = 'Register In Darta Desk';
$this->params['breadcrumbs'][] = ['label' => 'Darta Desks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
if($data_information->type=='शोक प्रस्ताव')
    $context = 'ससम्माननीय सभामुखले निम्नलिखित शोक प्रस्ताव प्रस्तुत गर्नुहुने ः– <br />'.strip_tags($data_information->title);
else
    $context = $data_information->title;

?>



<h4 class="text-center"><STRONG><?= $data_information->type; ?></STRONG></h4>
<?php $form = ActiveForm::begin(['action'=>Yii::$app->homeUrl.'/program/daily-paper/generate-daily-paper']); ?>
<div class="row well">
    <div class="col-md-6">

        <?= $form->field($model, 'parliament_id')->hiddenInput(['value'=>$data_information->parliament_id])->label(false) ?>

        <?= $form->field($model, 'meeting_id')->hiddenInput(['value'=>$data_information->meeting_id])->label(false) ?>
        <?= $form->field($model, 'main_type')->hiddenInput(['value'=>$data_information->main_type])->label(false) ?>
        <div class="row">
            <h4 class="text-center"><strong>MEETING DETAILS</strong></h4>
            <table class="table table-striped table-bordered" style="font-weight: bold;">
                <tr><td>संविधान </td><td> : <?= $data_information->parliament ? $data_information->parliament->name :  '' ;?></td></tr>
                <tr><td>अधिवेशन </td><td> : <?= $meeting_details ? $meeting_details->aadhibasen : ''; ?></td></tr>
                <tr><td>बैठक संख्या </td><td> : <?= $meeting_details ? $meeting_details->meeting_number : ''; ?></td></tr>
                <tr><td>मिती</td><td> : <?= $meeting_details ? $meeting_details->meeting_nepali_year.'-'.$meeting_details->meeting_nepali_month.'-'.$meeting_details->meeting_nepali_date : ''; ?></td></tr>
                <tr><td>समय </td><td> : <?= $meeting_details ? $meeting_details->nepali_time : ''; ?></td></tr>
                <tr><td>मन्त्रालय </td><td> : <?= $data_information ? $data_information->ministry : ''; ?></td></tr>
                <tr><td>विधेयक बिषय</td><td> : <?= $data_information ? $data_information->check_state : ''; ?></td></tr>
            </table>
        </div>

        <?= $form->field($model, 'ministry')->hiddenInput(['value'=>$data_information->ministry])->label(false); ?>

        <?= $form->field($model, 'state')->hiddenInput(['value'=>$data_information->check_state ])->label(false); ?>

        <?= $form->field($model, 'meeting_header')->hiddenInput(['value'=>$data_information->type])->label(false); ?>

        <?= $form->field($model, 'forum_id')->hiddenInput(['value'=>$data_information->id])->label(false); ?>

        
    </div>
    <div class="col-md-6">
        
        <?= $form->field($model, 'context')->widget(CKEditor::className(), [
            'options' => ['rows' => 6, 'cols'=>40, 'value'=>$context],
            'preset' => 'advance'
        ])->label('बिबरण') ?>
    </div>

</div>

    <div class="form-group text-center">
        <?= Html::submitButton('GENEREATE DAILY PAPER', ['class' => 'btn btn-success']) ?>
    </div>
<?php ActiveForm::end(); ?>




