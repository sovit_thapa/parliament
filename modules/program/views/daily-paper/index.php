<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\program\services\DailyPaperService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daily Papers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-paper-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   <p>
        <?= Html::a('View Daily Paper', ['/program/daily-paper/view-d-p/?meeting_number='.$meeting_id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
           
            [
                'attribute' => 'meeting_nepali_date',
                'label'=>'मिति',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->meetingDetails ? $data->meetingDetails->_nepali_year.'-'.$data->meetingDetails->_nepali_month.'-'.$data->meetingDetails->_nepali_day : '';
                }
            ],
             [
                'attribute' => 'meeting_no',
                'label'=>'Meeting No',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->meetingDetails ? $data->meetingDetails->_meeting_number : '';
                }
            ],
            'meeting_header',

            [
                'attribute' => 'context',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->context;
                }
            ],
            //'meeting_header',
            //'forum_id',
            'minister',
            'related_committee',
            'order_number',
            'state',
            // 'created_by',
            // 'created_date',

        ],
    ]); ?>
</div>
