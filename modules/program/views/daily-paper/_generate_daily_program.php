<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\UtilityFunctions;
use app\modules\program\models\DartaDesk;
use  yii\helpers\ArrayHelper;
use dosamigos\datetimepicker\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\ParliamentActivities */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parliament-activities-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row well">
    <div class="col-md-6">
    <?=
    $form
        ->field($model, 'forum_id')
        ->dropDownList(ArrayHelper::map(DartaDesk::find()->where(['not in','activities_status',['पारित','अस्वीकृत']])->where(['and','status','1'])->all(), 'id', 'title'),['onchange' => '
            $.post("daily-program-format?darta_id = "+$(this).val(), function(data) {
            $( "#daily_program_format" ).html(data);
            });
        ']);
    ?>

    <?= $form->field($model, 'order_number')->textInput() ?>
    </div>
    <div class="col-md-6">
           <div id="daily_program_format"></div>
    </div>
    </div>

    <div class="form-group text-center">
        <?= Html::submitButton('Generate Daily Program', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
$(document).ready(function(){
    var document_id = $('#parliamentactivities-forum_id').val()
     $.ajax({
        type:"GET",
        url: 'daily-program-format',
        data: {darta_id_:document_id},
        success: function(data) {
           $( "#daily_program_format" ).html(data);
       }
    });
}); 
JS;
$this->registerJs($script);

?>