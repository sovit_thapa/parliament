<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\program\models\DailyPaper */

$this->title = 'Create Daily Paper';
$this->params['breadcrumbs'][] = ['label' => 'Daily Papers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-paper-create">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
