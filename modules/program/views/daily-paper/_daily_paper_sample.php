
<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
	use app\modules\program\models\DailyPaper;
    use app\components\UtilityFunctions;
?>  

	<div class="row well" style="box-shadow: 3px 3px 3px 4px #888888; min-height: 800px;">
	
	<div class="row text-center">
		<h3><STRONG>व्यवस्थापिका–संसद</STRONG></h3>
		<h4><?= $meeting_details->aadhibasen; ?></h4>
		<h3>दैनिक कार्यसूची</h3>
		<h4><?= $meeting_details->meeting_nepali_year.' '.UtilityFunctions::NepaliMonth($meeting_details->meeting_nepali_month).' '.$meeting_details->meeting_nepali_date.' गते'; ?></h4>
		<h5><?= $meeting_details->nepali_time; ?></h5>
	</div>
		<br /><br />
	<?php
	if(!empty($daily_paper_details)){
		$sn = 1;
		foreach ($daily_paper_details as $activities) {
			$header = $activities->meeting_header;
			if(in_array($activities->meeting_header, ['राष्ट्रपति','प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालय','राष्ट्रपतिको कार्यालय']))
				$header = $activities->meeting_header.'को पत्र';

			$content = '';
			if($activities->state=='अनुमति माग्ने' && $activities->meeting_header == 'व्यवस्थापन कार्य')
        		$content .= 'मा '.$activities->ministry.' मन्त्री श्री '.$activities->minister."ले ः– <br />";
        	$content .= $activities->context;
        	if($header=='व्यवस्थापन कार्य')
        	{
            	$minister_related_daily_paper = DailyPaper::find()->where('meeting_id=:meeting_id AND status=:status AND ministry=:ministry AND minister=:minister AND id!=:id',[':meeting_id'=>$activities->meeting_id,':status'=>'active',':ministry'=>trim($activities->ministry),':minister'=>trim($activities->minister), ':id'=>$activities->id])->orderBy('order_number ASC')->all();
            	if(!empty($minister_related_daily_paper)){
            		foreach ($minister_related_daily_paper as $related_d_p) {
            			$content .= $related_d_p->context;
            		}
            	}
        	}

			?>
			<div class="row">
					<h4 class="text-center"><strong><?= $header; ?></strong></h4>
				<div class="col-md-1"></div>
				<div class="col-md-11" style="font-size: 18px !important;"> <strong><?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp;';?></strong> <?= ltrim($content, '<p>'); ?></div>
			</div>

			<?php
			$sn++;
		}
	}
	?>
	<br />
<div class="row" style="float: right; text-align: justify; margin-right: 10px !important;">
	मनोहरप्रसाद भट्टराई <br />
व्यवस्थापिका–संसदको महासचिव
</div>
</div>