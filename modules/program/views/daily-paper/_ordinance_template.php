<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\components\UtilityFunctions;
use dosamigos\ckeditor\CKEditor;
use app\modules\setting\models\Ministry;


/* @var $this yii\web\View */
/* @var $model app\modules\program\models\DartaDesk */

$this->title = 'Register In Darta Desk';
$this->params['breadcrumbs'][] = ['label' => 'Darta Desks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>



<h4 class="text-center"><STRONG><?= $data_information->type; ?></STRONG></h4>
<?php $form = ActiveForm::begin(['action'=>Yii::$app->homeUrl.'/program/daily-paper/generate-daily-paper']); ?>
<div class="row well">
	<div class="col-md-6">

    	<?= $form->field($model, 'parliament_id')->hiddenInput(['value'=>$data_information->parliament_id])->label(false) ?>

    	<?= $form->field($model, 'meeting_id')->hiddenInput(['value'=>$data_information->meeting_id])->label(false) ?>
        <?= $form->field($model, 'main_type')->hiddenInput(['value'=>$data_information->main_type])->label(false) ?>

    	<div class="row">
    		<h4 class="text-center"><strong>MEETING DETAILS</strong></h4>
            <table class="table table-striped table-bordered" style="font-weight: bold;">
                <tr><td>अधिवेशन </td><td>: <?= $meeting_details ? $meeting_details->adhibasen : ''; ?></td></tr>
                <tr><td>बैठक संख्या </td><td>: <?= $meeting_details ? $meeting_details->_meeting_number : ''; ?></td></tr>
                <tr><td>मिती </td><td>: <?= $meeting_details ? $meeting_details->_nepali_year.'-'.$meeting_details->_nepali_month.'-'.$meeting_details->_nepali_day : ''; ?></td></tr>
                <tr><td>समय </td><td>: <?= $meeting_details ? $meeting_details->_time : ''; ?></td></tr>
            </table>
    	</div>

        <?= $form->field($model, 'ministry')->hiddenInput(['value'=>$data_information->ministry])->label(false); ?>

        <?= $form->field($model, 'state')->hiddenInput(['value'=>$data_information->check_state ])->label(false); ?>

        <?= $form->field($model, 'meeting_header')->hiddenInput(['value'=>$data_information->type])->label(false); ?>

        <?= $form->field($model, 'forum_id')->hiddenInput(['value'=>$data_information->id])->label(false); ?>

	    <?= $form->field($model, 'order_number')->textInput(['value'=>$order_number]) ?>
		
	</div>
	<div class="col-md-6">
		
	    <?= $form->field($model, 'context')->widget(CKEditor::className(), [
	        'options' => ['rows' => 6, 'cols'=>40, 'value'=>$data_information->title],
	        'preset' => 'advance'
	    ])->label('विधेयक/नाम') ?>
	</div>

</div>

    <div class="form-group text-center">
        <?= Html::submitButton('GENEREATE DAILY PAPER', ['class' => 'btn btn-success']) ?>
    </div>
<?php ActiveForm::end(); ?>


