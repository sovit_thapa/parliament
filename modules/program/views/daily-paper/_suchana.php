<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\components\UtilityFunctions;
use app\modules\setting\models\PoliticalParty;
use app\modules\setting\models\ParliamentMember;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\Select2;
/* @var $this yii\web\View */
/* @var $model app\modules\program\models\DartaDesk */
/* @var $form yii\widgets\ActiveForm */
$data = ['अकबाल अहमद शाह','अमेरिका  कुमारी','इन्द्रा  झा','कमलेश्वशर  पूरी गोस्वामी','केदार नन्दरन  चौधरी'];
$parliament_member_list = ParliamentMember::find()->all();
$member_list = array();
if(!empty($parliament_member_list)){
    foreach ($parliament_member_list as $member_info) {
        $party = $member_info->political ? $member_info->political->name : '';
        $member_list[$member_info->id] = $member_info->member_title.' '.$member_info->first_name.' '.$member_info->last_name.' - '.$party;
    }
}
?>

<div class="darta-desk-form">

    <?php $form = ActiveForm::begin(['action'=>Yii::$app->homeUrl.'program/daily-paper/add-statement-section']); ?>
    <div class="row well">
    
        <?php 
        // Tagging support Multiple
        $model->member =  explode(',', $model->member); // initial value
        echo $form->field($model, 'member')->widget(Select2::classname(), [
            'data' => $member_list,
            'options' => ['placeholder' => 'Select a color ...', 'multiple' => true],
            'pluginOptions' => [
                'tags' => true,
                'tokenSeparators' => [',', ' '],
                'maximumInputLength' => 10
            ],
        ])->label('नाम लिस्टहरु');
        ?>

        <?= $form->field($model, 'forum_id')->hiddenInput(['value'=> $darta_id])->label(false) ?>
        <?= $form->field($model, 'statement_type')->hiddenInput(['value'=> $darta_type])->label(false) ?>

    </div>
    <div class="form-group text-center">
        <?= Html::submitButton($btn_title, ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
