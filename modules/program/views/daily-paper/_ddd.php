<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
	use app\modules\program\models\DailyPaper;
	use app\modules\program\models\DartaDesk;
    use app\components\UtilityFunctions;
	use app\modules\setting\models\ParliamentPartyOfficial;
?>  

	<div class="row well" style="box-shadow: 3px 3px 3px 4px #888888; min-height: 800px;">
	<a class="btn btn-primary hidden-print" href="<?= Yii::$app->request->baseUrl.'/program/daily-paper/view-d-p?meeting_number='.$meeting_details->id.'&&exporttoword=true' ?>" target="_blank">Export to Word</a>
	<a class="btn btn-primary hidden-print" href="<?= Yii::$app->request->baseUrl.'/program/daily-paper/view-d-p?meeting_number='.$meeting_details->id.'&&exporttopdf=true' ?>" target="_blank">Export to PDF</a>
	<div class="row text-center">
		<h3><STRONG>व्यवस्थापिका–संसद</STRONG></h3>
		<h4><?= $meeting_details->adhibasen; ?></h4>
		<h3>दैनिक कार्यसूची</h3>
		<h4><?= $meeting_details->_nepali_year.' '.UtilityFunctions::NepaliMonth($meeting_details->_nepali_month).' '.$meeting_details->_nepali_day.' गते'; ?></h4>
		<h5><?= $meeting_details->_time; ?></h5>
	</div>
		<br /><br />
	<form method="post" action="<?= Yii::$app->homeUrl; ?>program/daily-paper/re-arrange-rp">
	<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken(); ?>" />
	
	<?php
	$header_array_section = array_keys($daily_paper_header_array_);
	$sn = 1;
	for ($k=0; $k < sizeof($header_array_section) ; $k++) { 
		$header = $header_array_section[$k];

		if(in_array($header, ['राष्ट्रपति','प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालय','राष्ट्रपतिको कार्यालय'])){
			$header_title = $header.'को पत्र';
			if($header == 'प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालय')
				$header_title = 'प्रधानमन्त्री तथा मन्त्रिपरिषद् कार्यालयको पत्र';
			if($header == 'राष्ट्रपतिको कार्यालय')
				$header_title = 'राष्ट्रपति कार्यालयको पत्र';
		}
		else
			$header_title = $header;

		$internal_values_array = isset($daily_paper_header_array_[$header]) ? $daily_paper_header_array_[$header] : array();


		?>
		<h4 class="text-center"><strong><?= $header_title; ?></strong></h4>
		<div class="row">
		<div class="col-md-1" ></div>
		<div class="col-md-11" style="font-size: 14pt; text-align: justify-all;">
		<?php
		for ($i=0; $i < sizeof($daily_paper_id_array) ; $i++){
			$id = $daily_paper_id_array[$i];
			$previous_id = isset($daily_paper_id_array[$i-1]) ? $daily_paper_id_array[$i-1] : null;
			$next_id = isset($daily_paper_id_array[$i+1]) ? $daily_paper_id_array[$i+1] : null;
			$daily_paper_details = DailyPaper::findOne($id);
			$previous_daily_paper_details = DailyPaper::findOne($previous_id);
			$next_daily_paper_details = DailyPaper::findOne($next_id);
			$ministry = $daily_paper_details->ministry;
			$type = $daily_paper_details->meeting_header;
			if($type == $header){
	        	$ministry_details = ParliamentPartyOfficial::find()->where(['parliament_id'=>UtilityFunctions::SambhidhanSava(),'ministry'=> $ministry, 'post'=>['मन्त्री','प्रधानमन्त्री','उप प्रधानमन्त्री'], 'status'=>'सकृय'])->one();
	            $miniter_name = $ministry_details && $ministry_details->parliamentMember ? "मा० ".$ministry.' '.$ministry_details->post." ".$ministry_details->parliamentMember->member_title.' '.$ministry_details->parliamentMember->first_name.' '.$ministry_details->parliamentMember->last_name : '';
	            if(!empty($next_daily_paper_details) && $next_daily_paper_details->meeting_header == $type && $next_daily_paper_details->ministry == $ministry && (empty($previous_daily_paper_details) || ($previous_daily_paper_details->meeting_header != $type && $previous_daily_paper_details->ministry != $ministry)) ){
	            	?>
	            	<div class="row">
	            		<?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp;'.$miniter_name.' :- '; ?>
	            	</div>
	            	<?php
	            	$sn++;

	            }
	            elseif(!empty($previous_daily_paper_details) && $previous_daily_paper_details->meeting_header == $type && $previous_daily_paper_details->ministry == $ministry){
	            	echo " body only <br />";
	            	
	            }
	            else{
	            	if($daily_paper_details->main_type == 'letter'){
	            		?>
	            		<div class="row" ><?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp;'; ?> सम्माननीय सभामुखले <?= $header; ?>बाट प्राप्त पत्रको व्यहोरा पढेर सुनाउनु हुने । </div>
	            		<?php
	            	}
	            	$sn++;
	            }
			}
		}
		?>

		</div>
		</div>
	<?php
	}
	?>
	<div class="row text-center">
	<button type="submit" class="btn btn-primary">ARRANGE READING PAPER</button></div><br /><br />
<div class="row" style="float: right; text-align: justify; margin-right: 10px !important;">
	मनोहरप्रसाद भट्टराई <br />
व्यवस्थापिका–संसदको महासचिव
</div>
</form>
</div>



