<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
	use app\modules\program\models\DailyPaper;
	use app\modules\program\models\DartaDesk;
    use app\components\UtilityFunctions;
	use app\modules\program\models\Statement;
	use app\modules\setting\models\ParliamentMember;
	use app\modules\setting\models\ParliamentPartyOfficial;
?>  

	<div class="row well" style="box-shadow: 3px 3px 3px 4px #888888; min-height: 800px;">
	<a class="btn btn-primary hidden-print" href="<?= Yii::$app->request->baseUrl.'/program/daily-paper/view-d-p?meeting_number='.$meeting_details->id.'&&exporttoword=true' ?>" target="_blank">Export to Word</a>
	<a class="btn btn-primary hidden-print" href="<?= Yii::$app->request->baseUrl.'/program/daily-paper/view-d-p?meeting_number='.$meeting_details->id.'&&exporttopdf=true' ?>" target="_blank">Export to PDF</a>
	<a class="btn btn-primary hidden-print" href="<?= Yii::$app->request->baseUrl.'/program/daily-paper/view-d-p?meeting_number='.$meeting_details->id.'&&exporttoprint=true' ?>" target="_blank">Print</a>
	<div class="row text-center">
		<h3><STRONG>व्यवस्थापिका–संसद</STRONG></h3>
		<h4><?= $meeting_details->adhibasen; ?></h4>
		<h3>दैनिक कार्यसूची</h3>
		<h4><?= $meeting_details->_nepali_year.' '.UtilityFunctions::NepaliMonth($meeting_details->_nepali_month).' '.$meeting_details->_nepali_day.' गते'; ?></h4>
		<h5><?= $meeting_details->_time; ?></h5>
	</div>
		<br /><br />
	<form method="post" action="<?= Yii::$app->homeUrl; ?>program/daily-paper/re-arrange-rp">
	<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken(); ?>" />
	
	<?php
	$header_array_section = array_keys($daily_paper_header_array_);
	$sn = 1;
	for ($k=0; $k < sizeof($header_array_section) ; $k++) { 
		$header = $header_array_section[$k];

		if(in_array($header, ['राष्ट्रपति','प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालय','राष्ट्रपतिको कार्यालय'])){
			$header_title = $header.'को पत्र';
			if($header == 'प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालय')
				$header_title = 'प्रधानमन्त्री तथा मन्त्रिपरिषद् कार्यालयको पत्र';
			if($header == 'राष्ट्रपतिको कार्यालय')
				$header_title = 'राष्ट्रपति कार्यालयको पत्र';
		}
		else
			$header_title = $header;

		$internal_values_array = isset($daily_paper_header_array_[$header]) ? $daily_paper_header_array_[$header] : array();


		?>
		<h4 class="text-center"><strong><?= $header_title; ?></strong></h4>
		<div class="row">
		<div class="col-md-1" ></div>
		<div class="col-md-11" style="font-size: 14pt; text-align: justify-all;">
		<?php
		for ($i=0; $i < sizeof($daily_paper_id_array) ; $i++){
			$id = $daily_paper_id_array[$i];
			$previous_id = isset($daily_paper_id_array[$i-1]) ? $daily_paper_id_array[$i-1] : null;
			$next_id = isset($daily_paper_id_array[$i+1]) ? $daily_paper_id_array[$i+1] : null;
			$daily_paper_details = DailyPaper::findOne($id);
			$previous_daily_paper_details = DailyPaper::findOne($previous_id);
			$next_daily_paper_details = DailyPaper::findOne($next_id);
			$ministry = $daily_paper_details->ministry;
			$type = $daily_paper_details->meeting_header;
			if($type == $header){
	        	$miniter_name = UtilityFunctions::ActiveMinister($ministry); 
            	if($daily_paper_details->main_type == 'bill' && $daily_paper_details->state == 'अनुमति माग्ने'){
            		$birod_suchana = Statement::find()->where(['forum_id'=>$daily_paper_details->forum_id, 'statement_type'=>'विरोधको सूचना','status'=>1])->one();

                    $member_array = $birod_suchana ?  explode(',', $birod_suchana->member) : array();
                    $first_member = '';
                    $parliament_member_information = isset($member_array[0]) ? ParliamentMember::findOne($member_array[0]) : array();
                    $first_member = !empty($parliament_member_information) ? 'मा० '.$parliament_member_information->member_title.' '.$parliament_member_information->first_name.' '.$parliament_member_information->last_name : '';
            	}


		        $sp_text = '';
		        $suppoter_array = explode(',', $daily_paper_details->suppoter);
		        for ($o=0; $o < sizeof($suppoter_array); $o++) { 
		            if($o == sizeof($suppoter_array)-1)
		                $sp_text .= ' र  '.'मा० '.$suppoter_array[$o];
		            if($o < sizeof($suppoter_array)-1)
		                $sp_text .= ' , '.'मा० '.$suppoter_array[$o];
		        }
		        $sp_text = ltrim($sp_text, ' , ');
	            ?>
	            <?php
	            //header with body 
	            if(!empty($next_daily_paper_details) && $next_daily_paper_details->meeting_header == $type && $next_daily_paper_details->ministry == $ministry && (empty($previous_daily_paper_details) || (!empty($previous_daily_paper_details) && $previous_daily_paper_details->ministry != $ministry))){
	            		$inc_sn = 1;
	            		if(isset($birod_suchana) && !empty($birod_suchana)){
	            			?>
			            	<div class="row">
	            				<?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp;“'.strip_tags($daily_paper_details->context); ?>” लाई प्रस्तुत गर्न अनुमति माग्ने प्रस्ताव माथि <?= $first_member; ?>ले प्रस्तुत गर्नु भएको बिधेयकको बिरोधको सुचनालाई निर्णयार्थ प्रस्तुत गरिने । 
			            	</div>
			            	<div class="row">
			            		<?= UtilityFunctions::NepaliNumber($sn+1).'. &nbsp;&nbsp;'.$miniter_name.'ले ः– '; ?>
			            	</div>
	            			<?php
	            			$sn = $sn+1;
	            		}else{
	            			if($daily_paper_details->main_type == 'report'){
			        		$savapati_name = UtilityFunctions::SavapatiName($daily_paper_details->related_committee);
		            		if($daily_paper_details->meeting_header == 'संवैधानिक आयोगको प्रतिवेदन'){
		            			$presentor = !empty($daily_paper_details->presentor) ? $daily_paper_details->presentor : $savapati_name;
			            		?>

				            	<div class="row">
				            		<?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp;'.$presentor.'ले ः– '; ?>
				            	</div>
			            		<?php
		            		}
		            		else{
		            			?>
			            		<div class="row" ><?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp; '.$daily_paper_details->related_committee; ?>का <?= $savapati_name; ?>ले ः–
			            		</div>
			            		<?php
		            		}
	            			}else{
			            	?>
			            	<div class="row">
			            		<?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp;'.$miniter_name.'ले ः– '; ?>
			            	</div>
				            <?php
				        	}
		        		}
		            ?>
	            	<div class="row">
	            	<div class="col-md-1"></div>
	            	<div class="col-md-11">
	            	<?php
	            	if($daily_paper_details->main_type == 'bill'){
					    if($daily_paper_details->state == 'अनुमति माग्ने'){
					        if($birod_suchana){
					                $context = UtilityFunctions::NepaliNumber($sn).' " '.strip_tags($daily_paper_details->context)."\" लाई प्रस्तुत गर्न अनुमति माग्ने प्रस्ताव माथि ............. ले प्रस्तुत गर्नु भएको बिधेयकको बिरोधको सुचनालाई निर्णयार्थ प्रस्तुत गरिने । <br /> ".UtilityFunctions::NepaliNumber($sn+1).' '.strip_tags($daily_paper_details->context)." प्रस्तुत गर्नुहुने ।";

					        }else{
			            	$_first_char = $inc_sn;
			            	$_second_char = $inc_sn+1;
			            	$inc_sn = $inc_sn + 1;
					        	?>
					        	<div class="row"><?= UtilityFunctions::NepaliCharacter($_first_char).'.&nbsp;&nbsp;&nbsp;'.strip_tags($daily_paper_details->context); ?> लाई प्रस्तुत गर्न अनुमति माग्ने प्रस्ताव प्रस्तुत गर्नुहुने । <br /><?= UtilityFunctions::NepaliCharacter($_second_char).'&nbsp;&nbsp;&nbsp;'.strip_tags($daily_paper_details->context); ?>प्रस्तुत गर्नुहुने ।
					        	</div>
					        	<?php
					        }

					    }

					    if($daily_paper_details->state=='विचार गरियोस्'){
		            		$_first_char = $inc_sn;
			            	$inc_sn = $inc_sn;
					    	?>
					        <div class="row"><?= UtilityFunctions::NepaliCharacter($_first_char).'.&nbsp;&nbsp;&nbsp;“'.strip_tags($daily_paper_details->context); ?> माथि विचार गरियोस्” भनी प्रस्ताव प्रस्तुत गर्न अनुमति दिन्छु ।</div>
					    	<?php
					    }
				    	if($daily_paper_details->state=='दफावार छलफल समितिमा पठाइयोस्'){
		            		$_first_char = $inc_sn;
			            	$inc_sn = $inc_sn + 1;
					    	?>
					        	<div class="row"><?= UtilityFunctions::NepaliCharacter($_first_char).'.&nbsp;&nbsp;&nbsp;“'.strip_tags($daily_paper_details->context); ?> लाई दफावार छलफलको लागि समितिमा पठाईयोस्” भनी प्रस्ताव प्रस्तुत गर्नुहुने ।</div>
					    	<?php
				    	}
				    	if($daily_paper_details->state=='प्रतिवेदन सहित छलफल'){
				    		?>
					        	<div class="row">
					        	<?= UtilityFunctions::NepaliCharacter(1).'.&nbsp;&nbsp;&nbsp;';?> “<?= $daily_paper_details->related_committee; ?> समितिको प्रतिवेदन सहितको <?= strip_tags($daily_paper_details->context); ?> माथि छलफल गरियोस्” भनी प्रस्ताव प्रस्तुत गर्नुहुने । 
					        	<br /><?= UtilityFunctions::NepaliCharacter(2).'.&nbsp;&nbsp;&nbsp;'; ?>“ <?= strip_tags($daily_paper_details->context); ?> लाई पारित गरियोस्” भनी प्रस्ताव प्रस्तुत गर्नुहुने ।
					        	</div>
					        	<?php

				    	}
	            	}
	            	if($daily_paper_details->main_type == 'report'){
	            		?>
	            		<div class="row"><?= UtilityFunctions::NepaliCharacter(1).'.&nbsp;&nbsp;&nbsp;“'.strip_tags($daily_paper_details->context); ?> माथि विचार गरियोस्” पेश गर्नुहुने ।</div>
	            		<?php
	            	}
	            	if($daily_paper_details->main_type == 'aggrement'){
	            		if($daily_paper_details->state=='पेश'){
		            		$_first_char = $inc_sn;
			            	$inc_sn = $inc_sn + 1;
		            		?>
						    <div class="row"><?= UtilityFunctions::NepaliCharacter($_first_char).'.&nbsp;&nbsp;&nbsp;“'.strip_tags($daily_paper_details->context); ?>” माथि संक्षिप्त वक्तव्य दिनुहुँदै सदन समक्ष प्रस्तुत गर्नुहुने ।</div>
		            		<?php
	            		}
	            		if(in_array($daily_paper_details->state,['छलफल','पारित'])){
				            	$_first_char = $inc_sn;
				            	$_second_char = $inc_sn+1;
				            	$inc_sn = $inc_sn + 2;
					        	?>
					        	<div class="row">
					        	<?= UtilityFunctions::NepaliCharacter($_first_char).'.&nbsp;&nbsp;&nbsp;“'.strip_tags($daily_paper_details->context); ?> माथि छलफल गरियोस्” भनी प्रस्ताव प्रस्तुत गर्नुहुने । <br /><?= UtilityFunctions::NepaliCharacter($_second_char).'.&nbsp;&nbsp;&nbsp;“'.strip_tags($daily_paper_details->context); ?>लाई अनुमोदन गरियोस्” भनी प्रस्ताव प्रस्तुत गर्नुहुने ।
					        	</div>
					        	<?php
					        }
					}
	            	?>
	            	</div>
	            	</div>
	            	<?php
	            	$sn++;

	            }
	            //body only
	            elseif(!empty($previous_daily_paper_details) && $previous_daily_paper_details->meeting_header == $type && $previous_daily_paper_details->ministry == $ministry){	
		            	?>

		            	<div class="row">
		            	<div class="col-md-1"></div>
		            	<div class="col-md-11">
		            	<?php
		            	if($daily_paper_details->main_type == 'bill'){
						    if($daily_paper_details->state == 'अनुमति माग्ने'){
						        if(isset($birod_darta_information) && $birod_darta_information){
						                $context = $anu_1.' " '.strip_tags($data_information->title)."\" लाई प्रस्तुत गर्न अनुमति माग्ने प्रस्ताव माथि ".$member_name."ले प्रस्तुत गर्नु भएको बिधेयकको बिरोधको सुचनालाई निर्णयार्थ प्रस्तुत गरिने । <br /> ".$anu_2.' '.strip_tags($data_information->title)." प्रस्तुत गर्नुहुने ।";

						        }else{
					            	$_first_char = $inc_sn+1;
					            	$_second_char = $inc_sn+2;
					            	$inc_sn = $inc_sn + 2;
						        	?>
						        	<div class="row"><?= UtilityFunctions::NepaliCharacter($_first_char).'.&nbsp;&nbsp;&nbsp;'.strip_tags($daily_paper_details->context); ?> लाई प्रस्तुत गर्न अनुमति माग्ने प्रस्ताव प्रस्तुत गर्नुहुने । <br /><?= UtilityFunctions::NepaliCharacter($_second_char).'&nbsp;&nbsp;&nbsp;'.strip_tags($daily_paper_details->context); ?>प्रस्तुत गर्नुहुने ।
						        	</div>
						        	<?php
						        }

						    }

						    if($daily_paper_details->state=='विचार गरियोस्'){
					            	$_first_char = $inc_sn+1;
					            	$inc_sn = $inc_sn + 1;
						    	?>
						        <div class="row"><?= UtilityFunctions::NepaliCharacter($_first_char).'.&nbsp;&nbsp;&nbsp;“'.strip_tags($daily_paper_details->context); ?> माथि विचार गरियोस्” भनी प्रस्ताव प्रस्तुत गर्न अनुमति दिन्छु ।</div>
						    	<?php
						    }
					    	if($daily_paper_details->state=='दफावार छलफल समितिमा पठाइयोस्'){
					            	$_first_char = $inc_sn+1;
					            	$inc_sn = $inc_sn + 1;
						    	?>
						        	<div class="row"><?= UtilityFunctions::NepaliCharacter($_first_char).'.&nbsp;&nbsp;&nbsp;“'.strip_tags($daily_paper_details->context); ?> लाई दफावार छलफलको लागि समितिमा पठाईयोस्” भनी प्रस्ताव प्रस्तुत गर्नुहुने ।</div>
						    	<?php
					    	}
					    	if($daily_paper_details->state=='प्रतिवेदन सहित छलफल'){
					            	$_first_char = $inc_sn+1;
					            	$_second_char = $inc_sn+2;
					            	$inc_sn = $inc_sn + 2;
					    		?>
						        	<div class="row">
						        	<?= UtilityFunctions::NepaliCharacter($_first_char).'.&nbsp;&nbsp;&nbsp;';?> “<?= $daily_paper_details->related_committee; ?> समितिको प्रतिवेदन सहितको <?= strip_tags($daily_paper_details->context); ?> माथि छलफल गरियोस्” भनी प्रस्ताव प्रस्तुत गर्नुहुने । 
						        	<br /><?= UtilityFunctions::NepaliCharacter($_second_char).'.&nbsp;&nbsp;&nbsp;'; ?>“ <?= strip_tags($daily_paper_details->context); ?> लाई पारित गरियोस्” भनी प्रस्ताव प्रस्तुत गर्नुहुने ।
						        	</div>
						        	<?php

					    	}
		            	}
		            	if($daily_paper_details->main_type== 'report'){
			            	$_first_char = $inc_sn+1;
			            	$inc_sn = $inc_sn + 1;
		            		?>
						    <div class="row"><?= UtilityFunctions::NepaliCharacter($_first_char).'.&nbsp;&nbsp;&nbsp;“'.strip_tags($daily_paper_details->context); ?>” पेश गर्नुहुने ।</div>
		            		<?php
		            	}
		            	if($daily_paper_details->main_type== 'aggrement'){

		            		if($daily_paper_details->state=='पेश'){
			            		$_first_char_ = $inc_sn;
				            	$inc_sn = $inc_sn + 1;
			            		?>
							    <div class="row"><?= UtilityFunctions::NepaliCharacter($_first_char_).'.&nbsp;&nbsp;&nbsp;“'.strip_tags($daily_paper_details->context); ?>” माथि संक्षिप्त वक्तव्य दिनुहुँदै सदन समक्ष प्रस्तुत गर्नुहुने ।</div>
			            		<?php
		            		}
		            		if(in_array($daily_paper_details->state,['छलफल','पारित'])){
					            	$_first_char = $inc_sn;
					            	$_second_char = $inc_sn+1;
					            	$inc_sn = $inc_sn + 2;
						        	?>
						        	<div class="row">
						        	<?= UtilityFunctions::NepaliCharacter($_first_char).'.&nbsp;&nbsp;&nbsp;“'.strip_tags($daily_paper_details->context); ?> माथि छलफल गरियोस्” भनी प्रस्ताव प्रस्तुत गर्नुहुने । <br /><?= UtilityFunctions::NepaliCharacter($_second_char).'.&nbsp;&nbsp;&nbsp;“'.strip_tags($daily_paper_details->context); ?>लाई अनुमोदन गरियोस्” भनी प्रस्ताव प्रस्तुत गर्नुहुने ।
						        	</div>
						        	<?php
						        }
		            	}
		            	?>
		            	</div>
		            	</div>
		            	<?php            	
	            }
	            //full body
	            else{
	            	if($daily_paper_details->main_type == 'letter'){
	            		?>
	            		<div class="row" ><?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp;'; ?> सम्माननीय सभामुखले <?= $header; ?>बाट प्राप्त पत्रको व्यहोरा पढेर सुनाउनु हुने । </div>
	            		<?php
	            	}
	            	if($daily_paper_details->main_type == 'other' && $daily_paper_details->meeting_header =='शोक प्रस्ताव'){
	            		?>
	            		<div class="row" ><?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp;'; ?> सम्माननीय सभामुखले निम्नलिखित शोक प्रस्ताव प्रस्तुत गर्नुहुने ः– <br />  &nbsp;&nbsp; &nbsp;&nbsp; <?= $daily_paper_details->context;?>
	            		</div>
	            		<?php
	            	}
	            	if($daily_paper_details->main_type == 'proposal' && $daily_paper_details->meeting_header =='जरुरी सार्वजनिक महत्वको प्रस्ताव'){
	            		?>
	            		<div class="row" ><?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp;'; ?> जरुरी सार्वजनिक महत्वको प्रस्तावका प्रस्तावक मा० <?= $daily_paper_details->presentor; ?>ले “<?= strip_tags($daily_paper_details->context);?>”  भन्ने विषयको जरुरी सार्वजनिक महत्वको प्रस्ताव प्रस्तुत गर्नुहुने ।
	            		<br /> उक्त प्रस्तावको <?= $sp_text; ?>ले समर्थन गर्नुहुने ।
	            		</div>
	            		<?php
	            	}
	            	if($daily_paper_details->main_type == 'proposal' && $daily_paper_details->meeting_header =='संकल्प प्रस्ताव'){?>
	            		<div class="row" ><?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp; '.$daily_paper_details->presentor; ?>ले निम्नलिखित व्यहोराको संकल्प प्रस्ताव प्रस्तुत गर्नुहुने ः– <br />“<?= strip_tags($daily_paper_details->context);?>”  <br />
	            		उक्त प्रस्तावको समर्थकहरु <?= $sp_text; ?>ले प्रस्तावको समर्थन गर्नुहुने ।
	            		</div>
	            		<?php
	            	}
	            	if($daily_paper_details->main_type == 'proposal' && $daily_paper_details->meeting_header =='ध्यानाकर्षण प्रस्ताव'){?>
	            		<div class="row" ><?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp; '.$daily_paper_details->presentor; ?>'ले " <?= strip_tags($daily_paper_details->context); ?>" भन्ने जरुरी सार्वजनिक महत्वको विषयमाथि <?= $miniter_name; ?> को ध्यानाकर्षण गर्नुहुने ।
	            		</div><br />
	            		<?php
	            	}
	            	if($daily_paper_details->main_type == 'report'){
			        	$savapati_name = UtilityFunctions::SavapatiName($daily_paper_details->related_committee);
	            		if($daily_paper_details->meeting_header == 'संवैधानिक आयोगको प्रतिवेदन'){
	            			$presentor = !empty($daily_paper_details->presentor) ? $daily_paper_details->presentor : $savapati_name;
		            		?>
		            		<div class="row" ><?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp; '.$presentor; ?>ले " <?= strip_tags($daily_paper_details->context); ?>" पेश गर्नुहुने ।
		            		</div><br />
		            		<?php
	            		}
	            		else{
	            			?>
		            		<div class="row" ><?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp; '.$daily_paper_details->related_committee; ?>का <?= $savapati_name; ?>ले “<?= strip_tags($daily_paper_details->context); ?>” पेश गर्नुहुने ।
		            		</div><br />
		            		<?php
	            		}
	            	}
	            	if($daily_paper_details->main_type == 'aggrement' && $daily_paper_details->state == 'पेश'){
            			?>
	            		<div class="row" ><?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp;'.$miniter_name.'ले “'.strip_tags($daily_paper_details->context); ?> ” माथि संक्षिप्त वक्तव्य दिनुहुँदै सदन समक्ष प्रस्तुत गर्नुहुने ।
	            		</div><br />
	            		<?php
	            	}
	            	if($daily_paper_details->main_type == 'aggrement' && in_array($daily_paper_details->state,['छलफल','पारित'])){
			        	?>
			        	<div class="row">
        				<?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp;'.$miniter_name.'ले ः– '; ?>
			        	</div>
			        	<div class="row">
			        	<div class="col-md-1"></div>
			        	<div class="col-md-11"><?= UtilityFunctions::NepaliCharacter(1).'.&nbsp;&nbsp;&nbsp;“'.strip_tags($daily_paper_details->context); ?> माथि छलफल गरियोस्” भनी प्रस्ताव प्रस्तुत गर्नुहुने । <br /><?= UtilityFunctions::NepaliCharacter(2).'.&nbsp;&nbsp;&nbsp;“'.strip_tags($daily_paper_details->context); ?>लाई अनुमोदन गरियोस्” भनी प्रस्ताव प्रस्तुत गर्नुहुने ।
			        	</div>
			        	</div><br />
			        	<?php
	            	}

	            	if($daily_paper_details->main_type == 'ordinance'){
	            		if($daily_paper_details->state == 'पेश'){
	            		?>
	            		<div class="row"><?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp;'.$miniter_name.'ले “'.strip_tags($daily_paper_details->context); ?> लाई पेश गर्न अनुमति” माग्ने प्रस्ताव प्रस्तुत गर्नुहुने ।</div>
	            		<?php
	            		}
	            	}
	            	if($daily_paper_details->main_type == 'bill'){
					    if($daily_paper_details->state == 'अनुमति माग्ने'){

	            		if(isset($birod_suchana) && !empty($birod_suchana)){
	            			?><br />
			            	<div class="row">
	            				<?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp;“'.strip_tags($daily_paper_details->context); ?>” लाई प्रस्तुत गर्न अनुमति माग्ने प्रस्ताव माथि <?= $first_member; ?>ले प्रस्तुत गर्नु भएको बिधेयकको बिरोधको सुचनालाई निर्णयार्थ प्रस्तुत गरिने । 
			            	</div>
			            	<br />
			            	<div class="row">
			            	<?= UtilityFunctions::NepaliNumber($sn+1).'.&nbsp;&nbsp;&nbsp;“'.strip_tags($daily_paper_details->context); ?>लाई प्रस्तुत गर्न अनुमति” माग्ने प्रस्ताव प्रस्तुत गर्नुहुने ।
			            	</div><br />
	            			<?php
	            			$sn = $sn+1;
	            		}else{
					        	?>
					        	<div class="row">
	            				<?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp;'.$miniter_name.'ले ः– '; ?>
					        	</div>
					        	<div class="row">
					        	<div class="col-md-1"></div>
					        	<div class="col-md-11"><?= UtilityFunctions::NepaliCharacter(1).'.&nbsp;&nbsp;&nbsp;'.strip_tags($daily_paper_details->context); ?> लाई प्रस्तुत गर्न अनुमति माग्ने प्रस्ताव प्रस्तुत गर्नुहुने । <br /><?= UtilityFunctions::NepaliCharacter(2).'.&nbsp;&nbsp;&nbsp;'.strip_tags($daily_paper_details->context); ?>प्रस्तुत गर्नुहुने ।
					        	</div>
					        	</div><br />
					        	<?php
					        }

					    }
					    if($daily_paper_details->state=='विचार गरियोस्'){
					    	?>
					        	<div class="row"><?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp;'.$miniter_name.'ले “'.strip_tags($daily_paper_details->context); ?> माथि विचार गरियोस्” भनी प्रस्ताव प्रस्तुत गर्न अनुमति दिन्छु ।</div><br />
					    	<?php
					    }
				    	if($daily_paper_details->state=='दफावार छलफल समितिमा पठाइयोस्'){
					    	?>
					        	<div class="row"><?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp;'.$miniter_name.'ले “'.strip_tags($daily_paper_details->context); ?> लाई दफावार छलफलको लागि समितिमा पठाईयोस्” भनी प्रस्ताव प्रस्तुत गर्नुहुने ।</div><br />
					    	<?php
				    	}
				    	if($daily_paper_details->state=='प्रतिवेदन सहित छलफल'){
				    		?>
					        	<div class="row">
	            				<?= UtilityFunctions::NepaliNumber($sn).'. &nbsp;&nbsp;'.$miniter_name.'ले ः– '; ?>
					        	</div>
					        	<div class="row">
					        	<div class="col-md-1"></div>
					        	<div class="col-md-11">
					        	<?= UtilityFunctions::NepaliCharacter(1).'.&nbsp;&nbsp;&nbsp;';?> “<?= $daily_paper_details->related_committee; ?> समितिको प्रतिवेदन सहितको <?= strip_tags($daily_paper_details->context); ?> माथि छलफल गरियोस्” भनी प्रस्ताव प्रस्तुत गर्नुहुने । 
					        	<br /><?= UtilityFunctions::NepaliCharacter(2).'.&nbsp;&nbsp;&nbsp;'; ?>“ <?= strip_tags($daily_paper_details->context); ?> लाई पारित गरियोस्” भनी प्रस्ताव प्रस्तुत गर्नुहुने ।
					        	</div>
					        	</div><br />
					        	<?php

				    	}
	            	}
	            	$sn++;
	            }
			}
		}
		?>

		</div>
		</div>
	<?php
	}
	?><br />
	<div class="row text-center">
	<button type="submit" class="btn btn-primary hidden-print">ARRANGE READING PAPER</button></div><br /><br />
<div class="row" style="float: right; text-align: justify; margin-right: 10px !important;">
	<?= UtilityFunctions::ActiveMahasachib(); ?> <br />
व्यवस्थापिका–संसदको महासचिव
</div>
</form>
</div>



