<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\program\services\DailyPaperService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'TODAY DAILY PAPER';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-paper-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'context',
                'label'=>'Meeting',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->context;
                }
            ],
            'minister',
            'related_committee',
            'order_number',
            'state',
            'status',
            ['class' => 'yii\grid\ActionColumn',
                'header' => '&nbsp;&nbsp;&nbsp;&nbsp;ACTION&nbsp;&nbsp;&nbsp;&nbsp;LINK&nbsp;&nbsp;&nbsp;&nbsp;',
                'template' => '{button1}{button2}{new_action3}{new_action4}',
                'buttons' => [
                    'button1' => function ($url, $model) {
                        if($model->status=='active'){
                        return Html::a('<span class="glyphicon glyphicon-eye-open btn btn-primary btn-xs"></span>', Url::to(['/program/daily-paper/view/?id='.$model->id]), [
                            'title' => Yii::t('app', 'VIEW DETAILS'),
                        ]);
                        }
                    },
                    'button2' => function ($url, $model) {
                        if($model->status=='active'){
                        return Html::a('&nbsp; <span class="glyphicon glyphicon-book btn btn-primary btn-xs"></span>', Url::to(['/program/reading-paper/generate-r-p/?id='.$model->id]), [
                            'title' => Yii::t('app', 'GENERATE READING PAPER'),
                        ]);
                        }

                    },
                    'new_action4' => function ($url, $model) {
                        if($model->status=='active'){
                            $type = 'disable';
                            $icon = 'glyphicon glyphicon-trash';
                            $title = 'DISABLE DAILY PAPER';
                        }else{
                            $type = 'active';
                            $icon = 'glyphicon glyphicon-ok';
                            $title = 'ACTIVE DAILY PAPER';
                        }

                        return Html::a('&nbsp; <span class="'.$icon.' btn btn-primary btn-xs"></span>', Url::to(['/program/reading-paper/daily-paper-processing/?id='.$model->id.'&type='.$type]), [
                            'title' => Yii::t('app', $title),
                        ]);
                    },

                ],
            ],
        ],
    ]); ?>
</div>
