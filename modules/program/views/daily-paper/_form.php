<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\components\UtilityFunctions;
use dosamigos\ckeditor\CKEditor;
use app\modules\setting\models\Ministry;


/* @var $this yii\web\View */
/* @var $model app\modules\program\models\DartaDesk */

$this->title = 'Register In Darta Desk';
$this->params['breadcrumbs'][] = ['label' => 'Darta Desks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h4 class="text-center"><STRONG><?= $model->meeting_header; ?></STRONG></h4>
<?php $form = ActiveForm::begin(); ?>
<div class="row well">
    <div class="col-md-6">

        <div class="row">
            <h4 class="text-center"><strong>MEETING DETAILS</strong></h4>
             <table class="table table-striped table-bordered" style="font-weight: bold;">
                <tr><td>संविधान </td><td> : <?= $model->parliament ? $model->parliament->name :  '' ;?></td></tr>
               
                <tr><td>मन्त्रालय </td><td> : <?= $model->ministry; ?></td></tr>
                <tr><td>विधेयक बिषय</td><td> : <?= $model->state; ?></td></tr>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        
        <?= $form->field($model, 'context')->widget(CKEditor::className(), [
            'options' => ['rows' => 6, 'cols'=>40, 'value'=>$model->context],
            'preset' => 'advance'
        ])->label('विधेयक/नाम') ?>
    </div>

</div>

    <div class="form-group text-center">
        <?= Html::submitButton('GENEREATE DAILY PAPER', ['class' => 'btn btn-success']) ?>
    </div>
<?php ActiveForm::end(); ?>


