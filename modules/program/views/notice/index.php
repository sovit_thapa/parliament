<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\program\services\NoticeService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notice-index">

    <h4 class="text-center"><strong><?= Html::encode($this->title) ?></strong></h4>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Generate Notice', ['create?meeting_number='.$model->id], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',

            [
                'attribute' => 'meeting_id',
                'format' => 'html',
                'label' => 'Meeting Number',
                'value' => function ($data) {
                    return $data->meetingDetails ? $data->meetingDetails->_meeting_number :  '';
                }
            ],
            'text:ntext',
            'notice_date',
            'notice_type',
            // 'is_post',
            // 'created_by',
            // 'created_date',

            [
            'header'=>'ACTION',
            'class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
