<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\Notice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notice-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row well">

    <div class="col-md-5" style="box-shadow: 5px 5px 5px 5px #888888; padding: 10px;">
        <!-- Map will be created here -->
        <table class="table table-striped table-bordered" style="font-weight: bold;">
          <tr>
            <th>अधिवेशन</th>
            <th>बैठक संख्या</th>
            <th>मिती(AD)</th>
            <th>मिती(BS)</th>
            <th>समय</th>
          </tr>
          <tr>
            <td><?= $meeting_details->adhibasen; ?></td>
            <td><?= $meeting_details->_meeting_number; ?></td>
            <td><?= $meeting_details->_date_time; ?></td>
            <td><?= $meeting_details->_nepali_year.'-'.$meeting_details->_nepali_month.'-'.$meeting_details->_nepali_day; ?></td>
            <td><?= $meeting_details->_time; ?></td>
          </tr>
        </table>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-5" style="box-shadow: 5px 5px 5px 5px #888888; padding: 10px;">

    <?= $form->field($model, 'meeting_id')->hiddenInput(['value'=>$meeting_number])->label(false); ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

   <!--  <?= $form->field($model, 'notice_date')->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '9999-99-99',
]); ?> -->

    <?= $form->field($model, 'notice_type')->dropDownList([ 'end_meeting' => 'End Meeting', 'cancel_meeting' => 'Cancel Meeting', ], ['prompt' => '']) ?>

    <?php
    if(!$model->isNewRecord)
        echo $form->field($model, 'is_post')->dropDownList([ 1 => 'ACTIVE', '0' => 'Cancel', ]); ?>

   <!--  <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'created_date')->textInput() ?> -->
    </div>
    </div>

    <div class="form-group text-center">
        <?= Html::submitButton('SAVE NOTICE', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
