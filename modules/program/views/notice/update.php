<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\Notice */

$this->title = 'Update Notice: ' ;
$this->params['breadcrumbs'][] = ['label' => 'Notices', 'url' => ['create?meeting_number='.$model->meeting_id]];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="notice-update">

    <h4 class="text-center"><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'meeting_details' =>$meeting_details, 
        'meeting_number'=>$meeting_number,
    ]) ?>

</div>
