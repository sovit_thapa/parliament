<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\Notice */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Notices', 'url' => ['create?meeting_number='.$model->meeting_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notice-view">

    <h4 class="text-center">UPDATE NOTICE FOR MEETING : <?= $model->meetingDetails ? $model->meetingDetails->_meeting_number :  ''; ?></h4>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute' => 'meeting_id',
                'format' => 'html',
                'label' => 'Meeting Number',
                'value' => function ($model) {
                    return $model->meetingDetails ? $model->meetingDetails->_meeting_number :  '';
                }
            ],
            [
                'attribute' => 'text',
                'format' => 'html',
                'label' => 'TEXT',
                'value' => function ($model) {
                    return $model->text;
                }
            ],
            'notice_date',
            'notice_type',
            [
                'attribute' => 'is_post',
                'value' => function ($data) {
                    return $data->is_post == 1 ? 'YES' : 'NO';
                }
            ],
            'created_by',
            'created_date',
        ],
    ]) ?>

</div>
