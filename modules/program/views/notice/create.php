<?php

use yii\helpers\Html;

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\Notice */

$this->title = 'Generate Notice';
$this->params['breadcrumbs'][] = ['label' => 'Notices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notice-create">

    <h4 class="text-center"><?= Html::encode($this->title) ?></h4><hr />

    <?= $this->render('_form', [
        'model' => $model,'meeting_details' =>$meeting_details, 'meeting_number'=>$meeting_number
    ]) ?>

</div>
<h4 class="text-center"> NOTICE LIST</h4>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'text',
                'format' => 'html',
                'label' => 'TEXT',
                'value' => function ($data) {
                    return $data->text;
                }
            ],
            'notice_date',
            'notice_type',
            [
                'attribute' => 'is_post',
                'value' => function ($data) {
                    return $data->is_post == 1 ? 'YES' : 'NO';
                }
            ],
            // 'created_by',
            // 'created_date',

            [
            'header'=>'ACTION',
            'class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>