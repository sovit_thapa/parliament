<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\program\services\DartaDeskService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = !empty($title) ? $title : 'DARTA LIST';
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['darta-desk/index/?section='.$section]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="darta-desk-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />
    <?php  echo $this->render('_search', ['model' => $searchModel, 'hide_type' => $hide_type,'hide_ministry' => $hide_ministry, 'type' => $type, 'title' => $title,'state_type' => $state_type, 'state_display'=>$state_display,'section'=>$section]); ?>

    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'target'=>'TARGET_SELF',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'type',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->type;
                },
                'visible' => !$hide_type ,
            ],
            [
                'attribute' => 'title',
                'format' => 'html',
                'label' => $label,
                'value' => function ($data) {
                    return $data->title;
                }
            ],

            [
                'attribute' => 'ministry',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->ministry ? $data->ministry : '';
                },
                'visible' => !$hide_ministry ,
            ],
            [
                'attribute' => 'minister',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->minister ? $data->minister : '';
                },
                'visible' => !$hide_ministry ,
            ],
            [
                'attribute' => 'related_committee',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->related_committee ? $data->related_committee : '';
                },
                'visible' => !$hide_ministry ,
            ],
            [
                'attribute' => 'check_state',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->check_state ? $data->check_state : '';
                },
                'visible' => $state_display ,
            ],
            [
                'attribute' => 'updated_date',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->updated_date ? $data->updated_date : $data->created_date;
                },
                'visible' => $date_dispaly ,
            ],
        ],
        'filename' => $title.date('Y-m-d'),
        'pdfLibraryPath'=>'@vendor/kartik-v/mpdf',
        'onInitSheet' => function (PHPExcel_Worksheet $sheet, $grid) {
            $sheet->getDefaultStyle()->applyFromArray(['borders' => [ 'allborders' => ['style' => PHPExcel_Style_Border::BORDER_THIN ]]]);
        },
    ]); ?>




    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'type',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->type;
                },
                'visible' => !$hide_type ,
            ],
            [
                'attribute' => 'title',
                'format' => 'html',
                'label' => $label,
                'value' => function ($data) {
                    return $data->title;
                }
            ],

            [
                'attribute' => 'ministry',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->ministry ? $data->ministry : '';
                },
                'visible' => !$hide_ministry ,
            ],
            [
                'attribute' => 'minister',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->minister ? $data->minister : '';
                },
                'visible' => !$hide_ministry ,
            ],
            [
                'attribute' => 'related_committee',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->related_committee ? $data->related_committee : '';
                },
                'visible' => !$hide_ministry ,
            ],
            [
                'attribute' => 'check_state',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->check_state ? $data->check_state : '';
                },
                'visible' => $state_display ,
            ],
            [
                'attribute' => 'updated_date',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->updated_date ? $data->updated_date : $data->created_date;
                },
                'visible' => $date_dispaly ,
            ],

            [
                'header' => 'LINKSECTION',
                'class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
