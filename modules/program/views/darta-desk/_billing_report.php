<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\DartaDesk */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row well">

  <H4 class="text-center"><STRONG>व्यवस्थापिका–संसदमा विधेयक सम्बन्धमा भए गरेका कारवाहीको विवरण</STRONG></H4>

   <?= Html::a('EXPORT TO CSV', Yii::$app->request->baseUrl.'/'.Yii::$app->controller->module->id.'/'.Yii::$app->controller->id.'/'.Yii::$app->controller->action->id.'?type='.Yii::$app->request->queryParams['type'].'&export=csv', ['class'=>'btn btn-info pull-right hidden-print']) ?>

  <table style="border: 2px solid #000;" class="table">
    <thead><?php
      for ($i=0; $i < sizeof($column) ; $i++) { 
        ?>
        <th style="border: 2px solid #000;"><?= $column[$i]; ?></th>
        <?php
      }
      ?></thead>
      <?php
      for ($k=0; $k < sizeof($billing_information) ; $k++) { 
        ?>

        <tr><?php
          for ($l=0; $l < sizeof($column) ; $l++) { 
            $column_name = $column[$l];
            ?>
            <td style="border: 2px solid #000;"><?= $billing_information[$k][$column_name]; ?></td>
            <?php
          }
          ?>

        </tr>
        <?php
      }
      ?>
    </table>
  </div>