<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\DartaDesk */

$this->title = $title. ' :  UPDATE ';
$this->params['breadcrumbs'][] = ['label' => 'Darta Desks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => substr(strip_tags($model->title),0, 50), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="darta-desk-update">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />

    <?= $this->render('_form', [
        'model' => $model,'type'=>$type,'title'=>$title, 'hide_type' => $hide_type, 'hide_ministry' => $hide_ministry, 'label'=>$label,'bitaran_miti' => $bitaran_miti,'pesh_miti' => $pesh_miti,'drata_miti'=>$drata_miti, 'section'=>$section,'suppoter'=>$suppoter, 'presentor'=>$presentor
            ]) ?>

</div>



