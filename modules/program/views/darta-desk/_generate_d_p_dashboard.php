<?php

use yii\helpers\Html;

use app\components\UtilityFunctions;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\DartaDesk */

$this->title = 'DAILY PAPER DASHBOARD';
$this->params['breadcrumbs'][] = $this->title;
?>

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />
    <div class="row">
          <a href="#" class="small-box-footer">
          <!-- small box -->
          <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-question-circle"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">प्रत्यक्ष प्रशन </span>
              <span class="info-box-number">------</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        </a>
          <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/d-p-sample/?section=letter" >
          <!-- small box -->
          <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-envelope"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">पत्र</span>
              <span class="info-box-number">------</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        </a>

            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/d-p-sample/?section=bill" class="small-box-footer">
            <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-bars"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">विधेयक</span>
                <span class="info-box-number">------</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div></a>


            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/d-p-sample/?section=aggrement" class="small-box-footer">
            <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-flag"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">सन्धि / महासन्धि / सम्झौता</span>
                <span class="info-box-number">------</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div></a>
</div>
<hr/>




     <div class="row">
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/d-p-sample/?section=report" class="small-box-footer">
            <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-line-chart"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">प्रतिवेदन पेश</span>
                <span class="info-box-number">------</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div></a>
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/d-p-sample/?section=ordinance" class="small-box-footer">
            <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-book"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">अध्यादेश </span>
                <span class="info-box-number">------</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div></a>

            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/d-p-sample/?section=proposal" class="small-box-footer">
            <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-file-text" aria-hidden="true"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">प्रस्तावहरु </span>
                <span class="info-box-number">------</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div></a>
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/d-p-sample/?section=other" class="small-box-footer">
            <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-database" aria-hidden="true"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">अन्य </span>
                <span class="info-box-number">------</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div></a>


    </div><hr />