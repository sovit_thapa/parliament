<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\program\models\DartaDesk */

$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['/program/darta-desk/index/?section='.$section]];
$this->params['breadcrumbs'][] = 'दर्ता गर्नुस ';
?>
<div class="darta-desk-create">

    <h3 class="text-center"><STRONG><?= Html::encode($this->title) ?></STRONG></h3><hr />

    <?= $this->render('_form', [
        'model' => $model,'type'=>$type,'title'=>$title, 'hide_type' => $hide_type, 'hide_ministry' => $hide_ministry, 'label'=>$label,'bitaran_miti' => $bitaran_miti,'pesh_miti' => $pesh_miti,'drata_miti'=>$drata_miti, 'section'=>$section,'suppoter'=>$suppoter, 'presentor'=>$presentor
            
    ]) ?>

</div>