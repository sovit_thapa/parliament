<?php

use yii\helpers\Html;

use app\components\UtilityFunctions;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\DartaDesk */

$this->title = 'DARTA DASHBOARD';
$this->params['breadcrumbs'][] = ['label' => 'Darta Desks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />
    <div class="row">

        <div class="col-lg-3 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua" style="box-shadow: 10px 10px 5px #888888; min-height: 200px! important;">
        <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?type=letter" >
            <div class="inner" style="color: white;"><br />
              <h3 class="text-center">पत्र दर्ता </h3>
              
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            </a><br /><br />
            <hr />
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/index/?type=letter" class="small-box-footer">SEARCH SECTION  &nbsp;&nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua" style="box-shadow: 10px 10px 5px #888888; min-height: 200px! important;">
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?type=bill" >
            <div class="inner" style="color: white;"><br /> 
              <h3 class="text-center">विधेयक</h3>
              
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            </a>
            <br /><br /><hr />
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/index/?type=bill" class="small-box-footer">SEARCH SECTION  &nbsp;&nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>


        <div class="col-lg-3 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua" style="box-shadow: 10px 10px 5px #888888; min-height: 200px! important;">

            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?type=aggrement" >
            <div class="inner" style="color: white;"><br /> 
              <h3 class="text-center">सन्धि / महासन्धि / <br /> सम्झौता</h3>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div></a>
            <br /><hr />
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/index/?type=aggrement" class="small-box-footer">SEARCH SECTION  &nbsp;&nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua" style="box-shadow: 10px 10px 5px #888888; min-height: 200px! important;">
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?type=report" >
            <div class="inner" style="color: white;"><br /> 
              <h3 class="text-center">प्रतिवेदन पेश</h3>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div></a><br /><br /><hr />
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/index/?type=report" class="small-box-footer">SEARCH SECTION  &nbsp;&nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
    </div><hr />




     <div class="row">

        <div class="col-lg-3 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua" style="box-shadow: 10px 10px 5px #888888; min-height: 200px! important;">
        <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?type=rule" >
            <div class="inner" style="color: white;"><br />
              <h3 class="text-center">नीति तथा कार्यक्रम </h3>
              
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            </a><br /><br />
            <hr />
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/index/?type=rule" class="small-box-footer">SEARCH SECTION  &nbsp;&nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>



        <div class="col-lg-3 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua" style="box-shadow: 10px 10px 5px #888888; min-height: 200px! important;">
        <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?type=income" >
            <div class="inner" style="color: white;"><br />
              <h3 class="text-center">राजस्व व्ययको <br />बार्षिक अनुमान</h3>
              
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            </a><br />
            <hr />
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/index/?type=income" class="small-box-footer">SEARCH SECTION  &nbsp;&nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        

        <div class="col-lg-3 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua" style="box-shadow: 10px 10px 5px #888888; min-height: 200px! important;">
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?type=ordinance" >
            <div class="inner" style="color: white;"><br /> 
              <h3 class="text-center">अध्यादेश</h3>
              
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            </a>
            <br /><br /><hr />
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/index/?type=ordinance" class="small-box-footer">SEARCH SECTION  &nbsp;&nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>


        <div class="col-lg-3 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua" style="box-shadow: 10px 10px 5px #888888; min-height: 200px! important;">

            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?type=proposal" >
            <div class="inner" style="color: white;"><br /> 
              <h3 class="text-center">प्रस्तावहरु</h3>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div></a>
            <br /><br /><hr />
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/index/?type=proposal" class="small-box-footer">SEARCH SECTION  &nbsp;&nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

    </div><hr />


    <div class="row">
    
        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua" style="box-shadow: 10px 10px 5px #888888; min-height: 200px! important;">
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?type=election" >
            <div class="inner" style="color: white;"><br /> 
              <h3 class="text-center">निर्वाचन</h3>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div></a><br /><br /><hr />
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/index/?type=election" class="small-box-footer">SEARCH SECTION  &nbsp;&nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua" style="box-shadow: 10px 10px 5px #888888; min-height: 200px! important;">
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?type=other" >
            <div class="inner" style="color: white;"><br /> 
              <h3 class="text-center">अन्य</h3>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div></a><br /><br /><hr />
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/index/?type=other" class="small-box-footer">SEARCH SECTION  &nbsp;&nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua" style="box-shadow: 10px 10px 5px #888888; min-height: 200px! important;">

            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?type=direct-question" >
            <div class="inner" style="color: white;"><br /> 
              <h3 class="text-center">प्रत्यक्ष प्रस्नहरू</h3>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div></a>
            <br /><br /><hr />
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/index/?type=direct-question" class="small-box-footer">SEARCH SECTION  &nbsp;&nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

    </div>