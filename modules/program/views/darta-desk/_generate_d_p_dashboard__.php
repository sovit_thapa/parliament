<?php

use yii\helpers\Html;

use app\components\UtilityFunctions;

/* @var $this yii\web\View */ 
/* @var $model app\modules\program\models\DartaDesk */

$this->title = 'READY FOR DAILY PAPER DASHBOARD';
$this->params['breadcrumbs'][] = ['label' => 'Darta Desks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />

    <div class="row">

        <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/d-p-sample" >
          <div class="col-lg-12 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-aqua" style="box-shadow: 10px 10px 5px #888888; min-height: 200px! important;">
              <div class="inner" style="color: white;"><br />
                <h3 class="text-center">ACTIVE LIST READY FOR DAILY PAPER  </h3>
                
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div><br /><br />
              <hr />
              <div class="small-box-footer">SEARCH SECTION  &nbsp;&nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></div>
            </div>
          </div>
        </a>
    </div>
    <div class="row">

        <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/d-p-sample/?type=letter" >
          <div class="col-lg-6 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-aqua" style="box-shadow: 10px 10px 5px #888888; min-height: 200px! important;">
              <div class="inner" style="color: white;"><br />
                <h3 class="text-center">पत्र दर्ता </h3>
                
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div><br /><br />
              <hr />
              <div class="small-box-footer">SEARCH SECTION  &nbsp;&nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></div>
            </div>
          </div>
        </a>

        <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/d-p-sample/?type=bill" >
          <div class="col-lg-6 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-aqua" style="box-shadow: 10px 10px 5px #888888; min-height: 200px! important;">
              <div class="inner" style="color: white;"><br /> 
                <h3 class="text-center">व्यवस्थापन कार्य</h3>
                
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <br /><br /><hr />
              <div class="small-box-footer">SEARCH SECTION  &nbsp;&nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></div>
            </div>
          </div>
        </a>
    </div><hr />
    <div class="row">

      <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/d-p-sample/?type=aggrement" >
        <div class="col-lg-6 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua" style="box-shadow: 10px 10px 5px #888888; min-height: 200px! important;">

            <div class="inner" style="color: white;"><br /> 
              <h3 class="text-center">सन्धि / महासन्धि / सम्झौता</h3>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <br /><br /><hr />
            <div class="small-box-footer">SEARCH SECTION  &nbsp;&nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></div>
          </div>
        </div>
      </a>

      <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/d-p-sample/?type=sympathy" >
        <div class="col-lg-6 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua" style="box-shadow: 10px 10px 5px #888888; min-height: 200px! important;">
            <div class="inner" style="color: white;"><br /> 
              <h3 class="text-center">शोक प्रस्ताव</h3>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div><br /><br /><hr />
            <div class="small-box-footer">SEARCH SECTION  &nbsp;&nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></div>
          </div>
        </div>
      </a>
    </div>