<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\modules\setting\models\Ministry;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\program\services\DartaDeskService */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="darta-desk-search">
<?php
    $action_ = isset($action) ? $action : 'index';
?>
    <?php $form = ActiveForm::begin([
        'action' => [$action_],
        'method' => 'get',
    ]); ?>
    <div class="row well">
            <?= $form->field($model, 'main_type')->hiddenInput(['value'=>$section])->label(false) ?>
        <div class="col-md-6">
          <?php
        if(!$hide_type){
            ?>

            <?= $form->field($model, 'type')->dropDownList($type); ?>
            <?php
        }else{
            $value = isset($type[0]) ? $type[0] : '';
            ?>
            <?= $form->field($model, 'type')->hiddenInput(['value'=>$value])->label(false) ?>
            <?php
        }
        if(!$hide_ministry){
            ?>

        <?= $form->field($model, 'ministry')->dropDownList(ArrayHelper::map(Ministry::find()->all(), 'title', 'title'),
        [
        'prompt' => 'Select Ministry',

        ]);
        ?>
            <?php
        }
        if($hide_type && $hide_ministry){
            ?>
                <br /><br /><H3 class="text-center"><STRONG>WELCOME TO REGISTER SECTION</STRONG> </H3>
            <?php
        }
    ?>


        </div>
        <?php
            if($state_display){
        ?>
        <div class="col-md-6">
        <?= $form->field($model, 'check_state')->dropDownList($state_type); ?>

        </div>
        <?php
            }
        ?>

    </div>


    <?php // echo $form->field($model, 'related_committee') ?>

    <?php // echo $form->field($model, 'remarks') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'updated_date') ?>

    <div class="form-group text-center">
        <?= Html::submitButton('Search Information', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset Information', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
