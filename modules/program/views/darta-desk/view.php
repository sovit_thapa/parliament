<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\DartaDesk */

$this->title = $darta_information->main_type;
$this->params['breadcrumbs'][] = ['label' => 'Darta Desks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<br /><br />
<div class="darta-desk-view">
    <p class="text-center">
        <?= Html::a('Update Regiter Data', ['update', 'id' => $darta_information->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete Regiter Data', ['delete', 'id' => $darta_information->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php
        if ($darta_information) {
            $row_span = $hide_ministry ?  3 : 0;
            $colspan = $hide_ministry ?  2 : 0;
            ?>
            <table class="table table-striped table-bordered">
                <thead><th colspan="2"><p class="text-center" style="font-weight: bold; font-size: 14pt !important;"><?= $title." : ".$darta_information->check_state; ?></p></th></thead>
                <?php
                    if($hide_ministry){
                        ?>
                        <tr><td colspan="<?= $colspan; ?>"><p style="text-align: justify-all;" ><?= $darta_information->title; ?></p></td></tr>
                        <?php
                    }else{
                ?>
                <tr><td style="width: 50% !important;">मन्त्रालय : <?= $darta_information->ministry; ?></td><td rowspan="<?= $row_span; ?>" style="width: 50% !important;"><p style="text-align: justify-all;" ><?= $darta_information->title; ?></p></td></tr>
                <tr><td>मन्त्री : <?= $darta_information->minister; ?></td></tr>
                <tr><td>सम्बन्धित समिति : <?= $darta_information->related_committee; ?></td></tr>
                <?php
                }
                    if(!empty($darta_processing)){
                        foreach ($darta_processing as $processing) {
                            ?>
                            <tr><td>STATE : <?= $processing->state; ?> </td><td>STATE STATUS : <?= $processing->state_status.' / '.$processing->_result; ?> </td></tr>
                            <?php
                        }
                    }
                ?>
            </table>
            <?php
        }

    ?>

</div>
