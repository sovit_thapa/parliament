<?php

use yii\helpers\Html;

use app\components\UtilityFunctions;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\DartaDesk */

$this->title = 'DARTA DASHBOARD';

$this->params['breadcrumbs'][] = $this->title;
?>

<style>
  .div-square {
    padding:5px;
    border:3px double #e1e1e1;
    -webkit-border-radius:8px;
    -moz-border-radius:8px;
    border-radius:8px;
    margin:5px;
    min-height: 150px !important;

  }
  .div-square> a,.div-square> a:hover {
    color:#0073b7;
    text-decoration:none;
  }
</style>

<h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />
    <div class="row">
          <a href="#" class="small-box-footer">
          <!-- small box -->
          <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-question-circle"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">प्रत्यक्ष प्रशन </span>
              <span class="info-box-number">------</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        </a>
          <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?section=letter" class="small-box-footer">
          <!-- small box -->
          <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-envelope"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">पत्र दर्ता</span>
              <span class="info-box-number">------</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        </a>
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?section=bill" class="small-box-footer">
            <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-bars"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">विधेयक</span>
                <span class="info-box-number">------</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div></a>

            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?section=aggrement" class="small-box-footer">
            <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-flag"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">सन्धि / महासन्धि / सम्झौता</span>
                <span class="info-box-number">------</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div></a>
</div>
<hr/>




     <div class="row">
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?section=report" class="small-box-footer">
            <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-line-chart"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">प्रतिवेदन पेश</span>
                <span class="info-box-number">------</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div></a>
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?section=ordinance" class="small-box-footer">
            <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-book"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">अध्यादेश </span>
                <span class="info-box-number">------</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div></a>

            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?section=proposal" class="small-box-footer">
            <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-file-text" aria-hidden="true"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">प्रस्तावहरु </span>
                <span class="info-box-number">------</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div></a>
            <a href="<?= Yii::$app->homeUrl; ?>program/darta-desk/create/?section=other" class="small-box-footer">
            <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-database" aria-hidden="true"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">अन्य </span>
                <span class="info-box-number">------</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div></a>


    </div><hr />


