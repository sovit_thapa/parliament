<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\program\services\DartaDeskService */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = !empty($title) ? $title : 'DARTA LIST';
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['darta-desk/d-p-sample/?section='.$section]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="darta-desk-index">

    <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr />
    <?php  echo $this->render('_search', ['model' => $searchModel, 'hide_type' => $hide_type,'hide_ministry' => $hide_ministry, 'type' => $type, 'title' => $title,'state_type' => $state_type, 'state_display'=>$state_display,'section'=>$section,'action'=>'d-p-sample']); ?>

    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'target'=>'TARGET_SELF',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'type',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->type;
                },
                'visible' => !$hide_type ,
            ],
            [
                'attribute' => 'title',
                'format' => 'html',
                'label' => $label,
                'value' => function ($data) {
                    return $data->title;
                }
            ],

            [
                'attribute' => 'ministry',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->ministry ? $data->ministry : '';
                },
                'visible' => !$hide_ministry ,
            ],
            [
                'attribute' => 'minister',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->minister ? $data->minister : '';
                },
                'visible' => !$hide_ministry ,
            ],
            [
                'attribute' => 'related_committee',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->related_committee ? $data->related_committee : '';
                },
                'visible' => !$hide_ministry ,
            ],
            [
                'attribute' => 'check_state',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->check_state ? $data->check_state : '';
                },
                'visible' => $state_display ,
            ],
            [
                'attribute' => 'updated_date',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->updated_date ? $data->updated_date : $data->created_date;
                },
                'visible' => $date_dispaly ,
            ],
        ],
        'filename' => $title.date('Y-m-d'),
        'pdfLibraryPath'=>'@vendor/kartik-v/mpdf',
        'onInitSheet' => function (PHPExcel_Worksheet $sheet, $grid) {
            $sheet->getDefaultStyle()->applyFromArray(['borders' => [ 'allborders' => ['style' => PHPExcel_Style_Border::BORDER_THIN ]]]);
        },
    ]); ?>




    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'type',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->type;
                },
                'visible' => !$hide_type ,
            ],
            [
                'attribute' => 'title',
                'format' => 'html',
                'label' => $label,
                'value' => function ($data) {
                    return $data->title;
                }
            ],

            [
                'attribute' => 'ministry',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->ministry ? $data->ministry : '';
                },
                'visible' => !$hide_ministry ,
            ],
            [
                'attribute' => 'minister',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->minister ? $data->minister : '';
                },
                'visible' => !$hide_ministry ,
            ],
            [
                'attribute' => 'related_committee',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->related_committee ? $data->related_committee : '';
                },
                'visible' => !$hide_ministry ,
            ],
            [
                'attribute' => 'check_state',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->check_state ? $data->check_state : '';
                },
                'visible' => $state_display ,
            ],
            [
                'attribute' => 'updated_date',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->updated_date ? $data->updated_date : $data->created_date;
                },
                'visible' => $date_dispaly ,
            ],
            [
                'attribute' => 'statement_type',
                'format' => 'raw',
                'value' => function ($data) {
                    return "Working";
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'header' => 'दैनिक कार्यसूचीको लिन्कहरु ',
                'template' => '{new_action1}{birodha}{asuikritgarnus}{samitimapathaunus} {punasamitimaphirta}{upgradeaadhyadesh}',
                'buttons' => [
                    'new_action1' => function ($url, $model) {
                        if(!in_array($model->check_state, ['संशोधनको म्याद','दफावार छलफल समितिमा पठाइयोस्','अध्यादेश अस्वीकार गरियोस्'])){
                            return Html::a('<span class="btn btn-primary">दैनिक कार्यसूचीमा राख्नुस </span>', Url::to(['/program/daily-paper/generate-daily-paper/?id=' . $model->id]), [
                                'title' => Yii::t('app', 'दैनिक कार्यसूचीमा राख्नुस '),
                            ]);
                        }
                        if($model->check_state == 'दफावार छलफल समितिमा पठाइयोस्'){
                            return Html::a('<span class="btn btn-primary">दैनिक कार्यसूचीमा राख्नुस </span>', Url::to(['/program/daily-paper/bill-partibedan/?id=' . $model->id]), [
                                'title' => Yii::t('app', 'दैनिक कार्यसूचीमा राख्नुस '),
                            ]);
                        }
                    },
                    'birodha' => function ($url, $model) {
                        if($model->main_type == 'bill' && $model->check_state == 'अनुमति माग्ने'){
                        return Html::a('<br /><br /><span class="btn btn-primary">विरोधको सूचना दर्ता गर्नुहोस</span>', Url::to(['/program/daily-paper/statement-darta/?id='.$model->id.'&type=birod_suchana']), [
                            'title' => Yii::t('app', 'विरोधको सूचना दर्ता गर्नुहोस'),
                            'visible' => false,
                        ]);

                        }
                        if($model->main_type == 'bill' && $model->check_state == 'संशोधनको म्याद'){
                        return Html::a('<br /><br /><span class="btn btn-primary">संशोधनको सूचना दर्ता गर्नुहोस</span>', Url::to(['/program/daily-paper/statement-darta/id=' . $model->id.'&type=samsodhan_suchana']), [
                            'title' => Yii::t('app', 'संशोधनको सूचना दर्ता गर्नुहोस'),
                            'visible' => false,
                        ]);

                        }
                    },
                    'asuikritgarnus' => function ($url, $model) {
                        if($model->main_type == 'ordinance' && $model->check_state == 'अध्यादेश अस्वीकार गरियोस्'){
                        return Html::a('<br /><br /><span class="btn btn-primary">अध्यादेश अस्वीकार गरियोस्</span>', Url::to(['/program/daily-paper/statement-darta/?id='.$model->id.'&type=asuikritgarnus']), [
                            'title' => Yii::t('app', 'अध्यादेश अस्वीकार गरियोस्'),
                            'visible' => false,
                        ]);

                        }
                    },


                    'samitimapathaunus' => function ($url, $model) {
                        if($model->main_type == 'bill' && $model->check_state == 'संशोधनको म्याद'){
                        return Html::a('<br /><br /><span class="btn btn-primary">समितिमा पठाउँनुस</span>', Url::to(['/program/darta-desk/upgrade-samitima/?id=' . $model->id]), [
                            'title' => Yii::t('app', 'समितिमा पठाउँनुस'),
                        ]);
                        }
                    },

                    'upgradeaadhyadesh' => function ($url, $model) {
                        if($model->main_type == 'ordinance' && $model->check_state == 'अध्यादेश अस्वीकार गरियोस्'){
                        return Html::a('<br /><br /><span class="btn btn-primary">छलफलमा पठाउँनुस</span>', Url::to(['/program/darta-desk/upgrade-aadhyadesh/?id=' . $model->id]), [
                            'title' => Yii::t('app', 'छलफलमा पठाउँनुस'),
                        ]);
                        }
                    },



                ],
            ],
        ],
    ]); ?>
</div>
