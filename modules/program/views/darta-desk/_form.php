<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\components\UtilityFunctions;
use dosamigos\ckeditor\CKEditor;
use app\modules\setting\models\MinistryRelatedCommittee;
use app\modules\setting\models\Ministry;
use dosamigos\datepicker\DatePicker;
use kartik\widgets\Select2;
use app\modules\setting\models\ParliamentMember;

/* @var $this yii\web\View */
/* @var $model app\modules\program\models\DartaDesk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="darta-desk-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row well">
    <?= $form->field($model, 'main_type')->hiddenInput(['value'=> $section])->label(false) ?>

    <?= $form->field($model, 'parliament_id')->hiddenInput(['value'=>UtilityFunctions::SambhidhanSava()])->label(false) ?>
    <?= $form->field($model, 'meeting_id')->hiddenInput(['value'=>UtilityFunctions::ActiveMeeting()])->label(false) ?>
    <div class="col-md-6">

    <?php
        if(!$hide_type){
            ?>

            <?= $form->field($model, 'type',['template' => '{label} <div class="row">{input}{error}{hint}</div>'])->radioList($type,['style' => 'zoom:2'])->label(false) ?>
            <?php
        }else{
            $value = isset($type[0]) ? $type[0] : '';
            ?>
            <?= $form->field($model, 'type')->hiddenInput(['value'=>$value])->label(false) ?>
            <?php
        }
        if(!$hide_ministry){
            ?>

        <?= $form->field($model, 'ministry')->dropDownList(ArrayHelper::map(Ministry::find()->all(), 'title', 'title'),
        [
        'prompt' => 'Select Ministry',

        ]);
        ?>
            <?php
        }
        if($section=='report'){
            ?>

            <?= $form->field($model, 'related_committee')->dropDownList(ArrayHelper::map(MinistryRelatedCommittee::find()->all(), 'committee', 'committee'));
            ?>
            <?= $form->field($model, 'presentor')->textInput();
            ?>
            <?php

        }
        if($hide_type && $hide_ministry){
            ?>
                <br /><br /><H3 class="text-center"><STRONG>WELCOME TO REGISTER SECTION</STRONG> </H3>
            <?php
        }
        if($drata_miti){
    ?>
        <?= $form->field($model, 'drata_miti')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '9999-99-99',
        ])->label('दर्ता मिती'); ?>
        <?php
    }
    if($bitaran_miti){
        ?>
        <?= $form->field($model, 'bitaran_miti')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '9999-99-99',
        ])->label('वितरण मिती'); ?>
        <?php
    }
    if($pesh_miti){
        ?>
        <?= $form->field($model, 'pesh_miti')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '9999-99-99',
        ])->label('पेश मिती'); ?>
    <?php
        }
        $member_array = array();
        $parliament_member = ParliamentMember::find()->where(['status'=>'सकृय'])->all();
        if(!empty($parliament_member)){
            foreach ($parliament_member as $details_member) {
                $member_array[$details_member->member_title.' '.$details_member->first_name.' '.$details_member->last_name] = $details_member->member_title.' '.$details_member->first_name.' '.$details_member->last_name;
            }
        }
    if($presentor && $section != 'other'){
        ?>
        <?php 
            echo $form->field($model, 'presentor')->dropDownList($member_array);
        ?>
    <?php
        }
        if($presentor && $section == 'other'){
            echo $form->field($model, 'presentor')->textInput()->label('नाम');
        }
    if($suppoter){
        ?>
        <?php 
        $model->suppoter =  explode(',', $model->suppoter); 
        echo $form->field($model, 'suppoter')->widget(Select2::classname(), [
            'options' => ['placeholder' => 'Select a color ...', 'multiple' => true],
            'data'=>$member_array,
            'pluginOptions' => [
                'tags' => true,
                'tokenSeparators' => [',', ' '],
                'maximumInputLength' => 10
            ],
        ]);
        ?>
    <?php
        }
    ?>
    </div>
    <div class="col-md-6">
    <?= $form->field($model, 'title')->widget(CKEditor::className(), [
        'options' => ['rows' => 4],
        'preset' => 'advance'
    ])->label($label) ?>
    </div>
    </div>
    <div class="row">
    <div class="col-md-12">
        <?php
        if(!$model->isNewRecord)
        echo $form->field($model, 'status')->dropDownList(['1'=>'Active','0'=>'De-Active']) ?>
    </div>
    </div>
    <div class="form-group text-center">
        <?= Html::submitButton('दर्ता गर्नुहोस', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
