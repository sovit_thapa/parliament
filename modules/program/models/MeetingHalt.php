<?php

namespace app\modules\program\models;

use Yii;
use yii\filters\AccessControl;

/**
 * This is the model class for table "meeting_halt".
 *
 * @property int $id
 * @property int $meeting_id
 * @property string $_time minutes/hours/days
 * @property int $status
 * @property int $created_by
 * @property string $created_date
 */
class MeetingHalt extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meeting_halt';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meeting_id', '_time'], 'required'],
            [['meeting_id', 'status', 'created_by'], 'integer'],
            [['created_date'], 'safe'],
            [['_time'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meeting_id' => 'Meeting ID',
            '_time' => 'Time',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
        ];
    }
    public function beforeSave($insert){
        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->id;
            $this->created_date = new \yii\db\Expression('NOW()');
        }
        return parent::beforeSave($insert);
    }
}
