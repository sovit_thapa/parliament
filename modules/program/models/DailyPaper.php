<?php

namespace app\modules\program\models;

use Yii;
use yii\filters\AccessControl;
use app\modules\program\models\MeetingDetails;
use app\modules\setting\models\ParliamentDetails;
use app\modules\program\models\DartaDesk;
use app\modules\setting\models\ParliamentPartyOfficial;
use app\modules\setting\models\MinistryRelatedCommittee;
use app\components\UtilityFunctions;
/**
 * This is the model class for table "daily_paper".
 *
 * @property int $id
 * @property int $parliament_id
 * @property int $meeting_id
 * @property string $meeting_header
 * @property int $forum_id
 * @property string $context
 * @property string $minister
 * @property string $related_committee
 * @property int $order_number
 * @property string $status
 * @property int $created_by
 * @property string $created_date
 */
class DailyPaper extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daily_paper';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parliament_id', 'forum_id', 'context', 'state'], 'required'],
            [['parliament_id', 'meeting_id', 'forum_id', 'order_number', 'created_by','minister_hierarchy','is_generated_rp'], 'integer'],
            [['context', 'status'], 'string'],
            [['created_date', 'minister', 'related_committee', 'ministry','main_type','presentor','suppoter'], 'safe'],
            [['meeting_header', 'minister', 'related_committee', 'main_type'], 'string', 'max' => 250],
            
            [['parliament_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParliamentDetails::className(), 'targetAttribute' => ['parliament_id' => 'id']],
            [['meeting_id'], 'exist', 'skipOnError' => true, 'targetClass' => MeetingDetails::className(), 'targetAttribute' => ['meeting_id' => 'id']],
            [['forum_id'], 'exist', 'skipOnError' => true, 'targetClass' => DartaDesk::className(), 'targetAttribute' => ['forum_id' => 'id']],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parliament_id' => 'Parliament',
            'meeting_id' => 'Meeting',
            'main_type' => 'Main type',
            'meeting_header' => 'शीर्षक/नाम',
            'state' => 'State',
            'forum_id' => 'Forum',
            'context' => 'नाम/बिवण',
            'ministry' => 'Ministry',
            'minister' => 'Minister',
            'minister_hierarchy' => 'Minister Hierarchy',
            'related_committee' => 'Related Committee',
            'order_number' => 'Order Number',
            'status' => 'Status',
            '_result' => 'Result',
            'suppoter' => 'suppoter',
            'presentor' => 'presentor',
            'is_generated_rp' => 'Reading Paper Generated',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
        ];
    }



    public function beforeSave($insert){
        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->id;
            $this->created_date = new \yii\db\Expression('NOW()');
        }
        return parent::beforeSave($insert);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParliament()
    {
        return $this->hasOne(ParliamentDetails::className(), ['id' => 'parliament_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeetingDetails()
    {
        return $this->hasOne(MeetingDetails::className(), ['id' => 'meeting_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDartaDesk()
    {
        return $this->hasOne(DartaDesk::className(), ['id' => 'forum_id']);
    }


    public function getStatement()
    {
        return $this->hasMany(Statement::className(), ['forum_id' => 'forum_id']);
    }

    public function formatStatementText()
    {
        $statements=$this->statement;
        $text='';
        foreach ($statements as $key => $value) 
        {
            $text.=$value->statement_type.',';
        }
        return rtrim($text,',');
    }

    public function formatSupporterText()
    {
        $ids=explode(',',$this->suppoter);
        $memberText='';
        foreach ($ids as $key => $value) 
        {
            $memberText.=$value;
            $memberText.="<br>";
        }
        return $memberText;
    }


}
