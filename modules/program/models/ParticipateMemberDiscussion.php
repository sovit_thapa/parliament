<?php

namespace app\modules\program\models;

use Yii;
use yii\filters\AccessControl;
use app\modules\program\models\MeetingDetails;
use app\modules\setting\models\ParliamentDetails;
use app\modules\setting\models\ParliamentMember;

/**
 * This is the model class for table "participate_member_discussion".
 *
 * @property int $id
 * @property string $type
 * @property int $parliament_id
 * @property int $meeting_id
 * @property int $reading_paper_id
 * @property int $parliament_member_id
 * @property int $speech_order
 * @property string $text_
 * @property int $is_read
 * @property string $read_at
 * @property int $status
 * @property int $updated_by
 * @property string $updated_date
 * @property int $created_by
 * @property string $created_date
 */
class ParticipateMemberDiscussion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'participate_member_discussion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'string'],
            [['parliament_id', 'meeting_id', 'parliament_member_id'], 'required'],
            [['parliament_id', 'meeting_id', 'reading_paper_id', 'parliament_member_id', 'speech_order', 'is_read', 'status', 'updated_by', 'created_by'], 'integer'],
            [['read_at', 'updated_date', 'created_date'], 'safe'],
            [['text_'], 'string', 'max' => 200],
            [['parliament_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParliamentDetails::className(), 'targetAttribute' => ['parliament_id' => 'id']],
            [['parliament_member_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParliamentMember::className(), 'targetAttribute' => ['parliament_member_id' => 'id']],
            [['meeting_id'], 'exist', 'skipOnError' => true, 'targetClass' => MeetingDetails::className(), 'targetAttribute' => ['meeting_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'parliament_id' => 'Parliament ID',
            'meeting_id' => 'Meeting ID',
            'reading_paper_id' => 'Reading Paper ID',
            'parliament_member_id' => 'नाम, थर',
            'speech_order' => 'Speech Order',
            'text_' => 'Text',
            'is_read' => 'Is Read',
            'read_at' => 'Read At',
            'status' => 'Status',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'party'=>'राजनैतिक पार्टी',
        ];
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParliament()
    {
        return $this->hasOne(ParliamentDetails::className(), ['id' => 'parliament_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(ParliamentMember::className(), ['id' => 'parliament_member_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeeting()
    {
        return $this->hasOne(MeetingDetails::className(), ['id' => 'meeting_id']);
    }
    public function getStatement()
    {
        return $this->hasOne(Statement::className(), ['id' => 'statement_id']);
    }

    public function beforeSave($insert){
        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->id;
            $this->created_date = new \yii\db\Expression('NOW()');
        }
        if(!$this->isNewRecord){
            $this->updated_by = Yii::$app->user->id;
            $this->updated_date = new \yii\db\Expression('NOW()');
        }
        return parent::beforeSave($insert);
    }

}
