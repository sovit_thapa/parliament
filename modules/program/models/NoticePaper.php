<?php

namespace app\modules\program\models;

use Yii;

/**
 * This is the model class for table "notice_paper".
 *
 * @property int $id
 * @property int $parliament_id
 * @property int $meeting_id
 * @property int $reaing_paper_id
 * @property string $main_type
 * @property string $meeting_header
 * @property string $state
 * @property string $context
 * @property string $date
 * @property string $ministry
 * @property string $minister
 * @property string $related_committee
 * @property int $order_number
 * @property int $paper_status
 * @property int $created_by
 * @property string $created_date
 */
class NoticePaper extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notice_paper';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parliament_id'], 'required'],
            [['parliament_id', 'meeting_id', 'reaing_paper_id', 'order_number', 'paper_status', 'created_by'], 'integer'],
            [['main_type', 'meeting_header', 'state', 'context'], 'string'],
            [['date', 'created_date'], 'safe'],
            [['ministry', 'minister', 'related_committee'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parliament_id' => 'Parliament ID',
            'meeting_id' => 'Meeting ID',
            'reaing_paper_id' => 'Reaing Paper ID',
            'main_type' => 'Main Type',
            'meeting_header' => 'Meeting Header',
            'state' => 'State',
            'context' => 'Context',
            'date' => 'Date',
            'ministry' => 'Ministry',
            'minister' => 'Minister',
            'related_committee' => 'Related Committee',
            'order_number' => 'Order Number',
            'paper_status' => 'Paper Status',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
        ];
    }
}
