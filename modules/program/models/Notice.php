<?php

namespace app\modules\program\models;

use Yii;
use app\modules\program\models\MeetingDetails;
/**
 * This is the model class for table "notice".
 *
 * @property int $id
 * @property int $meeting_id
 * @property string $text
 * @property string $notice_date
 * @property string $notice_type
 * @property int $is_post
 * @property int $created_by
 * @property string $created_date
 */
class Notice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meeting_id', 'text'], 'required'],
            [['meeting_id', 'is_post', 'created_by'], 'integer'],
            [['text', 'notice_type'], 'string'],
            [['notice_date', 'created_date'], 'safe'],
            [['meeting_id'], 'exist', 'skipOnError' => true, 'targetClass' => MeetingDetails::className(), 'targetAttribute' => ['meeting_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meeting_id' => 'Meeting ID',
            'text' => 'Text',
            'notice_date' => 'Notice Date',
            'notice_type' => 'Notice Type',
            'is_post' => 'Is Post',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeetingDetails()
    {
        return $this->hasOne(MeetingDetails::className(), ['id' => 'meeting_id']);
    }
    public function beforeSave($insert){
        if($this->isNewRecord){
            $meeting_detail = MeetingDetails::findOne($this->meeting_id);
            $this->notice_date = $meeting_detail ? $meeting_detail->_date_time :  data('Y-m-d');
            $this->created_by = Yii::$app->user->id;
            $this->created_date = new \yii\db\Expression('NOW()');
        }
        return parent::beforeSave($insert);
    }
}
