<?php

namespace app\modules\program\models;

use Yii;
use yii\filters\AccessControl;
use app\modules\setting\models\ParliamentMember;

/**
 * This is the model class for table "statement".
 *
 * @property int $id
 * @property string $statement_type
 * @property string $member member are separated by comma
 * @property int $forum_id id for darta desk
 * @property string $state
 * @property int $status
 * @property string $_result
 * @property int $statement_order
 * @property int $is_generate_dp
 * @property int $is_generate_rp
 * @property int $reading_paper_id
 * @property int $created_by
 * @property string $created_date
 */
class Statement extends \yii\db\ActiveRecord
{
    public $political_party;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['statement_type', 'state', '_result'], 'string'],
            [['member', 'forum_id'], 'required'],
            [['forum_id', 'status', 'statement_order', 'is_generate_dp', 'is_generate_rp', 'reading_paper_id', 'created_by','daily_paper_id'], 'integer'],
            [['created_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'statement_type' => 'Statement Type',
            'member' => 'Member',
            'forum_id' => 'Forum ID',
            'state' => 'State',
            'status' => 'Status',
            '_result' => 'Result',
            'statement_order' => 'Statement Order',
            'is_generate_dp' => 'Generated Dp',
            'is_generate_rp' => 'Generated Rp',
            'daily_paper_id' => 'Daily Paper',
            'reading_paper_id' => 'Reading Paper',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
        ];
    }

    public function parliamentMembers()
    {
        $ids=explode(',',$this->member);
        $memberText='';
        foreach ($ids as $key => $value) 
        {
            if($value != null)
            {
                $member=ParliamentMember::findOne($value);
                $memberText.=$member->member_title.' '.$member->first_name.' '.$member->last_name;
                $memberText.="<br>";
            }
        }
        return $memberText;
    }
}
