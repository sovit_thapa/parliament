<?php

namespace app\modules\program\models;

use yii\helpers\Html;
use Yii;
use yii\filters\AccessControl;
use app\modules\setting\models\ParliamentDetails;

/**
 * This is the model class for table "meeting_details".
 *
 * @property int $id
 * @property int $parliament_id parliament id, if present
 * @property string $adhibasen
 * @property string $adhibasen_year
 * @property string $_date_time
 * @property int $_nepali_year
 * @property int $_nepali_month
 * @property int $_nepali_day
 * @property int $_time
 * @property string $status
 * @property int $created_by
 * @property string $created_date
 */
class MeetingDetails extends \yii\db\ActiveRecord
{
    public $meeting_links;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meeting_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parliament_id', '_nepali_year', '_nepali_month', '_nepali_day', 'created_by', '_meeting_number'], 'integer'],
            [['adhibasen', 'adhibasen_year', '_nepali_year', '_nepali_month', '_nepali_day', '_time'], 'required'],
            [['_date_time', 'created_date','_time'], 'safe'],
            [['status'], 'string'],
            [['adhibasen'], 'string', 'max' => 250],
            [['adhibasen_year'], 'string', 'max' => 20],
            [['parliament_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParliamentDetails::className(), 'targetAttribute' => ['parliament_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parliament_id' => 'Parliament',
            'adhibasen' => 'Adhibasen',
            'adhibasen_year' => 'Adhibasen Year',
            '_meeting_number' => 'Meeting Number',
            '_date_time' => 'Date Time',
            '_nepali_year' => 'Nepali Year',
            '_nepali_month' => 'Nepali Month',
            '_nepali_day' => 'Nepali Day',
            '_time' => 'Time',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParliament()
    {
        return $this->hasOne(ParliamentDetails::className(), ['id' => 'parliament_id']);
    }
    public function beforeSave($insert){
        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->id;
            $this->created_date = new \yii\db\Expression('NOW()');
        }
        return parent::beforeSave($insert);
    }

    public function Details($status, $meeting_number){
        $link = Html::a("<span class='glyphicon glyphicon' aria-hidden='true'>DP</span>", ['daily-paper/view-d-p/', 'meeting_number' => $meeting_number], ['class' => 'btn btn-primary btn-small']).Html::a("<span class='glyphicon glyphicon' aria-hidden='true'>RP</span>", ['reading-paper/today-all-r-p?meeting_id='.$meeting_number], ['class' => 'btn btn-primary btn-small']);
        if($status=='active'){
            $link .= Html::a("<span class='glyphicon glyphicon-file' aria-hidden='true'></span>", ['notice/create/', 'meeting_number' => $meeting_number], ['class' => 'btn btn-primary btn-small']).Html::a("<span class='glyphicon glyphicon-remove-circle' aria-hidden='true'></span>", ['meeting-details/meeting-cancellation/', 'meeting_number' => $meeting_number], ['class' => 'btn btn-primary btn-small']);
        }
        return $link;
    }
}
