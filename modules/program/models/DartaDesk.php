<?php

namespace app\modules\program\models;

use Yii;
use yii\filters\AccessControl;
use app\modules\setting\models\ParliamentDetails;
use app\modules\setting\models\ParliamentPartyOfficial;
use app\modules\setting\models\MinistryRelatedCommittee;
use app\components\UtilityFunctions;
/**
 * This is the model class for table "darta_desk".
 *
 * @property int $id
 * @property int $parliament_id
 * @property string $title
 * @property string $type
 * @property string $minister
 * @property string $related_committee
 * @property string $remarks
 * @property int $status
 * @property string $state
 * @property int $created_by
 * @property string $created_date
 * @property int $updated_by
 * @property string $updated_date
 *
 * @property ParliamentDetails $parliament
 */
class DartaDesk extends \yii\db\ActiveRecord
{
    public $bitaran_miti, $pesh_miti, $drata_miti, $statement_type;
    const letter = ['राष्ट्रपति' => 'राष्ट्रपतिको पत्र','प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालय' => 'प्रधानमन्त्री तथा मन्त्रिपरिषद् कार्यालयको पत्र' ,'राष्ट्रपतिको कार्यालय' => 'राष्ट्रपति कार्यालयको पत्र'];
    const report = ['संवैधानिक आयोगको प्रतिवेदन' => 'संवैधानिक आयोगको प्रतिवेदन','समितिको प्रतिवेदन' => 'समितिको प्रतिवेदन','समितिको बार्षिक प्रतिवेदन' => 'समितिको बार्षिक प्रतिवेदन'];
    const aggrement = ['सन्धि' => 'सन्धि','महासन्धि' => 'महासन्धि','सम्झौता' => 'सम्झौता'];
    const porposal = ['ध्यानाकर्षण प्रस्ताव' => 'ध्यानाकर्षण प्रस्ताव','जरुरी सार्वजनिक महत्वको प्रस्ताव' => 'जरुरी सार्वजनिक महत्वको प्रस्ताव','संकल्प प्रस्ताव' => 'संकल्प प्रस्ताव'];
    const election = ['सभामुख' =>'सभामुखको निर्वाचन','उप-सभामुख' =>'उप-सभामुखको निर्वाचन','राष्ट्रपति' =>'राष्ट्रपतिको निर्वाचन','उप-राष्ट्रपति' =>'उप-राष्ट्रपतिको निर्वाचन'];
    const other = ['अनुपस्थितिको सूचना' => 'अनुपस्थितिको सूचना','स्थान रिक्तता' => 'स्थान रिक्तता','समिति गठन' => 'समिति गठन','शोक प्रस्ताव'=>'शोक प्रस्ताव','समिति गठन'=>'समिति गठन','समितिमा थपघट तथा हेरफेर' => 'समितिमा थपघट तथा हेरफेर','बैठकमा अध्यक्षता गर्ने सदस्यको मनोनयन' => 'बैठकमा अध्यक्षता गर्ने सदस्यको मनोनयन','राष्ट्रपतिबाट सम्बोधन' => 'राष्ट्रपतिबाट सम्बोधन','अन्य' => 'अन्य'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'darta_desk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parliament_id', 'title'], 'required'],
            [['parliament_id', 'status', 'created_by', 'updated_by', 'is_generate_dp','meeting_id'], 'integer'],
            [['title', 'type', 'remarks', 'state', 'ministry'], 'string'],
            [['created_date', 'updated_date','main_type','presentor','suppoter'], 'safe'],
            [['minister', 'related_committee', 'ministry','main_type'], 'string', 'max' => 250],
            [['parliament_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParliamentDetails::className(), 'targetAttribute' => ['parliament_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parliament_id' => 'Parliament',
            'meeting_id' => 'Meeting',
            'title' => 'नाम/विधेयक',
            'main_type' => 'Main Type',
            'type' => 'शिर्षक',
            'ministry' => 'मन्त्रालय',
            'minister' => 'मन्त्री',
            'related_committee' => 'सम्बन्धित समिति',
            'remarks' => 'Remarks',
            'status' => 'Status',
            'state' => 'प्रकृया',
            'presentor' => 'प्रस्तावक',
            'suppoter' => 'समर्थक',
            'state_status' => 'प्रकृया नतिजा',
            'check_state' => 'अबको प्रकृया',
            'is_generate_dp' => "DP Genereated",
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'updated_by' => 'Updated By',
            'updated_date' => 'मिती',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParliament()
    {
        return $this->hasOne(ParliamentDetails::className(), ['id' => 'parliament_id']);
    }



    public function beforeSave($insert){
        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->id;
            $this->meeting_id = UtilityFunctions::ActiveMeeting();
            $this->created_date = new \yii\db\Expression('NOW()');
            $ministry_details = ParliamentPartyOfficial::find()->where(['ministry'=> $this->ministry, 'post'=>['मन्त्री','उप प्रधानमन्त्री','प्रधानमन्त्री'], 'status'=>'सकृय'])->one();
            $this->minister = $ministry_details && $ministry_details->parliamentMember ? "मा० ".$this->ministry." मन्त्री श्री ".$ministry_details->parliamentMember->first_name.' '.$ministry_details->parliamentMember->last_name : '';
            $related_committee = MinistryRelatedCommittee::find()->where(['ministry'=>$this->ministry])->one();
            $this->related_committee = $related_committee ? $related_committee->committee : '';

        }else{
            $this->updated_by = Yii::$app->user->id;
            $this->updated_date = new \yii\db\Expression('NOW()');
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivities()
    {  
        return $this->hasMany(ParliamentActivities::className(), ['forum_id' => 'id']);
    }

}
