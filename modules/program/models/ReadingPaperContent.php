<?php

namespace app\modules\program\models;

use Yii;

/**
 * This is the model class for table "reading_paper_content".
 *
 * @property int $id
 * @property int $reading_paper_id
 * @property int $statement_id
 * @property string $page_type
 * @property string $pages
 * @property string $content
 * @property int $order_
 * @property string $next_page seperated by comma
 * @property string $_end
 * @property int $_next_id
 * @property int $content_status
 * @property int $is_static
 * @property int $is_read
 * @property int $read_by
 * @property string $read_date
 * @property int $created_by
 * @property string $created_date
 */
class ReadingPaperContent extends \yii\db\ActiveRecord
{
    public $_first_content, $_second_content, $_third_content, $_discuss,$_discuss_body,$_discuss_reply, $_end_discuss,$_decision_general,$_decision_no_discuss, $_st_decision,$_st_re_back_one, $_st_re_back_two,$_result_yes,$_result_no,$_result_overall_no,$_result_overall_yes, $_st_last_content, $_discuss_suppoter, $_st_discuss, $_st_discuss_body,
        $_st_reply, $_st_result, $_st_yes, $_st_no, $_general_yes, $_general_no, $_discuss_end, $_decision, $_result, $_pertibedan_decision, $_pertibedan_result, $_pertibedan_yes, $_pertibedan_no, $_bidhyak_decision, $_bidhyak_result, $_bidhyak_yes, $_bidhyak_no, $_prastab_first_content, $_prastab_second_content, $_prastab_decision, $_prastab_result, $_prastab_yes, $_prastab_no, $_final_decision, $_final_result, $_final_no, $_final_yes, $_general_result;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reading_paper_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reading_paper_id', 'content'], 'required'],
            [['reading_paper_id', 'statement_id', 'order_', 'content_status', 'is_static', 'is_read', 'read_by', 'created_by'], 'integer'],
            [['page_type', 'pages', 'content', 'next_page', '_end'], 'string'],
            [['read_date', 'created_date','_next_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reading_paper_id' => 'Reading Paper ID',
            'statement_id' => 'Statement ID',
            'page_type' => 'Page Type',
            'pages' => 'Pages',
            'content' => 'Content',
            'order_' => 'Order',
            'next_page' => 'Next Page',
            '_end' => 'End',
            '_next_id' => 'Next ID',
            'content_status' => 'Content Status',
            'is_static' => 'Is Static',
            'is_read' => 'Is Read',
            'read_by' => 'Read By',
            'read_date' => 'Read Date',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
        ];
    }
}
