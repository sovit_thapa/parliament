<?php

namespace app\modules\program\models;

use Yii;
use yii\filters\AccessControl;

/**
 * This is the model class for table "reading_paper_content".
 *
 * @property int $id
 * @property int $reading_paper_id
 * @property string $page_type
 * @property string $pages
 * @property string $content
 * @property int $is_static
 * @property int $is_read
 * @property int $read_by
 * @property string $read_date
 * @property int $created_by
 * @property string $created_date
 */
class ReadingPaperContent extends \yii\db\ActiveRecord
{

    public $_first_content, $_second_content, $_third_content, $_discuss,$_discuss_body,$_discuss_reply,$_decision_general,$_decision_no_discuss, $_st_decision,$_st_re_back_one, $_st_re_back_two,$_result_yes,$_result_no,$_result_overall_no,$_result_overall_yes, $_st_last_content;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reading_paper_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'required'],
            [['reading_paper_id', 'is_static', 'is_read', 'read_by', 'created_by', 'order_', 'statement_id'], 'integer'],
            [['page_type', 'pages', 'content'], 'string'],
            [['read_date', 'created_date','reading_paper_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reading_paper_id' => 'Reading Paper ID',
            'page_type' => 'Page Type',
            'pages' => 'Pages',
            'content' => 'Content',
            'order_' => 'Order',
            'is_static' => 'Is Static',
            'statement_id' => 'Statement',
            'is_read' => 'Is Read',
            'read_by' => 'Read By',
            'read_date' => 'Read Date',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
        ];
    }
}
