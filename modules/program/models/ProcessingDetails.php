<?php

namespace app\modules\program\models;

use Yii;
use yii\filters\AccessControl;

/**
 * This is the model class for table "processing_details".
 *
 * @property int $id
 * @property int $parliament_id
 * @property int $meeting_id
 * @property int $darta_id
 * @property string $type
 * @property string $date
 * @property string $ministry
 * @property string $minister
 * @property string $related_committee
 * @property string $state
 * @property string $state_status
 * @property string $_result
 * @property string $remarks
 * @property int $status
 * @property int $created_by
 * @property string $created_date
 */
class ProcessingDetails extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'processing_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parliament_id', 'darta_id', 'date'], 'required'],
            [['parliament_id', 'meeting_id', 'darta_id', 'status', 'created_by'], 'integer'],
            [['type', 'state', 'state_status', '_result', 'remarks', 'ministry'], 'string'],
            [['date', 'created_date'], 'safe'],
            [['ministry', 'minister', 'related_committee'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parliament_id' => 'Parliament ID',
            'meeting_id' => 'Meeting ID',
            'darta_id' => 'Darta ID',
            'type' => 'Type',
            'date' => 'Date',
            'ministry' => 'Ministry',
            'minister' => 'Minister',
            'related_committee' => 'Related Committee',
            'state' => 'State',
            'state_status' => 'State Status',
            '_result' => 'Result',
            'remarks' => 'Remarks',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
        ];
    }


    public function beforeSave($insert){
        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->id;
            $this->created_date = new \yii\db\Expression('NOW()');
        }
        return parent::beforeSave($insert);
    }
}
