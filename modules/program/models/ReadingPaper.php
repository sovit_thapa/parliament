<?php

namespace app\modules\program\models;

use Yii;
use yii\filters\AccessControl;
use app\modules\setting\models\ParliamentDetails;

/**
 * This is the model class for table "reading_paper".
 *
 * @property int $id
 * @property int $parliament_id
 * @property int $meeting_id
 * @property int $forum_id
 * @property string $meeting_header
 * @property string $context
 * @property string $date
 * @property string $minister
 * @property string $related_committee
 * @property int $order_number
 * @property string $status
 * @property int $goes_through_voting
 * @property string $win_by
 * @property int $is_read
 * @property string $read_at
 * @property int $paper_status
 * @property int $created_by
 * @property string $created_date
 */
class ReadingPaper extends \yii\db\ActiveRecord
{
    const standard_order = ['प्रधानमन्त्रीको बक्तब्य','मन्त्रीको बक्तब्य','शून्य समय','विशेष समय','प्रत्यक्ष प्रशनहरु','राष्ट्रपति','प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालय','राष्ट्रपतिको कार्यालय','व्यवस्थापन कार्य','समितिको प्रतिवेदन','महासन्धि','सन्धि','सम्झौता','नीति तथा कार्यक्रम','राजस्व व्ययको बार्षिक अनुमान','ध्यानाकर्षण प्रस्ताव','जरुरी सार्वजनिक महत्वको प्रस्ताव','संकल्प प्रस्ताव','स्थगन प्रस्ताव','सभामुखको निर्वाचन ','उप-सभामुखको निर्वाचन','राष्ट्रपतिको निर्वाचन','उप-राष्ट्रपतिको निर्वाचन','अनुपस्थितिको सूचना','स्थान रिक्तता','समिति गठन','संवैधानिक आयोगको प्रतिवेदन','समितिको प्रतिवेदन','समितिको बार्षिक प्रतिवेदन','शोक प्रस्ताव','समिति गठन','समितिमा थपघट तथा हेरफेर','बैठकमा अध्यक्षता गर्ने सदस्यको मनोनयन','राष्ट्रपतिबाट सम्बोधन','अन्य'];
    const aumati_magne =['_first_content','_second_content'];
    const time_template =['_discuss','_discuss_body','_second_content'];
    const bichar_gariyos_general = ['_first_content','_second_content','_discuss','_discuss_body', '_discuss_reply' ,'_decision_general'];
    const bichar_gariyos_no_discuss = ['_first_content','_decision_no_discuss'];
    const bichar_gariyos_result = ['_result_yes','_result_no','_result_overall_no','_result_overall_yes'];
    const result_nepali = ['बहुमत स्वीकृत','बहुमत अस्वीकृति','सर्बसम्ती अस्वीकृति','सर्बसम्ती स्वीकृत'];
    const letter = ['_first_content'];
    const report_reading_pages = ['_first_content'];
    const crt_bill_anumati = ['_first_content','_second_content','_third_content'];
    const anumati_bill_birod_template = ['_first_content','_second_content','_third_content','_st_decision','_st_re_back_one','_st_re_back_two','_result','_result_yes','_result_no','_st_last_content'];
    const crt_bill_bichar_garios = ['_first_content','_second_content','_discuss','_discuss_body', '_discuss_reply','_discuss_end' ,'_decision_general','_decision_no_discuss','_result','_result_yes','_result_no','_result_overall_no','_result_overall_yes'];


    const samitima_pathaiyos = ['_first_content','_second_content','_third_content', '_decision' ,'_result','_result_yes','_result_no','_result_overall_no','_result_overall_yes'];

    const dhyan_akarsan_prasthab = ['_first_content','_second_content','_third_content', '_discuss_suppoter' ,'_discuss_reply'];
    const jaruri_sarbajanik_mahato = ['_first_content', '_discuss_suppoter','_discuss','_discuss_body' ,'_discuss_reply','_st_last_content'];
    const sankalpa_prastab = ['_first_content', '_second_content', '_third_content', '_discuss_suppoter','_discuss','_discuss_body','_discuss_end' ,'_discuss_reply','_decision','_result','_result_yes','_result_no','_result_overall_yes'];
    const condolence = ['_first_content','_second_content','_third_content'];
    
    const pertibedan_puna_samitima_pathaiyos = ['_first_content','_st_discuss','_st_discuss_body','_st_reply','_st_decision','_st_result','_st_yes','_st_no','_decision_general','_general_result','_general_yes','_general_no','_second_content','_discuss','_discuss_body','_discuss_reply','_discuss_end','_decision','_decision_no_discuss','_result','_result_yes','_result_no','_pertibedan_decision','_pertibedan_result','_pertibedan_yes','_pertibedan_no','_bidhyak_decision','_bidhyak_result','_bidhyak_yes','_bidhyak_no','_prastab_first_content','_prastab_decision','_prastab_result','_prastab_yes','_prastab_no','_third_content','_final_decision','_final_result','_final_no','_final_yes'];
    const pertibedan_sahitko_chalfal = ['_first_content','_decision_general','_general_result','_general_yes','_general_no','_second_content','_discuss','_discuss_body','_discuss_reply','_discuss_end','_decision','_decision_no_discuss','_result','_result_yes','_result_no','_pertibedan_decision','_pertibedan_result','_pertibedan_yes','_pertibedan_no','_bidhyak_decision','_bidhyak_result','_bidhyak_yes','_bidhyak_no','_prastab_first_content','_prastab_decision','_prastab_result','_prastab_yes','_prastab_no','_third_content','_final_decision','_final_result','_final_no','_final_yes'];
    const ordinance_anumati = ['_first_content'];
    const suikrigarios = ['_first_content','_second_content','_decision','_result','_result_yes','_result_no','_result_overall_no','_result_overall_yes'];
    const asuikrit = ['_first_content','_second_content','_third_content','_discuss','_discuss_body', '_discuss_reply','_end_discuss','_st_decision','_st_re_back_one','_st_re_back_two','_st_result','_st_yes','_st_no','_decision_general','_decision','_result','_result_yes','_result_no'];
    const asui_sukrit_ = ['_first_content','_second_content','_decision_general','_result_yes','_result_no','_result_overall_no','_result_overall_yes'];
    const aggrement_pesh = ['_first_content'];
    const aggrement_chalfal_anumodan = ['_first_content','_second_content','_discuss','_discuss_body','_discuss_reply','_discuss_end','_decision','_decision_no_discuss','_result','_result_yes','_result_no','_result_overall_no','_result_overall_yes'];
    const billing_process = ['वितरण','अनुमति माग्ने','विचार गरियोस्','संशोधनको म्याद','दफावार छलफल समितिमा पठाइयोस्','दफावार छलफल सदनमा संशोधन सहित','दफावार छलफल सदनमा संशोधन विना','समितिको प्रतिवेदन पेश','प्रतिवेदन सहितको विधेयक समितिमा फिर्ता','पुनः समितिको प्रतिवेदन पेश','प्रतिवेदन सहित छलफल','पारित'];
    const letter_processing = ['दर्ता','वितरण','पारित'];
    const aggrement_processing = ['दर्ता','वितरण','पेश','छलफल','अनुमोदन'];
    const sympathy_processing = ['दर्ता','वितरण','पारित'];
    const bill_state = ['अनुमति माग्ने','विचार गरियोस्','संशोधनको म्याद','दफावार छलफल समितिमा पठाइयोस्','समितिको प्रतिवेदन पेश','प्रतिवेदन सहित छलफल','पारित'];
    const letter_state = ['दर्ता','पारित'];
    const aggrement_state = ['दर्ता','पेश','छलफल','अनुमोदन'];
    const samiti_bill = ['दर्ता','अनुमति माग्ने','विचार गरियोस्','संशोधनको म्याद','दफावार छलफल समितिमा पठाइयोस्','समितिको प्रतिवेदन पेश','प्रतिवेदन सहित छलफल','पारित'];
    const samiti_bina_bill = ['दर्ता','अनुमति माग्ने','विचार गरियोस्','संशोधनको म्याद','दफावार छलफल सदनमा संशोधन सहित','दफावार छलफल सदनमा संशोधन विना','पारित'];
    const report_state = ['समितिको प्रतिवेदन पेश','पारित'];
    const rule_state = ['पेश','टेबुल', 'छलफल','पारित']; // working on it
    const income_state = ['पेश','टेबुल', 'छलफल','पारित']; // working on it
    const ordinance_state = ['वितरण','पेश','अध्यादेश अस्वीकार गरियोस्','छलफल','अनुमोदन'];
    const proposal_state = ['पेश', 'छलफल','पारित'];
    const election_state = ['पेश', 'पारित'];
    const other_state = ['पेश', 'पारित'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reading_paper';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parliament_id', 'state'], 'required'],
            [['parliament_id', 'meeting_id', 'daily_paper_id', 'order_number', 'goes_through_voting', 'is_read', 'paper_status', 'created_by','primary_order','forum_id'], 'integer'],
            [['meeting_header', 'context', 'status', 'win_by','main_type','page_type'], 'string'],
            [['date', 'read_at', 'created_date', 'minister', 'related_committee', 'ministry' ], 'safe'],
            [['minister', 'related_committee', 'ministry'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parliament_id' => 'Parliament',
            'meeting_id' => 'Meeting',
            'daily_paper_id' => 'Daily Paper',
            'forum_id' => 'Darta',
            'main_type' => 'Maint Type',
            'meeting_header' => 'नाम',
            'state' => 'State',
            'context' => 'विवरन ',
            'date' => 'Date',
            'ministry' => 'Ministry',
            'minister' => 'Minister',
            'related_committee' => 'Related Committee',
            'order_number' => 'Order Number',
            'primary_order' => 'Primary Order',
            'status' => 'Status',
            'goes_through_voting' => 'Goes Through Voting',
            'win_by' => 'Win By',
            'is_read' => 'Is Read',
            'read_at' => 'Read At',
            'page_type' => 'Paper Type',
            'paper_status' => 'Paper Status',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
        ];
    }



    public function beforeSave($insert){
        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->id;
            $this->created_date = new \yii\db\Expression('NOW()');
        }
        return parent::beforeSave($insert);
    }

    public function getMeetingDetails()
    {
        return $this->hasOne(MeetingDetails::className(), ['id' => 'meeting_id']);
    }
    public function getStatement()
    {
        return $this->hasMany(Statement::className(), ['forum_id' => 'forum_id']);
    }
    public function getParliament()
    {
        return $this->hasOne(ParliamentDetails::className(), ['id' => 'parliament_id']);
    }

}
