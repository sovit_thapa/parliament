<?php

namespace app\modules\program\services;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\program\models\NoticePaper;

/**
 * NoticePaperService represents the model behind the search form of `app\modules\program\models\NoticePaper`.
 */
class NoticePaperService extends NoticePaper
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parliament_id', 'meeting_id', 'reaing_paper_id', 'order_number', 'paper_status', 'created_by'], 'integer'],
            [['main_type', 'meeting_header', 'state', 'context', 'date', 'ministry', 'minister', 'related_committee', 'created_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NoticePaper::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parliament_id' => $this->parliament_id,
            'meeting_id' => $this->meeting_id,
            'reaing_paper_id' => $this->reaing_paper_id,
            'date' => $this->date,
            'order_number' => $this->order_number,
            'paper_status' => $this->paper_status,
            'created_by' => $this->created_by,
            'created_date' => $this->created_date,
        ]);

        $query->andFilterWhere(['like', 'main_type', $this->main_type])
            ->andFilterWhere(['like', 'meeting_header', $this->meeting_header])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'context', $this->context])
            ->andFilterWhere(['like', 'ministry', $this->ministry])
            ->andFilterWhere(['like', 'minister', $this->minister])
            ->andFilterWhere(['like', 'related_committee', $this->related_committee]);

        return $dataProvider;
    }
}
