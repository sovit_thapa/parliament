<?php

namespace app\modules\program\services;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\program\models\Notice;

/**
 * NoticeService represents the model behind the search form of `app\modules\program\models\Notice`.
 */
class NoticeService extends Notice
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'meeting_id', 'is_post', 'created_by'], 'integer'],
            [['text', 'notice_date', 'notice_type', 'created_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Notice::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'meeting_id' => $this->meeting_id,
            'notice_date' => $this->notice_date,
            'is_post' => $this->is_post,
            'created_by' => $this->created_by,
            'created_date' => $this->created_date,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'notice_type', $this->notice_type]);

        return $dataProvider;
    }
}
