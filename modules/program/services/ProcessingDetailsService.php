<?php

namespace app\modules\program\services;

use Yii;
use yii\filters\AccessControl;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\program\models\ProcessingDetails;

/**
 * ProcessingDetailsService represents the model behind the search form of `app\modules\program\models\ProcessingDetails`.
 */
class ProcessingDetailsService extends ProcessingDetails
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parliament_id', 'meeting_id', 'darta_id', 'status', 'created_by'], 'integer'],
            [['type', 'date', 'ministry', 'minister', 'related_committee', 'state', 'state_status', 'remarks', 'created_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProcessingDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parliament_id' => $this->parliament_id,
            'meeting_id' => $this->meeting_id,
            'darta_id' => $this->darta_id,
            'date' => $this->date,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'created_date' => $this->created_date,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'ministry', $this->ministry])
            ->andFilterWhere(['like', 'minister', $this->minister])
            ->andFilterWhere(['like', 'related_committee', $this->related_committee])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'state_status', $this->state_status])
            ->andFilterWhere(['like', 'remarks', $this->remarks]);

        return $dataProvider;
    }
}
