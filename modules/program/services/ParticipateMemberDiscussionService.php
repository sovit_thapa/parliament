<?php

namespace app\modules\program\services;

use Yii;
use yii\filters\AccessControl;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\program\models\ParticipateMemberDiscussion;

/**
 * ParticipateMemberDiscussionService represents the model behind the search form of `app\modules\program\models\ParticipateMemberDiscussion`.
 */
class ParticipateMemberDiscussionService extends ParticipateMemberDiscussion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parliament_id', 'meeting_id', 'reading_paper_id', 'parliament_member_id', 'speech_order', 'is_read', 'status', 'updated_by', 'created_by'], 'integer'],
            [['type', 'text_', 'read_at', 'updated_date', 'created_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ParticipateMemberDiscussion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parliament_id' => $this->parliament_id,
            'meeting_id' => $this->meeting_id,
            'reading_paper_id' => $this->reading_paper_id,
            'parliament_member_id' => $this->parliament_member_id,
            'speech_order' => $this->speech_order,
            'is_read' => $this->is_read,
            'read_at' => $this->read_at,
            'status' => $this->status,
            'updated_by' => $this->updated_by,
            'updated_date' => $this->updated_date,
            'created_by' => $this->created_by,
            'created_date' => $this->created_date,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'text_', $this->text_]);

        return $dataProvider;
    }
}
