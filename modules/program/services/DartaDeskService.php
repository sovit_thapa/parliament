<?php

namespace app\modules\program\services;

use Yii;
use yii\filters\AccessControl;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\program\models\DartaDesk;

/**
 * DartaDeskService represents the model behind the search form of `app\modules\program\models\DartaDesk`.
 */
class DartaDeskService extends DartaDesk
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_by', 'updated_by', 'is_generate_dp'], 'integer'],
            [['title', 'type', 'parliament_id', 'minister', 'ministry', 'related_committee', 'remarks', 'state', 'state_status', 'created_date', 'updated_date', 'check_state','main_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $section)
    {   
        if(!empty($section))
            $query = DartaDesk::find()->where(['main_type' => $section]);
        else
            $query = DartaDesk::find();
        $query->joinWith('parliament');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,

            'sort'=> [
            'defaultOrder' => ['id'=>SORT_DESC],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'created_date' => $this->created_date,
            'updated_by' => $this->updated_by,
            'updated_date' => $this->updated_date,
            'is_generate_dp' => $this->is_generate_dp,
            'type' => $this->type,
        ]);
        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'main_type', $section])
            ->andFilterWhere(['like', 'parliament_details.name', $this->parliament_id])
            ->andFilterWhere(['like', 'minister', $this->minister])
            ->andFilterWhere(['like', 'ministry', $this->ministry])
            ->andFilterWhere(['like', 'state_status', $this->state_status])
            ->andFilterWhere(['like', 'created_date', $this->created_date])
            ->andFilterWhere(['like', 'updated_date', $this->updated_date])
            ->andFilterWhere(['like', 'related_committee', $this->related_committee])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'check_state', $this->check_state]);

        return $dataProvider;
    }

    public function dpactivities($darta_number){
        return DartaDesk::find()->where([self::tableName() .'.id'=>$darta_number])->joinWith(['activities'])->orderBy('parliament_activities.reading_order ASC')->all();
    }
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function dratsection($params, $section)
    {
        //to be checked
        if(!empty($section))
            $query = DartaDesk::find()->where('darta_desk.status=:status AND is_generate_dp!=:is_generate_dp',[':status'=>1, ':is_generate_dp'=>1])->andWhere(['main_type'=>$section])->andWhere(['not in','darta_desk.state_status',['फिर्ता', 'अस्वीकृत']]);
        else
            $query = DartaDesk::find()->where('darta_desk.status=:status AND check_state!=:check_state AND is_generate_dp!=:is_generate_dp',[':check_state'=>'प्रकृया पुरा',':status'=>1, ':is_generate_dp'=>1])->andWhere(['not in','darta_desk.state_status',['फिर्ता', 'अस्वीकृत']]);
        $query->joinWith('parliament');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);
        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'main_type', $this->main_type])
            ->andFilterWhere(['like', 'parliament_details.name', $this->parliament_id])
            ->andFilterWhere(['like', 'minister', $this->minister])
            ->andFilterWhere(['like', 'ministry', $this->ministry])
            ->andFilterWhere(['like', 'state_status', $this->state_status])
            ->andFilterWhere(['like', 'created_date', $this->created_date])
            ->andFilterWhere(['like', 'updated_date', $this->updated_date])
            ->andFilterWhere(['like', 'related_committee', $this->related_committee])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'check_state', $this->check_state]);

        return $dataProvider;
    }
}
