<?php

namespace app\modules\program\services;

use Yii;
use yii\filters\AccessControl;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\program\models\MeetingDetails;

/**
 * MeetingDetailsService represents the model behind the search form of `app\modules\program\models\MeetingDetails`.
 */
class MeetingDetailsService extends MeetingDetails
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parliament_id', '_nepali_year', '_nepali_month', '_nepali_day', 'created_by', '_meeting_number'], 'integer'],
            [['adhibasen', 'adhibasen_year', '_date_time', 'status', 'created_date', '_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MeetingDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parliament_id' => $this->parliament_id,
            '_meeting_number' => $this->_meeting_number,
            '_date_time' => $this->_date_time,
            '_nepali_year' => $this->_nepali_year,
            '_nepali_month' => $this->_nepali_month,
            '_nepali_day' => $this->_nepali_day,
            'created_by' => $this->created_by,
            'created_date' => $this->created_date,
        ]);

        $query->andFilterWhere(['like', 'adhibasen', $this->adhibasen])
            ->andFilterWhere(['like', '_time', $this->_time])
            ->andFilterWhere(['like', 'adhibasen_year', $this->adhibasen_year])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
