<?php

namespace app\modules\program\services;

use Yii;
use yii\filters\AccessControl;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\program\models\ReadingPaper;
use app\components\UtilityFunctions;

/**
 * ReadingPaperService represents the model behind the search form of `app\modules\program\models\ReadingPaper`.
 */
class ReadingPaperService extends ReadingPaper
{
    public $meeting_date;
    public $nepali_year;
    public $nepali_month;
    public $nepali_day;
    public $meeting_nepali_date;
    public $meeting_no;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parliament_id', 'meeting_id', 'forum_id', 'order_number', 'goes_through_voting', 'is_read', 'paper_status', 'created_by'], 'integer'],
            [['meeting_header', 'context', 'date', 'minister', 'related_committee', 'status', 'win_by', 'read_at', 'created_date', 'ministry', 'state','meeting_date','meeting_nepali_date','meeting_no'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $today = null)
    {
        if($today)
            $query = ReadingPaper::find()->where('meeting_id=:meeting_id',[':meeting_id'=>UtilityFunctions::ActiveMeeting()]);
        else
            $query = ReadingPaper::find();

        $query->joinWith('meetingDetails');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        if($this->meeting_nepali_date != null)
        {
            $parts = explode('-', $this->meeting_nepali_date);
            if(isset($parts[0]))
                     $this->nepali_year=$parts[0];
            if(isset($parts[1]))
                      $this->nepali_month=$parts[1];
            if(isset($parts[2]))
                      $this->nepali_day=$parts[2];
            
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parliament_id' => $this->parliament_id,
            'forum_id' => $this->forum_id,
            'date' => $this->date,
            'order_number' => $this->order_number,
            'goes_through_voting' => $this->goes_through_voting,
            'is_read' => $this->is_read,
            'read_at' => $this->read_at,
            'paper_status' => $this->paper_status,
            'created_by' => $this->created_by,
            'created_date' => $this->created_date,
        ]);

        $query->andFilterWhere(['like', 'meeting_header', $this->meeting_header])
            ->andFilterWhere(['like', 'meeting_details._date_time', $this->meeting_date])
            ->andFilterWhere(['like', 'meeting_details._nepali_year', $this->nepali_year])
            ->andFilterWhere(['like', 'meeting_details._nepali_month', $this->nepali_month])
            ->andFilterWhere(['like', 'meeting_details._nepali_day', $this->nepali_day])
             ->andFilterWhere(['like', 'meeting_details._meeting_number', $this->meeting_no])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'context', $this->context])
            ->andFilterWhere(['like', 'ministry', $this->ministry])
            ->andFilterWhere(['like', 'minister', $this->minister])
            ->andFilterWhere(['like', 'related_committee', $this->related_committee])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'win_by', $this->win_by]);

        return $dataProvider;
    }
}
