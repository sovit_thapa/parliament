<?php

namespace app\modules\program\services;

use Yii;
use yii\filters\AccessControl;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\program\models\DailyPaper;
use app\components\UtilityFunctions;

/**
 * DailyPaperService represents the model behind the search form of `app\modules\program\models\DailyPaper`.
 */
class DailyPaperService extends DailyPaper
{
    public $meeting_date;
    public $nepali_year;
    public $nepali_month;
    public $nepali_day;
    public $meeting_nepali_date;
    public $meeting_no;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parliament_id', 'meeting_id', 'forum_id', 'order_number', 'created_by'], 'integer'],
            [['meeting_header', 'context', 'minister', 'related_committee', 'status', 'created_date', 'ministry', 'state','is_generated_rp','meeting_date','meeting_nepali_date','meeting_no'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $active = null, $fresh_dp = null)
    {
        $query = DailyPaper::find();
        $query->joinWith('meetingDetails');
        if($active)
            $query->where(['meeting_id'=>UtilityFunctions::ActiveMeeting(),'daily_paper.status'=>$active]);
        if($active && $fresh_dp)
            $query->andWhere(['is_generated_rp'=>0]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        if($this->meeting_nepali_date != null)
        {
            $parts = explode('-', $this->meeting_nepali_date);
            if(isset($parts[0]))
                     $this->nepali_year=$parts[0];
            if(isset($parts[1]))
                      $this->nepali_month=$parts[1];
            if(isset($parts[2]))
                      $this->nepali_day=$parts[2];
            
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'meeting_id' => $this->meeting_id,
            'forum_id' => $this->forum_id,
            'order_number' => $this->order_number,
            'created_by' => $this->created_by,
            'created_date' => $this->created_date,
        ]);

        $query->andFilterWhere(['like', 'meeting_header', $this->meeting_header])
         ->andFilterWhere(['like', 'meeting_details._date_time', $this->meeting_date])
           ->andFilterWhere(['like', 'meeting_details._date_time', $this->meeting_date])
            ->andFilterWhere(['like', 'meeting_details._nepali_year', $this->nepali_year])
            ->andFilterWhere(['like', 'meeting_details._meeting_number', $this->meeting_no])
            ->andFilterWhere(['like', 'meeting_details._nepali_month', $this->nepali_month])
            ->andFilterWhere(['like', 'context', $this->context])
            ->andFilterWhere(['like', 'ministry', $this->ministry])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'minister', $this->minister])
            ->andFilterWhere(['like', 'related_committee', $this->related_committee])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }



    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchactivedp($params)
    {
        $query = DailyPaper::find();
        $query->where(['meeting_id'=>UtilityFunctions::ActiveMeeting()]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parliament_id' => $this->parliament_id,
            'meeting_id' => $this->meeting_id,
            'forum_id' => $this->forum_id,
            'order_number' => $this->order_number,
            'created_by' => $this->created_by,
            'created_date' => $this->created_date,
        ]);

        $query->andFilterWhere(['like', 'meeting_header', $this->meeting_header])
            ->andFilterWhere(['like', 'context', $this->context])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'ministry', $this->ministry])
            ->andFilterWhere(['like', 'minister', $this->minister])
            ->andFilterWhere(['like', 'related_committee', $this->related_committee])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
