<?php

namespace app\modules\program\controllers;

use Yii;
use yii\filters\AccessControl;
use app\modules\program\models\MeetingDetails;
use app\modules\program\services\MeetingDetailsService;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\UtilityFunctions;
use app\modules\program\models\DailyPaper;
use app\modules\program\models\DartaDesk;


/**
 * MeetingDetailsController implements the CRUD actions for MeetingDetails model.
 */
class MeetingDetailsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
         'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin','officer'],

                    ],
                     [
                        'allow' => true,
                        'actions'=>['reader-dashboard','meeting-details'],
                        'roles' => ['reader'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

        ];
    }

    /**
     * Lists all MeetingDetails models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MeetingDetailsService();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
    *meeting cancellation only free dp and rp and donot delete is contain and status
    */
    public function actionMeetingCancellation(){
        if(isset($_GET['meeting_number']) && is_numeric($_GET['meeting_number'])){
            $id = $_GET['meeting_number'];
            $meeting_details = MeetingDetails::findOne($id);
            if(empty($meeting_details))
                throw new NotFoundHttpException('The requested page does not exist.');
            $daily_paper_detail = DailyPaper::find()->where(['meeting_id'=>$id,'status'=>'active'])->groupBy(['forum_id'])->all();
            $darta_id_array = $error_array = array();
            if(!empty($daily_paper_detail)){
                foreach ($daily_paper_detail as $daily_paper) {
                    $darta_id_array[] = $daily_paper->forum_id;
                }
            }
            $transaction = Yii::$app->db->beginTransaction();
            for ($i=0; $i < sizeof($darta_id_array) ; $i++) {
                $drt_id = $darta_id_array[$i];
                $darat_details = DartaDesk::findOne($drt_id);
                if(!empty($darat_details)){
                    $darat_details->is_generate_dp = 0;
                    if(!$darat_details->update(false))
                       $error_array[] = 'false'; 
                }
            }
            $meeting_details->status = 'cancelled';
            if(!$meeting_details->update(false))
                $error_array[] = 'false'; 

            if(!in_array('false', $error_array))
                $transaction->commit();
            else
                $transaction->rollback();
            return $this->redirect(['index']);
        }else
            throw new NotFoundHttpException('The requested page does not exist.');
        echo "<pre>";
        echo print_r($_GET);
        exit;
    }

    /**
     * Displays a single MeetingDetails model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MeetingDetails model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MeetingDetails();

        if ($model->load(Yii::$app->request->post())) {
            $model->_meeting_number = UtilityFunctions::MeetingNumber();
            if($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
            else
            {
                return $this->render('create', [
                    'model' => $model,
                ]);

            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    /**
    * Meeting Details
    */

    public function actionMeetingDetails($meeting_id=null,$status=false){
        $meetin_details = MeetingDetails::find()->where(['status' => 'active'])->one();
        if(empty($meetin_details))  
            return $this->redirect(['create']);
        return $this->render('_drn_paper',['meetin_details'=>$meetin_details]);
    }

    /**
     * Updates an existing MeetingDetails model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionReaderDashboard(){
        $meetin_details = MeetingDetails::find()->where(['status' => 'active'])->one();
        return $this->render('_reading_dashboard',['meetin_details'=>$meetin_details]);
    }

    /**
     * Deletes an existing MeetingDetails model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MeetingDetails model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MeetingDetails the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MeetingDetails::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
