<?php

namespace app\modules\program\controllers;

use Yii;
use yii\filters\AccessControl;
use app\modules\program\models\DartaDesk;
use app\modules\program\services\DartaDeskService;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\modules\program\models\ProcessingDetails;
use app\modules\setting\models\MinistryRelatedCommittee;
use app\modules\program\models\ReadingPaper;

use app\components\UtilityFunctions;

/**
 * DartaDeskController implements the CRUD actions for DartaDesk model.
 */
class DartaDeskController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
         'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin','officer'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

        ];
    }

    /**
     * Lists all DartaDesk models.
     * @return mixed
     */
    public function actionIndex()
    {
        $hide_type = $hide_ministry = false;
        $section = isset($_GET['section']) ? $_GET['section'] : $_GET['DartaDeskService']['main_type'];
        $form_information = UtilityFunctions::DrataFormTemplate($section);
        $type = isset($form_information['type']) ? $form_information['type'] : array();
        $state_type = isset($form_information['state_type']) ? $form_information['state_type'] : array();
        $label = isset($form_information['label']) ? $form_information['label'] : null;
        $title = isset($form_information['title']) ? $form_information['title'] : null;
        $hide_type = isset($form_information['hide_type']) ? $form_information['hide_type'] : null;
        $hide_ministry = isset($form_information['hide_ministry']) ? $form_information['hide_ministry'] : null;
        $bitaran_miti = isset($form_information['bitaran_miti']) ? $form_information['bitaran_miti'] : null;
        $pesh_miti = isset($form_information['pesh_miti']) ? $form_information['pesh_miti'] : null;
        $drata_miti = isset($form_information['drata_miti']) ? $form_information['drata_miti'] : null;
        $state_display = isset($form_information['state_display']) ? $form_information['state_display'] : null;
        $date_dispaly = isset($form_information['date_dispaly']) ? $form_information['date_dispaly'] : null;
        $searchModel = new DartaDeskService();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $section);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'hide_type' => $hide_type,
            'hide_ministry' => $hide_ministry,
            'type' => $type,
            'title' => $title,
            'state_type' => $state_type,
            'bitaran_miti' => $bitaran_miti,
            'pesh_miti' => $pesh_miti,
            'drata_miti'=>$drata_miti,
            'state_display' => $state_display,
            'label' => $label,
            'section' => $section,
            'date_dispaly'=>$date_dispaly
        ]);
    }


    public function actionDarta(){
        return $this->render('_darta_dashboard');
    }

    public function actionDailyPaperDashboard(){
        return $this->render('_generate_d_p_dashboard');
    }

    public function actionDPSample(){

        $hide_type = $hide_ministry = false;
        $dt_main_type = isset($_GET['DartaDeskService']['main_type']) ? $_GET['DartaDeskService']['main_type'] : null;
        $section = isset($_GET['section']) ? $_GET['section'] : $dt_main_type;
        $form_information = UtilityFunctions::DrataFormTemplate($section);
        $type = isset($form_information['type']) ? $form_information['type'] : array();
        $state_type = isset($form_information['state_type']) ? $form_information['state_type'] : array();
        $label = isset($form_information['label']) ? $form_information['label'] : null;
        $title = isset($form_information['title']) ? $form_information['title'] : null;
        $hide_type = isset($form_information['hide_type']) ? $form_information['hide_type'] : null;
        $hide_ministry = isset($form_information['hide_ministry']) ? $form_information['hide_ministry'] : null;
        $bitaran_miti = isset($form_information['bitaran_miti']) ? $form_information['bitaran_miti'] : null;
        $pesh_miti = isset($form_information['pesh_miti']) ? $form_information['pesh_miti'] : null;
        $drata_miti = isset($form_information['drata_miti']) ? $form_information['drata_miti'] : null;
        $state_display = isset($form_information['state_display']) ? $form_information['state_display'] : null;
        $date_dispaly = isset($form_information['date_dispaly']) ? $form_information['date_dispaly'] : null;
        $searchModel = new DartaDeskService();
        $dataProvider = $searchModel->dratsection(Yii::$app->request->queryParams, $section);
        return $this->render('_darta_desk', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'hide_type' => $hide_type,
            'hide_ministry' => $hide_ministry,
            'type' => $type,
            'title' => $title,
            'state_type' => $state_type,
            'bitaran_miti' => $bitaran_miti,
            'pesh_miti' => $pesh_miti,
            'drata_miti'=>$drata_miti,
            'state_display' => $state_display,
            'label' => $label,
            'section' => $section,
            'date_dispaly'=>$date_dispaly
        ]);
    }

    /**
    *check related state for bill as well as samdhi, samjhauta
    */

    public function actionCheckState(){
        $check_state = null;
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost && $request->post('depdrop_parents') && is_array($request->post('depdrop_parents'))) {
            $check_state = $request->post('depdrop_parents')[0];
        }
        $option_array = array();
        if($check_state=='व्यवस्थापन कार्य'){
            $option_array[0]['id'] = 'वितरण';$option_array[0]['name'] = 'वितरण';
            $option_array[1]['id'] = 'अनुमति माग्ने';$option_array[1]['name'] = 'अनुमति माग्ने';
            $option_array[2]['id'] = 'संशोधनको म्याद';$option_array[2]['name'] = 'संशोधनको म्याद';
            $option_array[3]['id'] = 'दफावार छलफल समितिमा पठाइयोस्';$option_array[3]['name'] = 'दफावार छलफलको लागि समितिमा पठाइयोस्';
            $option_array[4]['id'] = 'दफावार छलफल सदनमा संशोधन सहित';$option_array[4]['name'] = 'दफावार छलफल सदनमा संशोधन सहित';
            $option_array[5]['id'] = 'दफावार छलफल सदनमा संशोधन विना';$option_array[5]['name'] = 'दफावार छलफल सदनमा संशोधन विना';
            $option_array[6]['id'] = 'समितिको प्रतिवेदन पेश';$option_array[6]['name'] = 'समितिको प्रतिवेदन पेश';
            $option_array[7]['id'] = 'प्रतिवेदन सहितको विधेयक समितिमा फिर्ता';$option_array[7]['name'] = 'प्रतिवेदन सहितको विधेयक समितिमा फिर्ता';
            $option_array[8]['id'] = 'पुनः समितिको प्रतिवेदन पेश';$option_array[8]['name'] = 'पुनः समितिको प्रतिवेदन पेश';
            $option_array[9]['id'] = 'प्रतिवेदन सहित छलफल';$option_array[9]['name'] = 'प्रतिवेदन सहित छलफल';
            $option_array[10]['id'] = 'पारित';$option_array[10]['name'] = 'पारित';
        }

        if(in_array($check_state, ['सन्धि','महासन्धि','सम्झौता'])){
            $option_array[0]['id'] = 'वितरण';$option_array[0]['name'] = 'वितरण';
            $option_array[1]['id'] = 'पेश';$option_array[1]['name'] = 'पेश';
            $option_array[2]['id'] = 'छलफल';$option_array[2]['name'] = 'छलफल';
            $option_array[3]['id'] = 'अनुमोदन';$option_array[3]['name'] = 'अनुमोदन';
        }
        echo Json::encode(['output' =>$option_array, 'selected' => '']);
    }


    /**
    *
    */

    public function actionUpgradeSamitima(){
        if(isset($_GET['id']) && is_numeric($_GET['id'])){
            $id = $_GET['id'];
            $darta_details = DartaDesk::findOne($id);
            if(empty($darta_details))
                throw new NotFoundHttpException('Bill is empty, please check it again');
            $state = $darta_details->check_state;
            $darta_details->state = $state;
            $darta_details->state_status = 'स्वीकृत';
            $darta_details->check_state = 'दफावार छलफल समितिमा पठाइयोस्';
            if($darta_details->save())
                return $this->redirect(['d-p-sample', 'section' => 'bill']);
        }else
            throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
    *upgrade aadhyadesh
    */

    public function actionUpgradeAadhyadesh(){
        if(isset($_GET['id']) && is_numeric($_GET['id'])){
            $id = $_GET['id'];
            $darta_details = DartaDesk::findOne($id);
            if(empty($darta_details))
                throw new NotFoundHttpException('Bill is empty, please check it again');
            $state = $darta_details->check_state;
            $darta_details->state = $state;
            $darta_details->state_status = 'स्वीकृत';
            $darta_details->check_state = 'छलफल';
            if($darta_details->save())
                return $this->redirect(['d-p-sample', 'section' => $darta_details->main_type]);
        }else
            throw new NotFoundHttpException('The requested page does not exist.');
    }


    /**
     * Displays a single DartaDesk model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $darta_information = $this->findModel($id);
        $type_array = ['सन्धि' => 'सन्धि','महासन्धि' => 'महासन्धि','सम्झौता'=>'सम्झौता', 'राष्ट्रपति' => 'राष्ट्रपतिको पत्र','प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालय' => 'प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालयको पत्र' ,'राष्ट्रपति कार्यालय' => 'राष्ट्रपति कार्यालयको पत्र', 'व्यवस्थापन कार्य' => 'व्यवस्थापन कार्य', 'शोक प्रस्ताव' =>'शोक प्रस्ताव'];
        $title = isset($type_array[$darta_information->type]) ? $type_array[$darta_information->type] : '';
        $hide_type = $hide_ministry = false;
        if(in_array($darta_information->type, ['राष्ट्रपति','प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालय','राष्ट्रपति कार्यालय'])){
            $hide_type = false;
            $hide_ministry = true;
        }
        if(in_array($darta_information->type, ['सन्धि','महासन्धि','सम्झौता'])){
            $hide_type = false;
            $hide_ministry = false;
        }
        if($darta_information->type=='व्यवस्थापन कार्य'){
            $hide_type = true;
            $hide_ministry = false;
        }
        if($darta_information->type=='शोक प्रस्ताव'){
            $hide_type = true;
            $hide_ministry = false;
        }
        $darta_processing = ProcessingDetails::find()->where(['darta_id'=>$id])->all();
        return $this->render('view', [
            'darta_information' => $darta_information,'title'=>$title, 'hide_type'=>$hide_type, 'hide_ministry' => $hide_ministry,'darta_processing'=>$darta_processing
        ]);
    }

    /**
     * Creates a new DartaDesk model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DartaDesk();
        $errors_array = array();
        $transaction = Yii::$app->db->beginTransaction();
        $section = isset($_GET['section']) ? $_GET['section'] : null;
        $form_information = UtilityFunctions::DrataFormTemplate($section);
        $type = isset($form_information['type']) ? $form_information['type'] : array();
        $type_state = isset($form_information['type_state']) ? $form_information['type_state'] : array();
        $label = isset($form_information['label']) ? $form_information['label'] : null;
        $title = isset($form_information['title']) ? $form_information['title'] : null;
        $hide_type = isset($form_information['hide_type']) ? $form_information['hide_type'] : null;
        $hide_ministry = isset($form_information['hide_ministry']) ? $form_information['hide_ministry'] : null;
        $bitaran_miti = isset($form_information['bitaran_miti']) ? $form_information['bitaran_miti'] : null;
        $pesh_miti = isset($form_information['pesh_miti']) ? $form_information['pesh_miti'] : null;
        $drata_miti = isset($form_information['drata_miti']) ? $form_information['drata_miti'] : null;
        $presentor = isset($form_information['presentor']) ? $form_information['presentor'] : null;
        $suppoter = isset($form_information['suppoter']) ? $form_information['suppoter'] : null;

        if ($model->load(Yii::$app->request->post())) {
            $org_state = $model->state;
            $all_state = UtilityFunctions::StateProcess($model->main_type);
            $index = array_search($org_state, $all_state);
            $check_state = isset($all_state[$index+1]) ? $all_state[$index+1] : $org_state;
            $state = !empty($org_state) ? $org_state : $all_state[0];
            $state_status = $index==0 ? 'प्रकृया' : 'स्वीकृत';
            $model->state = $state;
            $model->state_status = $state_status;
            $model->check_state = $check_state;
            if(!empty($model->related_committee)){
                $committee = $model->related_committee;
                $committee_details = MinistryRelatedCommittee::find()->where(['committee'=>$committee,'_status'=>1])->one();
                $model->ministry = !empty($committee_details) ? $committee_details->ministry : '';
            }
            if(!empty($model->suppoter))
                $model->suppoter = implode(',', $model->suppoter);
            if(!$model->save()){
                $errors_array[] = 'false';
                echo "<pre>";
                echo print_r($model->errors);
                exit;
            }
            $drata_miti = isset($_POST['DartaDesk']['drata_miti']) ? $_POST['DartaDesk']['drata_miti'] : null;
            $bitaran_miti = isset($_POST['DartaDesk']['bitaran_miti']) ? $_POST['DartaDesk']['bitaran_miti'] : null;
            $darta_processing = new ProcessingDetails();
            $darta_processing->attributes = $model->attributes;
            $darta_processing->darta_id = $model->id;
            $darta_processing->state=$state;
            $darta_processing->status = 1;
            $darta_processing->date = $drata_miti ? UtilityFunctions::NepaliToEnglish($drata_miti) : new \yii\db\Expression('NOW()');
            if(!$darta_processing->save()){
                $errors_array[] = 'false';
                echo "<pre>";
                echo print_r($darta_processing->errors);
                exit;

            }
            if($bitaran_miti){
                $darta_processing = new ProcessingDetails();
                $darta_processing->attributes = $model->attributes;
                $darta_processing->darta_id = $model->id;
                $darta_processing->state='वितरण';
                $darta_processing->status = 1;
                $darta_processing->date = UtilityFunctions::NepaliToEnglish($bitaran_miti);
                if(!$darta_processing->save()){
                    $errors_array[] = 'false';
                    echo "<pre>";
                    echo print_r($darta_processing->errors);
                    exit;

                }
            }
            if(!in_array('false', $errors_array)){
                $transaction->commit();
                return $this->redirect(['view', 'id' => $model->id]);
            }else{

                $transaction->rollback();
                throw new NotFoundHttpException('Please consult to administrator, case cannot register.');
            }

        } else {
            return $this->render('create', [
                'model' => $model,'type'=>$type,'title'=>$title, 'hide_type' => $hide_type, 'hide_ministry' => $hide_ministry, 'label'=>$label,'bitaran_miti' => $bitaran_miti,'pesh_miti' => $pesh_miti,'drata_miti'=>$drata_miti, 'section'=>$section,'suppoter'=>$suppoter, 'presentor'=>$presentor
            ]);
        }
    }

    /**
     * Updates an existing DartaDesk model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $section = $model->main_type;
        $form_information = UtilityFunctions::DrataFormTemplate($section);
        $type = isset($form_information['type']) ? $form_information['type'] : array();
        $type_state = isset($form_information['type_state']) ? $form_information['type_state'] : array();
        $label = isset($form_information['label']) ? $form_information['label'] : null;
        $title = isset($form_information['title']) ? $form_information['title'] : null;
        $hide_type = isset($form_information['hide_type']) ? $form_information['hide_type'] : null;
        $hide_ministry = isset($form_information['hide_ministry']) ? $form_information['hide_ministry'] : null;
        $bitaran_miti = isset($form_information['bitaran_miti']) ? $form_information['bitaran_miti'] : null;
        $pesh_miti = isset($form_information['pesh_miti']) ? $form_information['pesh_miti'] : null;
        $drata_miti = isset($form_information['drata_miti']) ? $form_information['drata_miti'] : null;
        $presentor = isset($form_information['presentor']) ? $form_information['presentor'] : null;
        $suppoter = isset($form_information['suppoter']) ? $form_information['suppoter'] : null;
        if($model->load(Yii::$app->request->post())){
            $org_state = $model->state;
            $all_state = UtilityFunctions::StateProcess($model->main_type);
            $index = array_search($org_state, $all_state);
            $check_state = isset($all_state[$index+1]) ? $all_state[$index+1] : $org_state;
            $state = !empty($org_state) ? $org_state : $all_state[0];
            $state_status = $index==0 ? 'प्रकृया' : 'स्वीकृत';
            $model->state = $state;
            $model->state_status = $state_status;
            $model->check_state = $check_state;
            if(!empty($model->suppoter))
                $model->suppoter = implode(',', $model->suppoter);
            if ($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,'type'=>$type,'title'=>$title, 'hide_type' => $hide_type, 'hide_ministry' => $hide_ministry, 'label'=>$label,'bitaran_miti' => $bitaran_miti,'pesh_miti' => $pesh_miti,'drata_miti'=>$drata_miti, 'section'=>$section,'suppoter'=>$suppoter, 'presentor'=>$presentor
            ]);
        }
    }



    public function actionBillingReport($export=null)
    {
        $column_header = ReadingPaper::billing_process;
        $column[] = 'क्र.सं.';
        $column[] = 'विधेयकको नाम';
        for ($i=0; $i < sizeof($column_header)-1; $i++) { 
            $column[] = $column_header[$i];
        }
        $billing_information = array();
        $darta_processing = ProcessingDetails::find()->where('type=:type group by darta_id',[':type'=>'व्यवस्थापन कार्य'])->all();
        if(!empty($darta_processing)){
            $sn = 0;
            foreach ($darta_processing as $darat) {
                $billing_information[$sn]['क्र.सं.'] = $sn+1;
                $darta_informaiton = DartaDesk::findOne($darat->darta_id);
                $billing_information[$sn]['विधेयकको नाम'] = $darta_informaiton ? strip_tags($darta_informaiton->title) : '';
                for ($j=0; $j < sizeof($column_header)-1; $j++) { 
                    $date='';
                    $section = $column_header[$j];
                    $darta_processing_individual = ProcessingDetails::find()->where('type=:type AND darta_id=:darta_id AND state=:state order by date',[':type'=>'व्यवस्थापन कार्य', ':darta_id'=>$darat->darta_id, ':state'=>$section])->all();
                    $date_array = array();
                    if(!empty($darta_processing_individual)){
                        foreach ($darta_processing_individual as $individuals) {
                            if(!in_array($individuals->date, $date_array)){
                                $date .=','.$individuals->date;
                                $date_array[]  = $individuals->date;
                            }
                        }
                    }
                    $billing_information[$sn][$section] = ltrim($date,',');
        
                }
                $sn++;
            }
        }

         //Exporting CSV FILE
        if($export != null && $export == 'csv')
        {
            $filename = 'Data-'.Date('Y-m-d:H-i-s').'-Report.csv';
            //header("Content-type: application/vnd-ms-excel");
            header("content-type:application/csv;");
            header("Content-Disposition: attachment; filename=".$filename);
            echo "\xEF\xBB\xBF";
            echo $this->renderPartial('export/csvreport', ['billing_information'=>$billing_information,'column'=>$column]);
        }
        else
        {
            return $this->render('_billing_report', ['billing_information'=>$billing_information,'column'=>$column]);
        }
    }


    public function actionReport($type='ordinance',$export=null)
    {
        $data=$this->reportInfo($type);
        $column_header = $data['column_header'];
        $column[] = 'क्र.सं.';
        $column[] = 'विधेयकको नाम';
        for ($i=0; $i < sizeof($column_header); $i++) { 
            $column[] = $column_header[$i];
        }
        $billing_information = array();
        $darta_processing = $data['darta_processing'];
        if(!empty($darta_processing)){
            $sn = 0;
            foreach ($darta_processing as $darat) {
                $billing_information[$sn]['क्र.सं.'] = $sn+1;
                $darta_informaiton = DartaDesk::findOne($darat->darta_id);
                $billing_information[$sn]['विधेयकको नाम'] = $darta_informaiton ? strip_tags($darta_informaiton->title) : '';
                for ($j=0; $j < sizeof($column_header); $j++) { 
                    $date='';
                    $section = $column_header[$j];
                    $darta_processing_individual = $this->dartaProcessingIndividual($type,$darat->darta_id,$section);
                    $date_array = array();
                    if(!empty($darta_processing_individual)){
                        foreach ($darta_processing_individual as $individuals) {
                            if(!in_array($individuals->date, $date_array)){
                                $date .=','.$individuals->date;
                                $date_array[]  = $individuals->date;
                            }
                        }
                    }
                    $billing_information[$sn][$section] = ltrim($date,',');
        
                }
                $sn++;
            }
        }


         //Exporting CSV FILE
        if($export != null && $export == 'csv')
        {
            $filename = 'Data-'.Date('Y-m-d:H-i-s').'-Report.csv';
            //header("Content-type: application/vnd-ms-excel");
            header("content-type:application/csv;");
            header("Content-Disposition: attachment; filename=".$filename);
            echo "\xEF\xBB\xBF";
            echo $this->renderPartial('export/csvreport', ['billing_information'=>$billing_information,'column'=>$column]);
        }
        else
        {
            return $this->render('_billing_report', ['billing_information'=>$billing_information,'column'=>$column]);
        }
    }


    private function reportInfo($type)
    {
        $data=array();
        if($type=='ordinance')
        {
            $data['column_header']=ReadingPaper::ordinance_state;
            $data['darta_processing']=ProcessingDetails::find()->where('type=:type group by darta_id',[':type'=>'अध्यादेश'])->all();
            return $data;
        }
        else if ($type=='treaties')
        {
            $data['column_header']=ReadingPaper::aggrement_processing;
            $data['darta_processing']=ProcessingDetails::find()->where('type=:type || type=:type2 || type=:type3 group by darta_id',[':type'=>'सन्धि',':type2'=>'सम्झौता',':type3'=>'महासन्धि'])->all();
            return $data;
        }

    }
    private function dartaProcessingIndividual($type,$darta_id,$state)
    {
        if($type=='ordinance')
        {
           $data=ProcessingDetails::find()->where('type=:type AND darta_id=:darta_id AND state=:state order by date',[':type'=>'अध्यादेश', ':darta_id'=>$darta_id, ':state'=>$state])->all();
           return $data;
        }
        else if ($type=='treaties')
        {
           $data=ProcessingDetails::find()->where('(type=:type || type=:type2 || type=:type3) AND darta_id=:darta_id AND state=:state order by date',[':type'=>'सन्धि',':type2'=>'सम्झौता',':type3'=>'महासन्धि',':darta_id'=>$darta_id, ':state'=>$state])->all();
            return $data;
        }


    }
    /**
    * search section, please check
    */

    /**
     * Deletes an existing DartaDesk model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DartaDesk model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DartaDesk the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DartaDesk::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
