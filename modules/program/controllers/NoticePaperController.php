<?php

namespace app\modules\program\controllers;

use Yii;
use app\modules\program\models\NoticePaper;
use app\modules\program\services\NoticePaperService;
use app\modules\program\models\MeetingDetails;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\UtilityFunctions;
use kartik\mpdf\Pdf;

/**
 * NoticePaperController implements the CRUD actions for NoticePaper model.
 */
class NoticePaperController extends Controller
{
     /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
         'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin','officer'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

        ];
    }


    /**
    *notice section, please check it
    */

    public function actionNotice(){
        $meeting_id = isset($_GET['meeting_number']) && is_numeric($_GET['meeting_number']) ? $_GET['meeting_number'] : null;
        if(!$meeting_id)
            $meeting_id = UtilityFunctions::ActiveMeeting();

        $meeting_details = MeetingDetails::findOne($meeting_id);
        $notice_detail = NoticePaper::find()->where('meeting_id=:meeting_id AND paper_status=:paper_status order by order_number ASC',[':meeting_id'=>$meeting_id, ':paper_status'=>1])->all();


            if(isset($_GET['exporttoprint']) && $_GET['exporttoprint'] == true)
            {
                return $this->renderAjax('_notice_paper',['notice_detail'=>$notice_detail,'meeting_details'=>$meeting_details]);
            }
           if(isset($_GET['exporttoword']) && $_GET['exporttoword'] == true)
            {
                $this->layout='@app/views/layouts/exportLayout';
                $content=$this->renderPartial('_notice_paper',['notice_detail'=>$notice_detail,'meeting_details'=>$meeting_details]);
                $var = $content;
                $div = $content;
                header("Pragma: no-cache");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: private", false);
                header("Content-type: application/vnd.ms-word");
                header("Content-Disposition: attachment; filename=\"Notice.doc");
                header("Content-Transfer-Encoding: binary");
                ob_clean();
                flush();
                return $this->render('_daily_paper',['daily_paper_header_array_'=>$daily_paper_header_array_ ,'daily_paper_array'=>$daily_paper_array, 'meeting_details'=>$meeting_details]);
            }
            if(isset($_GET['exporttopdf']) && $_GET['exporttopdf'] == true)
            {
                 $this->layout='@app/views/layouts/exportLayout';
                $content=$this->renderPartial('_notice_paper',['notice_detail'=>$notice_detail,'meeting_details'=>$meeting_details]);
                $pdf = new Pdf([
                    
                    'mode' => Pdf::MODE_UTF8, 

                    // A4 paper format
                    'format' => Pdf::FORMAT_A4, 
                    // portrait orientation
                    'orientation' => Pdf::ORIENT_PORTRAIT, 
                    // stream to browser inline
                    'destination' => Pdf::DEST_BROWSER, 
                    // your html content input
                    'content' => $content,  
                    // format content from your own css file if needed or use the
                    // enhanced bootstrap css built by Krajee for mPDF formatting 
                    'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
                    // any css to be embedded if required
                    'cssInline' => '.kv-heading-1{font-size:18px}', 
                     // set mPDF properties on the fly
                    'options' => ['title' => 'Notice Paper'],
                     // call mPDF methods on the fly
                    'methods' => [ 
                        'SetHeader'=>['Notice Paper'], 
                        'SetFooter'=>['{PAGENO}'],
                    ]
                ]);
    
                // return the pdf output as per the destination setting
                return $pdf->render(); 
            }
        return $this->render('_notice_paper',['notice_detail'=>$notice_detail,'meeting_details'=>$meeting_details]);

    }

    /**
     * Lists all NoticePaper models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NoticePaperService();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NoticePaper model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NoticePaper model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NoticePaper();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing NoticePaper model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing NoticePaper model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NoticePaper model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NoticePaper the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NoticePaper::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
