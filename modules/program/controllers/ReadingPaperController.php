<?php

namespace app\modules\program\controllers;

use Yii;
use yii\filters\AccessControl;
use app\modules\program\models\ReadingPaper;
use app\modules\program\models\NoticePaper;
use app\modules\program\models\ReadingPaperContent;
use app\modules\program\models\DailyPaper;
use app\modules\program\models\DartaDesk;
use app\modules\program\models\MeetingDetails;
use app\modules\program\models\ParticipateMemberDiscussion;
use app\modules\program\services\ReadingPaperService;
use app\modules\setting\models\ParliamentMember;
use app\modules\setting\models\PoliticalParty;
use yii\web\Controller;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\UtilityFunctions;
use app\modules\program\models\Statement;


/**
 * ReadingPaperController implements the CRUD actions for ReadingPaper model.
 */
class ReadingPaperController extends Controller
{
     /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
         'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin','officer'],
                    ],

                    [
                        'allow' => true,
                        'actions'=>['today-all-r-p'],
                        'roles' => ['reader'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

        ];
    }

    /**
     * Lists all ReadingPaper models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReadingPaperService();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
         $meeting_id=MeetingDetails::find()->where('status=:status',[':status'=>'active'])->one();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'meeting_id'=>$meeting_id
        ]);
    }

    /**
     * Displays a single ReadingPaper model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model=$this->findModel($id);
        $dailyPaper=DailyPaper::findOne($model->daily_paper_id);
        $statements=$model->statement;
        $next_page_id = array();
        $reading_content_details = ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND content_status=:content_status',[':reading_paper_id'=>$id, ':content_status'=>1])->orderBy(['order_'=>SORT_ASC])->all();
        if(!empty($reading_content_details)){
            foreach ($reading_content_details as $reading_content) {
                $next_page_id[] = $reading_content->id.":".$reading_content->_next_id;
            }
        }
        return $this->render('view', [
            'dailyPaper'=>$dailyPaper,
            'model' => $model,
            'statements'=>$statements,
            'reading_content_details' => $reading_content_details,
            'id'=>$id,
            'next_page_id' => $next_page_id
        ]);
    }

    /**
     * Creates a new ReadingPaper model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ReadingPaper();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionAllReadingSection($meeting_id)
    {
            $this->layout='@app/views/layouts/main';
             $child_contents=ReadingPaper::find()->where('meeting_id=:meeting_id AND paper_status=:paper_status'  ,[':meeting_id'=>$meeting_id,':paper_status'=>1])->orderBy(['primary_order' => SORT_ASC, 'order_number'=>SORT_ASC])->all();
            return $this->render('reading_paper_list',['contents'=>$child_contents]);
    } 
    /**
    * add primister speech in reading paper section
    */
    public function actionPriministerSpeech(){
        if(isset($_GET['meeting']) && isset($_GET['status'])){
            $meeting = $_GET['meeting'];
            $status = $_GET['status'];
            $error_array = array();
            $transaction = Yii::$app->db->beginTransaction();
            $meeting_detail = MeetingDetails::find()->where(['id'=>$meeting,'status'=>['active','halt']])->one();
            if(empty($meeting_detail))
                throw new NotFoundHttpException('The requested action cannot perform, meeting is not authorized.');
            if($status == 'add'){
                $reading_paper_id = null;
                $reading_paper_status = ReadingPaper::find()->where(['meeting_header'=>'प्रधानमन्त्रीको बक्तब्य','meeting_id'=>$meeting,'parliament_id'=>UtilityFunctions::SambhidhanSava()])->one();
                if(!empty($reading_paper_status)){
                    $reading_paper_status->paper_status = 1;
                    if(!$reading_paper_status->save(false))
                        $error_array[] = 'false';
                    
                    $reading_paper_id = $reading_paper_status->id;
                }else{
                    $model = new ReadingPaper();
                    $model->parliament_id = UtilityFunctions::SambhidhanSava();
                    $model->meeting_id = $meeting;
                    $model->meeting_header = 'प्रधानमन्त्रीको बक्तब्य';
                    $model -> main_type = 'speech';
                    $model -> state = 'दर्ता';
                    $model -> status = 'प्रकृयामा';
                    $model->context = 'सर्वप्रथम सम्माननीय प्रधानमन्त्रीले बोल्नको लागि समय माग गर्नुभएको छ, म उहाँलाई समय दिन्छु ।';
                    $model->date = new \yii\db\Expression('NOW()');
                    $model->order_number = 1;
                    $model->primary_order = 1;
                    $model -> page_type = 'general';

                    if(!$model->save())
                        $error_array[] = 'false';
                    $reading_paper_id = $model->id;
                }

                $reading_content_section = ReadingPaperContent::find()->where(['reading_paper_id'=>$reading_paper_id,'pages'=>'_first_content'])->one();
                if(empty($reading_content_section))
                    $reading_content_section = new ReadingPaperContent();
                $reading_content_section -> reading_paper_id = $reading_paper_id;
                $reading_content_section -> page_type = 'general';
                $reading_content_section -> pages = '_first_content';
                $reading_content_section -> _end = 'paper';
                $reading_content_section -> content = 'सर्वप्रथम सम्माननीय प्रधानमन्त्रीले बोल्नको लागि समय माग गर्नुभएको छ, म उहाँलाई समय दिन्छु ।';
                $reading_content_section -> order_ =1;
                $reading_content_section -> created_by = Yii::$app->user->id;
                $reading_content_section -> created_date = new \yii\db\Expression('NOW()');
                $reading_content_section -> _end = 'paper';
                if(!$reading_content_section->save()){
                    $error_array[] = 'false';
                    echo "<pre>";
                    echo print_r($reading_content_section->errors);
                    exit;
                }
            }
            if($status == 'remove'){
                $reading_paper_status = ReadingPaper::find()->where(['meeting_header'=>'प्रधानमन्त्रीको बक्तब्य','meeting_id'=>$meeting,'parliament_id'=>UtilityFunctions::SambhidhanSava(),'paper_status'=>1])->one();
                if(!empty($reading_paper_status)){
                    $reading_paper_status->paper_status = '0';
                    if(!$reading_paper_status->save(false))
                        $error_array[] = 'false';
                }
            }
            if(!in_array('false', $error_array)){
                $transaction->commit();
                $this->redirect(['meeting-details/meeting-details']);
            }
            else{
                $transaction->rollback();
                throw new NotFoundHttpException('The requested action cannot perform, please inform admin.');
            }
        }
    }

    /**
    * adding zero time and special time for parliament member
    */
    public function actionPrepareRP(){
        if(isset($_GET['meeting']) && isset($_GET['section'])){
            $meeting = $_GET['meeting'];
            $section = $_GET['section'];
             $pages_ = [];
            if($section == 'zero_time')
                $pages_ = ReadingPaper::time_template;
            if($section == 'special_time')
                $pages_ = ReadingPaper::time_template;
            $meeting_detail = MeetingDetails::find()->where(['id'=>$meeting,'status'=>['active','halt']])->one();
            if(empty($meeting_detail))
                throw new NotFoundHttpException('The requested action cannot perform, meeting is not authorized.');
            $title = $section == 'zero_time' ? 'शून्य समय' : 'विशेष समय';
            $parliament_member = UtilityFunctions::ActiveParliamentMember();
            $political_party = PoliticalParty::find()->where(['status'=>1])->all();
            $model = new ParticipateMemberDiscussion();
            $new_model = new ReadingPaper();
            $previous_participate_member = ParticipateMemberDiscussion::find()->where(['meeting_id'=>$meeting,'parliament_id'=>UtilityFunctions::SambhidhanSava(), 'status'=>1, 'type'=>$section])->all();
            $participate_member = array();
            if(!empty($previous_participate_member)){
                $sn = 0;
                foreach ($previous_participate_member as $member_) {
                    $party_id = $member_->member ? $member_->member->political_id : 0;
                    $party_details = PoliticalParty::findOne($party_id);
                    $participate_member[$sn]['name'] = $member_->member ? $member_->member->member_title.' '.$member_->member->first_name.' '.$member_->member->last_name : 'null';
                    $participate_member[$sn]['party'] = $party_details ? $party_details->name : ' ';
                    $participate_member[$sn]['speech_order'] = $member_->speech_order;
                    $participate_member[$sn]['member_id'] = $member_->parliament_member_id;
                    $sn++;
                }
            }
            return $this->render('_discussion_forum',['model'=>$model,'meeting_detail'=>$meeting_detail, 'title'=>$title,'political_party'=>$political_party,'section'=>$section,'participate_member'=>$participate_member,'pages_'=>$pages_, 'new_model'=>$new_model]);

        }else
            throw new NotFoundHttpException('The requested action cannot performed.');
    }

    /**
    *participate member during discussion in particular reading paper
    */

    public function actionReadingPaperDiscuss(){
        if(isset($_GET['id']) && is_numeric($_GET['id'])){
            $id = $_GET['id'];
            $statement_id=isset($_GET['statement_id'])?$_GET['statement_id']:null;
            $meeting_detail = MeetingDetails::find()->where(['id'=>UtilityFunctions::ActiveMeeting(),'status'=>['active','halt','next']])->one();
            if(empty($meeting_detail))
                throw new NotFoundHttpException('The requested action cannot perform, meeting is not authorized.');
            $title = 'छलफलमा भागलिनेहरु';
            $parliament_member = UtilityFunctions::ActiveParliamentMember();
            $political_party = PoliticalParty::find()->where(['status'=>1])->all();
            $model = new ParticipateMemberDiscussion();
            $previous_participate_member = ParticipateMemberDiscussion::find()->where(['meeting_id'=>UtilityFunctions::ActiveMeeting(),'parliament_id'=>UtilityFunctions::SambhidhanSava(), 'status'=>1, 'type'=>'on_topic','reading_paper_id'=>$id,'statement_id'=>$statement_id])->orderBy('speech_order')->all();
            $participate_member = array();
            if(!empty($previous_participate_member)){
                $sn = 0;
                foreach ($previous_participate_member as $member_) {
                    $party_id = $member_->member ? $member_->member->political_id : 0;
                    $party_details = PoliticalParty::findOne($party_id);
                    $participate_member[$sn]['name'] = $member_->member ? $member_->member->member_title.' '.$member_->member->first_name.' '.$member_->member->last_name : 'null';
                    $participate_member[$sn]['party'] = $party_details ? $party_details->name : ' ';
                    $participate_member[$sn]['speech_order'] = $member_->speech_order;
                    $participate_member[$sn]['member_id'] = $member_->parliament_member_id;
                    $participate_member[$sn]['statement'] = $member_->statement == null? '':$member_->statement->statement_type;
                    $participate_member[$sn]['statement_id'] = $member_->statement == null? '':$member_->statement->id;
                    $participate_member[$sn]['reading_id'] = $member_->reading_paper_id;
                    $participate_member[$sn]['participate_member_id'] = $member_->id;
                    $sn++;
                }
            }
            return $this->render('_discuss_member_in_bill',['model'=>$model,'meeting_detail'=>$meeting_detail, 'title'=>$title,'political_party'=>$political_party,'participate_member'=>$participate_member,'id'=>$id,'section'=>'on_topic','statement_id'=>$statement_id]);

        }else
            throw new NotFoundHttpException('The requested action cannot performed.');

    }
/**
*available list of parliament member to participation for zero and special time discussion
*/
    public function actionParliamentMember(){

        if(isset($_GET['meeting']) && isset($_GET['party'])){
            $meeting = $_GET['meeting'];
            $party = $_GET['party'];
            $reading_paper_id = isset($_GET['reading_paper_id']) ? $_GET['reading_paper_id'] : null;
            $section = isset($_GET['section']) ? $_GET['section'] : null;
            $meeting_detail = MeetingDetails::find()->where(['id'=>$meeting,'status'=>['active','halt','next']])->one();
            if(empty($meeting_detail))
                throw new NotFoundHttpException('The requested action cannot perform, meeting is not authorized.');
            $party_information = PoliticalParty::findOne($party);
            $participate_member = $member_order = array();
            if($reading_paper_id){
                $previous_participate_member = ParticipateMemberDiscussion::find()->where(['meeting_id'=>$meeting,'parliament_id'=>UtilityFunctions::SambhidhanSava(), 'status'=>1, 'type'=>$section,'reading_paper_id'=>$reading_paper_id])->all();
            }else{
                $previous_participate_member = ParticipateMemberDiscussion::find()->where(['meeting_id'=>$meeting,'parliament_id'=>UtilityFunctions::SambhidhanSava(), 'status'=>1, 'type'=>$section])->all();
            }
            if(!empty($previous_participate_member)){
                foreach ($previous_participate_member as $member_) {
                    $participate_member[] = $member_->parliament_member_id;
                    $member_order[$member_->parliament_member_id]  = $member_->speech_order;
                }
            }
            $parliament_member = UtilityFunctions::ActiveParliamentMember($party, $participate_member);
            $html = $this->renderPartial('_party_wise_member',['parliament_member'=>$parliament_member, 'party_information'=>$party_information, 'participate_member'=>$participate_member, 'member_order'=>$member_order,'previous_participate_member'=>$previous_participate_member]);
            return Json::encode($html);
        }
        else
            throw new NotFoundHttpException('The requested action cannot performed.');
    }

    /**
    *add participate member in on-topic discussion forum
    */
    public function actionSaveOnTopicMember(){
        if(isset($_POST['neworder']))
        {
           $neworders=$_POST['neworder'];
           $null_array=true;
           foreach ($neworders as $key => $neworder) 
           {
              if($neworder != null)
              {
                $null_array=false;
              }
           }
           if(!$null_array)
           {
                foreach ($neworders as $key => $value) 
                {
                   if($value != null)
                   {
                        $model=ParticipateMemberDiscussion::findOne($key);
                        $model->speech_order=$value;
                        $model->save();
                   }
                }
                $reading_paper_id = $_POST['reading_paper_id'];
                $statement_id=$_POST['statement_id'];
                if($_POST['statement_id'] == null)
                    return $this->redirect(['reading-paper/reading-paper-discuss/?id='.$reading_paper_id]);
                    else
                    return $this->redirect(['reading-paper/reading-paper-discuss/?id='.$reading_paper_id.'&statement_id='.$_POST['statement_id']]);

           }
        }
        if(isset($_POST['member_array']) && isset($_POST['meeting_id']) && is_numeric($_POST['meeting_id']) && isset($_POST['type']) && $_POST['type']=='on_topic'){
            $participate_member_id = $_POST['member_array'];
            $meeting_id = $_POST['meeting_id'];
            $type = $_POST['type'];
            $reading_paper_id = $_POST['reading_paper_id'];
            $statement_id=$_POST['statement_id'];
            $error_array = array();
            $transaction = Yii::$app->db->beginTransaction();
            for ($i=0; $i < sizeof($participate_member_id) ; $i++) {
                $speech_order_section = isset($_POST['member_order_'.$participate_member_id[$i]]) ? $_POST['member_order_'.$participate_member_id[$i]] : 100; 
                $model = ParticipateMemberDiscussion::find()->where(['meeting_id'=>$meeting_id,'type'=>$type,'reading_paper_id'=>$reading_paper_id,'parliament_member_id'=>$participate_member_id[$i]])->one();
                if(empty($model))
                    $model = new ParticipateMemberDiscussion();
                $model->type = $type;
                $model->statement_id=$statement_id;
                $model->parliament_id = UtilityFunctions::SambhidhanSava();
                $model->meeting_id = $meeting_id;
                $model->reading_paper_id = $reading_paper_id;
                $model->parliament_member_id = $participate_member_id[$i];
                $model->speech_order = $speech_order_section;
                $model->text_ = 'speech for '.$type;
                $model->status = 1;
                if(!$model->save(false)){
                    $error_array[] = 'false';
                    echo "<pre>";
                    echo "last error : ".print_r($model->errors);
                    exit;
                }
            }
            if(!in_array('false', $error_array)){
                $transaction->commit();
                if($_POST['statement_id'] == null)
                $this->redirect(['reading-paper/reading-paper-discuss/?id='.$reading_paper_id]);
                else
                $this->redirect(['reading-paper/reading-paper-discuss/?id='.$reading_paper_id.'&statement_id='.$_POST['statement_id']]);

            }else{
                $transaction->rollback();
                throw new NotFoundHttpException('You are not authorized to perform this action.');

            }

        }
        else
            throw new NotFoundHttpException('You are not authorized to perform this action.');
    }


    /**
    *save parliament member in zero and special time for discussion
    */
    public function actionSaveParticipateMember(){
        if(isset($_POST['member_array']) && isset($_POST['meeting_id']) && is_numeric($_POST['meeting_id']) && isset($_POST['type']) && in_array($_POST['type'], ['zero_time', 'special_time', 'on_topic'])){
            $participate_member_id = $_POST['member_array'];
            $meeting_id = $_POST['meeting_id'];
            $type = $_POST['type'];
            $full_content = '';
            $order = 2;
            $primary_order = 100;
            $error_array = $pages_ = $reading_content_section_array = array();
            $transaction = Yii::$app->db->beginTransaction();
            if($type == 'zero_time'){
                $pages_ = ReadingPaper::time_template;
                $section = 'शून्य समय';
                $primary_order = 2;
                $order = 2;
            }
            if($type == 'special_time'){
                $section = 'विशेष समय';
                $pages_ = ReadingPaper::time_template;
                $order = 3;
                $primary_order = 3;
            }

            $reading_paper_model = ReadingPaper::find()->where(['parliament_id'=>UtilityFunctions::SambhidhanSava(),'meeting_id'=>UtilityFunctions::ActiveMeeting(),'meeting_header'=>$section])->one();
            if(empty($reading_paper_model))
                $reading_paper_model = new ReadingPaper();
            $reading_paper_model -> parliament_id = UtilityFunctions::SambhidhanSava();
            $reading_paper_model -> meeting_id = UtilityFunctions::ActiveMeeting();
            $reading_paper_model -> main_type = 'time';
            $reading_paper_model -> meeting_header = $section;
            $reading_paper_model -> state = 'दर्ता';
            $reading_paper_model -> status = 'प्रकृयामा';
            $reading_paper_model -> context = '';
            $reading_paper_model -> date = new \yii\db\Expression('NOW()');
            $reading_paper_model -> order_number = $order;
            $reading_paper_model -> primary_order = $primary_order;
            $reading_paper_model -> page_type = 'general';
            if(!$reading_paper_model->save()){
                $error_array[] = 'false';
                echo "<pre>";
                echo "Reading paper : ".print_r($reading_paper_model->errors);
                exit;
            }

             for ($i=0; $i < sizeof($pages_) ; $i++) { 
                $next_page_information = UtilityFunctions::NextPageTemplate($reading_paper_model->id, null, $pages_[$i], $reading_paper_model->main_type, $reading_paper_model->meeting_header, null);
                $next_page = isset($next_page_information['next_page']) ? $next_page_information['next_page'] : null;
                $_end = isset($next_page_information['_end']) ? $next_page_information['_end'] : null;
                $_next_id = isset($next_page_information['_next_id']) ? $next_page_information['_next_id'] : null;
                $content_ = isset($_POST['ReadingPaper'][$pages_[$i]]) ? $_POST['ReadingPaper'][$pages_[$i]] : '';
                $full_content .='<br />'.$content_;
                $reading_content_section = ReadingPaperContent::find()->where(['reading_paper_id'=>$reading_paper_model->id,'pages'=>$pages_[$i]])->one();
                if(empty($reading_content_section))
                    $reading_content_section = new ReadingPaperContent();
                $reading_content_section -> reading_paper_id = $reading_paper_model->id;
                $reading_content_section -> page_type = 'general';
                $reading_content_section -> pages = $pages_[$i];
                $reading_content_section -> content = $content_;
                $reading_content_section -> order_ = $i + 1;
                $reading_content_section -> next_page = $next_page;
                $reading_content_section -> _end = $_end;
                $reading_content_section->created_by = Yii::$app->user->id;
                $reading_content_section->created_date = new \yii\db\Expression('NOW()');
                if(!$reading_content_section->save()){
                    $error_array[] = 'false';
                    echo "<pre>";
                    echo print_r($reading_content_section->errors);
                    exit;
                }
                $reading_content_section_array[] = $reading_content_section->id.','.$_end.','.$next_page;

            }
            $reading_paper_model->context = $full_content;
            if(!$reading_paper_model->save(false))
                $error_array[] = 'false';
            for ($i=0; $i < sizeof($participate_member_id) ; $i++) {
                $speech_order_section = isset($_POST['member_order_'.$participate_member_id[$i]]) ? $_POST['member_order_'.$participate_member_id[$i]] : null; 
                $model = ParticipateMemberDiscussion::find()->where(['meeting_id'=>$meeting_id,'type'=>$type,'reading_paper_id'=>$reading_paper_model->id,'parliament_member_id'=>$participate_member_id[$i]])->one();
                if(empty($model))
                    $model = new ParticipateMemberDiscussion();
                $model->type = $type;
                $model->parliament_id = UtilityFunctions::SambhidhanSava();
                $model->meeting_id = $meeting_id;
                $model->reading_paper_id = $reading_paper_model->id;
                $model->parliament_member_id = $participate_member_id[$i];
                $model->speech_order = $speech_order_section;
                $model->text_ = 'speech for '.$type;
                $model->status = 1;
                if(!$model->save(false)){
                    $error_array[] = 'false';
                    echo "<pre>";
                    echo "last error : ".print_r($model->errors);
                    exit;
                }
            }

            
            if(!empty($reading_content_section_array)){
                for ($p=0; $p < sizeof($reading_content_section_array) ; $p++) {
                    $content_information = explode(',', $reading_content_section_array[$p]);
                    $cont_id = isset($content_information[0]) ? $content_information[0] : null;
                    $_end_sec_ = isset($content_information[1]) ? $content_information[1] : null;
                    $next_pages = isset($content_information[2]) && !empty($content_information[2]) ? $content_information[2] : null;
                    $main_page_conent = ReadingPaperContent::findOne($cont_id);
                    $next_reading_paper_id = array();
                    if($next_pages && !empty($main_page_conent)){
                        for ($r=2; $r < sizeof($content_information) ; $r++) { 
                            $page_name_sec = $content_information[$r];
                            $content_page_info = ReadingPaperContent::find()->where('pages=:pages AND reading_paper_id=:reading_paper_id AND id >:id',[':pages'=>$page_name_sec, ':reading_paper_id'=>$main_page_conent->reading_paper_id, ':id'=>$cont_id])->orderBy(['id' => SORT_ASC])->one();
                            $next_reading_paper_id[] = !empty($content_page_info) ? $content_page_info->id : null;
                        }
                    }
                    if($_end_sec_ == 'statement' && isset($reading_content_section_array[$p+1])){
                        $content_information_next =  explode(',', $reading_content_section_array[$p+1]);
                        $cont_id_next = isset($content_information_next[0]) ? $content_information_next[0] : null;
                        $content_page_info_next = ReadingPaperContent::find()->where('pages=:pages AND reading_paper_id=:reading_paper_id AND id >=:id',[':pages'=>'_first_content', ':reading_paper_id'=>$main_page_conent->reading_paper_id, ':id'=>$cont_id_next])->orderBy(['id' => SORT_ASC])->one();
                        $next_reading_paper_id[] = $content_page_info_next ? $content_page_info_next->id : null;
                    }
                    $main_page_conent->_next_id = !empty($next_reading_paper_id) ? implode(',', $next_reading_paper_id) :  null;
                    if(!$main_page_conent->save()){
                        $error_array[] = 'false';
                        echo "<pre>";
                        echo print_r($main_page_conent->errors)." : Content section";
                        exit;
                    }
                }
            }
            if(!in_array('false', $error_array)){
                $transaction->commit();
                $this->redirect(['reading-paper/prepare-r-p/?meeting='.UtilityFunctions::ActiveMeeting().'&section='.$type]);
            }else{
                $transaction->rollback();
                throw new NotFoundHttpException('You are not authorized to perform this action.');

            }

        }
        else
            throw new NotFoundHttpException('You are not authorized to perform this action.');
    }



    /**
    *delete participate member in parliament section
    */

    public function actionDeleteParticipateMember(){
        if(isset($_GET['member_id']) && isset($_GET['section'])){
            $member_id = is_numeric($_GET['member_id']) ? $_GET['member_id'] : null;
            $section = $_GET['section'];
            $reading_paper = is_numeric($_GET['reading_paper']) ? $_GET['reading_paper'] : null;
            $query = ParticipateMemberDiscussion::find()->where(['parliament_member_id'=>$member_id,'meeting_id'=>UtilityFunctions::ActiveMeeting()]);
            if($section)
                $query->andWhere(['type'=>$section]);
            if($reading_paper)
                $query->andWhere(['reading_paper_id'=>$reading_paper]);
            $statement_details = $query->one();
            if(!empty($statement_details)){
                $statement_details->status = 0;
                $statement_details->save(false);
                if(!$reading_paper)
                    return $this->redirect(['/program/reading-paper/prepare-r-p/?meeting='.UtilityFunctions::ActiveMeeting().'&section='.$section]);
                else
                    return $this->redirect(['/program/reading-paper/prepare-r-p/?meeting='.UtilityFunctions::ActiveMeeting().'&section='.$section.'&reading_paper='.$reading_paper]);
            }else
            throw new NotFoundHttpException('Member is not found, please check it again');
        }else
            throw new NotFoundHttpException('You are not authorized to perform this action.');
           


    }

     /**
    *delete participate member in from participate member discussion
    */
     public function actionDeleteParticipateMemberDiscussion()
     {
         if(isset($_GET['participate_member_id']) && isset($_GET['reading_id']))
            {
                $id=$_GET['participate_member_id'];
                $model=ParticipateMemberDiscussion::findOne($id);
                if($model != null)
                {
                    if($model->status == 0)throw new NotFoundHttpException('Member Already Inactive/Deleted!!');
                    $model->status=0;
                    if($model->save())
                    {
                        $statement_id=isset($_GET['statement_id'])? $_GET['statement_id'] == '' ? null:$_GET['statement_id']:null;
                        return $this->redirect(['reading-paper-discuss','id'=>$_GET['reading_id'],'statement_id'=>$statement_id]);
                    }
                }
                else
                {
                    throw new NotFoundHttpException('Member Not Found!!');
                }
            }
            else
            {
                 throw new NotFoundHttpException('Member Not Found!!');
            }

     }


    public function actionViewSampleRP(){
        //in dp there should be type and its state also
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $model = new ReadingPaperContent();
        $daily_paper_detail = DailyPaper::findOne($id);
        if(empty($daily_paper_detail))
            throw new NotFoundHttpException('Daily Paper Cannot make its reading paper, please contact admin.');
        $active_darta_section = DartaDesk::findOne($daily_paper_detail->forum_id);
        if(empty($active_darta_section))
            throw new NotFoundHttpException('Daily Paper Cannot make its reading paper, please contact admin.');

        $reading_paper = ReadingPaper::find()->where(['daily_paper_id'=>$id,'paper_status'=>1])->one();
        if(empty($reading_paper))
            throw new NotFoundHttpException('Daily Paper Cannot make its reading paper, please contact admin.');
        $meeting_detail = MeetingDetails::findOne(UtilityFunctions::ActiveMeeting());
        if(in_array($active_darta_section->type, ['राष्ट्रपति','प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालय','राष्ट्रपतिको कार्यालय'])){
            return $this->render('letter/_content',['active_darta_section'=>$active_darta_section,'meeting_detail'=>$meeting_detail,]);
        }
        if($active_darta_section->type == 'व्यवस्थापन कार्य'){
            if($active_darta_section->check_state == 'अनुमति माग्ने'){
                $anumati_magne_phase = ReadingPaper::aumati_magne;
                $paper = isset($_GET['paper']) ? $_GET['paper'] : 'first_paper';
                Yii::$app->session['read_paper_'] = $paper;
                return $this->render('bill-anumati/'.$paper,['active_darta_section'=>$active_darta_section,'meeting_detail'=>$meeting_detail,'model'=>$model, 'id'=>$id]);
            }
            if($active_darta_section->check_state == 'विचार गरियोस्'){
                $participate_member_discussion = ParticipateMemberDiscussion::find()->where('parliament_id=:parliament_id AND meeting_id=:meeting_id AND type=:type AND reading_paper_id=:reading_paper_id AND status=:status order by speech_order ASC',[':parliament_id'=>UtilityFunctions::SambhidhanSava(),':meeting_id'=>UtilityFunctions::ActiveMeeting(),':type'=>'on_topic',':reading_paper_id'=>$reading_paper->id, ':status'=>1])->all();
                if(!empty($participate_member_discussion))
                    $all_pages = ReadingPaper::bichar_gariyos_general;
                else
                    $all_pages = ReadingPaper::bichar_gariyos_no_discuss;
                $result_pages = ReadingPaper::bichar_gariyos_result;
                $participate_member = array();
                if(!empty($participate_member_discussion)){
                    foreach ($participate_member_discussion as $prt_member) {
                        $participate_member[] = $prt_member->parliament_member_id;
                    }
                }
                $firt_mem_id = isset($participate_member[0]) ? $participate_member[0] : null;
                $_discuss_member = isset($participate_member[1]) ? $participate_member[1] : null;
                $parliament_member = ParliamentMember::findOne($firt_mem_id);
               // $all_pages = array_merge($page_only, $result_pages);
                $first_member = $parliament_member ?  $parliament_member->member_title.' '.$parliament_member->first_name.' '.$parliament_member->last_name: 'null';
                $paper = isset($_GET['paper']) ? $_GET['paper'] : '_content';
                $index = array_search($paper,$all_pages);
                $next_page = isset($all_pages[$index + 1]) ? $all_pages[$index + 1] : null;
                $prv_page = isset($all_pages[$index - 1]) ? $all_pages[$index - 1] : null;
                if(in_array($paper, $result_pages)){
                    $sizeof = sizeof($all_pages);
                    $prv_page = isset($all_pages[$sizeof - 1]) ? $all_pages[$sizeof - 1] : null;
                    $next_page = null;
                }
                $member_id = $next_member = $prv_member = null;
                if($paper == '_discuss_body'){
                    $member_id = isset($_GET['participate_member']) ? $_GET['participate_member'] : $_discuss_member;
                    $phase_index = array_search($member_id, $participate_member);
                    $next_member = isset($participate_member[$phase_index+1]) ? $participate_member[$phase_index+1] : null;
                    $prv_member = isset($participate_member[$phase_index-1]) && $phase_index > 1 ? $participate_member[$phase_index-1] : null;
                    if($prv_member)
                        $prv_page ='_discuss_body';
                    else
                        $prv_page ='_discuss';
                    if($next_member)
                        $next_page = '_discuss_body';
                    else
                        $next_page = '_discuss_reply';
                }
                $parliament_member_details = ParliamentMember::findOne($member_id);

                $member_name = $parliament_member_details ?  $parliament_member_details->member_title.' '.$parliament_member_details->first_name.' '.$parliament_member_details->last_name: 'null';
                return $this->render('bichar-gariyos/'.$paper,['active_darta_section'=>$active_darta_section,'meeting_detail'=>$meeting_detail,'model'=>$model, 'id'=>$id, 'next_page'=>$next_page, 'prv_page'=>$prv_page, 'first_member'=>$first_member, 'member_name'=>$member_name , 'next_member'=>$next_member, 'prv_member'=>$prv_member, 'member_id'=>$member_id]);
            }
        }

        
    }

    /**
    * generate rp
    */
    public function actionGenerateRP(){
        
        if(!UtilityFunctions::ActiveMeeting())
            throw new NotFoundHttpException('Not Authorized, Please check ACTIVE/TODAY MEETING');
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $model = new ReadingPaperContent();
        $state = null;
        $daily_paper_detail = DailyPaper::findOne($id);
        $data_desk = DartaDesk::findOne($daily_paper_detail->forum_id);
        if(empty($daily_paper_detail) || empty($data_desk))
            throw new NotFoundHttpException('Daily Paper Cannot make its reading paper, please contact admin.');
        $page_type = 'general';
        $pages_ = $statement_section = $asuikriti_prastab = [];
        if($daily_paper_detail->main_type == 'letter')
            $pages_ = ReadingPaper::letter;
        if($daily_paper_detail->main_type == 'bill' && $daily_paper_detail->state == 'अनुमति माग्ने'){
            $statement_section = Statement::find()->where(['forum_id'=>$daily_paper_detail->forum_id,'statement_type'=>'विरोधको सूचना'])->orderBy(['statement_order'=>SORT_ASC])->all();
            if(!empty($statement_section)){
                $page_type = 'birod-darta';
                $pages_ = ReadingPaper::anumati_bill_birod_template;
            }
            else
                $pages_ = ReadingPaper::crt_bill_anumati;
        }
        if($daily_paper_detail->main_type == 'bill' && $daily_paper_detail->state == 'विचार गरियोस्')
            $pages_ = ReadingPaper::crt_bill_bichar_garios;
        if($daily_paper_detail->main_type == 'bill' && $daily_paper_detail->state == 'दफावार छलफल समितिमा पठाइयोस्')
            $pages_ = ReadingPaper::samitima_pathaiyos;
        if($daily_paper_detail->main_type == 'bill' && $daily_paper_detail->state == 'समितिको प्रतिवेदन पेश')
            $pages_ = ReadingPaper::report_reading_pages;
        if($daily_paper_detail->main_type == 'bill' && $daily_paper_detail->state == 'प्रतिवेदन सहित छलफल'){
            $statement_section = Statement::find()->where(['forum_id'=>$daily_paper_detail->forum_id,'statement_type'=>'प्रतिवेदन सहितको विधेयक समितिमा फिर्ता'])->orderBy(['statement_order'=>SORT_ASC])->all();
            if(!empty($statement_section)){
                $pages_ = ReadingPaper::pertibedan_puna_samitima_pathaiyos;
            }
            else
                $pages_ = ReadingPaper::pertibedan_sahitko_chalfal;
        }
        if($daily_paper_detail->main_type == 'proposal' && $daily_paper_detail->meeting_header == 'ध्यानाकर्षण प्रस्ताव')
            $pages_ = ReadingPaper::dhyan_akarsan_prasthab;
        if($daily_paper_detail->main_type == 'proposal' && $daily_paper_detail->meeting_header == 'जरुरी सार्वजनिक महत्वको प्रस्ताव')
            $pages_ = ReadingPaper::jaruri_sarbajanik_mahato;
        if($daily_paper_detail->main_type == 'proposal' && $daily_paper_detail->meeting_header == 'संकल्प प्रस्ताव')
            $pages_ = ReadingPaper::sankalpa_prastab;
        if($daily_paper_detail->main_type == 'ordinance' && $daily_paper_detail->state == 'पेश')
            $pages_ = ReadingPaper::ordinance_anumati;
        $asuikriti_prastab = Statement::find()->where(['forum_id'=>$daily_paper_detail->forum_id,'statement_type'=>'अध्यादेश अस्वीकार गरियोस्'])->one();
        if($daily_paper_detail->main_type == 'ordinance' && $daily_paper_detail->state == 'छलफल'){
            if(empty($asuikriti_prastab))
                $pages_ = ReadingPaper::suikrigarios;
            else
                $pages_ = ReadingPaper::asuikrit;
        }

        if($daily_paper_detail->main_type == 'other' && $daily_paper_detail->meeting_header == 'शोक प्रस्ताव')
            $pages_ = ReadingPaper::condolence;
        if($daily_paper_detail->main_type == 'report')
            $pages_ = ReadingPaper::report_reading_pages;
        if($daily_paper_detail->main_type== 'aggrement' && $daily_paper_detail->state == 'पेश')
            $pages_ = ReadingPaper::aggrement_pesh;
        if($daily_paper_detail->main_type== 'aggrement' && in_array($daily_paper_detail->state, ['छलफल','अनुमोदन']))
            $pages_ = ReadingPaper::aggrement_chalfal_anumodan;
        $full_content = '';
        $error_array = $reading_content_section_array = array();
        $transaction = Yii::$app->db->beginTransaction();
        if(isset($_POST['ReadingPaperContent'])){
            $max_order_reading_paper = ReadingPaper::find()->where(['meeting_id'=>UtilityFunctions::ActiveMeeting()])->orderBy([
              'primary_order'=>SORT_DESC,
              'order_number' => SORT_DESC
            ])->one();
            $order_number_ = $max_order_reading_paper ? $max_order_reading_paper->order_number :  1;
            $page_type = isset($_POST['ReadingPaperContent']['page_type']) ? $_POST['ReadingPaperContent']['page_type'] : '';
            $reading_paper_model = ReadingPaper::find()->where(['meeting_id'=>UtilityFunctions::ActiveMeeting(),'daily_paper_id'=>$daily_paper_detail->id,'meeting_header'=>$daily_paper_detail->meeting_header])->one();
            if(empty($reading_paper_model))
                $reading_paper_model = new ReadingPaper();
            $reading_paper_model -> attributes = $daily_paper_detail->attributes;
            $reading_paper_model -> status = 'प्रकृयामा';
            $reading_paper_model -> meeting_id = UtilityFunctions::ActiveMeeting();
            $reading_paper_model -> daily_paper_id = $daily_paper_detail->id;
            $reading_paper_model -> context = $data_desk ? $data_desk->title :  '';
            $reading_paper_model -> primary_order = 6;
            $reading_paper_model -> page_type = $page_type;
            $reading_paper_model -> order_number = $order_number_;
            if(!$reading_paper_model->save(false))
                $error_array[] = 'false';
            $reading_paper = $reading_paper_model->id;
            $content_order = 1;
            if(empty($statement_section) && (empty($asuikriti_prastab) || ($daily_paper_detail->state != 'छलफल' && $daily_paper_detail->main_type =='ordinance'))){
                for ($i=0; $i < sizeof($pages_) ; $i++) { 
                    $page_name = isset($pages_[$i]) ? $pages_[$i] : null;
                    $next_page_information = UtilityFunctions::NextPageTemplate($reading_paper_model->id, null,$page_name, $reading_paper_model->main_type, $reading_paper_model->meeting_header, $reading_paper_model->state, null);
                    $next_page = isset($next_page_information['next_page']) ? $next_page_information['next_page'] : null;
                    $_end = isset($next_page_information['_end']) ? $next_page_information['_end'] : null;
                    $content_ = isset($_POST['ReadingPaperContent'][$pages_[$i]]) ? $_POST['ReadingPaperContent'][$pages_[$i]] : '';
                    $full_content .='<br />'.$content_;
                    $reading_content_section = ReadingPaperContent::find()->where(['reading_paper_id'=>$reading_paper,'page_type'=>$page_type, 'pages'=>$pages_[$i]])->one();
                    if(empty($reading_content_section))
                        $reading_content_section = new ReadingPaperContent();
                    $reading_content_section -> reading_paper_id = $reading_paper;
                    $reading_content_section -> page_type = $page_type;
                    $reading_content_section -> pages = $pages_[$i];
                    $reading_content_section -> content = $content_;
                    $reading_content_section -> order_ = $content_order;
                    $reading_content_section -> content_status = 1;
                    $reading_content_section -> next_page = $next_page;
                    $reading_content_section -> _end = $_end;
                    $reading_content_section -> created_by = Yii::$app->user->id;
                    $reading_content_section -> created_date = new \yii\db\Expression('NOW()');
                    if(!$reading_content_section->save()){
                        $error_array[] = 'false';
                        echo "<pre>";
                        echo print_r($reading_content_section->attributes);
                        echo print_r($reading_content_section->errors);
                        exit;
                    }else
                        $reading_content_section_array[] = $reading_content_section->id.','.$_end.','.$next_page;
                    $content_order++;

                }
            }
            if(!empty($asuikriti_prastab) && $daily_paper_detail->state == 'छलफल' && $daily_paper_detail->main_type =='ordinance'){
                $another_page_array = ReadingPaper::asuikrit;
                for ($j=0; $j < sizeof($another_page_array) ; $j++) { 
                    $page_ = isset($another_page_array[$j]) ? $another_page_array[$j] : '';
                    $next_page_information = UtilityFunctions::NextPageTemplate($reading_paper_model->id, $asuikriti_prastab,$page_, $reading_paper_model->main_type, $reading_paper_model->meeting_header, $reading_paper_model->state, null);
                    $next_page = isset($next_page_information['next_page']) ? $next_page_information['next_page'] : null;
                    $_end = isset($next_page_information['_end']) ? $next_page_information['_end'] : null;
                    $content_ = isset($_POST['ReadingPaperContent'][$page_]) ? $_POST['ReadingPaperContent'][$page_] : '';
                    $full_content .='<br />'.$content_;
                    $reading_content_section = new ReadingPaperContent();
                    $reading_content_section -> reading_paper_id = $reading_paper;
                    $reading_content_section -> page_type = $page_type;
                    $reading_content_section -> pages = $another_page_array[$j];
                    $reading_content_section -> content = $content_;
                    $reading_content_section -> order_ = $content_order;
                    $reading_content_section -> next_page = $next_page;
                    $reading_content_section -> statement_id = $asuikriti_prastab->id;
                    $reading_content_section -> _end = $_end;
                    $reading_content_section -> created_by = Yii::$app->user->id;
                    $reading_content_section -> created_date = new \yii\db\Expression('NOW()');
                    if(!$reading_content_section->save()){
                        $error_array[] = 'false';
                        echo "<pre>";
                        echo print_r($reading_content_section->errors);
                        exit;
                    }else
                        $reading_content_section_array[] = $reading_content_section->id.','.$_end.','.$next_page;
                    $content_order++;
                }


            }    
            if(!empty($statement_section)){
                $statement_id_array = isset($_POST['ReadingPaperContent']['statement_id']) ? $_POST['ReadingPaperContent']['statement_id'] : array();
                for ($j=0; $j < sizeof($statement_id_array) ; $j++) { 
                    $statement_id = $statement_id_array[$j];
                    $statement_inforamtion = Statement::findOne($statement_id);
                    if(empty($statement_inforamtion))
                        throw new NotFoundHttpException('Problem in statment so, please contact administrator.');
                    $statement_inforamtion->is_generate_dp = 1;
                    $statement_inforamtion->is_generate_rp = 1;
                    $statement_inforamtion->reading_paper_id = $reading_paper_model->id;
                    if(!$statement_inforamtion->save()){
                        $error_array[] = 'false';
                        echo "<pre>";
                        echo "Statement Error : ".print_r($statement_inforamtion->errors);
                        exit;
                    }

                    for ($i=0; $i < sizeof($pages_) ; $i++) { 
                        $pages_section = isset($pages_[$i]) ? $pages_[$i] : '';
                        $next_page_information = UtilityFunctions::NextPageTemplate($reading_paper_model->id, $statement_id,$pages_section, $reading_paper_model->main_type, $reading_paper_model->meeting_header, $reading_paper_model->state, null);
                        $next_page = isset($next_page_information['next_page']) ? $next_page_information['next_page'] : null;
                        $_end = isset($next_page_information['_end']) ? $next_page_information['_end'] : null;
                        $content_ = isset($_POST['ReadingPaperContent'][$pages_section][$statement_id]) ? $_POST['ReadingPaperContent'][$pages_section][$statement_id] : '';
                        $full_content .='<br />'.$content_;

                        $reading_content_section = ReadingPaperContent::find()->where(['reading_paper_id'=>$reading_paper,'page_type'=>$page_type, 'pages'=>$pages_section,'statement_id'=>$statement_id])->one();
                        if(empty($reading_content_section))
                            $reading_content_section = new ReadingPaperContent();
                        $reading_content_section -> reading_paper_id = $reading_paper;
                        $reading_content_section -> page_type = $page_type;
                        $reading_content_section -> pages = $pages_[$i];
                        $reading_content_section -> content = $content_;
                        $reading_content_section -> order_ = $content_order;
                        $reading_content_section -> next_page = $next_page;
                        $reading_content_section -> _end = $_end;
                        $reading_content_section -> statement_id = $statement_id;
                        $reading_content_section -> created_by = Yii::$app->user->id;
                        $reading_content_section -> created_date = new \yii\db\Expression('NOW()');
                        if(!$reading_content_section->save()){
                            $error_array[] = 'false';
                            echo "<pre>";
                            echo print_r($reading_content_section->errors);
                            exit;
                        }else
                        $reading_content_section_array[] = $reading_content_section->id.','.$_end.','.$next_page;
                    $content_order++;

                    }
                }

            }
            if(!empty($reading_content_section_array)){
                for ($p=0; $p < sizeof($reading_content_section_array) ; $p++) {
                    $content_information = explode(',', $reading_content_section_array[$p]);
                    $cont_id = isset($content_information[0]) ? $content_information[0] : null;
                    $_end_sec_ = isset($content_information[1]) ? $content_information[1] : null;
                    $next_pages = isset($content_information[2]) && !empty($content_information[2]) ? $content_information[2] : null;
                    $main_page_conent = ReadingPaperContent::findOne($cont_id);
                    $next_reading_paper_id = array();
                    if($next_pages && !empty($main_page_conent)){
                        for ($r=2; $r < sizeof($content_information) ; $r++) { 
                            $page_name_sec = $content_information[$r];
                            $content_page_info = ReadingPaperContent::find()->where('pages=:pages AND reading_paper_id=:reading_paper_id AND id >:id',[':pages'=>$page_name_sec, ':reading_paper_id'=>$main_page_conent->reading_paper_id, ':id'=>$cont_id])->orderBy(['id' => SORT_ASC])->one();
                            $next_reading_paper_id[] = !empty($content_page_info) ? $content_page_info->id : null;
                        }
                    }
                    if($_end_sec_ == 'statement' && isset($reading_content_section_array[$p+1])){
                        $content_information_next =  explode(',', $reading_content_section_array[$p+1]);
                        $cont_id_next = isset($content_information_next[0]) ? $content_information_next[0] : null;
                        $content_page_info_next = ReadingPaperContent::find()->where('pages=:pages AND reading_paper_id=:reading_paper_id AND id >=:id',[':pages'=>'_first_content', ':reading_paper_id'=>$main_page_conent->reading_paper_id, ':id'=>$cont_id_next])->orderBy(['id' => SORT_ASC])->one();
                        $next_reading_paper_id[] = $content_page_info_next ? $content_page_info_next->id : null;
                    }
                    $main_page_conent->_next_id = !empty($next_reading_paper_id) ? implode(',', $next_reading_paper_id) :  null;
                    if(!$main_page_conent->save()){
                        $error_array[] = 'false';
                        echo "<pre>";
                        echo print_r($main_page_conent->errors)." : Content section";
                        exit;
                    }
                }
            }
            $notice_ = new NoticePaper();
            $notice_->attributes = $reading_paper_model->attributes;
            $notice_->reaing_paper_id = $reading_paper_model->id;
            $notice_->context = $full_content;
            $notice_->paper_status = 1;
            if(!$notice_->save()){
                $error_array[] = 'false';
                echo "<pre>";
                echo print_r($notice_->errors)." : notice_ section";
                exit;
            }
            $daily_paper_detail->is_generated_rp = 1;
            if(!$daily_paper_detail->save()){
                $error_array[] = 'false';
                echo "<pre>";
                echo print_r($daily_paper_detail->errors);
                exit;
            }

            if(!in_array('false', $error_array)){
                $transaction->commit();
                return $this->redirect(['daily-paper/to-day-d-p']);
            }else{
                $transaction->rollback();
                throw new NotFoundHttpException('You are not authorized to perform this action.');
            }
        }
        return $this->render('_generate_r_p',['model'=>$model,'pages_'=>$pages_,'data_desk'=>$data_desk,'type'=>$daily_paper_detail->main_type, 'title_'=>$daily_paper_detail->meeting_header,'state'=>$daily_paper_detail->state,'statement_section'=>$statement_section,'page_type'=>$page_type,'daily_paper_detail'=>$daily_paper_detail,'asuikriti_prastab'=>$asuikriti_prastab]);
        
    }

    /**
    *readung paper view section
    */
    public function actionReadReadingPaper(){
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $model = new ReadingPaperContent();
        $state = null;
        $daily_paper_detail = DailyPaper::findOne($id);
        $data_desk = DartaDesk::findOne($daily_paper_detail->forum_id);
        $reading_paper_details = ReadingPaper::find()->where(['meeting_id'=>$daily_paper_detail->meeting_id,'daily_paper_id'=>$id,'paper_status'=>1])->one();
        if(empty($reading_paper_details))
            throw new NotFoundHttpException('Daily Paper Cannot make its reading paper, please contact admin.');

        $meeting_detail = MeetingDetails::find()->where(['id'=>$reading_paper_details->meeting_id,'status'=>'सुचारू'])->one();
        $participate_member_discussion = ParticipateMemberDiscussion::find()->where('parliament_id=:parliament_id AND meeting_id=:meeting_id AND type=:type AND reading_paper_id=:reading_paper_id AND status=:status order by speech_order ASC',[':parliament_id'=>UtilityFunctions::SambhidhanSava(),':meeting_id'=>UtilityFunctions::ActiveMeeting(),':type'=>'on_topic',':reading_paper_id'=>$reading_paper_details->id, ':status'=>1])->all();
        if($reading_paper_details->meeting_header == 'व्यवस्थापन कार्य'){
            if($reading_paper_details->state == 'अनुमति माग्ने')
                $all_pages = ReadingPaper::aumati_magne;
            if($reading_paper_details->state == 'विचार गरियोस्')
                if(!empty($participate_member_discussion))
                    $all_pages = ReadingPaper::bichar_gariyos_general;
                else
                    $all_pages = ReadingPaper::bichar_gariyos_no_discuss;
                $result_pages = ReadingPaper::bichar_gariyos_result;
            }

            $participate_member = array();
            if(!empty($participate_member_discussion)){
                foreach ($participate_member_discussion as $prt_member) {
                    $participate_member[] = $prt_member->parliament_member_id;
                }
            }
            $firt_mem_id = isset($participate_member[0]) ? $participate_member[0] : null;
            $_discuss_member = isset($participate_member[1]) ? $participate_member[1] : null;
            $parliament_member = ParliamentMember::findOne($firt_mem_id);
           // $all_pages = array_merge($page_only, $result_pages);
            $first_member = $parliament_member ?  $parliament_member->member_title.' '.$parliament_member->first_name.' '.$parliament_member->last_name: 'null';
            $paper = isset($_GET['paper']) ? $_GET['paper'] : '_first_content';
            $index = array_search($paper,$all_pages);
            $next_page = isset($all_pages[$index + 1]) ? $all_pages[$index + 1] : null;
            $prv_page = isset($all_pages[$index - 1]) ? $all_pages[$index - 1] : null;
            if(in_array($paper, $result_pages)){
                $sizeof = sizeof($all_pages);
                $prv_page = isset($all_pages[$sizeof - 1]) ? $all_pages[$sizeof - 1] : null;
                $next_page = null;
            }
            $member_id = $next_member = $prv_member = null;
            if($paper == '_discuss_body'){
                $member_id = isset($_GET['participate_member']) ? $_GET['participate_member'] : $_discuss_member;
                $phase_index = array_search($member_id, $participate_member);
                $next_member = isset($participate_member[$phase_index+1]) ? $participate_member[$phase_index+1] : null;
                $prv_member = isset($participate_member[$phase_index-1]) && $phase_index > 1 ? $participate_member[$phase_index-1] : null;
                if($prv_member)
                    $prv_page ='_discuss_body';
                else
                    $prv_page ='_discuss';
                if($next_member)
                    $next_page = '_discuss_body';
                else
                    $next_page = '_discuss_reply';
            }
            $parliament_member_details = ParliamentMember::findOne($member_id);

            $member_name = $parliament_member_details ?  $parliament_member_details->member_title.' '.$parliament_member_details->first_name.' '.$parliament_member_details->last_name: 'null';


            $reading_paper_content = ReadingPaperContent::find()->where('reading_paper_id=:reading_paper_id AND pages=:pages',[':reading_paper_id'=>$reading_paper_details->id, ':pages'=>$paper])->one();
            return $this->render('_reading_paper',['meeting_detail'=>$meeting_detail,'model'=>$model, 'id'=>$id, 'next_page'=>$next_page, 'prv_page'=>$prv_page, 'first_member'=>$first_member, 'member_name'=>$member_name , 'next_member'=>$next_member, 'prv_member'=>$prv_member, 'member_id'=>$member_id,'reading_paper_content'=>$reading_paper_content,'paper'=>$paper]);


    }

    /**
    *view participate member in zero time, special time and in bill section
    */
    public function actionParticipateIn(){
        if(isset($_GET['section']) && isset($_GET['meeting_id'])){
            $meeting_id = $_GET['meeting_id'];
            $section = $_GET['section'];
            $participate_member = ParticipateMemberDiscussion::find()->where(['meeting_id'=>$meeting_id,'parliament_id'=>UtilityFunctions::SambhidhanSava(), 'type'=>$section])->all();
            $meeting_detail = MeetingDetails::find()->where(['id'=>$meeting_id,'status'=>'सुचारू'])->one();
            if(empty($meeting_detail))
                throw new NotFoundHttpException('The requested action cannot perform, meeting is not authorized.');
            return $this->render('_participate_in_discuss',['participate_member'=>$participate_member, 'meeting_detail'=>$meeting_detail]);
        }
        else
            throw new NotFoundHttpException('You are not authorized to perform this action.');
    }



    /**
    *disable active daily paper section as well as reading papaer
    */

    public function actionDailyPaperProcessing(){
        if(isset($_GET['id']) && isset($_GET['type']) && is_numeric($_GET['id']) && in_array($_GET['type'], ['disable','active'])){
            $errors_array = array();
            $id = $_GET['id'];
            $type = $_GET['type'];
            $current_status_string = $type=='disable' ? 'active' : 'cancelled';
            $current_status_numeric = $type=='disable' ? 1 : '0';
            $status = $type=='disable' ? '0' : 1;
            $dp_status = $type=='disable' ? 'cancelled' : 'active';
            $daily_paper_details = DailyPaper::find()->where(['id'=>$id, 'status'=>$current_status_string])->one();
            if(!empty($daily_paper_details)){
                $transaction = Yii::$app->db->beginTransaction();
                $reading_paper_status = ReadingPaper::find()->where(['meeting_id'=>$daily_paper_details->meeting_id,'daily_paper_id'=>$daily_paper_details->id,'paper_status'=>$current_status_numeric])->one();
                if(!empty($reading_paper_status)){
                    $reading_paper_status->paper_status = $status;
                    if(!$reading_paper_status->save())
                        $errors_array[] = 'false';
                }

                $daily_paper_details->status = $dp_status;
                if(!$daily_paper_details->save())
                    $errors_array[] = 'false';
                if(!in_array('false', $errors_array)){
                    $transaction->commit();
                    return $this->redirect(['daily-paper/active-daily-paper']);
                }else{
                    $transaction->rollback();
                    throw new NotFoundHttpException('You are not authorized for this action');

                }
            }
        }else
            throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
    * bulk reading paper
    */

    public function actionTodayAllRP(){
        $meeting_id = isset($_GET['meeting_id']) ? $_GET['meeting_id'] : UtilityFunctions::ActiveMeeting();
        $reading_paper_list = ReadingPaper::find()->where('meeting_id=:meeting_id AND paper_status=:paper_status order by primary_order, order_number'  ,[':meeting_id'=>$meeting_id,':paper_status'=>1])->all();
        return $this->render('_all_reading_paper',['reading_paper_list'=>$reading_paper_list]);
    }


    /**
    *reading paper index section (modify form)
    */
    public function actionToDayReadingPaper(){
        $searchModel = new ReadingPaperService();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'today');

        return $this->render('_today_reading_paper', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Updates an existing ReadingPaper model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ReadingPaper model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


      /**
    *delete Statement
    */
     public function actionDeleteStatement($statement_id,$reading_paper_id)
     {
                $model=Statement::findOne($statement_id);
                if($model != null)
                {
                    if($model->status == 0)throw new NotFoundHttpException('Statement Already Inactive/Deleted!!');
                    $model->status=0;
                    if($model->save())
                    {
                        return $this->redirect(['view','id'=>$reading_paper_id]);
                    }
                }
                else
                {
                    throw new NotFoundHttpException('Statement Not Found!!');
                }
            

     }


    /**
     * Finds the ReadingPaper model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ReadingPaper the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ReadingPaper::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
