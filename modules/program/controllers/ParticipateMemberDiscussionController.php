<?php

namespace app\modules\program\controllers;

use Yii;
use yii\filters\AccessControl;
use app\modules\program\models\ParticipateMemberDiscussion;
use app\modules\program\services\ParticipateMemberDiscussionService;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\components\UtilityFunctions;
use yii\filters\VerbFilter;
use app\modules\setting\models\PoliticalParty;
use app\modules\program\models\MeetingDetails;
use yii\data\ArrayDataProvider;

/**
 * ParticipateMemberDiscussionController implements the CRUD actions for ParticipateMemberDiscussion model.
 */
class ParticipateMemberDiscussionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
         'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

        ];
    }

    /**
     * Lists all ParticipateMemberDiscussion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ParticipateMemberDiscussionService();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ParticipateMemberDiscussion model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ParticipateMemberDiscussion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ParticipateMemberDiscussion();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    /**
    *add zero time participater
    */

    public function actionZeroTime(){
        $meeting_detail = MeetingDetails::find()->where(['id'=>UtilityFunctions::ActiveMeeting(),'status'=>'सुचारू'])->one();
        if(empty($meeting_detail))
            throw new NotFoundHttpException('The requested action cannot perform, meeting is not authorized.');
        $political_party = PoliticalParty::find()->where(['status'=>1])->all();
        $previous_participate_member = ParticipateMemberDiscussion::find()->where(['meeting_id'=>$meeting_detail->id,'parliament_id'=>UtilityFunctions::SambhidhanSava(), 'status'=>1, 'type'=>'zero_time'])->all();
        $participate_member = $columns = array();
        if(!empty($previous_participate_member)){
            $sn = 0;
            foreach ($previous_participate_member as $member_) {
                $party_id = $member_->member ? $member_->member->political_id : 0;
                $party_details = PoliticalParty::findOne($party_id);
                $participate_member[$sn]['name'] = $member_->member ? $member_->member->member_title.' '.$member_->member->first_name.' '.$member_->member->last_name : 'null';
                $participate_member[$sn]['party'] = $party_details ? $party_details->name : ' ';
                $participate_member[$sn]['speech_order'] = $member_->speech_order;
                $sn++;
            }
        }
        $political_party = PoliticalParty::find()->where(['status'=>1])->all();
        return $this->render('_zero_time',['political_party'=>$political_party, 'participate_member'=>$participate_member,'meeting_detail'=>$meeting_detail]);
    }

    /**
     * Updates an existing ParticipateMemberDiscussion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ParticipateMemberDiscussion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ParticipateMemberDiscussion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ParticipateMemberDiscussion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ParticipateMemberDiscussion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
