<?php

namespace app\modules\program\controllers;

use Yii;
use yii\filters\AccessControl;
use app\modules\program\models\DailyPaper;
use app\modules\program\models\ReadingPaper;
use app\modules\program\models\MeetingDetails;
use app\modules\program\services\DailyPaperService;
use app\modules\program\models\DartaDesk;
use app\modules\program\models\Statement;
use app\modules\setting\models\ParliamentMember;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\modules\setting\models\ParliamentPartyOfficial;
use app\modules\setting\models\MinistryRelatedCommittee;
use app\modules\program\models\ProcessingDetails;
use app\components\UtilityFunctions;
use kartik\mpdf\Pdf;
/**
 * DailyPaperController implements the CRUD actions for DailyPaper model.
 */
class DailyPaperController extends Controller
{
     /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
         'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin','officer'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

        ];
    }

    /**
     * Lists all DailyPaper models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DailyPaperService();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, null, null);
        $meeting_id=MeetingDetails::find()->where('status=:status',[':status'=>'active'])->one()->id;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'meeting_id'=>$meeting_id
        ]);
    }

    /**
     * Lists all DailyPaper models.
     * @return mixed
     */

     public function actionSample(){
        if(isset($_GET['id']) && is_numeric($_GET['id'])){
            $model = new DailyPaper();
            $darta_details = DartaDesk::findOne($_GET['id']);
            if(empty($darta_details))
                throw new NotFoundHttpException('The requested page does not exist.');
            $seriral_number = 0;
            $daily_paper_details = DailyPaper::find()->where(['meeting_id'=>UtilityFunctions::ActiveMeeting(),'ministry'=>$darta_details->ministry,'minister'=>$darta_details->minister])->all();
            if(!empty($daily_paper_details)){
                foreach ($daily_paper_details as $dp_info) {
                    if($dp_info->state=='अनुमति माग्ने')
                        $seriral_number = $seriral_number + 2;
                    else
                        $seriral_number = $seriral_number + 1;
                }
            }
            $max_daily_paper = DailyPaper::find()->select('MAX(order_number) as order_number')->where(['meeting_id'=>UtilityFunctions::ActiveMeeting()])->one();
            $order_number = $max_daily_paper ? $max_daily_paper->order_number + 1 : 1;
            $meeting_details = MeetingDetails::findOne(UtilityFunctions::ActiveMeeting());
            $letter = ['राष्ट्रपति','प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालय','राष्ट्रपतिको कार्यालय'];
            if($darta_details->main_type=='letter')
                return $this->render('_letter_template',['data_information'=>$darta_details,'model'=>$model, 'meeting_details'=>$meeting_details,'order_number'=>$order_number]);
            if($darta_details->type=='व्यवस्थापन कार्य'){
                $birod_darta_information = null;
                $member_name = '';
                if($darta_details->check_state == 'अनुमति माग्ने'){
                    $birod_darta_information = Statement::find()->where(['forum_id'=>$_GET['id'],'statement_type'=>'विरोधको सूचना','status'=>1])->one();
                    if(!empty($birod_darta_information)){
                        $member = explode(',', $birod_darta_information->member);
                        for ($i=0; $i < sizeof($member); $i++) { 
                            $member_information = ParliamentMember::findOne($member[$i]);
                            if(sizeof($member) >  1 && $i+1 == sizeof($member)){
                                $member_name .= $member_information ? ' र '.$member_information->member_title.' '.$member_information->first_name.' '.$member_information->last_name : '';

                            }else
                                $member_name .= $member_information ? ','.$member_information->member_title.' '.$member_information->first_name.' '.$member_information->last_name : '';
                        }
                        $member_name = ltrim($member_name, ',');
                    }
                }
                return $this->render('_bill_templates',['data_information'=>$darta_details,'model'=>$model, 'meeting_details'=>$meeting_details, 'seriral_number'=>$seriral_number,'order_number'=>$order_number,'birod_darta_information'=>$birod_darta_information,'member_name'=>$member_name]);
            }
            if($darta_details->main_type=='report')
                return $this->render('_pertibedan_format',['data_information'=>$darta_details,'model'=>$model, 'meeting_details'=>$meeting_details]);
            if($darta_details->main_type=='proposal')
                return $this->render('_general_d_p_template',['data_information'=>$darta_details,'model'=>$model, 'meeting_details'=>$meeting_details]);
            if($darta_details->main_type=='ordinance')
                return $this->render('_ordinance_template',['data_information'=>$darta_details,'model'=>$model, 'meeting_details'=>$meeting_details,'order_number'=>$order_number]);
            if($darta_details->main_type=='other')
                return $this->render('_other_template',['data_information'=>$darta_details,'model'=>$model, 'meeting_details'=>$meeting_details]);

        }
        throw new NotFoundHttpException('Please, try agin.');

     }

     /**
     */
     public function actionBillPartibedan(){
        if(isset($_GET['id']) && is_numeric($_GET['id'])){
            $model = new DailyPaper();
            $darta_details = DartaDesk::findOne($_GET['id']);
            $meeting_details = MeetingDetails::findOne(UtilityFunctions::ActiveMeeting());
            if(empty($darta_details) || empty($meeting_details))
                throw new NotFoundHttpException('The requested page does not exist.');
                return $this->render('_bill_partibedan',['data_information'=>$darta_details,'model'=>$model, 'meeting_details'=>$meeting_details]);
        }
        throw new NotFoundHttpException('Please, try agin.');
     }

     /**
     *generate custom daily paper
     */
    public function actionGenerateCMDailyPaper()
    {
        if(!UtilityFunctions::ActiveMeeting())
            throw new NotFoundHttpException('Not Authorized, Please check ACTIVE/TODAY MEETING');
        $model = new DailyPaper();
        $transaction = Yii::$app->db->beginTransaction();
        $error_array = array();
        if ($model->load(Yii::$app->request->post())) {
            $darta_desk = DartaDesk::findOne($model->forum_id);
            $ministry_details = ParliamentPartyOfficial::find()->where(['parliament_id'=>UtilityFunctions::SambhidhanSava(),'ministry'=> $model->ministry, 'post'=>'मन्त्री', 'status'=>'सकृय'])->one();
            $model -> meeting_id = UtilityFunctions::ActiveMeeting();
            $model -> state = $darta_desk->check_state;
            $minister = $ministry_details && $ministry_details->parliamentMember ? trim($ministry_details->parliamentMember->member_title.' '.$ministry_details->parliamentMember->first_name.' '.$ministry_details->parliamentMember->last_name) : null;
            $refine_one = str_replace("मा०","मन्त्री",$minister);
            $model->minister = $refine_one;
            $model->minister_hierarchy = !empty($ministry_details) ? (int) $ministry_details->_order : 100;
            $related_committee = MinistryRelatedCommittee::find()->where(['ministry'=>$model->ministry])->one();
            $model->related_committee = $related_committee ? $related_committee->committee : null;
            $model->order_number = UtilityFunctions::DailypaperOrder($darta_desk->type, $darta_desk->check_state);
            $model->created_by = Yii::$app->user->id;
            $model->created_date = new \yii\db\Expression('NOW()');
            if(!$model->save())
                $error_array[] = 'false';
            if(!empty($darta_desk)){
                $darta_desk->presentor = $model->presentor;
                $darta_desk->is_generate_dp = 1;
                if(!$darta_desk->update())
                    $error_array[] = 'false';
            }
            if(!in_array('false', $error_array)){
                $transaction->commit();
                return $this->redirect(['darta-desk/d-p-sample/?section='.$model->main_type]);
            }else{
                $transaction->commit();
                throw new NotFoundHttpException('Daily Paper is not created, please check again');
            }
        }
        else
            return $this->redirect(['darta-desk/sample/?id='.$_POST['DailyPaper']['id']]);
    }


    /**
     * Lists all DailyPaper models.
     * @return mixed
     */

    public function actionGenerateDailyPaper()
    {
        if(isset($_GET['id']) && is_numeric($_GET['id']) && UtilityFunctions::ActiveMeeting()){
            $id = $_GET['id'];
            $model = new DailyPaper();
            $darta_desk = DartaDesk::findOne($id);
            if(!empty($darta_desk)){
                $model->attributes = $darta_desk->attributes;
                $ministry_details = ParliamentPartyOfficial::find()->where(['parliament_id'=>UtilityFunctions::SambhidhanSava(),'ministry'=> $darta_desk->ministry, 'post'=>['मन्त्री','उप प्रधानमन्त्री','प्रधानमन्त्री'], 'status'=>'सकृय'])->one();
                $model->forum_id = $id;
                $model->meeting_header = $darta_desk->type;
                $model->context = strip_tags($darta_desk->title);
                $model->status = 'active';
                $model -> meeting_id = UtilityFunctions::ActiveMeeting();
                $model -> state = $darta_desk->check_state;
                $minister = $ministry_details && $ministry_details->parliamentMember ? $ministry_details->parliamentMember->first_name.' '.$ministry_details->parliamentMember->last_name : null;
                $model->minister = $minister;
                $model->minister_hierarchy = !empty($ministry_details) ? (int) $ministry_details->_order : 100;
                $related_committee = MinistryRelatedCommittee::find()->where(['ministry'=>$darta_desk->ministry])->one();
                $model->related_committee = $related_committee ? $related_committee->committee : null;
                $model->order_number = UtilityFunctions::DailypaperOrder($darta_desk->type, $darta_desk->check_state);
                $model->created_by = Yii::$app->user->id;
                $model->created_date = new \yii\db\Expression('NOW()');
                if(!$model->save()){
                    echo "<pre>";
                    echo print_r($model->errors);
                    exit;
                    throw new NotFoundHttpException('DAILY PAPER CANNOT GENERATE');
                }
                else{
                    $darta_desk->is_generate_dp = 1;
                    if($darta_desk->save())
                        return $this->redirect(['/program/darta-desk/d-p-sample/?section='.$darta_desk->main_type]);
                }

            }
            throw new NotFoundHttpException('DARTA DESK IS EMPTY, PLEASE CHECK IT AGAIN');
        }
        else
            throw new NotFoundHttpException('Not Authorized, Please check ACTIVE/TODAY MEETING');
    }

    /**
    * view daily paper sample all in one
    */

    public function actionViewDP(){
            $meeting_number = isset($_GET['meeting_number']) ? $_GET['meeting_number'] : UtilityFunctions::ActiveMeeting();
            $meeting_details = MeetingDetails::findOne($meeting_number);
            $daily_paper_header_array_ = $daily_paper_array = $daily_paper_id_array = array();
            if(empty($meeting_details))
                throw new NotFoundHttpException('Meeting number is un-authorized , please try again.');
            $daily_paper_details = DailyPaper::find()->where('meeting_id=:meeting_id AND status=:status',[':meeting_id'=>$meeting_number,':status'=>'active'])->orderBy(['order_number'=> SORT_ASC, 'minister_hierarchy'=>SORT_ASC])->all();
            //echo "<pre>";
            if(!empty($daily_paper_details)){
                $new_value = '';
                foreach ($daily_paper_details as $dpd) {
                    $daily_paper_array[$dpd->meeting_header.'_'.$dpd->state.'_'.$dpd->ministry][] = $dpd->id;
                    if(isset($daily_paper_header_array_[$dpd->meeting_header])){
                        if(!in_array($dpd->meeting_header.'_'.$dpd->state.'_'.$dpd->ministry, $daily_paper_header_array_[$dpd->meeting_header]))
                                $daily_paper_header_array_[$dpd->meeting_header][] = $dpd->meeting_header.'_'.$dpd->state.'_'.$dpd->ministry;
                    }
                    else{
                       $daily_paper_header_array_[$dpd->meeting_header][] = $dpd->meeting_header.'_'.$dpd->state.'_'.$dpd->ministry;
                    }
                    $daily_paper_id_array[] = $dpd->id;
                }
            }/*
            echo "<pre>";
            echo print_r($daily_paper_header_array_);
            echo print_r($daily_paper_array);
            exit;*/
            if(isset($_GET['exporttoprint']) && $_GET['exporttoprint'] == true)
            {
                return $this->renderAjax('_daily_paper_print',['daily_paper_header_array_'=>$daily_paper_header_array_ ,'daily_paper_array'=>$daily_paper_array, 'meeting_details'=>$meeting_details,'daily_paper_details'=>$daily_paper_details,'daily_paper_id_array'=>$daily_paper_id_array]);
            }
           if(isset($_GET['exporttoword']) && $_GET['exporttoword'] == true)
            {
                $this->layout='@app/views/layouts/exportLayout';
                $content=$this->renderPartial('_daily_paper',['daily_paper_header_array_'=>$daily_paper_header_array_ ,'daily_paper_array'=>$daily_paper_array, 'meeting_details'=>$meeting_details,'daily_paper_details'=>$daily_paper_details]);
                $var = $content;
                $div = $content;
                header("Pragma: no-cache");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: private", false);
                header("Content-type: application/vnd.ms-word");
                header("Content-Disposition: attachment; filename=\"dp.doc");
                header("Content-Transfer-Encoding: binary");
                ob_clean();
                flush();
                return $this->render('_daily_paper',['daily_paper_header_array_'=>$daily_paper_header_array_ ,'daily_paper_array'=>$daily_paper_array, 'meeting_details'=>$meeting_details]);
            }
            if(isset($_GET['exporttopdf']) && $_GET['exporttopdf'] == true)
            {
                 $this->layout='@app/views/layouts/exportLayout';
                $content=$this->renderPartial('_daily_paper',['daily_paper_header_array_'=>$daily_paper_header_array_ ,'daily_paper_array'=>$daily_paper_array, 'meeting_details'=>$meeting_details,'daily_paper_details'=>$daily_paper_details]);
                $pdf = new Pdf([
                    
                    'mode' => Pdf::MODE_UTF8, 

                    // A4 paper format
                    'format' => Pdf::FORMAT_A4, 
                    // portrait orientation
                    'orientation' => Pdf::ORIENT_PORTRAIT, 
                    // stream to browser inline
                    'destination' => Pdf::DEST_BROWSER, 
                    // your html content input
                    'content' => $content,  
                    // format content from your own css file if needed or use the
                    // enhanced bootstrap css built by Krajee for mPDF formatting 
                    'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
                    // any css to be embedded if required
                    'cssInline' => '.kv-heading-1{font-size:18px}', 
                     // set mPDF properties on the fly
                    'options' => ['title' => 'Daily Paper'],
                     // call mPDF methods on the fly
                    'methods' => [ 
                        'SetHeader'=>['Daily Paper'], 
                        'SetFooter'=>['{PAGENO}'],
                    ]
                ]);
    
                // return the pdf output as per the destination setting
                return $pdf->render(); 
            }


            return $this->render('_daily_paper',['daily_paper_header_array_'=>$daily_paper_header_array_ ,'daily_paper_array'=>$daily_paper_array, 'meeting_details'=>$meeting_details,'daily_paper_details'=>$daily_paper_details,'daily_paper_id_array'=>$daily_paper_id_array]);

    }

    /**
    *RE-ARRANGE DAILY PAPER ACCORDING TO
    */
    public function actionReArrangeRp(){
        if(isset($_POST['_daily_paper_'])){
            $daily_paper_array = $_POST['_daily_paper_'];
            $error_array = array();
            $transaction = Yii::$app->db->beginTransaction();
            for ($i=0; $i < sizeof($daily_paper_array) ; $i++) { 
                $daily_paper = DailyPaper::findOne($daily_paper_array[$i]);
                if(!empty($daily_paper)){
                    $daily_paper->order_number = $i + 1;
                    if(!$daily_paper->save(false))
                        $error_array[] = 'false';
                }
                $reading_paper = ReadingPaper::find()->where(['forum_id'=>$daily_paper->forum_id,'paper_status'=>1])->one();
                if(!empty($reading_paper)){
                    $reading_paper->order_number = $i + 1;
                    if(!$reading_paper->save(false))
                        $error_array[] = 'false';
                }

            }
            if(!in_array('false', $error_array)){
                $transaction->commit();
                return $this->redirect(['meeting-details/meeting-details']);
            }else{
                $transaction->rollback();
                throw new NotFoundHttpException('reading paper cannot re-arranged');
            }
        }else
            throw new NotFoundHttpException('reading paper cannot re-arranged, please check it again');
    }

    /**
    *darta birod ko suchana all will appear as a single 
    */
    public function actionStatementDarta(){
        $id = isset($_GET['id']) ? $_GET['id'] :  null;
        $type = isset($_GET['type']) ? $_GET['type'] : null;
        $darta_detail = DartaDesk::findOne($id);
        if(empty($darta_detail))
            throw new NotFoundHttpException('BILL IS NOT REGISTER, PLEASE CHECK IT AGAIN.');
        if(($type=='birod_suchana' && $darta_detail->check_state == 'अनुमति माग्ने' && $darta_detail->type == 'व्यवस्थापन कार्य') || ($type=='samsodhan_suchana' && $darta_detail->check_state == 'संशोधनको म्याद' && $darta_detail->type == 'व्यवस्थापन कार्य') || ($type=='asuikritgarnus' && $darta_detail->check_state == 'अध्यादेश अस्वीकार गरियोस्' && $darta_detail->type == 'अध्यादेश')){
            $darta_type = $btn_title = '';
            if($type== 'birod_suchana'){
                $darta_type = 'विरोधको सूचना';
                $btn_title = 'विरोधको सूचना दर्ता गर्नुहोस';
            }
            if($type== 'asuikritgarnus'){
                $darta_type = 'अध्यादेश अस्वीकार गरियोस्';
                $btn_title = 'अध्यादेश अस्वीकार गरियोस् को सूचना दर्ता गर्नुहोस';
            }
            if($type== 'samsodhan_suchana'){
                $darta_type = 'संशोधन';
                $btn_title = 'संशोधनको सूचना दर्ता गर्नुहोस';
            }
            $model = Statement::find()->where(['forum_id'=>$darta_detail->id,'statement_type'=>$darta_type])->one();
            if(empty($model))
                $model = new Statement();
            return $this->render('_suchana',['model'=>$model,'darta_id'=>$id,'btn_title'=>$btn_title,'darta_type'=>$darta_type]);

        }else
            throw new NotFoundHttpException('विरोधको सूचना दर्ता गर्न मिल्दैन, प्रशासन सँग कुरा गर्नुस |');

    }

    /**
    */
    public function actionAddStatementSection(){
        $model = new Statement();
        if(isset($_POST['Statement'])){
            $model->load(Yii::$app->request->post());
            $darta_desk = DartaDesk::findOne($model->forum_id);
            $darta_type = $darta_desk ? $darta_desk->main_type : null;
            $model->statement_order = 1;
            $model->state = $darta_desk->check_state;
            $model->member = implode(',', $model->member);
            if($model->save())
                return $this->redirect(['darta-desk/d-p-sample/?section='.$darta_type]);
            else
                throw new NotFoundHttpException('BILL IS NOT REGISTER, PLEASE CHECK IT AGAIN.');
        }
        else
            throw new NotFoundHttpException('BILL IS NOT REGISTER, PLEASE CHECK IT AGAIN.');
    }

    /**
    *today daily_paper 
    */

    public function actionActiveDailyPaper(){
        $searchModel = new DailyPaperService();
        $dataProvider = $searchModel->searchactivedp(Yii::$app->request->queryParams, null, null);

        return $this->render('_active_d_p', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
    *today daily_paper 
    */
    public function actionToDayDP(){
        $searchModel = new DailyPaperService();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,'active','1');

        return $this->render('_dp_dashboard', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DailyPaper model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $dailyPaper=$this->findModel($id);
        $statements=$dailyPaper->statement;
        return $this->render('view', [
            'model' => $dailyPaper,
            'statements'=>$statements
        ]);
    }

    /**
     * Creates a new DailyPaper model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DailyPaper();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DailyPaper model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $meeting_details = MeetingDetails::findOne($model->meeting_id);
        if(empty($meeting_details))
            throw new NotFoundHttpException('The requested page does not exist.');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model, 'meeting_details'=>$meeting_details
            ]);
        }
    }

    /**
     * Deletes an existing DailyPaper model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DailyPaper model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DailyPaper the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DailyPaper::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
