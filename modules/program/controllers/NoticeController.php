<?php

namespace app\modules\program\controllers;

use Yii;
use app\modules\program\models\Notice;
use app\modules\program\services\NoticeService;
use app\modules\program\models\MeetingDetails;
use app\components\UtilityFunctions;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NoticeController implements the CRUD actions for Notice model.
 */
class NoticeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
         'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin','officer'],

                    ],
                     [
                        'allow' => true,
                        'actions'=>['reader-dashboard','meeting-details'],
                        'roles' => ['reader'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

        ];
    }


    /**
     * Lists all Notice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NoticeService();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Notice model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Notice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $meeting_number = isset($_GET['meeting_number']) && is_numeric($_GET['meeting_number']) ? $_GET['meeting_number'] :  UtilityFunctions::ActiveMeeting();
        $meeting_details = MeetingDetails::findOne($meeting_number);
        if(empty($meeting_details))
            throw new NotFoundHttpException('Meeting Does not exit.');
        $model = new Notice();

        $searchModel = new NoticeService();
        $searchModel->meeting_id = $meeting_number;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['create']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'meeting_details' =>$meeting_details, 
                'meeting_number'=>$meeting_number,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Updates an existing Notice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $meeting_number = isset($_GET['meeting_number']) && is_numeric($_GET['meeting_number']) ? $_GET['meeting_number'] :  UtilityFunctions::ActiveMeeting();
        $meeting_details = MeetingDetails::findOne($meeting_number);
        if(empty($meeting_details))
            throw new NotFoundHttpException('Meeting Does not exit.');
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'meeting_details' =>$meeting_details, 
                'meeting_number'=>$meeting_number,
            ]);
        }
    }

    /**
     * Deletes an existing Notice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Notice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Notice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Notice::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
