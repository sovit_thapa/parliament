<?php
/**
 * Created by sublime.
 * User: Sovit Thapa
 * Date: 6/11/15
 * Time: 10:41 PM
 */
namespace app\components;
 
use Yii;
use yii\base\Component;
use app\modules\setting\models\ParliamentDetails;
use app\modules\setting\models\MahasachibDetails;
use app\modules\setting\models\Adhibasen;
use app\modules\setting\models\ParliamentMember;
use app\modules\setting\models\ParliamentPartyOfficial;
use app\modules\setting\models\Committee;
use app\modules\setting\models\CommitteeMemberDetails;
use app\modules\program\models\ParliamentActivities;
use app\modules\program\models\DartaDesk;
use app\modules\program\models\MeetingDetails;
use app\components\NepaliCalender;
use app\modules\program\models\ReadingPaper;
use app\modules\program\models\DailyPaper;
use app\modules\program\models\ReadingPaperContent;
use app\modules\program\models\ParticipateMemberDiscussion;
use app\modules\program\models\Statement;

class UtilityFunctions extends Component {

    /**
     * This function will create an SEO friendly string.
     * @param raw string
     * @return seo friendly string
     */
    public function seoUrl($string){
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);
        return trim($string, '-');
    }


    public static function SambhidhanSava(){
        $active_parliament_details = ParliamentDetails::find()->where(['status'=>1])->one();
        return $active_parliament_details ? $active_parliament_details->id : null;
    }
    public static function MeetingNumber(){
        $meeting_information = MeetingDetails::find()->where(['status' => ['active', 'end']])->orderBy(['_meeting_number'=>SORT_DESC])->one();
        return $meeting_information ? $meeting_information->_meeting_number + 1 : 1;
    }
    public static function ActiveAdhibasen(){
        $inforamtion = array();
        $active_adhibasen = Adhibasen::find()->where(['status'=>1])->one();
        $inforamtion['name'] = $active_adhibasen ? $active_adhibasen->name : null;
        $inforamtion['year'] = $active_adhibasen ? $active_adhibasen->year : null;
        return $inforamtion;
    }



    public static function ReadingPaperFormat($dp_id){
        $darta_desk = DartaDesk::findOne($dp_id);
        $reading_paper = '';
        if(empty($darta_desk))
            return null;
        if($darta_desk->type == 'व्यवस्थापन कार्य'){
            if($darta_desk->activities_status=='अनुमति माग्ने'){
                $reading_paper = " अब ".$darta_desk->minister."लाई \"".$darta_desk->title."\" लाई अनुमति माग्ने प्रस्ताव प्रस्तुत गर्न अनुमति दिन्छु । <br /> (पेश गरी सकेपछि) <br /> मा० सदस्यहरु, उक्त प्रस्तावमाथि व्यवस्थापिका–संसद नियमावली, २०७३ को नियम १२१ बमोजिम विरोधको सूचना प्राप्त नभएकोले \"".$darta_desk->title."\" प्रस्तुत गर्न बैठकको अनुमति प्राप्त भएको घोषणा गर्दछु् । <br /> (घोषणा गरि सकेपछि) <br /> अब ".$darta_desk->minister."लाई \"".$darta_desk->title."\" प्रस्तुत गर्न अनुमति दिन्छु । ";
            }
            /*if($darta_desk->activities_status=='विचार गरियोस्'){
                
            }
            if($darta_desk->activities_status=='समितिमा पठाइयोस्'){
                
            }
            if($darta_desk->activities_status=='पारित'){
                
            }*/
        }
        if($darta_desk->type == 'प्रतिवेदन पेश'){
            $reading_paper = 'अब '.$darta_desk->minister.' "'.$darta_desk->title.' " सदन समक्ष पेश गर्न अनुमति दिन्छु ।';
        }
        return $reading_paper;
    }


    /**
    *nepali to english Date Converter.
    */
    public static function NepaliToEnglish($nepali_date){
        $nepali_date_information = explode('-', $nepali_date);
        $year = isset($nepali_date_information[0]) ? $nepali_date_information[0] : null;
        $month = isset($nepali_date_information[1]) ? $nepali_date_information[1] : null;
        $day = isset($nepali_date_information[2]) ? $nepali_date_information[2] : null;
        $calender = new NepaliCalender();
        if($year && $month && $day)
            $today_information = $calender->nep_to_eng($year, $month, $day);
        else
            return date('Y-m-d');
        $year = isset($today_information['year']) ? $today_information['year'] : '0000';
        $month = isset($today_information['month']) ? $today_information['month'] : '00';
        $date = isset($today_information['date']) ? $today_information['date'] : '00';
        return $year.'-'.$month.'-'.$date;
    }
    /**
    *nepali to english Date Converter.
    */
    public static function EnglishToNepali($english_date){
        if($english_date=='0000-00-00')
            return '0000-00-00';
        $calender = new NepaliCalender();
        $today_explore = explode('-', $english_date);
        if(!isset($today_explore[0]) || !isset($today_explore[1]) || !isset($today_explore[2]))
            $today_explore = explode('-', date('Y-m-d'));
        $today_information = $calender->eng_to_nep($today_explore[0], $today_explore[1], $today_explore[2]);
        $year = isset($today_information['year']) ? $today_information['year'] : '0000';
        $month = isset($today_information['month']) ? $today_information['month'] : '00';
        $date = isset($today_information['date']) ? $today_information['date'] : '00';
        return $year.'-'.$month.'-'.$date;
    }


    /**
    *active parliament member according to party or all
    */

    public static function ActiveParliamentMember($party = null, $participate_member = null){
        $member_information = array();
        if($party)
            $member_list = ParliamentMember::find()->where(['political_id'=>$party,'parliament_id'=>UtilityFunctions::SambhidhanSava(), 'status'=>'सकृय'])->all();
        else
            $member_list = ParliamentMember::find()->where(['parliament_id'=>UtilityFunctions::SambhidhanSava(), 'status'=>'सकृय'])->all();
        if(!empty($member_list)){
            $sn = 0;
            foreach ($member_list as $information) {
                if($participate_member && !empty($participate_member) && in_array($information->id, $participate_member))
                    continue;
                $member_information[$sn]['id'] = $information->id;
                $member_information[$sn]['name'] = $information->member_title.' '.$information->first_name.' '.$information->last_name;
                $member_information[$sn]['political_id'] = $information->political_id;
                $sn++;
            }
        }
        return $member_information;

    }


    /**
    *active meeting
    */

    public static function ActiveMeeting(){
        $active_parliament_details = ParliamentDetails::find()->where(['status'=>1])->one();
        $parliament = $active_parliament_details ? $active_parliament_details->id : null;
        $active_meeting_details = MeetingDetails::find()->where(['parliament_id'=>$parliament,'status'=>['active','halt']])->one();
        return $active_meeting_details ? $active_meeting_details->id : null;
    }
    /**
    *active minister for respective ministry
    */

    public static function ActiveMinister($ministry){
        $ministry_details = ParliamentPartyOfficial::find()->where(['parliament_id'=>UtilityFunctions::SambhidhanSava(),'ministry'=> $ministry, 'post'=>['मन्त्री','प्रधानमन्त्री','उप प्रधानमन्त्री'], 'status'=>'सकृय'])->one();
            return $ministry_details && $ministry_details->parliamentMember ? "मा० ".$ministry.' '.$ministry_details->post." ".$ministry_details->parliamentMember->member_title.' '.$ministry_details->parliamentMember->first_name.' '.$ministry_details->parliamentMember->last_name : '';
    }


    /**
    *active parliament member according to party or all
    */

    public static function NepaliMonth($number){
        $nepali_month = array('बैशाख','जेष्ठ','आषाढ','श्रावण','भाद्र','आश्विन','कार्तिक','मंसिर','पौष','माघ','फागुन','चैत');
        return isset($nepali_month[(int) $number - 1]) ? $nepali_month[(int) $number - 1] : 'null';
    }


    /**
    *return nepali character , input as number (serial number)
    */
    public static function NepaliCharacter($index){
        $nepali_character = array('क', 'ख', 'ग', 'घ', 'ङ', 'च', 'छ', 'ज', 'झ', 'ञ', 'ट', 'ठ', 'ड', 'ढ', 'ण', 'त', 'थ', 'द', 'ध', 'न', 'प', 'फ', 'ब', 'भ', 'म', 'य', 'य', 'र', 'ल', 'व', 'श', 'ष', 'स', 'ह', 'क्ष', 'त्र', 'ज्ञ');
        return isset($nepali_character[(int) $index - 1]) ? $nepali_character[(int) $index - 1] : 'null';
    }

    public static function NepaliNumber($index){
        $number_informtion = str_split($index);
        $number = '';
        for ($i=0; $i < sizeof($number_informtion); $i++) { 
            if($number_informtion[$i] == '0')
                $number .= '०';
            if($number_informtion[$i] == '1')
                $number .= '१';
            if($number_informtion[$i] == '2')
                $number .= '२';
            if($number_informtion[$i] == '3')
                $number .= '३';
            if($number_informtion[$i] == '4')
                $number .= '४';
            if($number_informtion[$i] == '5')
                $number .= '५';
            if($number_informtion[$i] == '6')
                $number .= '६';
            if($number_informtion[$i] == '7')
                $number .= '७';
            if($number_informtion[$i] == '8')
                $number .= '८';
            if($number_informtion[$i] == '9')
                $number .= '९';
        }
        return $number;
    }

    /**
    *daily paper template 
    */

    public static function DailyProgramProposalTemplate($type, $presentor, $suppoter, $text, $ministry = null){
        $content = '';
        $sp_text = '';
        $suppoter_array = explode(',', $suppoter);
        for ($i=0; $i < sizeof($suppoter_array); $i++) { 
            if($i == sizeof($suppoter_array)-1)
                $sp_text .= ' र  '.$suppoter_array[$i];
            if($i < sizeof($suppoter_array)-1)
                $sp_text .= ' , '.$suppoter_array[$i];
        }
        $sp_text = ltrim($sp_text, ' , ');
        if($type=='जरुरी सार्वजनिक महत्वको प्रस्ताव'){
            $content = 'जरुरी सार्वजनिक महत्वको प्रस्तावका प्रस्तावक '.$presentor.' " '.strip_tags($text).' " भन्ने विषयको जरुरी सार्वजनिक महत्वको प्रस्ताव प्रस्तुत गर्नुहुने । <br /> उक्त प्रस्तावको '.$sp_text.'ले समर्थन गर्नुहुने ।';

        }
        if($type=='संकल्प प्रस्ताव'){
            $content = $presentor.'ले निम्नलिखित व्यहोराको संकल्प प्रस्ताव प्रस्तुत गर्नुहुने ः– <br /> " '.strip_tags($text).' " <br />उक्त प्रस्तावको समर्थकहरु '.$sp_text.'ले प्रस्तावको समर्थन गर्नुहुने ।';

        }
        if($type=='ध्यानाकर्षण प्रस्ताव'){
            $ministry_details = ParliamentPartyOfficial::find()->where(['ministry'=> $ministry, 'post'=>'मन्त्री', 'status'=>'सकृय','post'=>['मन्त्री','प्रधानमन्त्री','उप प्रधानमन्त्री']])->one();
            $minister = $ministry_details && $ministry_details->parliamentMember ? "मा० ".$ministry." मन्त्री श्री ".$ministry_details->parliamentMember->first_name.' '.$ministry_details->parliamentMember->last_name : '';
            $content = $presentor.'ले " '.strip_tags($text).' " भन्ने जरुरी सार्वजनिक महत्वको विषयमाथि '.$minister.' को ध्यानाकर्षण गर्नुहुने ।';

        }
        if($type=='स्थगन प्रस्ताव'){
            $content = 'waiting template';
        }
        return $content;

    }

    /**
    *
    */
    public static function BillReadingPaperTemplate($title, $ministry, $related_committee, $paper, $state, $statment_id = null, $presentor = null){
        $ministry_details = ParliamentPartyOfficial::find()->where(['parliament_id'=>UtilityFunctions::SambhidhanSava(),'ministry'=> $ministry, 'post'=>['मन्त्री','प्रधानमन्त्री','उप प्रधानमन्त्री'], 'status'=>'सकृय'])->one();
            $minister = $ministry_details && $ministry_details->parliamentMember ? "मा० ".$ministry.' '.$ministry_details->post." ".$ministry_details->parliamentMember->member_title." ".$ministry_details->parliamentMember->first_name.' '.$ministry_details->parliamentMember->last_name : '';
            if($state == 'अनुमति माग्ने'){
                $statement_detail = Statement::findOne($statment_id);
                if(empty($statement_detail)){
                    if($paper == '_first_content'){
                        $content = "अब ".strip_tags($minister)." लाई \" ".strip_tags($title)." \" लाई अनुमति माग्ने प्रस्ताव प्रस्तुत गर्न अनुमति दिन्छु ।";
                        return $content;

                    }
                    if($paper == '_second_content'){
                        $content = "मा० सदस्यहरु, उक्त प्रस्तावमाथि व्यवस्थापिका–संसद नियमावली, २०७३ को नियम १२१ बमोजिम विरोधको सूचना प्राप्त नभएकोले \" ".strip_tags($title)." \" प्रस्तुत गर्न बैठकको अनुमति प्राप्त भएको घोषणा गर्दछु् ।";
                        return $content;
                    }
                    if($paper == '_third_content'){
                        $content = " अब ".strip_tags($minister)." लाई ".strip_tags($title)." प्रस्तुत गर्न अनुमति दिन्छु । ";
                        return $content;
                    }
                }
                else{
                    $member_array = explode(',', $statement_detail->member);
                    $member_name_list = $first_member = '';
                    $parliament_member_information = isset($member_array[0]) ? ParliamentMember::findOne($member_array[0]) : array();
                    $first_member = !empty($parliament_member_information) ? $parliament_member_information->member_title.' '.$parliament_member_information->first_name.' '.$parliament_member_information->last_name : '';
                    for ($i=0; $i < sizeof($member_array) ; $i++) { 
                        $parliament_member = ParliamentMember::findOne($member_array[$i]);
                        $member_name_list .= $parliament_member ? ','.$parliament_member->member_title.' '.$parliament_member->first_name.' '.$parliament_member->last_name : '';
                    }
                    $member_name_list = ltrim($member_name_list,',');
                    $more_member = sizeof($member_array) > 1 ? ' संयुक्त रुपमा ' : '';
                    $second_title = sizeof($member_array) > 1 ? " संयुक्त रुपमा विरोधको सूचना दिनुहुने मूल प्रस्तावक " : " विरोधको सूचना दिनुहुने ";
                    if($paper == '_first_content'){
                        $content = "मा० सदस्यहरु, \"".strip_tags($title)."\" लाई प्रस्तुत गर्न अनुमति माग्ने प्रस्तावमाथि व्यवस्थापिका–संसद नियमावली, २०७३ को नियम १२० बमोजिम ".$member_name_list.' बाट '.$more_member." विरोधको सूचना प्राप्त हुन आएको व्यहोरा जानकारी गराउँछु ।";
                        return $content;

                    }
                    if($paper == '_second_content'){
                        $content = "अब म उक्त विधेयकलाई प्रस्तुत गर्न अनुमति माग्ने प्रस्तावमाथि ".$second_title.$first_member."लाई उक्त विरोध गर्नुपर्नाको कारण सहित संक्षिप्त वक्तव्य दिन अनुमति दिन्छु ।";
                        return $content;
                    }
                    if($paper == '_third_content'){
                        $content = " अब विधेयक प्रस्तुतकता ".strip_tags($minister)." लाई उक्त विधेयकलाई प्रस्तुत गर्न अनुमति माग्ने प्रस्तावमाथि प्राप्त विरोधका सम्बन्धमा संक्षिप्त वक्तव्य दिन अनुमति दिन्छु ।";
                        return $content;
                    }
                    if($paper == '_st_decision'){
                        $content = "अब म ".$second_title.$first_member." लाई आफ्नो विरोधको सूचना फिर्ता लिन चाहनु हुन्छ भने फिर्ता लिन समय दिन्छु ।";
                        return $content;
                    }
                    if($paper == '_st_re_back_one'){
                        $content = trim($first_member)."ले आफुले दिनुभएको विरोधको सूचना फिर्ता लिनु भएकोले उक्त सूचनालाई निर्णयार्थ प्रस्तुत गरिरहनु नपर्ने व्यहोरा जानकारी गराउँछु ।";
                        return $content;
                    }
                    if($paper == '_st_re_back_two'){
                        $content = "मा० सदस्यहरु, \"".strip_tags($title)."\" लाई अनुमति माग्ने प्रस्तावमाथि प्राप्त विरोधको सूचना ".trim($first_member)."ले फिर्ता लिनुभएकोले व्यवस्थापिका–संसद नियमावली, २०७३ को नियम १२१ बमोजिम \"".strip_tags($title)."\" प्रस्तुत गर्न अनुमति प्राप्त भएको घोषणा गर्दछु् ।";
                        return $content;
                    }
                    if($paper == '_result'){
                        $content = trim($first_member)."ले आफुले दिनुभएको विरोधको सूचना फिर्ता लिन नचाहनु भएकोले उक्त विरोधको सूचनालाई निर्णयार्थ प्रस्तुत गर्दछु । <br /> यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोला र यसको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोला । <br />
                        यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले  “हुन्छ”  भन्नुहोस् । <br />
                        (“हुन्छ” भनी सकेपछि)<br />
                        यसको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोस् । <br />
                        (“हुन्न” भनी सकेपछि )";
                        return $content;
                    }
                    if($paper=='_result_yes'){
                        $content = '“हुन्छ” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले "'.strip_tags($title).'" लाई प्रस्तुत गर्न अनुमति माग्ने प्रस्ताव माथि '.trim($first_member).'ले प्रस्तुत गर्नुभएको विरोधको सूचना बहुमतले स्वीकृत भएको घोषणा गर्दछु । <br /><br />अब म "'.strip_tags($title).'" लाई अनुमति माग्ने प्रस्तावमाथि प्राप्त विरोधको सूचना बहुमतले स्वीकृत भएकोले व्यवस्थापिका–संसद नियमावली, २०७३ को नियम १२१ बमोजिम "'.strip_tags($title).'" प्रस्तुत गर्न अनुमति अस्वीकृत भएको घोषणा गर्दछु् ।';
                        return $content;
                    }
                    if($paper=='_result_no'){
                        $content = ' "हुन्न” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले "'.strip_tags($title).'" लाई प्रस्तुत गर्न अनुमति माग्ने प्रस्ताव माथि '.trim($first_member).'ले प्रस्तुत गर्नुभएको विरोधको सूचना बहुमतले अस्वीकृत भएको घोषणा गर्दछु । <br /><br />अब म "'.strip_tags($title).'" लाई अनुमति माग्ने प्रस्तावमाथि प्राप्त विरोधको सूचना बहुमतले अस्वीकृत भएकोले व्यवस्थापिका–संसद नियमावली, २०७३ को नियम १२१ बमोजिम "'.strip_tags($title).'" प्रस्तुत गर्न अनुमति प्राप्त भएको घोषणा गर्दछु् ।';
                        return $content;
                    }
                    if($paper=='_st_last_content'){
                        $content = 'अब '.strip_tags($minister).' लाई '.strip_tags($title).' प्रस्तुत गर्न अनुमति दिन्छु ।';
                        return $content;
                    }



                    
                }
            }
            if($state == 'विचार गरियोस्'){
                if($paper=='_first_content'){
                    $content =  'अब '.strip_tags($minister).' लाई  "'.strip_tags($title).' माथि विचार गरियोस्"     भनी प्रस्ताव प्रस्तुत गर्न अनुमति दिन्छु ।';
                    return $content;
                }
                if($paper=='_second_content'){
                    $content = " अब ".strip_tags($title)." माथि विचार गरियोस् ” भन्ने प्रस्तावमाथि अर्थात् विधेयकमाथिको सामान्य छलफल प्रारम्भ हुन्छ । मा० सदस्यहरु, यस छलफलमा विधेयकको सैद्धान्तिक पक्षमा मात्र छलफल हुने व्यहोरा अनुरोध गर्दछु । ";
                    return $content;
                }
                if($paper=='_discuss'){
                    $content = "उक्त छलफलमा बोल्न चाहनुहुने मा० सदस्यहरुले आ–आफ्नो नाम सभामुखको डायससँगै बसेका सचिवालयका कर्मचारीहरुलाई उपलब्ध गराई दिनुहुन अनुरोध गर्दछु । छलफलमा भाग लिने प्रत्येक मा० सदस्यलाई ३ मिनेटको समय निर्धारण गरेको छु ।";
                    return $content;
                }
                if($paper=='_discuss_body'){
                    $content =  "अब ";
                    return $content;
                }
                if($paper=='_discuss_reply'){
                    $content = "अब ".strip_tags($minister)." लाई छलफलमा उठेका प्रश्नहरुको जवाफ दिन अनुमति दिन्छु । ";
                    return $content;
                }
                if($paper =='_discuss_end'){
                    $content = 'अब यो छलफल यहीं समाप्त हुन्छ ।';
                    return $content;
                }
                if($paper=='_decision_general'){
                    $content = "अब म ".strip_tags($title)." माथि विचार गरियोस्” भन्ने प्रस्तावलाई निर्णयार्थ प्रस्तुत गर्दछु । ";
                    return $content;
                }

                if($paper =='_decision_no_discuss'){
                    $content = 'मा० सदस्यहरु, "'.strip_tags($title).' विधेयक, २०६३ माथि विचार गरियोस्” भन्ने प्रस्तावमाथिको छलफलमा बोल्नको लागि कुनैपनि मा० सदस्यको नाम प्राप्त भएन, तसर्थ "'.strip_tags($title).' विधेयक, २०६३ माथि विचार गरियोस्” भन्ने प्रस्तावलाई निर्णयार्थ प्रस्तुत गर्दछु ।';
                    return $content;
                }
                if($paper == '_result'){
                    $content = "यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोला र यसको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोला । <br />
                        यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले  “हुन्छ”  भन्नुहोस् । <br />
                        (“हुन्छ” भनी सकेपछि)<br />
                        यसको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोस् । <br />
                        (“हुन्न” भनी सकेपछि )";
                    return $content;
                }
                if($paper=='_result_overall_yes'){
                    $content = ' "हुन्न” भन्ने आवाज सुनिएन, तसर्थ '.strip_tags($title)."माथि विचार गरियोस्” भन्ने प्रस्ताव सर्वसम्मतिले स्वीकृत भएको घोषणा गर्दछु ।";
                    return $content;
                }
                if($paper=='_result_overall_no'){
                    $content = '“हुन्छ” भन्ने आवाज सुनिएन, तसर्थ “'.strip_tags($title).'माथि विचार गरियोस्” भन्ने प्रस्ताव सर्वसम्मतिले अस्वीकृत भएको घोषणा गर्दछु ।';
                    return $content;
                }
                if($paper=='_result_yes'){
                    $content = '“हुन्छ” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले "'.strip_tags($title).' माथि विचार गरियोस्” भन्ने प्रस्ताव बहुमतले स्वीकृत भएको घोषणा गर्दछु ।';
                    return $content;
                }
                if($paper=='_result_no'){
                    $content = '“हुन्न” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले "'.strip_tags($title).' माथि विचार गरियोस्” भन्ने प्रस्ताव बहुमतले अस्वीकृत भएको घोषणा गर्दछु ।';
                    return $content;
                }
            }

            if($state == 'दफावार छलफल समितिमा पठाइयोस्'){

                if($paper=='_first_content'){
                    $content =  'अब '.strip_tags($minister).' लाई  “'.strip_tags($title).' लाई दफावार छलफलको लागि सम्बन्धित समितिमा पठाइयोस्” भनी प्रस्ताव प्रस्तुत गर्न अनुमति दिन्छु ।';
                    return $content;
                }
                if($paper=='_second_content'){
                    $content =  'अब “'.strip_tags($title).' माथिको दफावार छलफल सदनमा गरियोस् भन्ने प्रस्तावका प्रस्तावक '.$presentor.'लाई बोल्न समय दिन्छु ।';
                    return $content;
                }
                if($paper=='_third_content'){
                    $content =  'अब '.strip_tags($title).' लाई छलफलमा उठेका प्रश्नहरुको जवाफ दिन अनुमति दिन्छु ।';
                    return $content;
                }
                if($paper=='_decision'){
                    $content = " मा० सदस्यहरु, अब “".strip_tags($title)." लाई दफावार छलफलको लागि सम्बन्धित समितिमा पठाइयोस्” भन्ने प्रस्तावलाई निर्णयार्थ प्रस्तुत गर्दछु ।";
                    return $content;
                }
                if($paper=='_result'){
                    $content = "यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोला र यसको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोला ।<br />
                        यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोस् ।
                            (“हुन्छ” भनिसकेपछि)<br />
                        यसको विपक्षमा हुने मा० सदस्यहरुले हुन्न भन्नुहोस् ।
                            (“हुन्न” भनिसकेपछि)<br />";
                    return $content;
                }
                if($paper=='_result_overall_yes'){
                    $content = ' "हुन्न” भन्ने आवाज सुनिएन, तसर्थ '.strip_tags($title)."लाई दफावार छलफलको लागि सम्बन्धित समितिमा पठाइयोस्” भन्ने प्रस्ताव सर्वसम्मतिले स्वीकृत भएको घोषणा गर्दछु ।";
                    return $content;
                }
                if($paper=='_result_overall_no'){
                    $content = '“हुन्छ” भन्ने आवाज सुनिएन, तसर्थ “'.strip_tags($title).'लाई दफावार छलफलको लागि सम्बन्धित समितिमा पठाइयोस्” भन्ने प्रस्ताव सर्वसम्मतिले अस्वीकृत भएको घोषणा गर्दछु ।';
                    return $content;
                }
                if($paper=='_result_yes'){
                    $content = '“हुन्छ” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले “'.strip_tags($title).' लाई दफावार छलफलको लागि सम्बन्धित समितिमा पठाइयोस्” भन्ने प्रस्ताव स्वीकृत भएको घोषणा गर्दछु ।';
                    return $content;
                }
                if($paper=='_result_no'){
                    $content = 'हुन्न” भन्ने आवाज सुनिएन, तसर्थ “'.strip_tags($title).' लाई दफावार छलफलको लागि सम्बन्धित समितिमा पठाइयोस्” भन्ने प्रस्ताव सर्वसम्मतिले स्वीकृत भएको घोषणा गर्दछु ।';
                    return $content;
                }
            }

            if($state == 'समितिको प्रतिवेदन पेश' && $paper=='_first_content'){
                $savapati_name = '';
                $committee = Committee::find()->where(['name'=>$related_committee])->one();
                if(!empty($committee)){
                    $committee_details = CommitteeMemberDetails::find()->where(['committee_id'=>$committee->id, 'post'=>['सभापती','उप सभापती'],'status'=>1])->one();
                    if(!empty($committee_details)){
                        $member_id = !empty($committee_details) ? $committee_details->member_id : null;
                        $member_information = ParliamentMember::findOne($member_id);
                        $savapati_name = $member_information ? "मा० ".$committee_details->post." ".$member_information->member_title." ".$member_information->first_name.' '.$member_information->last_name : '';

                    }

                }
                $content =  'अब '.$related_committee.' का '.$savapati_name.'लाई “'.strip_tags($title).'” सदन समक्ष पेश गर्न अनुमति दिन्छु ।';
                return $content;
            }

            if($state == 'प्रतिवेदन सहित छलफल'){
                if($paper=='_first_content'){
                    $content =  'अब मा० '.$minister.'लाई “'.$related_committee.' समितिको प्रतिवेदन सहितको “'.strip_tags($title).'माथि छलफल गरियोस्” भन्ने प्रस्ताव पेश गर्न अनुमति दिन्छु ।';
                    return $content;
                }
                if($paper=='_decision_general'){
                    $content =  'अब म उक्त प्रस्तावलाई निर्णयार्थ प्रस्तुत गर्दछु ।';
                    return $content;
                }
                if($paper=='_general_result'){
                    $content = " यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोला र यसको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोला ।<br />
                            यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोस् ।<br />
                        (“हुन्छ” भनीसकेपछि)<br />
                            यसको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोस् ।<br />
                        (“हुन्न” भनीसकेपछि)
                        ";
                    return $content;
                }
                if($paper=='_general_yes'){
                    $content = "“हुन्छ” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले उक्त प्रस्ताव स्वीकृत भएको घोषणा  गर्दछु ।";
                    return $content;
                }
                if($paper=='_general_no'){
                    $content = '“हुन्न” भन्ने आवाज सुनिएन, तसर्थ उक्त प्रस्ताव सर्वसम्मतिले स्वीकृत भएको घोषणा गर्दछु ।';
                    return $content;
                }
                if($paper=='_second_content'){
                    $content = 'अब उक्त “'.$related_committee.' समितिको प्रतिवेदन सहितको “'.strip_tags($title).'” माथि छलफल प्रारम्भ हुन्छ । छलफलमा समितिको प्रतिवेदन र सो सम्बन्धी विधेयकका दफाहरु तथा आनुषंगिक अन्य दफाहरुमा मात्र छलफल गर्न सकिने व्यहोरा पनि मा० सदस्यहरुलाई जानकारी गराउँछु ।';
                    return $content;
                }
                if($paper=='_discuss'){
                    $content = 'उक्त छलफलमा बोल्न चाहनुहुने मा० सदस्यहरुले आ–आफ्नो नाम सभामुखको डायससँगै बसेका कर्मचारीहरुलाई उपलब्ध गराई दिनुहुन अनुरोध गर्दछु । छलफलमा भाग लिने प्रत्येक  मा० सदस्यलाई ३ मिनेटको समय निर्धारण गरेको छु ।';
                    return $content;
                }
                if($paper=='_discuss_body'){
                    $content = 'अब मा०';
                    return $content;
                }
                if($paper=='_discuss_reply'){
                    $content = 'अब मा० '.$minister.' लाई छलफलमा उठेका प्रश्नहरुको जवाफ दिन अनुमति दिन्छु ।';
                    return $content;
                }
                if($paper=='_discuss_end'){
                    $content = 'अब यो छलफल यहीं समाप्त हुन्छ ।';
                    return $content;
                }
                if($paper=='_decision'){
                    $content = 'अब म “'.strip_tags($title).'” का सम्बन्धमा '.$related_committee.' समितिको प्रतिवेदनको क्रमसंख्या ......... देखि ........ सम्म उल्लेखित संशोधनहरुलाई निर्णयार्थ प्रस्तुत गर्दछु ।';
                    return $content;
                }
                if($paper=='_decision_no_discuss'){
                    $content = 'उक्त विधेयकमाथिको छलफलमा बोल्नको लागि कुनैपनि मा० सदस्यको नाम प्राप्त भएन, तसर्थ “'.strip_tags($title).'” का सम्बन्धमा '.$related_committee.' समितिको प्रतिवेदनको क्रमसंख्या ....... देखि ......... सम्म उल्लेखित संशोधनहरुलाई निर्णयार्थ प्रस्तुत गर्दछु ।';
                    return $content;
                }
                if($paper=='_result'){
                    $content = 'यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोला र    यसको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोला ।<br />
                                यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोस् ।<br />
                            (“हुन्छ” भनी सकेपछि)<br />
                                यसको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोस् ।<br />
                            (“हुन्न” भनी सकेपछि)';
                    return $content;
                }
                if($paper=='_result_no'){
                    $content = '“हुन्न” भन्ने आवाज सुनिएन, तसर्थ “'.strip_tags($title).'” का सम्बन्धमा '.$related_committee.' समितिको प्रतिवेदनको क्रमसंख्या ......... देखि .......... सम्म उल्लेखित संशोधनहरु सर्वसम्मतिले स्वीकृत भएको घोषणा गर्दछु ।';
                    return $content;
                }
                if($paper=='_result_yes'){
                    $content = '“हुन्छ” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले “'.strip_tags($title).'” का सम्बन्धमा '.$related_committee.' समितिको प्रतिवेदनको क्रमसंख्या ......... देखि ......... सम्म उल्लेखित संशोधनहरु स्वीकृत भएको घोषणा गर्दछु ।';
                    return $content;
                }
                if($paper=='_pertibedan_decision'){
                    $content = 'मा० सदस्यहरु, अब म “'.strip_tags($title).'” का सम्बन्धमा '.$related_committee.' समितिको प्रतिवेदनको क्रमसंख्या .......... देखि ........ सम्म उल्लेखित संशोधनहरु “'.strip_tags($title).'” को अंग बनोस् भनी निर्णयार्थ प्रस्तुत गर्दछु ।';
                    return $content;
                }
                if($paper=='_pertibedan_result'){
                    $content = 'यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोला र यसको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोला ।<br />
                            यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोस् ।<br />
                        (“हुन्छ” भनी सकेपछि)<br />
                            यसको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोस् ।<br />
                        (“हुन्न” भनी सकेपछि)';
                    return $content;
                }
                if($paper=='_pertibedan_yes'){
                    $content = '“हुन्छ” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले “'.strip_tags($title).'” का सम्बन्धमा '.$related_committee.' समितिको प्रतिवेदनको क्रमसंख्या .......... देखि .......... सम्म उल्लेखित संशोधनहरु “'.strip_tags($title).'” को अंग बनेको घोषणा गर्दछु ।';
                    return $content;
                }
                if($paper=='_pertibedan_no'){
                    $content = '“हुन्न” भन्ने आवाज सुनिएन, तसर्थ “'.strip_tags($title).'” का सम्बन्धमा '.$related_committee.' समितिको प्रतिवेदनको क्रमसंख्या ........... देखि .......... सम्म उल्लेखित संशोधनहरु सर्वसम्मतिले “'.strip_tags($title).'” को अंग बनेको घोषणा गर्दछु ।';
                    return $content;
                }
                if($paper=='_bidhyak_decision'){
                    $content = 'अब म विधेयकको दफा ........ देखि ......... सम्म र सम्बन्धित अनुसूची १ देखि ५ सम्म “'.strip_tags($title).'” को अंग बनोस् भनी निर्णयार्थ प्रस्तुत गर्दछु ।';
                    return $content;
                }
                if($paper=='_bidhyak_result'){
                    $content = 'यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोला र यसको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोला ।<br />
                            यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोस् ।<br />
                        (“हुन्छ” भनी सकेपछि)<br />
                            यसको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोस् ।<br />
                        (“हुन्न” भनी सकेपछि)
                        ';
                    return $content;
                }
                if($paper=='_bidhyak_yes'){
                    $content = '“हुन्न” भन्ने आवाज सुनिएन, तसर्थ विधेयकको दफा ...... देखि ........ सम्म सम्म र सम्बन्धित अनुसूची १ देखि ५ सम्म ....... सर्वसम्मतिले “'.strip_tags($title).'” को अंग बनेको घोषणा गर्दछु ।';
                    return $content;
                }
                if($paper=='_bidhyak_no'){
                    $content = '“हुन्छ” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले विधेयकको दफा ......... देखि ........ सम्म सम्म र सम्बन्धित अनुसूची १ देखि ५ सम्म ........ “'.strip_tags($title).'” को अंग बनेको घोषणा गर्दछु ।';
                    return $content;
                }
                if($paper=='_prastab_first_content'){
                    $content = 'अब म विधेयकको प्रस्तावना र नाम पढेर सुनाउँछु । <br />
                            प्रस्तावना ः ..........<br />
                            संक्षिप्त नाम र प्रारम्भः 
                            (१)   यस ऐनको नाम “ ......... ” रहेको छ । <br />
                            (२) यो ऐन तुरुन्त प्रारम्भ हुनेछ ।';
                    return $content;
                }
                if($paper=='_prastab_decision'){
                    $content = 'अब म विधेयकको प्रस्तावना र नाम “'.strip_tags($title).'” को अंग बनोस् भनी निर्णयार्थ प्रस्तुत गर्दछु ।';
                    return $content;
                }
                if($paper=='_prastab_result'){
                    $content = 'यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोला र यसको विपक्षमा हुने  मा० सदस्यहरुले “हुन्न” भन्नुहोला ।<br />
                            यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोस् ।<br />
                        (“हुन्छ” भनी सकेपछि)<br />
                            यसको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोस् ।<br />
                        (“हुन्न” भनी सकेपछि)';
                    return $content;
                }
                if($paper=='_prastab_yes'){
                    $content = '“हुन्छ” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले विधेयकको प्रस्तावना र नाम “'.strip_tags($title).'” को अंग बनेको घोषणा गर्दछु ।';
                    return $content;

                }
                if($paper=='_prastab_no'){
                    $content = '“हुन्न” भन्ने आवाज सुनिएन, तसर्थ विधेयकको प्रस्तावना र नाम सर्वसम्मतिले “'.strip_tags($title).'” को अंग बनेको घोषणा गर्दछु ।';
                    return $content;
                }
                if($paper=='_third_content'){
                    $content = 'अब म मा० '.$minister.' लाई “'.strip_tags($title).' लाई पारित गरियोस्” भनी प्रस्ताव प्रस्तुत गर्न अनुमति दिन्छु ।';
                    return $content;
                }
                if($paper=='_final_decision'){
                    $content = 'अब यो प्रस्तावलाई निर्णयार्थ प्रस्तुत गर्दछु ।';
                    return $content;
                }
                if($paper == '_final_result'){
                    $content = 'यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोला र यसको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोला ।<br />
                            यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोस् ।<br />
                        (“हुन्छ” भनी सकेपछि)<br />
                            यसको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोस् ।<br />
                        (“हुन्न” भनी सकेपछि)
                        ';
                    return $content;

                }
                if($paper=='_final_no'){
                    $content = '“हुन्न” भन्ने आवाज सुनिएन, तसर्थ “'.strip_tags($title).'” सर्वसम्मतिले पारित भएको घोषणा गर्दछु ।';
                    return $content;

                }
                if($paper=='_final_yes'){
                    $content = '“हुन्छ” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले “'.strip_tags($title).'” पारित भएको घोषणा गर्दछु ।';
                    return $content;
                }




                
            }
    }


    /**
    *ordinance template section
    */
    public static function OrdinanceReadingPaperTemplate($title, $ministry, $related_committee, $paper, $state, $asuikriti_prastab = null, $asuikriti_type = null){
        $ministry_details = ParliamentPartyOfficial::find()->where(['ministry'=> $ministry, 'post'=>['प्रधानमन्त्री','उप प्रधानमन्त्री','मन्त्री'], 'status'=>'सकृय'])->one();
        $minister = $ministry_details && $ministry_details->parliamentMember ? "मा० ".$ministry." ".$ministry_details->post." श्री ".$ministry_details->parliamentMember->first_name.' '.$ministry_details->parliamentMember->last_name : '';
        if($state == 'पेश' && $paper == '_first_content'){
            $content = "मा० सदस्यहरु, अब".strip_tags($minister)." लाई “ ".strip_tags($title)." ” लाई पेश गर्न अनुमति दिन्छु ।";
            return $content;
        }
        if($state == 'छलफल'){
            if(!empty($asuikriti_prastab)){
                $member_array = $asuikriti_prastab ? explode(',', $asuikriti_prastab->member) : array();
                $member_id = isset($member_array[0]) ? $member_array[0] : null;
                $parliament_member_details = ParliamentMember::findOne($member_id);
                $name_ = $parliament_member_details ? $parliament_member_details->member_title.' '.$parliament_member_details->first_name.' '.$parliament_member_details->last_name : '';
                if($paper=='_first_content'){
                    $content = 'मा० सदस्यहरु, व्यवस्थापिका–संसद नियमावली, २०७३ को नियम १११ बमोजिम “'.strip_tags($title).'” लाई अस्वीकार गर्ने सूचना प्राप्त भएको व्यहोरा जानकारी गराउँछु । ';
                    return $content;
                }
                if($paper=='_second_content'){
                    $content =  'अब “'.strip_tags($title).' अस्वीकार गरियोस्” भन्ने सूचना दिनुहुने '.$name_.' लाई आफ्नो प्रस्तावको व्यहोरा पढी वक्तव्य दिन अनुमति दिन्छु ।';
                    return $content;
                }
                if($paper=='_third_content'){
                    $content =  'मा० सदस्यहरु, व्यवस्थापिका–संसद नियमावली, २०७३ को नियम १११ बमोजिम '.$name_.' ले आफ्नो प्रस्ताव फिर्ता लिनुभएकोले÷आफ्नो प्रस्ताव पेश नगर्नु भएकोले '.$minister.'लाई “'.strip_tags($title).' लाई स्वीकृत गरियोस्” भन्ने प्रस्ताव प्रस्तुत गर्न अनुमति दिन्छु ।';
                    return $content;
                }
                if($paper=='_discuss'){
                    $content =  'माननीय सदस्यहरु, अब उक्त अध्यादेश अस्वीकार गर्ने प्रस्तावमाथिको छलफल प्रारम्भ हुन्छ । उक्त छलफलमा बोल्न चाहनुहुने मा० सदस्यहरुले आ–आफ्नो नाम सभामुखको डायससँगै बसेका सचिवालयका कर्मचारीहरुलाई उपलब्ध गराई दिनुहुन अनुरोध गर्दछु । छलफलमा भाग लिने प्रत्येक मा० सदस्यलाई ३ मिनेटको समय निर्धारण गरेको छु ।';
                    return $content;
                }
                if($paper=='_discuss_body'){
                    $content =  'अब ';
                    return $content;
                }
                if($paper == '_discuss_reply'){
                    $content = 'अब '.$minister.' लाई छलफलमा उठेका प्रश्नहरुको जवाफ दिन अनुमति दिन्छु ।';
                    return $content;
                }
                if($paper == '_end_discuss'){
                    $content = 'मा० सदस्यहरु, अब यो छलफल यहीं समाप्त हुन्छ ।';
                    return $content;
                }
                if($paper == '_st_decision'){
                    $content = 'अव '.$name_.'ले पेश गर्नुभएको “'.strip_tags($title).' अस्वीकार गरियोस्” भन्ने प्रस्ताव फिर्ता लिन चाहनु हुन्छ भने म समय दिन्छु ।';
                    return $content;
                }
                if($paper == '_st_re_back_one'){
                    $content = 'मा० सदस्यहरु, '.$name_.'ले आफ्नो प्रस्ताव फिर्ता लिनु भएकोले उक्त प्रस्तावलाई निर्णयार्थ प्रस्तुत गरिरहनु परेन ।';
                    return $content;
                }
                if($paper == '_st_re_back_two'){
                    $content = 'मा० सदस्यहरु, '.$name_.'ले आफ्नो प्रस्ताव फिर्ता लिन नचाहनु भएकोले “'.strip_tags($title).' अस्वीकार गरियोस्” भन्ने प्रस्तावलाई निर्णयार्थ प्रस्तुत गर्दछु ।';
                    return $content;
                }

                
                if($paper=='_st_result'){
                    $content = "अब ".$name_."ले पेश गर्नुभएको “".strip_tags($title)." अस्वीकार गरियोस्” भन्ने प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोला र यसको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोला । <br />
                        यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोस ।<br />
                        (हुन्छ भनीसकेपछि)<br />
                        यो प्रस्तावको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोस ।<br />
                        (हुन्न भनीसकेपछि)";
                    return $content;
                }
                if($paper=='_st_yes'){
                    $content = '“हुन्छ” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले उक्त प्रस्ताव बहुमतले स्वीकृत भएको घोषणा गर्दछु ।<br />
                    “हुन्न” भन्ने आवाज सुनिएन, तसर्थ उक्त प्रस्ताव सर्वसम्मतिले स्वीकृत भएको घोषणा गर्दछु ।
                    ';
                    return $content;
                }
                if($paper=='_st_no'){
                    $content = '“हुन्न” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले उक्त प्रस्ताव बहुमतले अस्वीकृत भएको घोषणा गर्दछु ।';
                    return $content;
                }
                if($paper == '_decision_general'){
                    $content = 'मा० सदस्यहरु, '.$name_.'ले पेश गर्नुभएको “'.strip_tags($title).' अस्वीकार गरियोस्” भन्ने प्रस्ताव अस्वीकृत भएकोले/फिर्ता लिनुभएकोले '.$minister.' लाई “'.strip_tags($title).' लाई स्वीकृत गरियोस्” भन्ने प्रस्ताव प्रस्तुत गर्न अनुमति दिन्छु ।';
                    return $content;
                }
                if($paper == '_decision'){
                    $content = 'अव म उक्त प्रस्तावलाई निर्णयार्थ प्रस्तुत गर्दछु ।';
                    return $content;
                }
                if($paper == '_result'){
                    $content = 'यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोला र यसको विपक्षमा हुने  मा० सदस्यहरुले “हुन्न” भन्नुहोला ।<br />
                        यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोस ।<br />
                        (हुन्छ भनीसकेपछि)<br />
                        यो प्रस्तावको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोस ।<br />
                        (हुन्न भनीसकेपछि)';
                    return $content;
                }
                if($paper=='_result_yes'){
                    $content = '“हुन्छ” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले उक्त प्रस्ताव बहुमतले स्वीकृत भएको घोषणा गर्दछु ।<br />
                    “हुन्न” भन्ने आवाज सुनिएन, तसर्थ उक्त प्रस्ताव सर्वसम्मतिले स्वीकृत भएको घोषणा गर्दछु ।';
                    return $content;
                }
                if($paper=='_result_no'){
                    $content = '“हुन्न” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले उक्त प्रस्ताव बहुमतले अस्वीकृत भएको घोषणा गर्दछु ।';
                    return $content;
                }

            }else{

                if($paper=='_first_content'){
                    $content = 'मा० सदस्यहरु व्यवस्थापिका–संसद नियमावली, २०७३ को नियम ११० बमोजिम “'.strip_tags($title).' लाई अस्वीकार गर्ने सूचना प्राप्त नभएको व्यहोरा जानकारी गराउंछु । ';
                    return $content;
                }
                if($paper=='_second_content'){
                    $content =  'अव म '.$minister.'लाई “'.strip_tags($title).' लाई स्वीकृत गरियोस्” भन्ने प्रस्ताव प्रस्तुत गर्न अनुमति दिन्छु ।';
                    return $content;
                }
                if($paper=='_decision'){
                    $content =  'अव म उक्त प्रस्तावलाई निर्णयार्थ प्रस्तुत गर्दछु ।';
                    return $content;
                }
                if($paper=='_result'){
                    $content = "यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोला र यसको विपक्षमा  हुने  मा० सदस्यहरुले “हुन्न” भन्नुहोला । <br />
                            यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोस । <br />
                            (हुन्छ भनीसकेपछि) <br />
                            यो प्रस्तावको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोस । <br />
                            (हुन्न भनीसकेपछि)";
                    return $content;
                }
                if($paper=='_result_overall_yes'){
                    $content = ' "हुन्न” भन्ने आवाज सुनिएन, तसर्थ '.strip_tags($title)." लाई स्वीकृत गरियोस्” भन्ने प्रस्ताव सर्वसम्मतिले स्वीकृत भएको घोषणा गर्दछु ।";
                    return $content;
                }
                if($paper=='_result_overall_no'){
                    $content = '“हुन्छ” भन्ने आवाज सुनिएन, तसर्थ “'.strip_tags($title).' लाई स्वीकृत गरियोस्” भन्ने प्रस्ताव सर्वसम्मतिले अस्वीकृत भएको घोषणा गर्दछु ।';
                    return $content;
                }
                if($paper=='_result_yes'){
                    $content = '“हुन्छ” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले “'.strip_tags($title).' लाई स्वीकृत गरियोस्” भन्ने प्रस्ताव बहुमतले स्वीकृत भएको घोषणा गर्दछु ।';
                    return $content;
                }
                if($paper=='_result_no'){
                    $content = '“हुन्न” भन्ने आवाज सुनिएन, तसर्थ “'.strip_tags($title).' लाई स्वीकृत गरियोस्” भन्ने प्रस्ताव सर्वसम्मतिले स्वीकृत भएको घोषणा गर्दछु ।';
                    return $content;
                }

            }
        }
    }
    

    /**
    *proposal reading paper template
    */
    public static function ReadingPaperProposalTemplate($type, $presentor, $suppoter, $text, $ministry = null, $paper){
        $content = '';
        $sp_text = '';
        $suppoter_array = explode(',', $suppoter);
        for ($i=0; $i < sizeof($suppoter_array); $i++) { 
            if($i < sizeof($suppoter_array)-1)
                $sp_text .= ' र '.$suppoter_array[$i];
            if($i == sizeof($suppoter_array)-1)
                $sp_text .= ' , '.$suppoter_array[$i];
        }
        $sp_text = ltrim($sp_text,' , ');

        $ministry_details = ParliamentPartyOfficial::find()->where(['ministry'=> $ministry, 'post'=>'मन्त्री', 'status'=>'सकृय','post'=>['मन्त्री','प्रधानमन्त्री','उप प्रधानमन्त्री']])->one();
        $minister = $ministry_details && $ministry_details->parliamentMember ? "मा० ".$ministry." ".$ministry_details->post." श्री ".$ministry_details->parliamentMember->first_name.' '.$ministry_details->parliamentMember->last_name : '';
        if($type=='जरुरी सार्वजनिक महत्वको प्रस्ताव'){
            if($paper == '_first_content'){
                $content = 'अब म व्यवस्थापिका–संसद नियमावली, २०७३ को नियम ८७ बमोजिम जरुरी सार्वजनिक महत्वको प्रस्तावका प्रस्तावक '.$presentor.'लाई " '.preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $text).' " भन्ने सम्बन्धी सार्वजनिक महत्वको प्रस्तावमाथि संक्षिप्त वक्तव्य सहित छलफल प्रारम्भ गर्न अनुमति दिन्छु । (समय ... मिनेट)';
            }
            if($paper == '_discuss_suppoter'){
                $content = 'अब म यस प्रस्तावका समर्थक';
            }
            if($paper == '_discuss'){
                $content = 'मा० सदस्यहरु, अब उक्त प्रस्तावमाथिको छलफल प्रारम्भ हुन्छ । छलफलको लागि २ घण्टाको समय तोकेको छु । उक्त छलफलमा बोल्न चाहनुहुने मा० सदस्यहरुले आ–आफ्नो नाम सभामुखको डायससँगै बसेका कर्मचारीहरुलाई उपलब्ध गराई दिनुहुन अनुरोध गर्दछु । छलफलमा भाग लिने प्रत्येक मा० सदस्यलाई ३ मिनेटको समय निर्धारण गरेको छु ।';
            }
            if($paper == '_discuss_body'){
                $content = 'अब म ';
            }
            if($paper == '_discuss_reply'){
                $content = 'अब म '.$minister.'लाई उक्त प्रस्तावमाथिको छलफलमा उठेका प्रश्नहरुको जवाफ दिन अनुमति दिन्छु ।';
            }
            if($paper == '_st_last_content'){
                $content = 'अव उक्त प्रस्तावमाथिको छलफल यहीं समाप्त हुन्छ ।';
            }

        }
        if($type=='संकल्प प्रस्ताव'){
            if($paper == '_first_content'){
                $content = 'मा० सदस्यहरु, अव संविधानसभा (व्यवस्थापिका–संसदको कार्यसञ्चालन) नियमावली, २०७० को नियम ९९ बमोजिम संकल्प प्रस्तावका प्रस्तावक '.$presentor.'लाई “द'.strip_tags($text).'” भन्ने संकल्प प्रस्तावको व्यहोरा पढी वक्तव्य दिन अनुमति दिन्छु । (समय १० मि.)';
            }
            if($paper == '_second_content'){
                $content = 'अब '.$minister.'लाई उक्त संकल्प प्रस्तावका सम्बन्धमा आफ्नो भनाई राख्न अनुमति दिन्छु ।';
            }
            if($paper == '_third_content'){
                $content = 'मा० सदस्यहरु, अव उक्त प्रस्तावमाथिको छलफल प्रारम्भ हुन्छ । उक्त छलफलको लागि बढीमा २ घण्टाको समय निर्धारण गरेको छु । ';
            }
            if($paper == '_discuss_suppoter'){
                $content = 'अब उक्त संकल्प प्रस्तावका समर्थक';
            }
            if($paper == '_discuss'){
                $content = 'मा० सदस्यहरु, अब उक्त संकल्प प्रस्तावमाथिको छलफलमा बोल्न चाहनुहुने अन्य मा० सदस्यहरुले आधा घण्टा भित्र आ–आफ्नो नाम सजिलोको लागि सभामुखको डायससँगै बसेका सचिवालयका कर्मचारीहरुलाई उपलब्ध गराई दिनुहुन अनुरोध गर्दछु ।';
            }
            if($paper == '_discuss_body'){
                $content = 'अब म ';
            }
            if($paper == '_discuss_end'){
                $content = 'मा० सदस्यहरु, उक्त संकल्प प्रस्तावमाथिको छलफलमा मा० सदस्यहरुले बोल्ने क्रम यहीं समाप्त हुन्छ ।';
            }
            if($paper == '_discuss_reply'){
                $content = 'अब म '.$minister.'लाई उक्त प्रस्तावको सम्बन्धमा उठेका प्रश्नहरुको जवाफ दिन अनुमति दिन्छु ।';
            }
            if($paper == '_st_last_content'){
                $content = 'अव उक्त प्रस्तावमाथिको छलफल यहीं समाप्त हुन्छ ।';
            }
            if($paper == '_end_discuss'){
                $content = 'अब यो छलफल यहीं समाप्त हुन्छ ।';
            }
            if($paper == '_decision'){
                $content = 'अव म उक्त संकल्प प्रस्तावलाई निर्णयार्थ प्रस्तुत गर्दछु ।';
            }
            if($paper == '_result'){
                $content = 'यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोला र यसको विपक्षमा हुने  मा० सदस्यहरुले “हुन्न” भन्नुहोला ।<br />
                    यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोस ।<br />
                    (हुन्छ भनीसकेपछि)<br />
                    यो प्रस्तावको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोस ।<br />
                    (हुन्न भनीसकेपछि)';
            }
            if($paper == '_result_overall_yes'){
                $content = '“हुन्न” भन्ने आवाज सुनिएन, तसर्थ उक्त संकल्प प्रस्ताव सर्वसम्मतिले पारित भएको घोषणा गर्दछु ।';
            }
            if($paper == '_result_yes'){
                $content = '“हुन्छ” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले उक्त संकल्प प्रस्ताव बहुमतले पारित भएको घोषणा गर्दछु ।';
            }
            if($paper == '_result_no'){
                $content = '“हुन्न” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले उक्त संकल्प प्रस्ताव बहुमतले अस्वीकृत भएको घोषणा गर्दछु ।';
            }
        }
        if($type=='ध्यानाकर्षण प्रस्ताव'){
            if($paper == '_first_content'){
                $content = 'अब म व्यवस्थापिका–संसद नियमावली, २०७३ को नियम ७५ बमोजिम ध्यानाकर्षण प्रस्तावका प्रस्ताव '.$presentor.'लाई " '.preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $text).' " भन्ने सम्बन्धी जरुरी सार्वजनिक महत्वको विषयमाथि '.$minister. ' को ध्यानाकर्षण गराउन अनुमति दिन्छु ।';
            }
            if($paper == '_second_content'){
                $content = 'मा० सदस्यहरु, '.$minister.' ले उक्त ध्यानाकर्षणको सम्बन्धमा जवाफ दिन व्यवस्थापिका–संसदको अर्को दिनको बैठकको समय माग गर्नुभएकोले उक्त ध्यानाकर्षणको जवाफ दिने कार्यक्रम मिति ........ गते बस्ने बैठकमा हुने व्यहोरा जानकारी गराउँछु ।';
            }
            if($paper == '_third_content'){
                $content = 'अब म '.$minister.'लाई उक्त ध्यानाकर्षणको सम्बन्धमा जवाफ दिन अनुमति दिन्छु ।';
            }
            if($paper == '_discuss_suppoter'){
                $content = 'अब उक्त ध्यानाकर्षणका स्पष्टिकरणकर्ता';
            }
            if($paper == '_discuss_reply'){
                $content = 'अब म '.$minister.'लाई उक्त स्पष्टिकरणको सम्बन्धमा स्पष्ट पार्न अनुमति दिन्छु ।';
            }


        }
        if($type=='स्थगन प्रस्ताव'){
            $content = 'waiting template';
        }
        return $content;

    }
    /**
    *proposal reading paper template
    */

    public static function ReadingPaperAggrementTemplate($title, $ministry, $paper, $state, $type){
        $ministry_details = ParliamentPartyOfficial::find()->where(['parliament_id'=>UtilityFunctions::SambhidhanSava(),'ministry'=> $ministry, 'post'=>['मन्त्री','प्रधानमन्त्री','उप प्रधानमन्त्री'], 'status'=>'सकृय'])->one();
        $minister = $ministry_details && $ministry_details->parliamentMember ? "मा० ".$ministry.' '.$ministry_details->post." ".$ministry_details->parliamentMember->member_title." ".$ministry_details->parliamentMember->first_name.' '.$ministry_details->parliamentMember->last_name : '';
        if($state == 'पेश'){
            $content = "अब ".strip_tags($minister)." लाई “".strip_tags($title)."” माथि संक्षिप्त वक्तव्य दिनुहुँदै सदन समक्ष पेश गर्न अनुमति दिन्छु ।";
            return $content;
        }

        if(in_array($state, ['छलफल','अनुमोदन'])){
            if($paper=='_first_content'){
                $content =  'अव '.strip_tags($minister).' लाई “'.strip_tags($title).' माथि सामान्य छलफल गरियोस्” भनी प्रस्ताव प्रस्तुत गर्न अनुमति दिन्छु ।'; 
                return $content;
            }
            if($paper=='_second_content'){
                $content =  'अब “'.strip_tags($title).' माथि सामान्य छलफल प्रारम्भ हुन्छ । छलफलमा उक्त '.strip_tags($type).' अनुमोदन गर्ने वा नगर्ने भन्ने कुरामा सिमित रही सो को सैद्धान्तिक पक्षमा मात्र छलफल हुने व्यहोरा मा० सदस्यहरुलाई जानकारी गराउँछु ।'; 
                return $content;
            }
            if($paper=='_discuss'){
                $content = " उक्त छलफलमा बोल्न चाहनुहुने मा० सदस्यहरुले आ–आफ्नो नाम सजिलोको लागि सभामुखको डायससँगै बसेका कर्मचारीहरुलाई उपलब्ध गराई दिनुहुन अनुरोध गर्दछु । छलफलमा भाग लिने प्रत्येक मा० सदस्यलाई ३ मिनेटको समय निर्धारण गरेको छु । ";
                return $content;
            }
            if($paper=='_discuss_body'){
                $content =  "अब ";
                return $content;
            }
            if($paper=='_discuss_reply'){
                $content = "अब ".strip_tags($minister)." लाई छलफलमा उठेका प्रश्नहरुको जवाफ दिन अनुमति दिन्छु । ";
                return $content;
            }
            if($paper=='_discuss_end'){
                $content = 'अब यो छलफल यहीं समाप्त हुन्छ ।';
                return $content;
            }
            if($paper == '_decision'){
                $content = 'अव '.strip_tags($minister).' लाई “'.strip_tags($title).' लाई अनुमोदन गरियोस्” भनी प्रस्ताव प्रस्तुत गर्न अनुमति दिन्छु ।';
                return $content;
            }
            if($paper =='_decision_no_discuss'){
                $content = 'मा० सदस्यहरु, उक्त छलफलमा बोल्नको लागि कुनैपनि मा० सदस्यको नाम प्राप्त भएन, तसर्थ “'.strip_tags($title).' माथि सामान्य छलफल गरियोस्” भन्ने प्रस्तावमाथिको छलफल यहीं समाप्त हुन्छ ।';
                return $content;
            }
            if($paper=='_result'){
                $content = "मा० सदस्यहरु, अब म उक्त प्रस्तावलाई निर्णयार्थ प्रस्तुत गर्दछु ।
                    यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले “हुन्छ” भन्नुहोला र यसको विपक्षमा हुने  मा० सदस्यहरुले “हुन्न” भन्नुहोला ।<br />
                    यो प्रस्तावको पक्षमा हुने मा० सदस्यहरुले  “हुन्छ”  भन्नुहोस् ।<br />
                    (“हुन्छ” भनी सकेपछि)<br />
                            यसको विपक्षमा हुने मा० सदस्यहरुले “हुन्न” भन्नुहोस् ।<br />
                    (“हुन्न” भनी सकेपछि )";
                return $content;
            }
            if($paper=='_result_overall_yes'){
                $content = ' "हुन्न” भन्ने आवाज सुनिएन, तसर्थ “'.strip_tags($title).'”सर्वसम्मतिले स्वीकृत भएको घोषणा गर्दछु ।';
                return $content;
            }
            if($paper=='_result_overall_no'){
                $content = '“हुन्न” भन्ने आवाज सुनिएन तसर्थ “'.strip_tags($title).'” सर्वसम्मतिले अनुमोदन भएको घोषणा गर्दछु ।';
                return $content;
            }
            if($paper=='_result_yes'){
                $content = '“हुन्छ” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले “'.strip_tags($title).'” बहुमतले अनुमोदन भएको घोषणा गर्दछु ।';
                return $content;
            }
            if($paper=='_result_no'){
                $content = '“हुन्न” भन्ने पक्षका मा० सदस्यहरुको बहुमत भएकोले "'.strip_tags($title).'” बहुमतले अस्वीकृत भएको घोषणा गर्दछु ।';
                return $content;
            }
        }
    }

    /**
    *Reading Paper template for letters
    */
    public static function LettersReadingPaperTemplate($title,$type){
        $content = " अब म ".strip_tags($type)." बाट प्राप्त पत्रको व्यहोरा पढेर सुनाउछु । <br />".strip_tags($title);
        return $content;
    }
    /**
    *Reading Paper template for letters
    */
    public static function ReportReadingTemplate($committee, $ministry, $minister,$title, $type, $presentor){
            $minister = $savapati_name = '';
            $ministry_details = ParliamentPartyOfficial::find()->where(['parliament_id'=>UtilityFunctions::SambhidhanSava(),'ministry'=> $ministry, 'post'=>['मन्त्री','प्रधानमन्त्री','उप प्रधानमन्त्री'], 'status'=>'सकृय'])->one();
            $minister = $ministry_details && $ministry_details->parliamentMember ? "मा० ".$ministry.' '.$ministry_details->post." ".$ministry_details->parliamentMember->member_title." ".$ministry_details->parliamentMember->first_name.' '.$ministry_details->parliamentMember->last_name : '';
            $committee_details_section = Committee::find()->where(['name'=>$committee])->one();
            if(!empty($committee_details_section)){
                $committee_details_member = CommitteeMemberDetails::find()->where(['committee_id'=>$committee_details_section->id, 'post'=>['सभापती','उप सभापती'],'status'=>1])->one();
                if(!empty($committee_details_member)){
                    $member_id = !empty($committee_details_member) ? $committee_details_member->member_id : null;
                    $member_information = ParliamentMember::findOne($member_id);
                    $savapati_name = $member_information ? "मा० ".$committee_details_member->post." ".$member_information->member_title.' '.$member_information->first_name.' '.$member_information->last_name : '';

                }

            }
        if($type =='संवैधानिक आयोगको प्रतिवेदन')
            $content = "अब ".strip_tags($committee)."का ".strip_tags($presentor)."लाई “".strip_tags($title)."” सदन समक्ष पेश गर्न अनुमति दिन्छु । ";
        if($type =='समितिको प्रतिवेदन')
            $content =  'अब '.$committee.' का '.$savapati_name.'लाई “'.strip_tags($title).'” सदन समक्ष पेश गर्न अनुमति दिन्छु ।';
        if($type == 'समितिको बार्षिक प्रतिवेदन')
            $content =  'अब '.$committee.' का '.$savapati_name.'लाई “'.strip_tags($title).'” सदन समक्ष पेश गर्न अनुमति दिन्छु ।';
        return $content;
    }

    /**
    *condolence template
    */

    public static function CondolenceReadingPaperTemplate($title,$member, $paper){
        if($paper == '_first_content'){
            $content = " सर्वप्रथम म ".strip_tags($member)." को निधन भएकोमा निम्नलिखित शोक प्रस्ताव प्रस्तुत गर्दछु ः– <br />".strip_tags($title)." <br />उक्त शोक प्रस्तावलाई पारित गरिदिनु हुन अनुरोध गर्दछु ।";
        }
        if($paper == '_second_content'){
            $content = "उक्त शोक प्रस्ताव सर्वसम्मतिले पारित भएको घोषणा गर्दछु ।";
        }
        if($paper == '_third_content'){
            $content = "मा० सदस्यहरु, अब हामी स्वर्गीय ".$member." को आत्माको चीर शान्तिको लागि उठेर १ मिनेट मौन धारण गरौं ।";
        }
        return $content;
    }


/**
*/
public static function ZeroTimeTemplate($paper){
    if($paper=='_discuss')
        $content = "अव शून्य समयको कार्यक्रम प्रारम्भ हुन्छ । शून्य समयमा बोल्न चाहनुहुने  मा० सदस्यहरुको लागि १ मिनेटको समय निर्धारण गरिएको छ । निर्धारित समय भित्र  मा० सदस्यहरुले आफ्नो भनाई राख्नुहुन अनुरोध गर्दछु । "; 
    if($paper=='_discuss_body')
        $content = "अब";
    if($paper=='_second_content')
        $content = "मा० सदस्यहरु, शून्य समयमा मा० सदस्यहरुले बोल्ने क्रम यहीं समाप्त हुन्छ ।";
    return $content;
}
public static function SpecialTimeTemplate($paper){
    if($paper=='_discuss')
        $content = "मा० सदस्यहरु, विशेष समयमा बोल्नको लागि दलीय आधारमा मा० सदस्यहरुको नाम प्राप्त हुन आएको छ । म उहाँहरुलाई बोल्न समय दिन्छु ।"; 
    if($paper=='_discuss_body')
        $content = "अब";
    if($paper=='_second_content')
        $content = "मा० सदस्यहरु, विशेष समयमा मा० सदस्यहरुले बोल्ने क्रम यहीं समाप्त हुन्छ ।";
    return $content;
}


    /**
    *reading menu as well as it's contnet
    */

    public static function SingleReadingTitle(){
        $reading_paper_id = isset(Yii::$app->session['_reading_id']) ? Yii::$app->session['_reading_id'] : null;
        $reading_paper_details = ReadingPaper::findOne($reading_paper_id);
        if(empty($reading_paper_details))
            return array();
        $participate_member_discussion = ParticipateMemberDiscussion::find()->where('parliament_id=:parliament_id AND meeting_id=:meeting_id AND type=:type AND reading_paper_id=:reading_paper_id AND status=:status order by speech_order ASC',[':parliament_id'=>UtilityFunctions::SambhidhanSava(),':meeting_id'=>UtilityFunctions::ActiveMeeting(),':type'=>'on_topic',':reading_paper_id'=>$reading_paper_details->id, ':status'=>1])->all();
        if($reading_paper_details->meeting_header == 'व्यवस्थापन कार्य'){
            if($reading_paper_details->state == 'अनुमति माग्ने')
                $all_pages = ReadingPaper::aumati_magne;
            if($reading_paper_details->state == 'विचार गरियोस्')
                if(!empty($participate_member_discussion))
                    $all_pages = ReadingPaper::bichar_gariyos_general;
                else
                    $all_pages = ReadingPaper::bichar_gariyos_no_discuss;
            }
        return $all_pages;
    }

    /**
    */

    public static function ReadingTitle($id){
        $all_pages = $result_pages = $inforamtionation = $decision_page_statement = array();
        $reading_paper_details = ReadingPaper::findOne($id);
        if(empty($reading_paper_details))
            return array();
        //birod ko suchana barema rules to be made
        $statement_detail = Statement::find()->where(['forum_id'=>$reading_paper_details->forum_id,'status'=>1])->all();
        $participate_member_discussion = ParticipateMemberDiscussion::find()->where('parliament_id=:parliament_id AND meeting_id=:meeting_id AND type=:type AND reading_paper_id=:reading_paper_id AND status=:status order by speech_order ASC',[':parliament_id'=>UtilityFunctions::SambhidhanSava(),':meeting_id'=>UtilityFunctions::ActiveMeeting(),':type'=>'on_topic',':reading_paper_id'=>$reading_paper_details->id, ':status'=>1])->all();
        if($reading_paper_details->meeting_header == 'व्यवस्थापन कार्य'){
            if($reading_paper_details->state == 'अनुमति माग्ने'){
                if(!empty($statement_detail)){
                    foreach ($statement_detail as $statement_sec) {
                        $pages = ReadingPaper::anumati_bill_birod_rp_template;
                        $all_pages = array_merge($all_pages, $pages);
                    }
                    $inforamtionation['result_pages']  = ['_result_yes','_result_no'];
                }else
                    $all_pages = ReadingPaper::crt_bill_anumati;
            }
            if($reading_paper_details->state == 'विचार गरियोस्' && !empty($participate_member_discussion))
                    $all_pages = ReadingPaper::bichar_gariyos_general;
            if($reading_paper_details->state == 'विचार गरियोस्' && empty($participate_member_discussion))
                    $all_pages = ReadingPaper::bichar_gariyos_no_discuss;
                $result_pages = ReadingPaper::bichar_gariyos_result;
        }
        if($reading_paper_details->main_type == 'letter')
            $all_pages = ReadingPaper::letter;
        if($reading_paper_details->meeting_header == 'प्रधानमन्त्रीको बक्तब्य')
            $all_pages = ['_first_content'];
        if(in_array($reading_paper_details->meeting_header, ['शून्य समय','विशेष समय']))
            $all_pages = ReadingPaper::time_template;
        $inforamtionation['all_pages']  = $all_pages;
        $inforamtionation['result_pages']  = $result_pages;
        return $inforamtionation;
    }


    /**
    *reading menu as well as it's contnet
    */

    public static function MultipleReadingTitle(){
        $reading_id = array();
        $reading_paper_list = ReadingPaper::find()->where('meeting_id=:meeting_id AND paper_status=:paper_status order by primary_order, order_number'  ,[':meeting_id'=>UtilityFunctions::ActiveMeeting(),':paper_status'=>1])->all();
        if($reading_paper_list){
            foreach ($reading_paper_list as $reading_paper_info) {
            $reading_paper_id = $reading_paper_info->id;
            $reading_id[] = $reading_paper_id;
            }
        }
        return $reading_id;
    }


    public static function TotalDarta(){
        $detail = DartaDesk::find()->where(['status'=>1])->all();
        return sizeof($detail);
    }

    public static function ReadyForDP(){
        $details = DartaDesk::find()->where('darta_desk.status=:status AND check_state!=:check_state AND is_generate_dp!=:is_generate_dp',[':check_state'=>'प्रकृया पुरा',':status'=>1, ':is_generate_dp'=>1])->all();
        return sizeof($details);
    }

    public static function ActiveDP(){
        $query = DailyPaper::find()->where(['parliament_id'=>UtilityFunctions::SambhidhanSava(), 'meeting_id'=>UtilityFunctions::ActiveMeeting(), 'status'=>'active'])->all();
        return sizeof($query);
    }

    public static function DrataFormTemplate($darta_type){
        $type = $state_type = array();
        $title = '';
        $label = '';
        $hide_type = $hide_ministry = $bitaran_miti = $pesh_miti = $drata_miti = $state_display = $date_dispaly = $presentor = $suppoter = false;
        if($darta_type=='letter'){
            $type = DartaDesk::letter;
            $hide_type = false;
            $hide_ministry = true;
            $title = 'पत्र सँग संम्बन्धित'; 
            $label = 'बिबरण ';
            $state_type = ReadingPaper::letter_state;
        }
        if($darta_type=='aggrement'){
            $type = DartaDesk::aggrement;
            $hide_type = false;
            $hide_ministry = false;
            $title = 'सन्धि/महासन्धि/सम्झौता सँग संम्बन्धित';
            $label = 'सन्धि/महासन्धि/सम्झौता';
            $drata_miti = true;
            //$pesh_miti = true;
            $state_type = ReadingPaper::aggrement_state;
            $state_display = true;
        }
        if($darta_type=='bill'){
            $type = ['व्यवस्थापन कार्य'];
            $hide_type = true;
            $hide_ministry = false;
            $title = 'व्यवस्थापन कार्य सँग संम्बन्धित'; 
            $label = 'विधेयक';
            $bitaran_miti = true;
            $drata_miti = true;
            $state_type = ReadingPaper::samiti_bina_bill;
            $state_display = true;
        }
        if($darta_type=='report'){
            $type = ['संवैधानिक आयोगको प्रतिवेदन' =>'संवैधानिक आयोगको प्रतिवेदन','समितिको प्रतिवेदन'=>'समितिको प्रतिवेदन','समितिको बार्षिक प्रतिवेदन'=>'समितिको बार्षिक प्रतिवेदन'];
            $hide_ministry = true;
            $title = 'प्रतिवेदन सँग संम्बन्धित'; 
            $label = 'प्रतिवेदन';
            $pesh_miti = true;
            $state_type = ReadingPaper::report_state;
            $state_display = true;
        }
        if($darta_type=='rule'){
            $type = ['नीति तथा कार्यक्रम'];
            $hide_type = true;
            $hide_ministry = true;
            $title = 'नीति तथा कार्यक्रम सँग संम्बन्धित'; 
            $label = 'नीति तथा कार्यक्रम';
            $pesh_miti = true;
            $state_type = ReadingPaper::rule_state;
            $state_display = true;
        }
        if($darta_type=='income'){
            $type = ['राजस्व व्ययको बार्षिक अनुमान'];
            $hide_type = true;
            $hide_ministry = true;
            $title = 'राजस्व व्ययको बार्षिक अनुमान सँग संम्बन्धित'; 
            $label = 'राजस्व व्ययको बार्षिक अनुमान';
            $pesh_miti = true;
            $state_type = ReadingPaper::income_state;
            $state_display = true;
        }
        if($darta_type=='ordinance'){
            $type = ['अध्यादेश'];
            $hide_type = true;
            $title = 'अध्यादेश सँग संम्बन्धित'; 
            $label = 'अध्यादेश';
            $drata_miti = true;
            $state_type = ReadingPaper::ordinance_state;
            $state_display = true;
        }
        if($darta_type=='proposal'){
            $type = DartaDesk::porposal;
            $hide_type = false;
            $hide_ministry = false;
            $title = 'प्रस्ताव सँग संम्बन्धित'; 
            $label = 'प्रस्तावहरु';
            $pesh_miti = true;
            $state_type = ReadingPaper::proposal_state;
            $state_display = true;
            $suppoter = $presentor = true;
        }
        if($darta_type=='election'){
            $type = DartaDesk::election;
            $hide_type = false;
            $hide_ministry = true;
            $title = 'निर्वाचन सँग संम्बन्धित'; 
            $label = 'निर्वाचन';
            $state_type = ReadingPaper::election_state;
        }


        if($darta_type=='other'){
            $type = DartaDesk::other;
            $hide_type = false;
            $hide_ministry = true;
            $title = 'अन्य सँग संम्बन्धित';
            $label = 'अन्य'; 
            $presentor = true;
            $state_type = ReadingPaper::other_state;
        }
        $information['type'] = sizeof($type) > 0 ? $type : ['प्रत्यक्ष प्रशनहरु','व्यवस्थापन कार्य','राष्ट्रपति','प्रधानमन्त्री तथा मन्त्रिपरिषद्','राष्ट्रपति कार्यालय','संवैधानिक आयोगको प्रतिवेदन','समितिको प्रतिवेदन','समितिको बार्षिक प्रतिवेदन','सन्धि','महासन्धि','सम्झौता','नीति तथा कार्यक्रम','राजस्व व्ययको बार्षिक अनुमान','अध्यादेश','ध्यानाकर्षण प्रस्ताव','जरुरी सार्वजनिक महत्वको प्रस्ताव','संकल्प प्रस्ताव','स्थगन प्रस्ताव','सभामुखको निर्वाचन','उप-सभामुखको निर्वाचन','राष्ट्रपतिको निर्वाचन','उप-राष्ट्रपतिको निर्वाचन','अनुपस्थितिको सूचना','स्थान रिक्तता','समिति गठन','शोक प्रस्ताव','समिति गठन','समितिमा थपघट तथा हेरफेर','बैठकमा अध्यक्षता गर्ने सदस्यको मनोनयन','राष्ट्रपतिबाट सम्बोधन','अन्य'];
        $information['state_type'] = sizeof($state_type) ? $state_type : ['दर्ता','वितरण','पेश','अनुमति माग्ने','विचार गरियोस्','संशोधनको म्याद','समितिमा पठाइयोस्','दफावार छलफल सदनमा संशोधन सहित','दफावार छलफल सदनमा संशोधन विना','समितिको प्रतिवेदन पेश','प्रतिवेदन सहितको विधेयक समितिमा फिर्ता','पुनः समितिको प्रतिवेदन पेश','प्रतिवेदन सहित छलफल','टेबुल','छलफल','पारित','अनुमोदन'];
        $information['label'] = $label;
        $information['title'] = $title;
        $information['hide_type'] = $hide_type;
        $information['hide_ministry'] = $hide_ministry;
        $information['bitaran_miti'] = $bitaran_miti;
        $information['pesh_miti'] = $pesh_miti;
        $information['drata_miti'] = $drata_miti;
        $information['state_display'] = $state_display;
        $information['date_dispaly']  = $date_dispaly;
        $information['presentor']  = $presentor;
        $information['suppoter']  = $suppoter;
        return $information;
    }

    public static function StateProcess($type){
        $type_state = ['पारित','पारित'];
        if($type =='letter')
            $type_state = ReadingPaper::letter_state;
        if($type =='aggrement')
            $type_state = ReadingPaper::aggrement_state;
        if($type=='bill')
            $type_state = ReadingPaper::samiti_bill;
        if($type=='report')
            $type_state = ReadingPaper::report_state;
        if($type=='rule')
            $type_state = ReadingPaper::rule_state;
        if($type=='income')
            $type_state = ReadingPaper::income_state;
        if($type=='ordinance')
            $type_state = ReadingPaper::ordinance_state;
        if($type=='proposal')
            $type_state = ReadingPaper::proposal_state;
        if($type=='election')
            $type_state = ReadingPaper::election_state;
        if($type=='other')
            $type_state = ReadingPaper::other_state;
        return $type_state;

    }
    public static function NextPageInformation($reading_paper_id, $statement_id, $meeting_id, $paper){
        $reading_paper_id_array = $statement_array_id = $reading_paper_array = $next_page_array = $end_of_section = $page_informaiton = array();
        $prv_rp_id = $next_rp_id = $prv_paper = $next_paper = $prv_statement_id = $next_statement_id = $reading_paper_end = $statemet_end = null;
        $reading_paper_list = ReadingPaper::find()->where(['meeting_id'=>UtilityFunctions::ActiveMeeting(),'paper_status'=>1])->orderBy(['primary_order' => SORT_ASC, 'order_number'=>SORT_ASC])->all();
        if(!empty($reading_paper_list)){
            foreach ($reading_paper_list as $reading_paper) {
                $reading_paper_id_array[] = $reading_paper->id;
                $statement_information = Statement::find()->where(['reading_paper_id'=>$reading_paper->id,'status'=>1])->all();
                if(!empty($statement_information)){
                    foreach ($statement_information as $statement_section) {
                        $statement_array_id[$reading_paper->id][] = $statement_section->id;
                    }
                }
            }
        }
        $statement_array = isset($statement_array_id[$reading_paper_id]) ? $statement_array_id[$reading_paper_id] : array();
        if($statement_id){
            $reading_paper_content_details = ReadingPaperContent::find()->where(['reading_paper_id'=>$reading_paper_id, 'statement_id'=>$statement_id, 'content_status'=>1])->orderBy(['order_'=>SORT_ASC])->all();
        }else{
            $reading_paper_content_details = ReadingPaperContent::find()->where(['reading_paper_id'=>$reading_paper_id, 'content_status'=>1])->orderBy(['order_'=>SORT_ASC])->all();
        }

        if(!empty($reading_paper_content_details)){
            foreach ($reading_paper_content_details as $reading_content) {
                $reading_paper_array[] = $reading_content->pages;
                $next_page_array[$reading_content->pages] = $reading_content->next_page;
                $end_of_section[$reading_content->pages] = !empty($reading_content->_end) ? $reading_content->_end : null;
            }
        }
        $paper_index = array_search($paper, $reading_paper_array);
        if(!empty($end_of_section[$paper])){
            $end_type = $end_of_section[$paper];
            if($end_type == 'paper'){
                $prv_statement_id = $statement_id;
                $rp_index = array_search($reading_paper_id, $reading_paper_id_array);
                $next_rp_id = isset($reading_paper_id_array[$rp_index+1]) ? $reading_paper_id_array[$rp_index+1] : null;
                $next_statement_information = Statement::find()->where(['reading_paper_id'=>$next_rp_id,'status'=>1])->orderBy(['statement_order'=>SORT_ASC])->one();
                $next_statement_id = !empty($next_statement_information) ? $next_statement_information->id : null;
                $prv_rp_id = $reading_paper_id;
                $next_page = '_first_content';
                $prv_paper = isset($reading_paper_array[$paper_index-1]) ? $reading_paper_array[$paper_index-1] : null;
                if(!$prv_paper){
                    $prv_rp_id = isset($reading_paper_id_array[$rp_index-1]) ? $reading_paper_id_array[$rp_index-1] : null;
                    $cont_prv_ = ReadingPaperContent::find()->where(['reading_paper_id'=>$prv_rp_id, 'content_status'=>1])->orderBy(['order_'=>SORT_DESC])->one();
                    $prv_paper = !empty($cont_prv_) ? $cont_prv_->pages : null;
                }
                $reading_paper_end = true;
            }
            if($end_type == 'statement'){
                $next_rp_id = $prv_rp_id = $reading_paper_id;
                $st_index = array_search($statement_id, $statement_array);
                $next_statement_id = isset($statement_array[$st_index+1]) ? $statement_array[$st_index+1] : null;
                $prv_statement_id = $statement_id;
                $next_page = '_first_content';
                $prv_paper = isset($reading_paper_array[$paper_index-1]) ? $reading_paper_array[$paper_index-1] : null;
                $statemet_end = true;

            }
        }
        if($paper == '_first_content' && empty($end_of_section[$paper])) {
            $next_rp_id = $reading_paper_id;
            $next_page = isset($next_page_array[$paper]) ? $next_page_array[$paper] : null;
            $rp_index = array_search($reading_paper_id, $reading_paper_id_array);
            $prv_rp_id = isset($reading_paper_id_array[$rp_index-1]) ? $reading_paper_id_array[$rp_index-1] : null;
            $st_index = array_search($statement_id, $statement_array);
            $prv_statement_id = isset($statement_array[$st_index-1]) ? $statement_array[$st_index-1] : null;
            if($prv_statement_id){
                $next_statement_id = $statement_id;
                $prv_rp_id = $reading_paper_id;
                $next_page = isset($next_page_array[$paper]) ? $next_page_array[$paper] : null;
                $reading_paper_prv_content_details = ReadingPaperContent::find()->where(['reading_paper_id'=>$prv_rp_id, 'statement_id'=>$prv_statement_id, 'content_status'=>1, 'is_read'=>1])->orderBy(['order_'=>SORT_DESC])->one();
                $prv_paper = !empty($reading_paper_prv_content_details) ? $reading_paper_prv_content_details->pages : null;
            }
            if($prv_rp_id && !$prv_statement_id){
                $previous_statement = Statement::find()->where(['reading_paper_id'=>$prv_rp_id,'status'=>1])->orderBy(['statement_order'=>SORT_DESC])->one();
                if(!empty($previous_statement)){
                    $next_statement_id = $statement_id;
                    $prv_statement_id = $previous_statement->id;
                    $next_page = isset($next_page_array[$paper]) ? $next_page_array[$paper] : null;
                    $reading_paper_prv_content_details = ReadingPaperContent::find()->where(['reading_paper_id'=>$prv_rp_id, 'statement_id'=>$previous_statement->id, 'content_status'=>1, 'is_read'=>1])->orderBy(['order_'=>SORT_DESC])->one();
                    $prv_paper = !empty($reading_paper_prv_content_details) ? $reading_paper_prv_content_details->pages : null;
                }else{
                    $next_statement_id = $statement_id;
                    $next_page = isset($next_page_array[$paper]) ? $next_page_array[$paper] : null;
                    $reading_paper_prv_content_details = ReadingPaperContent::find()->where(['reading_paper_id'=>$prv_rp_id, 'content_status'=>1, 'is_read'=>1])->orderBy(['order_'=>SORT_DESC])->one();
                    $prv_paper = !empty($reading_paper_prv_content_details) ? $reading_paper_prv_content_details->pages : null;
                }
            }
            if(!$prv_rp_id && !$prv_statement_id){
                $next_statement_id = $statement_id;
                $next_page = isset($next_page_array[$paper]) ? $next_page_array[$paper] : null;
            }
        }
        if($paper != '_first_content' && empty($end_of_section[$paper])) {
            $next_page = isset($next_page_array[$paper]) ? $next_page_array[$paper] : null;
            $prv_paper = isset($reading_paper_array[$paper_index-1]) ? $reading_paper_array[$paper_index-1] : null;
            $prv_rp_id = $next_rp_id = $reading_paper_id; 
            $prv_statement_id = $next_statement_id = $statement_id;
        }
        $page_informaiton['prv_rp_id']  = $prv_rp_id;
        $page_informaiton['next_rp_id']  = $next_rp_id;
        $page_informaiton['prv_page']  = $prv_paper;
        $page_informaiton['next_page']  = $next_page;
        $page_informaiton['prv_statement_id']  = $prv_statement_id;
        $page_informaiton['next_statement_id']  = $next_statement_id;
        $page_informaiton['reading_paper_end'] = $reading_paper_end;
        $page_informaiton['statemet_end'] = $statemet_end;
        return $page_informaiton;

    }

    /**
    *next page template;
    */
    public static function NextPageTemplate($reading_paper_id, $statement, $page_name, $main_type, $type, $state, $asui_id = null){
       $next_page = $_end = $_next_id = null;
       if($main_type == 'bill'){
           if($state == 'अनुमति माग्ने'){
               if(empty($statement)){
                   $pages_array = ReadingPaper::crt_bill_anumati;
                   $index = array_search($page_name, $pages_array);
                   $next_page = isset($pages_array[$index+1]) ? $pages_array[$index+1] : null;
                   $_end = $next_page ?  null : 'paper';
               }
               else{

                   $statement_array = array();
                   $statement_information = Statement::find()->where(['status'=>1, 'reading_paper_id'=>$reading_paper_id])->orderBy(['statement_order'=>SORT_ASC])->all();
                   if(!empty($statement_information)){
                       foreach ($statement_information as $st_information) {
                           $statement_array[] = $st_information->id;
                       }
                   }
                   $st_index = array_search($statement, $statement_array);
                   $end_section = isset($statement_array[$st_index + 1]) ? 'statement' :  'paper';
                   $next_id_section = isset($statement_array[$st_index + 1]) ? $statement_array[$st_index + 1] :  null;
                   $pages_array = ReadingPaper::anumati_bill_birod_template;
                   $index = array_search($page_name, $pages_array);
                   $next_page = isset($pages_array[$index+1]) ? $pages_array[$index+1] : null;
                   if($page_name=='_st_decision')
                       $next_page = '_st_re_back_one,_result';
                   if($page_name=='_st_re_back_two')
                       $next_page = '_st_last_content';
                   if($page_name=='_result')
                       $next_page = '_result_yes,_result_no';
                   if($page_name=='_result_yes'){
                       $next_page = null;
                       $_end = 'paper';
                   }
                   if($page_name == '_st_last_content'){
                       $_end = $end_section;
                       $_next_id = $next_id_section;
                   }
               }
            }
            if($state == 'विचार गरियोस्'){
                $pages_array = ReadingPaper::crt_bill_bichar_garios;
                $index = array_search($page_name, $pages_array);
                $next_page = isset($pages_array[$index+1]) ? $pages_array[$index+1] : null;
                if($page_name=='_second_content')
                    $next_page = '_discuss,_decision_no_discuss';
                if(in_array($page_name, ['_decision_general','_decision_no_discuss']))
                    $next_page = '_result';
                if($page_name == '_result')
                    $next_page = '_result_yes,_result_no,_result_overall_no,_result_overall_yes';
                if(in_array($page_name, ['_result_yes','_result_no','_result_overall_no','_result_overall_yes'])){
                    $_end = 'paper';
                    $_next_id = null;
                    $next_page = null;
                }
            }

            if($state == 'दफावार छलफल समितिमा पठाइयोस्'){
                $pages_array = ReadingPaper::samitima_pathaiyos;
                $index = array_search($page_name, $pages_array);
                $next_page = isset($pages_array[$index+1]) ? $pages_array[$index+1] : null;
                if($page_name =='_first_content')
                    $next_page = '_second_content,_decision';
                if($page_name=='_result')
                    $next_page = '_result_yes,_result_no,_result_overall_no,_result_overall_yes';
                if(in_array($page_name, ['_result_yes','_result_no','_result_overall_no','_result_overall_yes'])){
                    $_end = 'paper';
                    $_next_id = null;
                    $next_page = null;
                }

            }
            if($state == 'समितिको प्रतिवेदन पेश'){
               $next_page = null;
               $_end = 'paper';
            }

            if($state == 'प्रतिवेदन सहित छलफल'){
                if(!empty($statement))
                    $pages_array = ReadingPaper::pertibedan_puna_samitima_pathaiyos;
                else
                    $pages_array = ReadingPaper::pertibedan_sahitko_chalfal;

               $index = array_search($page_name, $pages_array);
               $next_page = isset($pages_array[$index+1]) ? $pages_array[$index+1] : null;
               $_end = $next_page ?  null : 'paper';
                if($page_name=='_st_result')
                       $next_page = '_st_yes,_st_no';
                if($page_name == '_st_yes'){
                   $next_page = null;
                   $_end ='paper';
                }
                if($page_name == '_st_no'){
                   $next_page = '_decision_general';
                }
                if($page_name=='_general_result')
                    $next_page = '_general_yes,_general_no';
                if(in_array($page_name, ['_general_yes','_general_no']))
                    $next_page = '_second_content';
                if($page_name=='_result')
                    $next_page = '_result_yes,_result_no';
                if(in_array($page_name, ['_result_yes','_result_no']))
                    $next_page = '_pertibedan_decision';
                if($page_name=='_pertibedan_result')
                    $next_page = '_pertibedan_yes,_pertibedan_no';
                if(in_array($page_name, ['_pertibedan_yes','_pertibedan_no']))
                    $next_page = '_bidhyak_decision';
                if($page_name=='_bidhyak_result')
                    $next_page = '_bidhyak_yes,_bidhyak_no';
                if(in_array($page_name, ['_bidhyak_yes','_bidhyak_no']))
                    $next_page = '_prastab_first_content';
                if($page_name=='_prastab_result')
                    $next_page = '_prastab_yes,_prastab_no';
                if(in_array($page_name, ['_prastab_yes','_prastab_no']))
                    $next_page = '_third_content';
                if($page_name=='_final_result')
                    $next_page = '_final_no,_final_yes';
                if(in_array($page_name, ['_final_no','_final_yes'])){
                   $next_page = null;
                   $_end = 'paper';
                }
            }
       }
        if($main_type == 'ordinance'){
           if($state == 'पेश'){
               $next_page = null;
               $_end = 'paper';
           }
           if($state == 'छलफल'){
                if(empty($statement)){
                    $pages_array = ReadingPaper::suikrigarios;
                    $index = array_search($page_name, $pages_array);
                    $next_page = isset($pages_array[$index+1]) ? $pages_array[$index+1] : null;
                    if($page_name=='_result')
                        $next_page = '_result_yes,_result_no,_result_overall_no,_result_overall_yes';
                    if(in_array($page_name, ['_result_yes','_result_no','_result_overall_no','_result_overall_yes'])){
                        $_end = 'paper';
                        $_next_id = null;
                        $next_page = null;
                    }
                }else{
                    $pages_array = ReadingPaper::asuikrit;
                    $index = array_search($page_name, $pages_array);
                    $next_page = isset($pages_array[$index+1]) ? $pages_array[$index+1] : null;
                    if($page_name=='_second_content')
                        $next_page = '_third_content,_discuss';
                    if(in_array($page_name,['_third_content','_st_re_back_one','_st_no']))
                        $next_page = '_decision_general';
                    if($page_name == '_st_decision')
                        $next_page = '_st_re_back_one,_st_re_back_two';
                    if($page_name=='_st_result')
                        $next_page = '_st_yes,_st_no';
                    if($page_name=='_result')
                        $next_page = '_result_yes,_result_no';
                    if(in_array($page_name,['_st_yes','_result_yes','_result_no'])){
                        $_next_id = null;
                        $next_page = null;
                        $_end = 'paper';
                    }
                }
           }



       }
       if($main_type == 'time'){
           $pages_array = ReadingPaper::time_template;
           $index = array_search($page_name, $pages_array);
           $next_page = isset($pages_array[$index+1]) ? $pages_array[$index+1] : null;
           $_end = $page_name == '_second_content' ? 'paper' :  null;
       }
       if($main_type == 'letter'){
           $next_page = null;
           $_end = 'paper';
       }
       if($main_type == 'report'){
           $next_page = null;
           $_end = 'paper';
       }

        if($main_type == 'proposal' && $type == 'ध्यानाकर्षण प्रस्ताव'){
            $pages_array = ReadingPaper::dhyan_akarsan_prasthab;
            $index = array_search($page_name, $pages_array);
            $next_page = isset($pages_array[$index+1]) ? $pages_array[$index+1] : null;
            if($page_name=='_first_content')
                $next_page = '_second_content,_third_content';
            if(in_array($page_name, ['_second_content','_third_content']))
                $next_page = '_discuss_suppoter';
            if($page_name == '_discuss_reply'){
               $next_page = null;
               $_end = 'paper';
            }
        }
        if($main_type == 'proposal' && $type == 'जरुरी सार्वजनिक महत्वको प्रस्ताव'){
            $pages_array = ReadingPaper::jaruri_sarbajanik_mahato;
            $index = array_search($page_name, $pages_array);
            $next_page = isset($pages_array[$index+1]) ? $pages_array[$index+1] : null;
            if($page_name == '_st_last_content'){
               $next_page = null;
               $_end = 'paper';
            }
        }
        if($main_type == 'proposal' && $type == 'संकल्प प्रस्ताव'){
            $pages_array = ReadingPaper::sankalpa_prastab;
            $index = array_search($page_name, $pages_array);
            $next_page = isset($pages_array[$index+1]) ? $pages_array[$index+1] : null;
            if($page_name=='_result')
                $next_page = '_result_yes,_result_no,_result_overall_yes';
            if(in_array($page_name, ['_result_yes','_result_no','_result_overall_yes'])){
               $next_page = null;
               $_end = 'paper';
            }
        }
        if($main_type == 'other' && $type == 'शोक प्रस्ताव'){
            $pages_array = ReadingPaper::condolence;
            $index = array_search($page_name, $pages_array);
            $next_page = isset($pages_array[$index+1]) ? $pages_array[$index+1] : null;
            if($page_name == '_third_content'){
               $next_page = null;
               $_end = 'paper';
            }
        }
        if($main_type=='time'){
            $pages_array = ReadingPaper::time_template;
            $index = array_search($page_name, $pages_array);
            $next_page = isset($pages_array[$index+1]) ? $pages_array[$index+1] : null;
            if($page_name == '_third_content'){
               $next_page = null;
               $_end = 'paper';
            }
        }

        if($main_type == 'aggrement' && $state == 'पेश'){
           $next_page = null;
           $_end = 'paper';
        }
        if($main_type == 'aggrement' && in_array($state, ['छलफल','अनुमोदन'])){
            $pages_array = ReadingPaper::aggrement_chalfal_anumodan;
            $index = array_search($page_name, $pages_array);
            $next_page = isset($pages_array[$index+1]) ? $pages_array[$index+1] : null;
            if($page_name == '_second_content')
                $next_page = '_discuss,_decision_no_discuss';
            if(in_array($page_name, ['_decision','_decision_no_discuss']))
                $next_page = '_result';
            if($page_name=='_result')
                $next_page = '_result_yes,_result_no,_result_overall_yes,_result_overall_no';
            if(in_array($page_name, ['_result_yes','_result_no','_result_overall_yes','_result_overall_no'])){
               $next_page = null;
               $_end = 'paper';
            }
        }
        if($main_type == 'proposal' && $type == 'संकल्प प्रस्ताव'){
            $pages_array = ReadingPaper::sankalpa_prastab;
            $index = array_search($page_name, $pages_array);
            $next_page = isset($pages_array[$index+1]) ? $pages_array[$index+1] : null;
            if($page_name=='_result')
                $next_page = '_result_yes,_result_no,_result_overall_yes';
            if(in_array($page_name, ['_result_yes','_result_no','_result_overall_yes'])){
               $next_page = null;
               $_end = 'paper';
            }
        }
       $page_informaiton['next_page']  = $next_page;
       $page_informaiton['_end']  = $_end;
       $page_informaiton['_next_id']  = $_next_id;
       return $page_informaiton;
   }
    public static function ArrangeMainType($a, $b){
        $a = preg_replace('@^(a|an|the) @', '', $a);
        $b = preg_replace('@^(a|an|the) @', '', $b);
        return strcasecmp($a, $b);
    }

    public static function DailypaperOrder($type, $state){
        if($type == 'राष्ट्रपति')
            return 1;
        if($type == 'प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालय')
            return 2;
        if($type == 'राष्ट्रपतिको कार्यालय')
            return 3;
        //bill start from 4
        if($type == 'व्यवस्थापन कार्य'){
            $type_state = ReadingPaper::samiti_bill;
            for ($i=0; $i < sizeof($type_state); $i++) { 
                if($state == $type_state[$i])
                    return $i + 4;
            }
        }
        //pertibedan start from 14
        if($type == 'समितिको प्रतिवेदन'){
            $type_state = ReadingPaper::report_state;
            for ($j=0; $j < sizeof($type_state); $j++) { 
                if($state == $type_state[$j])
                    return $j + 14;
            }

        }
        //mahasandhi start from 18
        if($type == 'महासन्धि'){
            $type_state = ReadingPaper::aggrement_state;
            for ($j=0; $j < sizeof($type_state); $j++) { 
                if($state == $type_state[$j])
                    return $j + 18;
            }

        }
        //mahasandhi start from 21
        if($type == 'सन्धि'){
            $type_state = ReadingPaper::aggrement_state;
            for ($j=0; $j < sizeof($type_state); $j++) { 
                if($state == $type_state[$j])
                    return $j + 18;
            }

        }
        //mahasandhi start from 24
        if($type == 'सम्झौता'){
            $type_state = ReadingPaper::aggrement_state;
            for ($j=0; $j < sizeof($type_state); $j++) { 
                if($state == $type_state[$j])
                    return $j + 24;
            }
        }
        //adhyadesh
        if($type == 'अध्यादेश'){
            $type_state = ReadingPaper::ordinance_state;
            for ($j=0; $j < sizeof($type_state); $j++) { 
                if($state == $type_state[$j])
                    return $j + 26;
            }
        }
        if($type == 'शोक प्रस्ताव')
            return 56;
        //dhyan aakarsan prastab will be start from 26
        if($type == 'ध्यानाकर्षण प्रस्ताव'){
            $type_state = ReadingPaper::ordinance_state;
            for ($j=0; $j < sizeof($type_state); $j++) { 
                if($state == $type_state[$j])
                    return $j + 32;
            }
        }
        //dhyan aakarsan prastab will be start from 32
        if($type == 'जरुरी सार्वजनिक महत्वको प्रस्ताव'){
            $type_state = ReadingPaper::ordinance_state;
            for ($j=0; $j < sizeof($type_state); $j++) { 
                if($state == $type_state[$j])
                    return $j + 38;
            }
        }
        //dhyan aakarsan prastab will be start from 38
        if($type == 'जरुरी सार्वजनिक महत्वको प्रस्ताव'){
            $type_state = ReadingPaper::ordinance_state;
            for ($j=0; $j < sizeof($type_state); $j++) { 
                if($state == $type_state[$j])
                    return $j + 44;
            }
        }
        //dhyan aakarsan prastab will be start from 44
        if($type == 'संकल्प प्रस्ताव'){
            $type_state = ReadingPaper::ordinance_state;
            for ($j=0; $j < sizeof($type_state); $j++) { 
                if($state == $type_state[$j])
                    return $j + 50;
            }
        }
        //dhyan aakarsan prastab will be start from 50
        if($type == 'स्थगन प्रस्ताव'){
            $type_state = ReadingPaper::ordinance_state;
            for ($j=0; $j < sizeof($type_state); $j++) { 
                if($state == $type_state[$j])
                    return $j + 56;
            }
        }
        if($type == 'शोक प्रस्ताव')
            return 62;

    }

    public static function SavapatiName($committee){
        $savapati_name = '';
        $committee_details_section = Committee::find()->where(['name'=>$committee])->one();
        if(!empty($committee_details_section)){
            $committee_details_member = CommitteeMemberDetails::find()->where(['committee_id'=>$committee_details_section->id, 'post'=>['सभापती','उप सभापती'],'status'=>1])->one();
            if(!empty($committee_details_member)){
                $member_id = !empty($committee_details_member) ? $committee_details_member->member_id : null;
                $member_information = ParliamentMember::findOne($member_id);
                $savapati_name = $member_information ? "मा० ".$committee_details_member->post." ".$member_information->member_title.' '.$member_information->first_name.' '.$member_information->last_name : '';

            }

        }
    return $savapati_name;
    }


    public static function ActiveMahasachib(){
        $mahasachib = '';
        $mahasachib_details = MahasachibDetails::find()->where(['status'=>1])->one();
        if(!empty($mahasachib_details))
            return $mahasachib_details->name;
        return $mahasachib;
    }

}
