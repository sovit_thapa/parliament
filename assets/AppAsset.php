<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/admin.css',
        'css/shake.css',
        'css/AdminLTE.min.css',
        'css/plugins/morris.css',
        'css/style.css',
        'css/drage.css',
        'css/preeti.TTF',
        'css/Devnew.ttf',
        'css/skins/_all-skins.min.css',
        'font-awesome-4.3.0/css/font-awesome.min.css',
        'ionicons/css/ionicons.min.css',
    ];
    public $js = [
        'bootstrap/js/bootstrap.min.js',
        'js/app.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
