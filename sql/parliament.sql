-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 22, 2017 at 08:42 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `parliament`
--

-- --------------------------------------------------------

--
-- Table structure for table `adhibasen`
--

CREATE TABLE `adhibasen` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL COMMENT 'adhibasen name, ',
  `year` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'nepali year ',
  `start_date_time` datetime NOT NULL,
  `end_date_time` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `adhibasen`
--

INSERT INTO `adhibasen` (`id`, `name`, `year`, `start_date_time`, `end_date_time`, `status`, `created_by`, `created_date`) VALUES
(1, 'HIUDE AADHIBESAN', '2073/2074', '2017-05-19 05:25:20', '2017-05-19 03:50:13', 0, NULL, NULL),
(2, 'THIRD ADHIBASEN', '2073/2074', '2017-05-19 06:50:04', '2017-05-19 03:50:04', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '1', 1488095419);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, 'admin', NULL, NULL, 1488095411, 1488095411),
('reader', 1, 'read reading paper only', NULL, NULL, 1490080877, 1490080877);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `committee`
--

CREATE TABLE `committee` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `create_ip` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `committee`
--

INSERT INTO `committee` (`id`, `name`, `description`, `created_by`, `create_ip`, `created_date`) VALUES
(1, 'विधायन', '', NULL, NULL, NULL),
(2, 'राज्य व्यवस्था', '', NULL, NULL, NULL),
(3, 'विकास', '', NULL, NULL, NULL),
(4, 'विधायन', '', NULL, NULL, NULL),
(5, 'वातावरण संरक्षण', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `committee_details`
--

CREATE TABLE `committee_details` (
  `id` int(11) NOT NULL,
  `parliament_id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `committee_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_ip` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `committee_details`
--

INSERT INTO `committee_details` (`id`, `parliament_id`, `party_id`, `committee_id`, `number`, `status`, `created_by`, `created_ip`, `created_date`) VALUES
(1, 1, 1, 1, 12, 1, 1, '::1', '2017-03-08 13:09:32'),
(2, 1, 2, 2, 5, 1, 1, '192.168.100.36', '2017-05-02 12:10:59'),
(3, 1, 2, 1, 4, 1, 1, '192.168.100.36', '2017-05-02 13:14:17');

-- --------------------------------------------------------

--
-- Table structure for table `committee_member_details`
--

CREATE TABLE `committee_member_details` (
  `id` int(11) NOT NULL,
  `post` enum('सभापती','उप सभापती') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'सभापती',
  `parliament_id` int(11) NOT NULL,
  `committee_id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_ip` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `committee_member_details`
--

INSERT INTO `committee_member_details` (`id`, `post`, `parliament_id`, `committee_id`, `party_id`, `member_id`, `from_date`, `to_date`, `status`, `created_by`, `created_ip`, `created_date`) VALUES
(2, 'सभापती', 1, 1, 1, 2, '2017-05-21', '2017-05-21', 1, 1, '::1', '2017-03-20 15:08:05');

-- --------------------------------------------------------

--
-- Table structure for table `daily_paper`
--

CREATE TABLE `daily_paper` (
  `id` int(11) NOT NULL,
  `parliament_id` int(11) NOT NULL,
  `meeting_id` int(11) DEFAULT NULL,
  `forum_id` int(11) NOT NULL,
  `main_type` enum('speech','time','letter','aggrement','bill','report','rule','income','ordinance','proposal','election','other') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'other',
  `meeting_header` enum('प्रधानमन्त्रीको बक्तब्य','मन्त्रीको बक्तब्य','शून्य समय','विशेष समय','प्रत्यक्ष प्रशनहरु','व्यवस्थापन कार्य','राष्ट्रपति','प्रधानमन्त्री तथा मन्त्रिपरिषद् कार्यालय','राष्ट्रपति कार्यालय','संवैधानिक आयोगको प्रतिवेदन','समितिको प्रतिवेदन','समितिको बार्षिक प्रतिवेदन','सन्धि','महासन्धि','सम्झौता','नीति तथा कार्यक्रम','राजस्व व्ययको बार्षिक अनुमान','अध्यादेश','ध्यानाकर्षण प्रस्ताव','जरुरी सार्वजनिक महत्वको प्रस्ताव','संकल्प प्रस्ताव','स्थगन प्रस्ताव','सभामुखको निर्वाचन','उप-सभामुखको निर्वाचन','राष्ट्रपतिको निर्वाचन','उप-राष्ट्रपतिको निर्वाचन','अनुपस्थितिको सूचना','स्थान रिक्तता','समिति गठन','शोक प्रस्ताव','समिति गठन','समितिमा थपघट तथा हेरफेर','बैठकमा अध्यक्षता गर्ने सदस्यको मनोनयन','राष्ट्रपतिबाट सम्बोधन','अन्य') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'व्यवस्थापन कार्य',
  `header` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context` text COLLATE utf8_unicode_ci NOT NULL,
  `ministry` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `minister` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `minister_hierarchy` int(11) DEFAULT NULL,
  `related_committee` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_number` int(11) DEFAULT NULL,
  `is_generated_rp` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('active','processing','cancelled','delete','complete') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `state` enum('दर्ता','वितरण','अनुमति माग्ने','विचार गरियोस्','संशोधनको म्याद','दफावार छलफल समितिमा पठाइयोस्','दफावार छलफल सदनमा संशोधन सहित','दफावार छलफल सदनमा संशोधन विना','समितिको प्रतिवेदन पेश','प्रतिवेदन सहितको विधेयक समितिमा फिर्ता','पुनः समितिको प्रतिवेदन पेश','प्रतिवेदन सहित छलफल','पारित','पेश','छलफल','अनुमोदन') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'दर्ता',
  `_result` enum('प्रकृया','सर्बसम्ती स्वीकृत','सर्बसम्ती अस्वीकृति','बहुमत स्वीकृत','बहुमत अस्वीकृति','स्वीकृत','फिर्ता','अस्वीकृत') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'प्रकृया'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `daily_paper`
--

INSERT INTO `daily_paper` (`id`, `parliament_id`, `meeting_id`, `forum_id`, `main_type`, `meeting_header`, `header`, `context`, `ministry`, `minister`, `minister_hierarchy`, `related_committee`, `order_number`, `is_generated_rp`, `status`, `created_by`, `created_date`, `state`, `_result`) VALUES
(1, 1, 1, 2, 'bill', 'व्यवस्थापन कार्य', NULL, '<p>(क) bill one लाई प्रस्तुत गर्न अनुमति माग्ने प्रस्ताव प्रस्तुत गर्नुहुने ।<br />\r\n(ख) bill one प्रस्तुत गर्नुहुने ।</p>\r\n', 'गृह', 'मन्त्री श्री अमिय कुमार  यादब', 0, 'राज्य व्यवस्था', 2, 1, 'active', 1, '2017-05-22 00:09:00', 'अनुमति माग्ने', 'प्रकृया'),
(2, 1, 1, 3, 'bill', 'व्यवस्थापन कार्य', NULL, '<p>(क) bill two लाई प्रस्तुत गर्न अनुमति माग्ने प्रस्ताव प्रस्तुत गर्नुहुने ।<br />\r\n(ख) bill two प्रस्तुत गर्नुहुने ।</p>\r\n', 'अर्थ', 'मन्त्री श्री अमेरिका  कुमारी', 0, 'अर्थ', 3, 1, 'active', 1, '2017-05-22 00:09:33', 'अनुमति माग्ने', 'प्रकृया'),
(3, 1, 1, 1, 'letter', 'राष्ट्रपति', NULL, '<p>सम्माननीय सभामुखले राष्ट्रपति बाट प्राप्त पत्रको व्यहोरा पढेर सुनाउनु हुने।</p>\r\n', '', '', 100, NULL, 1, 1, 'active', 1, '2017-05-22 00:09:43', 'पारित', 'प्रकृया');

-- --------------------------------------------------------

--
-- Table structure for table `darta_desk`
--

CREATE TABLE `darta_desk` (
  `id` int(11) NOT NULL,
  `parliament_id` int(11) NOT NULL,
  `meeting_id` int(11) DEFAULT NULL,
  `main_type` enum('speech','time','letter','aggrement','bill','report','rule','income','ordinance','proposal','election','other') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'other',
  `type` enum('प्रधानमन्त्रीको बक्तब्य','मन्त्रीको बक्तब्य','शून्य समय','विशेष समय','प्रत्यक्ष प्रशनहरु','व्यवस्थापन कार्य','राष्ट्रपति','प्रधानमन्त्री तथा मन्त्रिपरिषद् कार्यालय','राष्ट्रपति कार्यालय','संवैधानिक आयोगको प्रतिवेदन','समितिको प्रतिवेदन','समितिको बार्षिक प्रतिवेदन','सन्धि','महासन्धि','सम्झौता','नीति तथा कार्यक्रम','राजस्व व्ययको बार्षिक अनुमान','अध्यादेश','ध्यानाकर्षण प्रस्ताव','जरुरी सार्वजनिक महत्वको प्रस्ताव','संकल्प प्रस्ताव','स्थगन प्रस्ताव','सभामुखको निर्वाचन','उप-सभामुखको निर्वाचन','राष्ट्रपतिको निर्वाचन','उप-राष्ट्रपतिको निर्वाचन','अनुपस्थितिको सूचना','स्थान रिक्तता','समिति गठन','शोक प्रस्ताव','समिति गठन','समितिमा थपघट तथा हेरफेर','बैठकमा अध्यक्षता गर्ने सदस्यको मनोनयन','राष्ट्रपतिबाट सम्बोधन','अन्य') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'अन्य',
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `ministry` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `minister` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `related_committee` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` enum('दर्ता','वितरण','पेश','अनुमति माग्ने','विचार गरियोस्','संशोधनको म्याद','समितिमा पठाइयोस्','दफावार छलफल सदनमा संशोधन सहित','दफावार छलफल सदनमा संशोधन विना','समितिको प्रतिवेदन पेश','प्रतिवेदन सहितको विधेयक समितिमा फिर्ता','पुनः समितिको प्रतिवेदन पेश','प्रतिवेदन सहित छलफल','टेबुल','छलफल','पारित','अनुमोदन') COLLATE utf8_unicode_ci DEFAULT 'दर्ता',
  `state_status` enum('प्रकृया','स्वीकृत','फिर्ता','अस्वीकृत') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'स्वीकृत',
  `check_state` enum('दर्ता','वितरण','पेश','अनुमति माग्ने','विचार गरियोस्','संशोधनको म्याद','समितिमा पठाइयोस्','दफावार छलफल सदनमा संशोधन सहित','दफावार छलफल सदनमा संशोधन विना','समितिको प्रतिवेदन पेश','प्रतिवेदन सहितको विधेयक समितिमा फिर्ता','पुनः समितिको प्रतिवेदन पेश','प्रतिवेदन सहित छलफल','टेबुल','छलफल','पारित','अनुमोदन') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'दर्ता',
  `is_generate_dp` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `remarks` text COLLATE utf8_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `presentor` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'for proposal only',
  `suppoter` text COLLATE utf8_unicode_ci COMMENT 'for proposal member are separeted by comma'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `darta_desk`
--

INSERT INTO `darta_desk` (`id`, `parliament_id`, `meeting_id`, `main_type`, `type`, `title`, `ministry`, `minister`, `related_committee`, `state`, `state_status`, `check_state`, `is_generate_dp`, `status`, `remarks`, `created_by`, `created_date`, `updated_by`, `updated_date`, `presentor`, `suppoter`) VALUES
(1, 1, 1, 'letter', 'राष्ट्रपति', '<p>president letter</p>\r\n', NULL, '', '', 'दर्ता', 'प्रकृया', 'पारित', 1, 1, NULL, 1, '2017-05-22 00:04:08', 1, '2017-05-22 00:09:43', NULL, NULL),
(2, 1, 1, 'bill', 'व्यवस्थापन कार्य', '<p>bill one</p>\r\n', 'गृह', 'मा० गृह मन्त्री श्री अमिय कुमार  यादब', 'राज्य व्यवस्था', 'दर्ता', 'प्रकृया', 'अनुमति माग्ने', 1, 1, NULL, 1, '2017-05-22 00:04:28', 1, '2017-05-22 00:09:00', NULL, NULL),
(3, 1, 1, 'bill', 'व्यवस्थापन कार्य', '<p>bill two</p>\r\n', 'अर्थ', 'मा० अर्थ मन्त्री श्री अमेरिका  कुमारी', 'अर्थ', 'दर्ता', 'प्रकृया', 'अनुमति माग्ने', 1, 1, NULL, 1, '2017-05-22 00:09:22', 1, '2017-05-22 00:09:33', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ethinic_cast`
--

CREATE TABLE `ethinic_cast` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ethinic_cast`
--

INSERT INTO `ethinic_cast` (`id`, `parent_id`, `name`, `description`, `created_by`, `created_date`) VALUES
(1, 0, 'Brahaman/Chhetri', NULL, NULL, NULL),
(2, 0, 'Tarai/Madhesi  ', NULL, NULL, NULL),
(3, 0, 'Dalits', NULL, NULL, NULL),
(4, 0, 'Janajati', '', NULL, NULL),
(5, 0, 'Other', '', NULL, NULL),
(6, 1, 'Hill Brahman', '', NULL, NULL),
(7, 1, 'Hill Chhetri', '', NULL, NULL),
(8, 1, 'Chhetri', '', NULL, NULL),
(9, 1, 'Tarai/Madhesi Brahman/Chhetri', '', NULL, NULL),
(10, 1, 'Thakuri', '', NULL, NULL),
(11, 1, 'Sanyasi', '', 0, '0000-00-00 00:00:00'),
(12, 1, 'Madhesi Brahman', '', 0, '0000-00-00 00:00:00'),
(13, 1, 'Nurang', '', 0, '0000-00-00 00:00:00'),
(14, 1, 'Rajput', '', 0, '0000-00-00 00:00:00'),
(15, 1, 'Kayastha', '', 0, '0000-00-00 00:00:00'),
(16, 1, 'Others', '', 0, '0000-00-00 00:00:00'),
(17, 2, 'Kewat', '', 0, '0000-00-00 00:00:00'),
(18, 2, 'Mallah', '', 0, '0000-00-00 00:00:00'),
(19, 2, 'Lohar', '', 0, '0000-00-00 00:00:00'),
(20, 2, 'Nuniya', '', 0, '0000-00-00 00:00:00'),
(21, 2, 'Kahar', '', 0, '0000-00-00 00:00:00'),
(22, 2, 'Lodha', '', 0, '0000-00-00 00:00:00'),
(23, 2, 'Rajbhar', '', 0, '0000-00-00 00:00:00'),
(24, 2, 'Bing', '', 0, '0000-00-00 00:00:00'),
(25, 2, 'Mali', '', 0, '0000-00-00 00:00:00'),
(26, 2, 'Kamar', '', 0, '0000-00-00 00:00:00'),
(27, 2, 'Dhuniya', '', 0, '0000-00-00 00:00:00'),
(28, 2, 'Yadav', '', 0, '0000-00-00 00:00:00'),
(29, 2, 'Teli', '', 0, '0000-00-00 00:00:00'),
(30, 2, 'Koiri', '', 0, '0000-00-00 00:00:00'),
(31, 2, 'Kurmi', '', 0, '0000-00-00 00:00:00'),
(32, 2, 'Sonar', '', 0, '0000-00-00 00:00:00'),
(33, 2, 'Baniya', '', 0, '0000-00-00 00:00:00'),
(34, 2, 'Kalwar', '', 0, '0000-00-00 00:00:00'),
(35, 2, 'Thakur/Hazam', '', 0, '0000-00-00 00:00:00'),
(36, 2, 'Kanu', '', 0, '0000-00-00 00:00:00'),
(37, 2, 'Sudhi', '', 0, '0000-00-00 00:00:00'),
(38, 2, 'Kumhar', '', 0, '0000-00-00 00:00:00'),
(39, 2, 'Haluwai', '', 0, '0000-00-00 00:00:00'),
(40, 2, 'Badhai', '', 0, '0000-00-00 00:00:00'),
(41, 2, 'Barai', '', 0, '0000-00-00 00:00:00'),
(42, 2, 'Bhediyar/ Gaderi', '', 0, '0000-00-00 00:00:00'),
(43, 2, 'Others', '', 0, '0000-00-00 00:00:00'),
(44, 3, 'Kami', '', 0, '0000-00-00 00:00:00'),
(45, 3, 'Damai/Dholi', '', 0, '0000-00-00 00:00:00'),
(46, 3, 'Sarki', '', 0, '0000-00-00 00:00:00'),
(47, 3, 'Badi', '', 0, '0000-00-00 00:00:00'),
(48, 3, 'Gaine', '', 0, '0000-00-00 00:00:00'),
(49, 3, 'Unidentified Dalits', '', 0, '0000-00-00 00:00:00'),
(50, 3, 'Tarai/Madhesi Dalit', '', 0, '0000-00-00 00:00:00'),
(51, 3, 'Chamar/Harijan', '', 0, '0000-00-00 00:00:00'),
(52, 3, 'Musahar', '', 0, '0000-00-00 00:00:00'),
(53, 3, 'Dushad/Paswan', '', 0, '0000-00-00 00:00:00'),
(54, 3, 'Tatma', '', 0, '0000-00-00 00:00:00'),
(55, 3, 'Khatwe', '', 0, '0000-00-00 00:00:00'),
(56, 3, 'Dhobi', '', 0, '0000-00-00 00:00:00'),
(57, 3, 'Baantar', '', 0, '0000-00-00 00:00:00'),
(58, 3, 'Chidimar', '', 0, '0000-00-00 00:00:00'),
(59, 3, 'Dom', '', 0, '0000-00-00 00:00:00'),
(60, 3, 'Halkhor', '', 0, '0000-00-00 00:00:00'),
(61, 3, 'Others', '', 0, '0000-00-00 00:00:00'),
(62, 4, 'Newar', '', 0, '0000-00-00 00:00:00'),
(63, 4, 'Tamang', '', 0, '0000-00-00 00:00:00'),
(64, 4, 'Kumal', '', 0, '0000-00-00 00:00:00'),
(65, 4, 'Sunuwar', '', 0, '0000-00-00 00:00:00'),
(66, 4, 'Majhi', '', 0, '0000-00-00 00:00:00'),
(67, 4, 'Danuwar', '', 0, '0000-00-00 00:00:00'),
(68, 4, 'Thami/Thangmi', '', 0, '0000-00-00 00:00:00'),
(69, 4, 'Darai', '', 0, '0000-00-00 00:00:00'),
(70, 4, 'Bhote', '', 0, '0000-00-00 00:00:00'),
(71, 4, 'Baramu/Bramhu', '', 0, '0000-00-00 00:00:00'),
(72, 4, 'Pahari', '', 0, '0000-00-00 00:00:00'),
(73, 4, 'Kusunda', '', 0, '0000-00-00 00:00:00'),
(74, 4, 'Raji', '', 0, '0000-00-00 00:00:00'),
(75, 4, 'Raute', '', 0, '0000-00-00 00:00:00'),
(76, 4, 'Chepang/Praja', '', 0, '0000-00-00 00:00:00'),
(77, 4, 'Hayu', '', 0, '0000-00-00 00:00:00'),
(78, 4, 'Magar', '', 0, '0000-00-00 00:00:00'),
(79, 4, 'Chyantal', '', 0, '0000-00-00 00:00:00'),
(80, 4, 'Rai', '', 0, '0000-00-00 00:00:00'),
(81, 4, 'Sherpa', '', 0, '0000-00-00 00:00:00'),
(82, 4, 'Bhujel/Gharti', '', 0, '0000-00-00 00:00:00'),
(83, 4, 'yakha', '', 0, '0000-00-00 00:00:00'),
(84, 4, 'Thakali', '', 0, '0000-00-00 00:00:00'),
(85, 4, 'Limbu', '', 0, '0000-00-00 00:00:00'),
(86, 4, 'Lepcha', '', 0, '0000-00-00 00:00:00'),
(87, 4, 'Bhote', '', 0, '0000-00-00 00:00:00'),
(88, 4, 'Byansi', '', 0, '0000-00-00 00:00:00'),
(89, 4, 'Jirel', '', 0, '0000-00-00 00:00:00'),
(90, 4, 'Hyalmo', '', 0, '0000-00-00 00:00:00'),
(91, 4, 'Walung', '', 0, '0000-00-00 00:00:00'),
(92, 4, 'Gurung', '', 0, '0000-00-00 00:00:00'),
(93, 4, 'Dura', '', 0, '0000-00-00 00:00:00'),
(94, 4, 'Tarai Janajati', '', 0, '0000-00-00 00:00:00'),
(95, 4, 'Tharu', '', 0, '0000-00-00 00:00:00'),
(96, 4, 'Jhangad', '', 0, '0000-00-00 00:00:00'),
(97, 4, 'Dhanuk', '', 0, '0000-00-00 00:00:00'),
(98, 4, 'Rajbanshi', '', 0, '0000-00-00 00:00:00'),
(99, 4, 'Gangai', '', 0, '0000-00-00 00:00:00'),
(100, 4, 'Santhal/Satar', '', 0, '0000-00-00 00:00:00'),
(101, 4, 'Dhimal', '', 0, '0000-00-00 00:00:00'),
(102, 4, 'Tajpuriya', '', 0, '0000-00-00 00:00:00'),
(103, 4, 'Meche', '', 0, '0000-00-00 00:00:00'),
(104, 4, 'Koche', '', 0, '0000-00-00 00:00:00'),
(105, 4, 'Kisan', '', 0, '0000-00-00 00:00:00'),
(106, 4, 'Munda', '', 0, '0000-00-00 00:00:00'),
(107, 4, 'Muslim', '', 0, '0000-00-00 00:00:00'),
(108, 4, 'Others', '', 0, '0000-00-00 00:00:00'),
(109, 5, 'Others', '', 0, '0000-00-00 00:00:00'),
(110, 1, 'Ram', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT '0' COMMENT 'parent_id for the location from the location to denotes higher level of that location',
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'name of the location',
  `nepali_name` varchar(200) DEFAULT NULL,
  `location_level` int(2) NOT NULL COMMENT 'level of that location',
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `location_slug` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='details of the hirerchical location ';

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `parent_id`, `name`, `nepali_name`, `location_level`, `created_date`, `created_by`, `location_slug`) VALUES
(20, 6, 'Ilam', 'Onfd ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(21, 6, 'Jhapa', 'emfkf ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(22, 6, 'Panchthar', 'kfFry/ ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(23, 6, 'Taplejung', 'tfKn]h''Ë ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(24, 7, 'Bhojpur', 'ef]hk''/ ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(25, 7, 'Dhankuta', 'wgs''6f', 3, '0000-00-00 00:00:00', 0, 'ds'),
(26, 7, 'Morang', 'df]/u', 3, '0000-00-00 00:00:00', 0, 'ds'),
(27, 7, 'Sankhuwasabha', ';+v''jf;ef ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(28, 7, 'Sunsari', ';''g;/L ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(29, 7, 'Terhathum', 't]x|y''d', 3, '0000-00-00 00:00:00', 0, 'ds'),
(30, 8, 'Khotang', 'vf]6fª', 3, '0000-00-00 00:00:00', 0, 'ds'),
(31, 8, 'Okhaldhunga', ' cf]vn9''Ëf', 3, '0000-00-00 00:00:00', 0, 'ds'),
(32, 8, 'Saptari', ';Kt/L ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(33, 8, 'Siraha', 'l;/fx ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(34, 8, 'Solukhumbu', ';f]n''v''Da'' ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(35, 8, 'Udayapur', 'pbok''/ ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(36, 9, 'Dhanusa', ' wg''iff ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(37, 9, 'Dolakha', 'bf]nvf ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(38, 9, 'Mahottari', 'dxf]Q/L ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(39, 9, 'Ramechhap', '/fd]5fk ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(40, 9, 'Sarlahi', ';nf{xL', 3, '0000-00-00 00:00:00', 0, 'ds'),
(41, 9, 'Sindhuli', 'l;Gw''nL ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(42, 10, 'Bhaktapur', 'eQmk''/ ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(43, 10, 'Dhading', 'wflbË', 3, '0000-00-00 00:00:00', 0, 'ds'),
(44, 10, 'Kathmandu', 'sf7df8f}', 3, '0000-00-00 00:00:00', 0, 'ds'),
(45, 10, 'Kabhrepalanchok', 'se|]knfGrf]s ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(46, 10, 'Lalitpur', 'nlntk''/ ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(47, 10, 'Nuwakot', 'g''jfsf]6 ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(48, 10, 'Rasuwa', '/;''jf ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(49, 10, 'Sindhupalchok', 'l;Gw''kfnrf]s', 3, '0000-00-00 00:00:00', 0, 'ds'),
(50, 11, 'Bara', 'af/f', 3, '0000-00-00 00:00:00', 0, 'ds'),
(51, 11, 'Chitwan', 'lrtjg ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(52, 11, 'Makwanpur', 'dsjfgk''/ ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(53, 11, 'Parsa', 'k;f{ ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(54, 11, 'Rautahat', '/f}tx6 ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(55, 12, 'Gorkha', 'uf]vf{ ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(56, 12, 'Kaski', 'sf:sL ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(57, 12, 'Lamjung', 'nDh''ª', 3, '0000-00-00 00:00:00', 0, 'ds'),
(58, 12, 'Manang', 'dgfË', 3, '0000-00-00 00:00:00', 0, 'ds'),
(59, 12, 'Syangja', ':ofª\\hf', 3, '0000-00-00 00:00:00', 0, 'ds'),
(60, 12, 'Tanahun', 'tgx''F ', 3, '0000-00-00 00:00:00', NULL, 'ds'),
(61, 13, 'Arghakhanchi', 'c3f{vfFrL ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(62, 13, 'Gulmi', 'u''NdL ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(63, 13, 'Kapilvastu', 'slknj:t'' ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(64, 13, 'Nawalparasi', 'gjnk/f;L ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(65, 13, 'Palpa', 'kfNkf ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(66, 13, 'Rupandehi', '?kGb]xL ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(67, 14, 'Baglung', 'afUn''Ë', 3, '0000-00-00 00:00:00', 0, 'ds'),
(68, 14, 'Mustang', 'd'':tfË', 3, '0000-00-00 00:00:00', 0, 'ds'),
(69, 14, 'Myagdi', 'DofUbL ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(70, 14, 'Parbat', 'kj{t ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(71, 15, 'Dang', 'bfª', 3, '0000-00-00 00:00:00', 0, 'ds'),
(72, 15, 'Pyuthan', 'Ko''7fg ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(73, 15, 'Rolpa', '/f]Nkf ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(74, 15, 'Rukum', '?s''d ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(75, 15, 'Salyan', ';Nofg ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(76, 16, 'Dolpa', '8f]Nkf ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(77, 16, 'Humla', 'x''Dnf ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(78, 16, 'Jumla', 'h''Dnf ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(79, 16, 'Kalikot', 'sflnsf]6 ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(80, 16, 'Mugu', 'd''u'' ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(81, 17, 'Banke', 'afFs] ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(82, 17, 'Bardiya', 'alb{of ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(83, 17, 'Dailekh', 'b}n]v ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(84, 17, 'Jajarkot', 'hfh/sf]6 ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(85, 17, 'Surkhet', ';''v]{t ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(86, 18, 'Achham', 'c5fd ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(87, 18, 'Bajhang', 'aemfFu', 3, '0000-00-00 00:00:00', 0, 'ds'),
(88, 18, 'Bajura', 'afh''/f ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(89, 18, 'Doti', '8f]6L ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(90, 18, 'Kailali', 's}nfnL ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(91, 19, 'Baitadi', 'a}t8L ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(92, 19, 'Dadeldhura', '88]Nw''/f ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(93, 19, 'Darchula', 'bfr{''nf ', 3, '0000-00-00 00:00:00', 0, 'ds'),
(94, 19, 'Kanchanpur', 's~rGk''/ ', 3, '0000-00-00 00:00:00', 0, 'ds');

-- --------------------------------------------------------

--
-- Table structure for table `meeting_details`
--

CREATE TABLE `meeting_details` (
  `id` int(11) NOT NULL,
  `parliament_id` int(11) DEFAULT NULL COMMENT 'parliament id, if present',
  `adhibasen` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `adhibasen_year` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `_meeting_number` int(11) NOT NULL,
  `_date_time` datetime DEFAULT NULL,
  `_nepali_year` int(11) NOT NULL,
  `_nepali_month` int(11) NOT NULL,
  `_nepali_day` int(11) NOT NULL,
  `_time` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('active','end','cancelled','halt','next') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `meeting_details`
--

INSERT INTO `meeting_details` (`id`, `parliament_id`, `adhibasen`, `adhibasen_year`, `_meeting_number`, `_date_time`, `_nepali_year`, `_nepali_month`, `_nepali_day`, `_time`, `status`, `created_by`, `created_date`) VALUES
(1, 1, 'THIRD ADHIBASEN', '2073/2074', 1, '2017-05-09 09:45:43', 2074, 2, 5, 'बिहान १०:३० ', 'active', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `meeting_halt`
--

CREATE TABLE `meeting_halt` (
  `id` int(11) NOT NULL,
  `meeting_id` int(11) NOT NULL,
  `_time` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'minutes/hours/days',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ministry`
--

CREATE TABLE `ministry` (
  `id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `created_date` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ministry`
--

INSERT INTO `ministry` (`id`, `title`, `description`, `created_by`, `created_date`) VALUES
(1, 'कानुन, न्याय तथा संसदीय मामिला', '', NULL, NULL),
(2, 'संघीय मामिला तथा स्थानीय विकास', '', NULL, NULL),
(3, 'अर्थ', '', NULL, NULL),
(4, 'गृह', '', NULL, NULL),
(5, 'युवा तथा खेलकूद', '', NULL, NULL),
(6, 'वन तथा भू–संरक्षण', '', NULL, NULL),
(7, 'शिक्षा', '', NULL, NULL),
(8, 'उद्योग', '', NULL, NULL),
(9, 'पशुपंक्ष्ी विकास', '', NULL, NULL),
(10, 'प्रधानमन्त्री तथा मन्त्रिपरिषद्', '', NULL, NULL),
(11, 'भौतिक पुर्वाधार तथा यातायात', '', NULL, NULL),
(12, 'श्रम तथा रोजगार', '', NULL, NULL),
(13, 'सिचाई', '', NULL, NULL),
(14, 'स्वास्थ्य तथा जनसंख्या', '', NULL, NULL),
(15, 'महिला, बालबालिका तथा समाज कल्याण', '', NULL, NULL),
(16, 'रक्षा', '', NULL, NULL),
(17, 'सहकारी तथा गरिबी निवारण', '', NULL, NULL),
(18, 'Title 1', '', NULL, NULL),
(19, 'bsdhds', 'shjas', NULL, NULL),
(20, 'Ministry', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ministry_related_committee`
--

CREATE TABLE `ministry_related_committee` (
  `id` int(11) NOT NULL,
  `parliament_id` int(11) NOT NULL,
  `ministry` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `committee` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `_status` tinyint(1) NOT NULL DEFAULT '1',
  `applied_from` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ministry_related_committee`
--

INSERT INTO `ministry_related_committee` (`id`, `parliament_id`, `ministry`, `committee`, `_status`, `applied_from`, `created_by`, `created_date`) VALUES
(1, 1, 'कानुन, न्याय तथा संसदीय मामिला', 'विधायन', 1, NULL, NULL, NULL),
(2, 1, 'गृह', 'राज्य व्यवस्था', 1, NULL, NULL, NULL),
(3, 1, 'संघीय मामिला तथा स्थानीय विकास', 'विकास', 1, NULL, NULL, NULL),
(4, 1, 'वन तथा भू–संरक्षण', 'वातावरण संरक्षण', 1, NULL, NULL, NULL),
(5, 1, 'युवा तथा खेलकूद', 'महिला, बालबालिका, ज्येष्ठ नागरिक र समाज कल्याण', 1, NULL, NULL, NULL),
(6, 1, 'अर्थ', 'अर्थ', 1, NULL, NULL, NULL),
(7, 1, 'शिक्षा', 'महिला, बालबालिका, ज्येष्ठ नागरिक र समाज कल्याण', 1, NULL, NULL, NULL),
(8, 1, 'पशुपंक्ष्ी विकास', 'कृषि तथा जलस्रोत', 1, NULL, NULL, NULL),
(9, 1, 'प्रधानमन्त्री तथा मन्त्रिपरिषद्', 'सुशासन तथा अनुगमन ', 1, NULL, NULL, NULL),
(10, 1, 'भौतिक पुर्वाधार तथा यातायात', 'विकास', 1, NULL, NULL, NULL),
(11, 1, 'श्रम तथा रोजगार', 'अन्तर्राष्ट्रिय र श्रम सम्बन्ध', 1, NULL, NULL, NULL),
(12, 1, 'सिचाई', 'कृषि तथा जलस्रोत', 1, NULL, NULL, NULL),
(13, 1, 'रक्षा', 'राज्य व्यवस्था', 1, NULL, NULL, NULL),
(14, 1, 'उद्योग', 'उद्योग वाणिज्य तथा उपभोक्ता हित सम्बन्ध', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `notice_paper`
--

CREATE TABLE `notice_paper` (
  `id` int(11) NOT NULL,
  `activities_header_details` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parliament_id` int(11) NOT NULL,
  `meeting_id` int(11) DEFAULT NULL,
  `meeting_header` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forum_id` int(11) NOT NULL,
  `context` text COLLATE utf8_unicode_ci NOT NULL,
  `nepali_year` int(4) NOT NULL,
  `nepali_month` int(2) NOT NULL,
  `nepali_days` int(2) NOT NULL,
  `nepali_time` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `minister` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `related_committee` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `order_number` int(11) DEFAULT NULL,
  `status` enum('active','in-active') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `official_post`
--

CREATE TABLE `official_post` (
  `id` int(11) NOT NULL,
  `type` enum('goverment','party') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'goverment',
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `is_only_one` enum('in_party','in_goverment','none') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'none',
  `description` text COLLATE utf8_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `create_ip` int(11) DEFAULT NULL,
  `created_date` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `official_post`
--

INSERT INTO `official_post` (`id`, `type`, `name`, `is_only_one`, `description`, `created_by`, `create_ip`, `created_date`) VALUES
(1, 'goverment', 'प्रधानमन्त्री', 'in_goverment', '', NULL, NULL, NULL),
(2, 'party', 'सभापती', 'in_party', '', NULL, NULL, NULL),
(3, 'party', 'उप सभापती ', 'none', '', NULL, NULL, NULL),
(4, 'goverment', 'मन्त्री', 'none', '', NULL, NULL, NULL),
(5, 'goverment', 'उप प्रधानमन्त्री', 'none', '', NULL, NULL, NULL),
(6, 'party', 'अध्यक्षय', 'in_party', '', NULL, NULL, NULL),
(7, 'party', 'उप अध्यक्षय', 'in_party', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `parliament_activities`
--

CREATE TABLE `parliament_activities` (
  `id` int(11) NOT NULL,
  `activities_header_details` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parliament_id` int(11) NOT NULL,
  `meeting_number` int(11) DEFAULT NULL,
  `aadhyadesh` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forum_id` int(11) NOT NULL,
  `context` text COLLATE utf8_unicode_ci NOT NULL,
  `nepali_year` int(4) NOT NULL,
  `nepali_month` int(2) NOT NULL,
  `nepali_days` int(2) NOT NULL,
  `nepali_time` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `minister` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `related_committee` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `order_number` int(11) DEFAULT NULL,
  `reading_order` int(11) DEFAULT NULL,
  `status` enum('active','cancelled','delete','complete') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `activities_section` enum('DP','RP','Notice') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'DP',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `parliament_activities`
--

INSERT INTO `parliament_activities` (`id`, `activities_header_details`, `parliament_id`, `meeting_number`, `aadhyadesh`, `forum_id`, `context`, `nepali_year`, `nepali_month`, `nepali_days`, `nepali_time`, `date`, `minister`, `related_committee`, `order_number`, `reading_order`, `status`, `activities_section`, `created_by`, `created_date`) VALUES
(1, NULL, 1, 1, 'दोस्रो अधिवेशन', 9, 'सार्वजनिक लेखा समितिका मा० सभापति श्री जनार्दन शर्माले प्रतिवेदन पेश बाट प्राप्त सार्वजनिक लेखा समितिको १८ औं प्रतिवेदन, २०७३ पेश गर्नुहुने ।', 2073, 11, 17, 'दिनको ३ः०० बजे', '2017-02-28 03:00:00', 'सार्वजनिक लेखा समितिका मा० सभापति श्री जनार्दन शर्मा', 'सार्वजनिक लेखा समिति', 1, 1, 'active', 'DP', 1, '2017-02-28 13:33:14'),
(2, NULL, 1, 1, 'दोस्रो अधिवेशन', 3, 'मा० उपप्रधानमन्त्री एवं गृह मन्त्री श्री विमलेन्द्र निधिले "राज्य व्यवस्था समितिको प्रतिवेदन सहितको मतदाता नामावली विधेयक, २०७३ माथि विचार गरियोस्” भनी प्रस्ताव प्रस्तुत गर्न अनुमति दिन्छु ।', 2073, 11, 17, 'दिनको ३ः०० बजे', '2017-02-28 03:00:00', 'मा० उपप्रधानमन्त्री एवं गृह मन्त्री श्री विमलेन्द्र निधि', 'राज्य व्यवस्था', 2, 2, 'active', 'DP', 1, '2017-02-28 13:35:02'),
(3, NULL, 1, 1, 'दोस्रो अधिवेशन', 4, 'मा० उपप्रधानमन्त्री एवं गृह मन्त्री श्री विमलेन्द्र निधिले ः– \r\n (क) राज्य व्यवस्था समितिको प्रतिवेदन सहितको निर्वाचन आयोग विधेयक, २०७३ लाई प्रस्तुत गर्न अनुमति माग्ने प्रस्ताव प्रस्तुत गर्नुहुने । \r\n (ख) राज्य व्यवस्था समितिको प्रतिवेदन सहितको निर्वाचन आयोग विधेयक, २०७३ प्रस्तुत गर्नुहुने ।', 2073, 11, 17, 'दिनको ३ः०० बजे', '2017-02-28 03:00:00', 'मा० उपप्रधानमन्त्री एवं गृह मन्त्री श्री विमलेन्द्र निधि', 'राज्य व्यवस्था', 3, 3, 'active', 'DP', 1, '2017-02-28 13:35:26'),
(4, NULL, 1, 1, 'दोस्रो अधिवेशन', 10, 'मा० उपप्रधानमन्त्री तथा गृह मन्त्री श्री विमलेन्द्र निधिले ः– \r\n (क) स्थानीय तह निर्वाचन विधेयक, २०७३ लाई प्रस्तुत गर्न अनुमति माग्ने प्रस्ताव प्रस्तुत गर्नुहुने । \r\n (ख) स्थानीय तह निर्वाचन विधेयक, २०७३ प्रस्तुत गर्नुहुने ।', 2073, 11, 17, 'दिनको ३ः०० बजे', '2017-02-28 03:00:00', 'मा० उपप्रधानमन्त्री तथा गृह मन्त्री श्री विमलेन्द्र निधि', 'गृह', 4, 4, 'active', 'DP', 1, '2017-02-28 15:34:13');

-- --------------------------------------------------------

--
-- Table structure for table `parliament_details`
--

CREATE TABLE `parliament_details` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `parliament_total_member` int(11) DEFAULT NULL,
  `start_session` date DEFAULT NULL,
  `end_session` date DEFAULT NULL,
  `start_session_nepali_date` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_session_nepali_date` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `extented_time` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` enum('active','in-active') COLLATE utf8_unicode_ci DEFAULT 'active',
  `created_by` int(11) DEFAULT NULL,
  `created_ip` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `parliament_details`
--

INSERT INTO `parliament_details` (`id`, `name`, `parliament_total_member`, `start_session`, `end_session`, `start_session_nepali_date`, `end_session_nepali_date`, `extented_time`, `description`, `status`, `created_by`, `created_ip`, `created_date`) VALUES
(1, '२०७२ सम्बिधन सभा', 601, '2014-02-27', '2017-11-30', NULL, NULL, NULL, NULL, 'active', NULL, NULL, NULL),
(2, '२०७२ सम्बिधन सभा', 601, '0000-00-00', '0000-00-00', '2072-10-10', '2075-10-10', NULL, '', 'active', 1, '::1', '2017-03-01 13:18:40');

-- --------------------------------------------------------

--
-- Table structure for table `parliament_member`
--

CREATE TABLE `parliament_member` (
  `id` int(11) NOT NULL,
  `parliament_id` int(11) NOT NULL,
  `political_id` int(11) NOT NULL,
  `member_title` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `district_id` int(11) NOT NULL,
  `elected_area_number` int(11) DEFAULT NULL,
  `ethinicity_id` int(11) NOT NULL,
  `elected_type` enum('प्रत्यक्ष-निर्वाचन','उपनिर्वाचन','समानुपातिक',' मनोनित','अन्य') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'प्रत्यक्ष-निर्वाचन',
  `date_of_birth` date DEFAULT NULL,
  `dob_nepali_day` int(2) DEFAULT NULL,
  `dob_nepali_month` int(2) DEFAULT NULL,
  `dob_nepali_year` int(4) DEFAULT NULL,
  `sex` enum('पुरुष','स्त्री','अन्य') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'पुरुष',
  `rural_area` enum('हो','होइन') COLLATE utf8_unicode_ci DEFAULT 'होइन',
  `oath_of_parliament_member` date DEFAULT NULL,
  `oath_nepali_day` int(2) DEFAULT NULL,
  `oath_nepali_month` int(2) DEFAULT NULL,
  `oath_nepali_year` int(4) DEFAULT NULL,
  `parliament_status` enum('active','over') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `status` enum('सकृय','निलम्बन','राजीनामा','पद खारेज','मृत्यू','राष्ट्रपतिमा निर्वाचित','उपराष्ट्रपतिमा निर्वाचित') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'सकृय',
  `created_by` int(11) DEFAULT NULL,
  `created_date` int(11) DEFAULT NULL,
  `created_ip` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `parliament_member`
--

INSERT INTO `parliament_member` (`id`, `parliament_id`, `political_id`, `member_title`, `first_name`, `last_name`, `district_id`, `elected_area_number`, `ethinicity_id`, `elected_type`, `date_of_birth`, `dob_nepali_day`, `dob_nepali_month`, `dob_nepali_year`, `sex`, `rural_area`, `oath_of_parliament_member`, `oath_nepali_day`, `oath_nepali_month`, `oath_nepali_year`, `parliament_status`, `status`, `created_by`, `created_date`, `created_ip`) VALUES
(1, 1, 10, 'मा० श्री', ' अइन्द्र सुन्दर', 'नेम्वाङ', 22, 1, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, 'पुरुष', '', NULL, NULL, NULL, NULL, 'active', '', 1, 2147483647, '192.168.100.36'),
(2, 1, 1, 'मा० श्री', ' अकबाल अहमद ', ' शाह', 63, 2, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(3, 1, 2, 'मा० श्री', ' अग्निप्रसाद ', 'खरेल', 21, 3, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(4, 1, 3, 'मा० श्री', 'अग्नी प्रसाद ', 'सापकोटा', 49, 4, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(5, 1, 3, 'मा० श्री', ' शंकर ', 'नायक', 33, 1, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(6, 1, 4, 'मा० श्री', 'अर्जुन नरसिं ', 'के.सी', 47, 1, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(7, 1, 4, 'मा० श्री', ' अर्जुन प्रसाद ', 'जोशी', 70, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(8, 1, 4, 'मा० श्री', 'अञ्जनी ', 'श्रेष्ठ', 44, 2, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(9, 1, 4, 'मा० श्री', ' अतहर कमाल ', 'मुसलमान', 63, 1, 5, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(10, 1, 3, 'मा० श्री', 'अन्जना ', 'चौधरी', 81, 1, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(11, 1, 4, 'मा० श्री', ' अन्जना ', 'ताम्ली', 25, 1, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(12, 1, 5, 'मा० श्री', 'अनन्त प्रसाद ', 'पौडेल', 52, 2, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(13, 1, 5, 'मा० श्री', ' अनारकली ', 'मिया', 75, 1, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(14, 1, 3, 'मा० श्री', 'अनिता कुमारी ', 'परियार', 38, 1, 3, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(15, 1, 4, 'मा० श्री', 'अनिता ', 'देवकोटा', 71, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(16, 1, 2, 'मा० श्री', ' अनिलकुमार ', 'रुंगटा', 53, 1, 5, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(17, 1, 7, 'मा० श्री', 'अनुराधा ', 'थापामगर', 42, 1, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(18, 1, 5, 'मा० श्री', 'अफिलाल ', 'ओखेडा', 87, 1, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(19, 1, 4, 'मा० श्री', 'अब्दुल रज्जाक ', 'गद्दी', 66, 1, 5, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(20, 1, 4, 'मा० श्री', ' अब्दुल हमिद ', 'सिध्दिकी', 81, 2, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(21, 1, 8, 'मा० श्री', 'अभिषेक प्रताप ', 'शाह', 63, 1, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(22, 1, 5, 'मा० श्री', 'अम्मर बहादुर', ' थापा', 83, 1, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(23, 1, 11, 'मा० श्री', 'अमन लाल ', 'मोदी', 26, 1, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(24, 1, 5, 'मा० श्री', 'अमर शर्मा देव राज ', 'भार', 81, NULL, 5, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(25, 1, 4, 'मा० श्री', 'अमर सिह ', 'पुन', 73, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(26, 1, 4, 'मा० श्री', ' अमरेश कुमार ', 'सिंह', 33, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(27, 1, 4, 'मा० श्री', 'अमिय कुमार ', 'यादब', 33, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(28, 1, 5, 'मा० श्री', ' अम्विका खवास ', 'राजवंशी', 21, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(29, 1, 4, 'मा० श्री', 'अम्विका ', 'बस्नेरत', 44, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(30, 1, 1, 'मा० श्री', 'अमेरिका ', 'कुमारी', 40, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(31, 1, 4, 'मा० श्री', 'अमृत कुमार ', 'अर्याल', 26, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(32, 1, 5, 'मा० श्री', 'अमृत कुमार ', 'बोहरा', 49, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(33, 1, 4, 'मा० श्री', 'अमृत लाल ', 'राजबंशी', 21, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(34, 1, 3, 'मा० श्री', 'अशोक कुमार ', 'मंडल', 32, NULL, 5, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(35, 1, 8, 'मा० श्री', ' अशोक कुमार ', 'राई', 35, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(36, 1, 4, 'मा० श्री', 'अशोक ', 'कोइराला', 26, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(37, 1, 5, 'मा० श्री', 'अष्ट  लक्ष्मीे ', 'शाक्य  (वोहरा)', 44, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(38, 1, 4, 'मा० श्री', 'आङटावा ', 'शेर्पा', 39, NULL, 5, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(39, 1, 5, 'मा० श्री', ' आदित्य नारायण ', 'कसौधन', 66, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(40, 1, 4, 'मा० श्री', 'आनन्द प्रसाद', ' ढुँगाना', 36, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(41, 1, 5, 'मा० श्री', ' आनन्द प्रसाद ', 'पोखरेल', 37, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(42, 1, 13, 'मा० श्री', ' आनन्दी ', 'पन्त', 66, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(43, 1, 4, 'मा० श्री', ' आरजु राणा ', 'देउवा', 46, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(44, 1, 3, 'मा० श्री', ' आशा ', 'कोइराला', 55, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(45, 1, 14, 'मा० श्री', ' आशा ', 'चतुर्वेदी', 53, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(46, 1, 4, 'मा० श्री', 'आशा ', 'वि.क.', 81, NULL, 3, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(47, 1, 5, 'मा० श्री', 'आसा ', 'यादव', 33, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(48, 1, 5, 'मा० श्री', ' इच्छा राज ', 'तामाङ्ग', 31, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(49, 1, 4, 'मा० श्री', ' इन्द्र बहादुर', ' वानिया', 52, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(50, 1, 1, 'मा० श्री', 'इन्द्रा', ' झा', 36, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(51, 1, 13, 'मा० श्री', 'इश्तियाक ', 'अहमद खान', 66, NULL, 5, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(52, 1, 4, 'मा० श्री', 'ईश्वरी ', 'न्यौपाने', 90, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(53, 1, 2, 'मा० श्री', ' उदय नेपाली ', 'श्रेष्ठ', 25, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(54, 1, 4, 'मा० श्री', 'उदय शम्शेर', ' जवरा', 46, NULL, 5, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(55, 1, 8, 'मा० श्री', ' उपेन्द्र ', 'यादव', 28, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(56, 1, 9, 'मा० श्री', 'उर्मिला देवी ', 'शाह', 36, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(57, 1, 3, 'मा० श्री', ' उमेश कुमार ', 'यादब', 32, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(58, 1, 6, 'मा० श्री', ' उमेश ', 'श्रेष्ठ', 46, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(59, 1, 3, 'मा० श्री', ' उषा किरण ', 'असाादब री', 50, NULL, 5, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(60, 1, 4, 'मा० श्री', ' उषा ', 'गुरुङ्ग', 21, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(61, 1, 8, 'मा० श्री', 'उषा ', 'यादव', 33, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(62, 1, 5, 'मा० श्री', ' ऋषिकेश ', 'पोखरेल', 26, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(63, 1, 15, 'मा० श्री', ' एक नाथ ', 'ढकाल', 54, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(64, 1, 3, 'मा० श्री', 'ओङदी ', 'शेर्पा', 23, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(65, 1, 3, 'मा० श्री', ' ओनसरी ', 'घर्ती', 73, NULL, 5, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(66, 1, 4, 'मा० श्री', 'ओम देवी ', 'मल्ल', 44, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(67, 1, 5, 'मा० श्री', ' कर्ण बहादुर', ' थापा', 88, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(68, 1, 3, 'मा० श्री', ' कर्ण बहादुर ', 'वि.क.', 88, NULL, 3, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(69, 1, 4, 'मा० श्री', 'कन्च न चन्द्रा ', 'बादे', 45, NULL, 5, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(70, 1, 13, 'मा० श्री', ' कमल ', 'थापा', 44, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(71, 1, 4, 'मा० श्री', ' कमल प्रसाद ', 'पंगेनी', 59, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(72, 1, 5, 'मा० श्री', 'कमला कुमारि', ' घिमिरे', 39, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(73, 1, 5, 'मा० श्री', ' कमला देवी ', 'महतो', 40, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(74, 1, 13, 'मा० श्री', ' कमला देवी ', 'शर्मा', 56, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(75, 1, 3, 'मा० श्री', 'कमला ', 'दोङ', 45, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(76, 1, 4, 'मा० श्री', 'कमला ', 'पन्त ', 55, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(77, 1, 10, 'मा० श्री', 'कमला ', 'विश्वकर्मा', 62, NULL, 3, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(78, 1, 1, 'मा० श्री', 'कमलेश्वशर ', 'पूरी गोस्वामी', 66, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(79, 1, 5, 'मा० श्री', 'कल्पना शर्मा ', 'जोशी', 44, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(80, 1, 4, 'मा० श्री', 'कल्पना ', 'सोप', 89, NULL, 3, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(81, 1, 10, 'मा० श्री', 'कलसा देवी ', 'महरा', 94, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(82, 1, 4, 'मा० श्री', ' कविता कुमारी ', 'सरदार', 26, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(83, 1, 13, 'मा० श्री', ' कान्ता ', 'भट्टराई', 44, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(84, 1, 5, 'मा० श्री', ' काशी नाथ ', 'अधिकारी', 60, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(85, 1, 8, 'मा० श्री', ' काशीम ', 'अली सिद्धिकी', 82, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(86, 1, 4, 'मा० श्री', 'किरण ', 'यादव', 38, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(87, 1, 5, 'मा० श्री', ' कु. टेकु ', 'नेपाली', 59, NULL, 3, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(88, 1, 13, 'मा० श्री', ' कुन्ती कुमारी', ' शाही', 59, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(89, 1, 4, 'मा० श्री', 'कुमारी लक्ष्मी ', 'राई', 44, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(90, 1, 4, 'मा० श्री', 'कुल बहादुर ', 'गुरुङ', 20, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(91, 1, 1, 'मा० श्री', ' केदार नन्दरन ', 'चौधरी', 40, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(92, 1, 5, 'मा० श्री', 'केदार प्रसाद ', 'संजेल', 20, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(93, 1, 5, 'मा० श्री', 'के.पी. ', 'शर्मा ओली', 21, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(94, 1, 4, 'मा० श्री', ' केशब कुमार ', 'बुढाथोकी', 21, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(95, 1, 3, 'मा० श्री', ' केशरी ', 'घर्ती मगर', 67, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(96, 1, 4, 'मा० श्री', ' केशव', ' थापा', 20, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(97, 1, 5, 'मा० श्री', 'केशव प्रसाद ', 'बडाल', 44, NULL, 5, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(98, 1, 4, 'मा० श्री', ' कौशर ', 'शाह', 44, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(99, 1, 5, 'मा० श्री', 'कृपा राम ', 'राना', 94, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(100, 1, 5, 'मा० श्री', 'कृपासुर ', 'शेर्पा', 24, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(101, 1, 3, 'मा० श्री', 'कृष्ण ', 'धिताल', 55, NULL, 5, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(102, 1, 5, 'मा० श्री', 'कृष्ण कुमार ', 'राई', 28, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(103, 1, 5, 'मा० श्री', ' कृष्ण प्रसाद ', 'पौडेल', 64, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(104, 1, 4, 'मा० श्री', 'कृष्ण प्रसाद ', 'सिटौला', 21, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(105, 1, 4, 'मा० श्री', 'कृष्ण बहादुर ', 'छन्तेल थापा', 62, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(106, 1, 3, 'मा० श्री', 'कृष्ण बहादुर', ' महरा', 73, NULL, 5, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(107, 1, 5, 'मा० श्री', ' कृष्ण भक्त ', 'पोख्रेल', 51, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(108, 1, 4, 'मा० श्री', 'खड्ग बहादुर ', 'बस्यालल', 85, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(109, 1, 4, 'मा० श्री', ' गगन कुमार ', 'थापा', 44, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(110, 1, 2, 'मा० श्री', 'गङ्गादेवी ', 'डाँगी', 26, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(111, 1, 6, 'मा० श्री', ' गङ्गादेवी ', 'डाँगी', 26, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(112, 1, 5, 'मा० श्री', 'गणेश कुमार ', 'काम्बाङ्ग', 22, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(113, 1, 5, 'मा० श्री', ' गणेश कुमार ', 'पहाडी', 41, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(114, 1, 4, 'मा० श्री', 'गणेश कुमार ', 'मण्डल', 33, NULL, 2, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(115, 1, 2, 'मा० श्री', ' गणेश ', 'थापा', 52, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(116, 1, 4, 'मा० श्री', ' गणेश प्रसाद ', 'विमली', 29, NULL, 5, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(117, 1, 5, 'मा० श्री', ' गणेश मान ', 'गुरुङ्ग', 59, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(118, 1, 3, 'मा० श्री', ' गणेशमान ', 'पुन', 74, NULL, 4, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(119, 1, 5, 'मा० श्री', ' गणेश ', 'सिंह ठगुन्ना', 93, NULL, 5, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36'),
(120, 1, 3, 'मा० श्री', ' गिरीराज ', 'मणी पोखरेल', 38, NULL, 1, 'प्रत्यक्ष-निर्वाचन', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, 'active', 'सकृय', 1, 2147483647, '192.168.100.36');

-- --------------------------------------------------------

--
-- Table structure for table `parliament_member_status`
--

CREATE TABLE `parliament_member_status` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `status` enum('सकृय','निलम्बन','राजीनामा','पद खारेज','मृत्यू','राष्ट्रपतिमा निर्वाचित','उपराष्ट्रपतिमा निर्वाचित') COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('premanent','temporary') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'premanent',
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `create_ip` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `parliament_member_status`
--

INSERT INTO `parliament_member_status` (`id`, `member_id`, `status`, `type`, `start_date`, `end_date`, `description`, `created_by`, `create_ip`, `created_date`) VALUES
(1, 1, 'निलम्बन', 'temporary', '2018-03-26', '2019-03-26', 'this is testing section for description', 1, '1:', '0000-00-00 00:00:00'),
(2, 1, 'निलम्बन', 'temporary', '0000-00-00', '0000-00-00', '0', 1, '1:', '0000-00-00 00:00:00'),
(3, 1, 'निलम्बन', 'temporary', '0000-00-00', '0000-00-00', '0', 1, '1:', '0000-00-00 00:00:00'),
(4, 3, 'निलम्बन', 'temporary', '2017-05-02', '2017-05-02', '', 1, '192.168.100.36', '2017-05-02 12:19:56'),
(5, 2, 'सकृय', 'premanent', '2017-05-02', '2017-05-02', '', 1, '192.168.100.36', '2017-05-02 13:11:24');

-- --------------------------------------------------------

--
-- Table structure for table `parliament_party_official`
--

CREATE TABLE `parliament_party_official` (
  `id` int(11) NOT NULL,
  `parliament_id` int(11) NOT NULL,
  `ministry` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `political_party` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parliament_member_id` int(11) NOT NULL,
  `start_from` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `_order` int(11) DEFAULT NULL,
  `letter_received_date` date DEFAULT NULL,
  `status` enum('सकृय','निलम्बन','राजीनामा','पद खारेज','मृत्यू','राष्ट्रपतिमा निर्वाचित','उपराष्ट्रपतिमा निर्वाचित') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'सकृय',
  `created_by` int(11) DEFAULT NULL,
  `created_ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `parliament_party_official`
--

INSERT INTO `parliament_party_official` (`id`, `parliament_id`, `ministry`, `political_party`, `post`, `parliament_member_id`, `start_from`, `to_date`, `_order`, `letter_received_date`, `status`, `created_by`, `created_ip`, `created_date`) VALUES
(1, 1, 'संघीय मामिला तथा स्थानीय विकास', 'तराई–मधेश लोकतान्त्रिक पार्टी', 'मन्त्री', 30, NULL, NULL, NULL, NULL, 'सकृय', 1, '::1', '2017-05-18 20:30:03'),
(2, 1, 'अर्थ', 'तराई–मधेश लोकतान्त्रिक पार्टी', 'मन्त्री', 30, NULL, NULL, NULL, NULL, 'सकृय', 1, '::1', '2017-05-18 20:30:03'),
(3, 1, 'शिक्षा', 'ने.क.पा. (ए.मा.ले)', 'मन्त्री', 47, NULL, NULL, 3, NULL, 'सकृय', 1, '::1', '2017-05-19 08:00:22'),
(4, 1, 'कानुन, न्याय तथा संसदीय मामिला', 'नेपाली कांगे्रस', 'मन्त्री', 6, NULL, NULL, NULL, NULL, 'सकृय', 1, '::1', '2017-05-21 15:40:09'),
(5, 1, 'गृह', 'नेपाली कांगे्रस', 'मन्त्री', 27, NULL, NULL, NULL, NULL, 'सकृय', 1, '::1', '2017-05-21 23:12:27'),
(7, 1, 'युवा तथा खेलकूद', 'संघीय समाजवादी फोरम, नेपाल', 'मन्त्री', 21, NULL, NULL, NULL, NULL, 'सकृय', 1, '202.51.88.45', '2017-05-22 00:07:01'),
(8, 1, 'वन तथा भू–संरक्षण', 'म.ज.अ.फोरम, नेपाल (लोकतान्त्रिक)', 'मन्त्री', 45, NULL, NULL, NULL, NULL, 'सकृय', 1, '202.51.88.45', '2017-05-22 00:08:04');

-- --------------------------------------------------------

--
-- Table structure for table `participate_member_discussion`
--

CREATE TABLE `participate_member_discussion` (
  `id` int(11) NOT NULL,
  `type` enum('zero_time','special_time','on_topic') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'on_topic',
  `parliament_id` int(11) NOT NULL,
  `meeting_id` int(11) NOT NULL,
  `reading_paper_id` int(11) DEFAULT NULL,
  `parliament_member_id` int(11) NOT NULL,
  `speech_order` int(11) DEFAULT NULL,
  `text_` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `read_at` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `political_party`
--

CREATE TABLE `political_party` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `party_establish_date` int(11) DEFAULT NULL,
  `party_register_date` int(11) DEFAULT NULL COMMENT 'register date in parliament',
  `description` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `create_ip` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `political_party`
--

INSERT INTO `political_party` (`id`, `name`, `party_establish_date`, `party_register_date`, `description`, `status`, `created_by`, `create_ip`, `created_date`) VALUES
(1, 'तराई–मधेश लोकतान्त्रिक पार्टी', 2017, 2017, '', 1, 1, NULL, '2017-03-24 13:03:25'),
(2, 'मन्त्रिपरिषद्बाट मनोनित', 2016, 2017, 'this is description', 1, 1, NULL, '2017-03-24 13:04:17'),
(3, 'ने.क.पा. (माओवादी केन्द्र)', 2017, 2017, '', 1, 1, NULL, '2017-03-24 13:05:02'),
(4, 'नेपाली कांगे्रस', 2017, 2017, '', 1, 1, NULL, '2017-03-24 13:05:30'),
(5, 'ने.क.पा. (ए.मा.ले)', 2017, 2017, '', 1, 1, NULL, '2017-03-24 13:05:56'),
(6, 'मन्त्रिपरिषद्बाट मनोनित', 2017, 2017, '', 1, 1, NULL, '2017-03-24 13:06:22'),
(7, 'नेपाल मजद्ुर किसान पार्टी', 2017, 2017, '', 1, 1, NULL, '2017-03-24 13:07:01'),
(8, 'संघीय समाजवादी फोरम, नेपाल', 2017, 2017, '', 1, 1, NULL, '2017-03-24 13:07:24'),
(9, 'संघीय समाजवादी फोरम, नेपाल', 2017, 2017, '', 1, 1, NULL, '2017-03-24 13:07:53'),
(10, 'नेपाल कम्युनिष्ट पार्टी (मार्क्सवादी–लेनिनवादी)', 2017, 2017, '', 1, 1, NULL, '2017-03-24 13:18:09'),
(11, 'मधेशी जनजाति', 2017, 2017, '', 1, 1, NULL, '2017-03-24 15:38:59'),
(13, 'राष्ट्रिय प्रजातन्त्र पार्टी', 2017, 2017, '', 1, 1, NULL, '2017-03-24 16:24:48'),
(14, 'म.ज.अ.फोरम, नेपाल (लोकतान्त्रिक)', 2017, 2017, '', 1, 1, NULL, '2017-03-24 16:38:02'),
(15, 'नेपाल परिवार दल', 2017, 2017, '', 1, 1, NULL, '2017-03-26 11:28:15'),
(16, 'new party', 2017, 2017, '', 1, 1, NULL, '2017-05-02 11:58:32'),
(17, 'nnzx', 2017, 2017, '', 1, 1, NULL, '2017-05-02 13:20:07');

-- --------------------------------------------------------

--
-- Table structure for table `processing_details`
--

CREATE TABLE `processing_details` (
  `id` int(11) NOT NULL,
  `parliament_id` int(11) NOT NULL,
  `meeting_id` int(11) DEFAULT NULL,
  `darta_id` int(11) NOT NULL,
  `type` enum('प्रधानमन्त्रीको बक्तब्य','मन्त्रीको बक्तब्य','शून्य समय','विशेष समय','प्रत्यक्ष प्रशनहरु','व्यवस्थापन कार्य','राष्ट्रपति','प्रधानमन्त्री तथा मन्त्रिपरिषद् कार्यालय','राष्ट्रपति कार्यालय','संवैधानिक आयोगको प्रतिवेदन','समितिको प्रतिवेदन','समितिको बार्षिक प्रतिवेदन','सन्धि','महासन्धि','सम्झौता','नीति तथा कार्यक्रम','राजस्व व्ययको बार्षिक अनुमान','अध्यादेश','ध्यानाकर्षण प्रस्ताव','जरुरी सार्वजनिक महत्वको प्रस्ताव','संकल्प प्रस्ताव','स्थगन प्रस्ताव','सभामुखको निर्वाचन','उप-सभामुखको निर्वाचन','राष्ट्रपतिको निर्वाचन','उप-राष्ट्रपतिको निर्वाचन','अनुपस्थितिको सूचना','स्थान रिक्तता','समिति गठन','शोक प्रस्ताव','समिति गठन','समितिमा थपघट तथा हेरफेर','बैठकमा अध्यक्षता गर्ने सदस्यको मनोनयन','राष्ट्रपतिबाट सम्बोधन','अन्य') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'व्यवस्थापन कार्य',
  `date` datetime NOT NULL,
  `ministry` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `minister` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `related_committee` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` enum('दर्ता','वितरण','पेश','अनुमति माग्ने','विचार गरियोस्','संशोधनको म्याद','दफावार छलफल समितिमा पठाइयोस्','दफावार छलफल सदनमा संशोधन सहित','दफावार छलफल सदनमा संशोधन विना','समितिको प्रतिवेदन पेश','प्रतिवेदन सहितको विधेयक समितिमा फिर्ता','पुनः समितिको प्रतिवेदन पेश','प्रतिवेदन सहित छलफल','छलफल','पारित','अनुमोदन') COLLATE utf8_unicode_ci DEFAULT 'दर्ता',
  `state_status` enum('स्वीकृत','फिर्ता लिनु','प्रकृया','अस्वीकृत') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'स्वीकृत',
  `_result` enum('अस्वीकृत','पारित','स्वीकृत','प्रकृया') COLLATE utf8_unicode_ci DEFAULT 'पारित',
  `remarks` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `processing_details`
--

INSERT INTO `processing_details` (`id`, `parliament_id`, `meeting_id`, `darta_id`, `type`, `date`, `ministry`, `minister`, `related_committee`, `state`, `state_status`, `_result`, `remarks`, `status`, `created_by`, `created_date`) VALUES
(1, 1, 1, 1, 'राष्ट्रपति', '2017-05-22 00:04:08', NULL, '', '', 'पारित', 'प्रकृया', 'प्रकृया', NULL, 1, 1, '2017-05-22 00:04:08'),
(2, 1, 1, 2, 'व्यवस्थापन कार्य', '2017-05-22 00:04:28', 'गृह', 'मा० गृह मन्त्री श्री अमिय कुमार  यादब', 'राज्य व्यवस्था', 'अनुमति माग्ने', 'प्रकृया', 'प्रकृया', NULL, 1, 1, '2017-05-22 00:04:28'),
(3, 1, 1, 3, 'व्यवस्थापन कार्य', '2017-05-22 00:09:22', 'अर्थ', 'मा० अर्थ मन्त्री श्री अमेरिका  कुमारी', 'अर्थ', 'अनुमति माग्ने', 'प्रकृया', 'प्रकृया', NULL, 1, 1, '2017-05-22 00:09:22');

-- --------------------------------------------------------

--
-- Table structure for table `reading_paper`
--

CREATE TABLE `reading_paper` (
  `id` int(11) NOT NULL,
  `parliament_id` int(11) NOT NULL,
  `meeting_id` int(11) DEFAULT NULL,
  `daily_paper_id` int(11) NOT NULL DEFAULT '0',
  `forum_id` int(11) NOT NULL,
  `main_type` enum('speech','time','letter','aggrement','bill','report','rule','income','ordinance','proposal','election','other') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'other',
  `meeting_header` enum('प्रधानमन्त्रीको बक्तब्य','मन्त्रीको बक्तब्य','शून्य समय','विशेष समय','प्रत्यक्ष प्रशनहरु','व्यवस्थापन कार्य','राष्ट्रपति','प्रधानमन्त्री तथा मन्त्रिपरिषद् कार्यालय','राष्ट्रपति कार्यालय','संवैधानिक आयोगको प्रतिवेदन','समितिको प्रतिवेदन','समितिको बार्षिक प्रतिवेदन','सन्धि','महासन्धि','सम्झौता','नीति तथा कार्यक्रम','राजस्व व्ययको बार्षिक अनुमान','अध्यादेश','ध्यानाकर्षण प्रस्ताव','जरुरी सार्वजनिक महत्वको प्रस्ताव','संकल्प प्रस्ताव','स्थगन प्रस्ताव','सभामुखको निर्वाचन','उप-सभामुखको निर्वाचन','राष्ट्रपतिको निर्वाचन','उप-राष्ट्रपतिको निर्वाचन','अनुपस्थितिको सूचना','स्थान रिक्तता','समिति गठन','शोक प्रस्ताव','समिति गठन','समितिमा थपघट तथा हेरफेर','बैठकमा अध्यक्षता गर्ने सदस्यको मनोनयन','राष्ट्रपतिबाट सम्बोधन','अन्य') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'व्यवस्थापन कार्य',
  `state` enum('दर्ता','वितरण','अनुमति माग्ने','विचार गरियोस्','संशोधनको म्याद','दफावार छलफल समितिमा पठाइयोस्','दफावार छलफल सदनमा संशोधन सहित','दफावार छलफल सदनमा संशोधन विना','समितिको प्रतिवेदन पेश','प्रतिवेदन सहितको विधेयक समितिमा फिर्ता','पुनः समितिको प्रतिवेदन पेश','प्रतिवेदन सहित छलफल','पारित','पेश','छलफल','अनुमोदन') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'दर्ता',
  `context` text COLLATE utf8_unicode_ci,
  `date` datetime DEFAULT NULL,
  `ministry` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `minister` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `related_committee` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_number` int(11) DEFAULT NULL,
  `primary_order` int(11) NOT NULL DEFAULT '0',
  `status` enum('प्रकृया','सर्बसम्ती स्वीकृत','सर्बसम्ती अस्वीकृति','बहुमत स्वीकृत','बहुमत अस्वीकृति','स्वीकृत','फिर्ता','अस्वीकृत') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'प्रकृया',
  `goes_through_voting` tinyint(1) DEFAULT '0',
  `win_by` enum('हुन्छ','हुन्न','अन्य') COLLATE utf8_unicode_ci DEFAULT 'हुन्छ',
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `read_at` datetime DEFAULT NULL,
  `page_type` enum('general','birod-darta','samsodhan-darta') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'general',
  `paper_status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `header` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reading_paper`
--

INSERT INTO `reading_paper` (`id`, `parliament_id`, `meeting_id`, `daily_paper_id`, `forum_id`, `main_type`, `meeting_header`, `state`, `context`, `date`, `ministry`, `minister`, `related_committee`, `order_number`, `primary_order`, `status`, `goes_through_voting`, `win_by`, `is_read`, `read_at`, `page_type`, `paper_status`, `created_by`, `created_date`, `header`) VALUES
(1, 1, 1, 1, 2, 'bill', 'व्यवस्थापन कार्य', 'अनुमति माग्ने', '<p>bill one</p>\r\n', NULL, 'गृह', 'मन्त्री श्री अमिय कुमार  यादब', 'राज्य व्यवस्था', 2, 6, '', 0, 'हुन्छ', 0, NULL, 'general', 1, 1, '2017-05-22 00:09:54', NULL),
(2, 1, 1, 2, 3, 'bill', 'व्यवस्थापन कार्य', 'अनुमति माग्ने', '<p>bill two</p>\r\n', NULL, 'अर्थ', 'मन्त्री श्री अमेरिका  कुमारी', 'अर्थ', 3, 6, '', 0, 'हुन्छ', 0, NULL, 'general', 1, 1, '2017-05-22 00:10:01', NULL),
(3, 1, 1, 3, 1, 'letter', 'राष्ट्रपति', 'पारित', '<p>president letter</p>\r\n', NULL, '', '', NULL, 1, 6, '', 0, 'हुन्छ', 0, NULL, 'general', 1, 1, '2017-05-22 00:10:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reading_paper_content`
--

CREATE TABLE `reading_paper_content` (
  `id` int(11) NOT NULL,
  `reading_paper_id` int(11) NOT NULL,
  `statement_id` int(11) DEFAULT NULL,
  `page_type` enum('general','birod-darta','samsodhan-darta') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'general',
  `pages` enum('_first_content','_second_content','_third_content','_discuss','_discuss_suppoter','_discuss_body','_discuss_reply','_decision_general','_decision_no_discuss','_st_decision','_st_re_back_one','_st_re_back_two','_result_yes','_result_no','_result_overall_no','_result_overall_yes','_st_last_content') COLLATE utf8_unicode_ci NOT NULL DEFAULT '_first_content',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `order_` int(11) NOT NULL DEFAULT '0',
  `next_page` text COLLATE utf8_unicode_ci COMMENT 'seperated by comma',
  `_end` enum('paper','statement') COLLATE utf8_unicode_ci DEFAULT NULL,
  `_next_id` int(11) DEFAULT NULL,
  `content_status` tinyint(1) NOT NULL DEFAULT '1',
  `is_static` tinyint(1) NOT NULL DEFAULT '0',
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `read_by` int(11) DEFAULT NULL,
  `read_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reading_paper_content`
--

INSERT INTO `reading_paper_content` (`id`, `reading_paper_id`, `statement_id`, `page_type`, `pages`, `content`, `order_`, `next_page`, `_end`, `_next_id`, `content_status`, `is_static`, `is_read`, `read_by`, `read_date`, `created_by`, `created_date`) VALUES
(1, 1, NULL, 'general', '_first_content', '<p>अब मा० गृह मन्त्री श्री अमिय कुमार यादब लाई &quot; bill one &quot; लाई अनुमति माग्ने प्रस्ताव प्रस्तुत गर्न अनुमति दिन्छु ।</p>\r\n', 1, '_second_content', NULL, NULL, 1, 0, 0, NULL, NULL, 1, '2017-05-22 00:09:54'),
(2, 1, NULL, 'general', '_second_content', '<p>मा० सदस्यहरु, उक्त प्रस्तावमाथि व्यवस्थापिका&ndash;संसद नियमावली, २०७३ को नियम १२१ बमोजिम विरोधको सूचना प्राप्त नभएकोले &quot; bill one &quot; प्रस्तुत गर्न बैठकको अनुमति प्राप्त भएको घोषणा गर्दछु् ।</p>\r\n', 2, '_third_content', NULL, NULL, 1, 0, 0, NULL, NULL, 1, '2017-05-22 00:09:54'),
(3, 1, NULL, 'general', '_third_content', '<p>अब मा० गृह मन्त्री श्री अमिय कुमार यादब लाई bill one प्रस्तुत गर्न अनुमति दिन्छु ।</p>\r\n', 3, NULL, 'paper', NULL, 1, 0, 0, NULL, NULL, 1, '2017-05-22 00:09:54'),
(4, 2, NULL, 'general', '_first_content', '<p>अब मा० अर्थ मन्त्री श्री अमेरिका कुमारी लाई &quot; bill two &quot; लाई अनुमति माग्ने प्रस्ताव प्रस्तुत गर्न अनुमति दिन्छु ।</p>\r\n', 1, '_second_content', NULL, NULL, 1, 0, 0, NULL, NULL, 1, '2017-05-22 00:10:01'),
(5, 2, NULL, 'general', '_second_content', '<p>मा० सदस्यहरु, उक्त प्रस्तावमाथि व्यवस्थापिका&ndash;संसद नियमावली, २०७३ को नियम १२१ बमोजिम विरोधको सूचना प्राप्त नभएकोले &quot; bill two &quot; प्रस्तुत गर्न बैठकको अनुमति प्राप्त भएको घोषणा गर्दछु् ।</p>\r\n', 2, '_third_content', NULL, NULL, 1, 0, 0, NULL, NULL, 1, '2017-05-22 00:10:01'),
(6, 2, NULL, 'general', '_third_content', '<p>अब मा० अर्थ मन्त्री श्री अमेरिका कुमारी लाई bill two प्रस्तुत गर्न अनुमति दिन्छु ।</p>\r\n', 3, NULL, 'paper', NULL, 1, 0, 0, NULL, NULL, 1, '2017-05-22 00:10:01'),
(7, 3, NULL, 'general', '_first_content', '<p>अब म राष्ट्रपति बाट प्राप्त पत्रको व्यहोरा पढेर सुनाउछु ।<br />\r\npresident letter</p>\r\n', 1, NULL, 'paper', NULL, 1, 0, 0, NULL, NULL, 1, '2017-05-22 00:10:10');

-- --------------------------------------------------------

--
-- Table structure for table `statement`
--

CREATE TABLE `statement` (
  `id` int(11) NOT NULL,
  `statement_type` enum('विरोधको सूचना','संशोधन') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'विरोधको सूचना',
  `member` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'member are separated by comma',
  `forum_id` int(11) NOT NULL COMMENT 'id for darta desk',
  `state` enum('दर्ता','वितरण','पेश','अनुमति माग्ने','विचार गरियोस्','संशोधनको म्याद','समितिमा पठाइयोस्','दफावार छलफल सदनमा संशोधन सहित','दफावार छलफल सदनमा संशोधन विना','समितिको प्रतिवेदन पेश','प्रतिवेदन सहितको विधेयक समितिमा फिर्ता','पुनः समितिको प्रतिवेदन पेश','प्रतिवेदन सहित छलफल','टेबुल','छलफल','पारित','अनुमोदन') COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `_result` enum('प्रकृया','सर्बसम्ती स्वीकृत','सर्बसम्ती अस्वीकृति','बहुमत स्वीकृत','बहुमत अस्वीकृति','स्वीकृत','फिर्ता','अस्वीकृत') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'प्रकृया',
  `statement_order` int(11) DEFAULT NULL,
  `is_generate_dp` tinyint(1) NOT NULL DEFAULT '0',
  `is_generate_rp` tinyint(1) NOT NULL DEFAULT '0',
  `reading_paper_id` int(11) DEFAULT NULL,
  `daily_paper_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` enum('admin','reader','writer') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'reader',
  `status` smallint(6) NOT NULL DEFAULT '1',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `role`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'cip-DkXAGdeBy4h3aWSMcYyrS-RMW1GX', '$2y$13$vCuhWRNEkUX7Su5wiY1BzOruAUh9YcBVtQMLQujaU.IoiRp.Smoiu', NULL, 'admin@admin.com', 'admin', 1, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adhibasen`
--
ALTER TABLE `adhibasen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `committee`
--
ALTER TABLE `committee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `committee_details`
--
ALTER TABLE `committee_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parliament_id` (`parliament_id`),
  ADD KEY `party_id` (`party_id`),
  ADD KEY `committee_id` (`committee_id`);

--
-- Indexes for table `committee_member_details`
--
ALTER TABLE `committee_member_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `committee_id` (`committee_id`),
  ADD KEY `party_id` (`party_id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `parliament_id` (`parliament_id`);

--
-- Indexes for table `daily_paper`
--
ALTER TABLE `daily_paper`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `darta_desk`
--
ALTER TABLE `darta_desk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parliament_id` (`parliament_id`);

--
-- Indexes for table `ethinic_cast`
--
ALTER TABLE `ethinic_cast`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`,`created_by`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `location_level` (`location_level`),
  ADD KEY `created_by_2` (`created_by`);

--
-- Indexes for table `meeting_details`
--
ALTER TABLE `meeting_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meeting_halt`
--
ALTER TABLE `meeting_halt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meeting_id` (`meeting_id`);

--
-- Indexes for table `ministry`
--
ALTER TABLE `ministry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ministry_related_committee`
--
ALTER TABLE `ministry_related_committee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parliament_id` (`parliament_id`);

--
-- Indexes for table `notice_paper`
--
ALTER TABLE `notice_paper`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `official_post`
--
ALTER TABLE `official_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parliament_activities`
--
ALTER TABLE `parliament_activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parliament_details`
--
ALTER TABLE `parliament_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parliament_member`
--
ALTER TABLE `parliament_member`
  ADD PRIMARY KEY (`id`),
  ADD KEY `district_id` (`district_id`),
  ADD KEY `parliament_id` (`parliament_id`),
  ADD KEY `ethinicity_id` (`ethinicity_id`),
  ADD KEY `political_id` (`political_id`);

--
-- Indexes for table `parliament_member_status`
--
ALTER TABLE `parliament_member_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `member_id` (`member_id`);

--
-- Indexes for table `parliament_party_official`
--
ALTER TABLE `parliament_party_official`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parliament_id` (`parliament_id`),
  ADD KEY `parliament_member_id` (`parliament_member_id`);

--
-- Indexes for table `participate_member_discussion`
--
ALTER TABLE `participate_member_discussion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parliament_id` (`parliament_id`),
  ADD KEY `meeting_id` (`meeting_id`),
  ADD KEY `reading_paper_id` (`reading_paper_id`),
  ADD KEY `parliament_member_id` (`parliament_member_id`);

--
-- Indexes for table `political_party`
--
ALTER TABLE `political_party`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `processing_details`
--
ALTER TABLE `processing_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parliament_id` (`parliament_id`);

--
-- Indexes for table `reading_paper`
--
ALTER TABLE `reading_paper`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parliament_id` (`parliament_id`),
  ADD KEY `meeting_id` (`meeting_id`),
  ADD KEY `forum_id` (`daily_paper_id`);

--
-- Indexes for table `reading_paper_content`
--
ALTER TABLE `reading_paper_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reading_paper_id` (`reading_paper_id`);

--
-- Indexes for table `statement`
--
ALTER TABLE `statement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adhibasen`
--
ALTER TABLE `adhibasen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `committee`
--
ALTER TABLE `committee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `committee_details`
--
ALTER TABLE `committee_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `committee_member_details`
--
ALTER TABLE `committee_member_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `daily_paper`
--
ALTER TABLE `daily_paper`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `darta_desk`
--
ALTER TABLE `darta_desk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ethinic_cast`
--
ALTER TABLE `ethinic_cast`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `meeting_details`
--
ALTER TABLE `meeting_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `meeting_halt`
--
ALTER TABLE `meeting_halt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ministry`
--
ALTER TABLE `ministry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `ministry_related_committee`
--
ALTER TABLE `ministry_related_committee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `notice_paper`
--
ALTER TABLE `notice_paper`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `official_post`
--
ALTER TABLE `official_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `parliament_activities`
--
ALTER TABLE `parliament_activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `parliament_details`
--
ALTER TABLE `parliament_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `parliament_member`
--
ALTER TABLE `parliament_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT for table `parliament_member_status`
--
ALTER TABLE `parliament_member_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `parliament_party_official`
--
ALTER TABLE `parliament_party_official`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `participate_member_discussion`
--
ALTER TABLE `participate_member_discussion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `political_party`
--
ALTER TABLE `political_party`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `processing_details`
--
ALTER TABLE `processing_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `reading_paper`
--
ALTER TABLE `reading_paper`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `reading_paper_content`
--
ALTER TABLE `reading_paper_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `statement`
--
ALTER TABLE `statement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `committee_details`
--
ALTER TABLE `committee_details`
  ADD CONSTRAINT `com_rel_fk_sec` FOREIGN KEY (`committee_id`) REFERENCES `committee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `parliament_rel_fk_sec` FOREIGN KEY (`parliament_id`) REFERENCES `parliament_details` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `po_party_rel_fk_sec` FOREIGN KEY (`party_id`) REFERENCES `political_party` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `committee_member_details`
--
ALTER TABLE `committee_member_details`
  ADD CONSTRAINT `committee_rel_fk` FOREIGN KEY (`committee_id`) REFERENCES `committee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `par_li_rel_fk` FOREIGN KEY (`parliament_id`) REFERENCES `parliament_details` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `parliament_mem_rel_fk` FOREIGN KEY (`member_id`) REFERENCES `parliament_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `party_sec_rel_fk` FOREIGN KEY (`party_id`) REFERENCES `political_party` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `darta_desk`
--
ALTER TABLE `darta_desk`
  ADD CONSTRAINT `parliament_fk_relation_sec` FOREIGN KEY (`parliament_id`) REFERENCES `parliament_details` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `parliament_member`
--
ALTER TABLE `parliament_member`
  ADD CONSTRAINT `ethinicity_rel_fk` FOREIGN KEY (`ethinicity_id`) REFERENCES `ethinic_cast` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `location_rl_fk` FOREIGN KEY (`district_id`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `parliament_rl_id` FOREIGN KEY (`parliament_id`) REFERENCES `parliament_details` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `political_rel_fk` FOREIGN KEY (`political_id`) REFERENCES `political_party` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `parliament_member_status`
--
ALTER TABLE `parliament_member_status`
  ADD CONSTRAINT `parliament_member_rel_fk` FOREIGN KEY (`member_id`) REFERENCES `parliament_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
