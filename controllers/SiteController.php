<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\modules\program\models\ReadingPaper;
use app\modules\program\models\DartaDesk;
use app\modules\program\models\ProcessingDetails;
use app\models\SignupForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii:: $app->user->can('admin') && Yii:: $app->user->can('officer')) 
            return $this->redirect('program/meeting-details/meeting-details?status=1');
        if (!Yii:: $app->user->can('admin') && Yii:: $app->user->can('reader')) 
            return $this->redirect('program/meeting-details/reader-dashboard');

        $column_header = ReadingPaper::billing_process;
        $column[] = 'क्र.सं.';
        $column[] = 'विधेयकको नाम';
        for ($i=0; $i < sizeof($column_header)-1; $i++) { 
            $column[] = $column_header[$i];
        }
        $billing_information = array();
        $darta_processing = ProcessingDetails::find()->where('type=:type group by darta_id',[':type'=>'व्यवस्थापन कार्य'])->all();
        if(!empty($darta_processing)){
            $sn = 0;
            foreach ($darta_processing as $darat) {
                $billing_information[$sn]['क्र.सं.'] = $sn+1;
                $darta_informaiton = DartaDesk::findOne($darat->darta_id);
                $billing_information[$sn]['विधेयकको नाम'] = $darta_informaiton ? strip_tags($darta_informaiton->title) : '';
                for ($j=0; $j < sizeof($column_header)-1; $j++) { 
                    $date='';
                    $section = $column_header[$j];
                    $darta_processing_individual = ProcessingDetails::find()->where('type=:type AND darta_id=:darta_id AND state=:state order by date',[':type'=>'व्यवस्थापन कार्य', ':darta_id'=>$darat->darta_id, ':state'=>$section])->all();
                    $date_array = array();
                    if(!empty($darta_processing_individual)){
                        foreach ($darta_processing_individual as $individuals) {
                            if(!in_array($individuals->date, $date_array)){
                                $date .=','.$individuals->date;
                                $date_array[]  = $individuals->date;
                            }
                        }
                    }
                    $billing_information[$sn][$section] = ltrim($date,',');
        
                }
                $sn++;
            }
        }
        return $this->render('index', ['billing_information'=>$billing_information,'column'=>$column]);
    }


    public function actionDraggeable(){
        return $this->render('_drgabble');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) 
        {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
}
